FROM openjdk:8-alpine
LABEL author="neo"
VOLUME /srv
COPY dist/* /srv/
COPY docker-entrypoint.sh /srv/
EXPOSE 8080
WORKDIR /srv
ENTRYPOINT ["/srv/docker-entrypoint.sh"]
