package com.piggy.gen;

import com.piggy.common.satoken.annotation.EnableCustomConfig;
import com.piggy.common.satoken.annotation.EnableCustomFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 代码生成
 *
 * @author shark
 */
@EnableCustomConfig
@EnableCustomFeignClients
@RefreshScope
@SpringBootApplication
public class PIGYGenApplication {
    public static void main(String[] args) {
        SpringApplication.run(PIGYGenApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  代码生成模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" );
    }
}
