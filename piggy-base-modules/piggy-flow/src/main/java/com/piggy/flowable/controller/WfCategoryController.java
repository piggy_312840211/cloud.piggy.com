package com.piggy.flowable.controller;

import com.piggy.api.flowable.domain.bo.WfCategoryBo;
import com.piggy.api.flowable.domain.vo.WfCategoryVo;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.validate.AddGroup;
import com.piggy.common.core.validate.EditGroup;
import com.piggy.common.core.validate.QueryGroup;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.domain.PageQuery;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.flowable.service.IWfCategoryService;
import com.piggy.common.core.domain.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 流程分类Controller
 *
 * @author KonBAI
 * @createTime 2022/3/10 00:12
 */
@Validated
@Api(value = "流程分类控制器", tags = {"流程分类管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/workflow/category")
public class WfCategoryController extends BaseController {

    private final IWfCategoryService flowCategoryService;

    /**
     * 查询流程分类列表
     */
    @ApiOperation("查询流程分类列表")
    @GetMapping("/list")
    public TableDataInfo<WfCategoryVo> list(WfCategoryBo bo, PageQuery pageQuery) {
        return flowCategoryService.queryPageList(bo, pageQuery);
    }

    /**
     * 查询全部的流程分类列表
     */
    @ApiOperation("查询全部流程分类列表")
    @GetMapping("/listAll")
    public R<List<WfCategoryVo>> listAll(@Validated(QueryGroup.class) WfCategoryBo bo) {
        return R.ok(flowCategoryService.queryList(bo));
    }

    /**
     * 导出流程分类列表
     */
    @ApiOperation("导出流程分类列表")
    @PostMapping("/export")
    public void export(@Validated WfCategoryBo bo, HttpServletResponse response) throws IOException {
        List<WfCategoryVo> list = flowCategoryService.queryList(bo);
        ExcelUtil<WfCategoryVo> util = new ExcelUtil<>(WfCategoryVo.class);
        util.exportExcel(response, list, "流程分类");
    }

    /**
     * 获取流程分类详细信息
     */
    @ApiOperation("获取流程分类详细信息")
    @GetMapping("/{categoryId}")
    public R<WfCategoryVo> getInfo(@ApiParam("主键")
                                                  @NotNull(message = "主键不能为空")
                                                  @PathVariable("categoryId") Long categoryId) {
        return R.ok(flowCategoryService.queryById(categoryId));
    }

    /**
     * 新增流程分类
     */
    @ApiOperation("新增流程分类")
    @PostMapping()
    public R add(@Validated(AddGroup.class) @RequestBody WfCategoryBo bo) {
        return toAjax(flowCategoryService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改流程分类
     */
    @ApiOperation("修改流程分类")
    @PutMapping()
    public R edit(@Validated(EditGroup.class) @RequestBody WfCategoryBo bo) {
        return toAjax(flowCategoryService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除流程分类
     */
    @ApiOperation("删除流程分类")
    @DeleteMapping("/{categoryIds}")
    public R remove(@ApiParam("主键串") @NotEmpty(message = "主键不能为空") @PathVariable Long[] categoryIds) {
        return toAjax(flowCategoryService.deleteWithValidByIds(Arrays.asList(categoryIds), true) ? 1 : 0);
    }
}
