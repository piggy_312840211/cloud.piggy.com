package com.piggy.flowable.service;

import com.piggy.api.flowable.domain.bo.WfProcessBo;
import com.piggy.api.flowable.domain.vo.WfDeployVo;
import com.piggy.common.core.web.domain.PageQuery;
import com.piggy.common.core.web.page.TableDataInfo;

import java.util.List;

/**
 * @author KonBAI
 * @createTime 2022/6/30 9:03
 */
public interface IWfDeployService {

    TableDataInfo<WfDeployVo> queryPageList(WfProcessBo processBo, PageQuery pageQuery);

    TableDataInfo<WfDeployVo> queryPublishList(String processKey, PageQuery pageQuery);

    void updateState(String definitionId, String stateCode);

    String queryBpmnXmlById(String definitionId);

    void deleteByIds(List<String> deployIds);
}
