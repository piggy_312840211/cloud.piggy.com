package com.piggy.flowable.controller;

import cn.hutool.json.JSONUtil;
import com.piggy.api.flowable.domain.bo.WfCopyBo;
import com.piggy.api.flowable.domain.bo.WfProcessBo;
import com.piggy.api.flowable.domain.vo.WfCopyVo;
import com.piggy.api.flowable.domain.vo.WfDefinitionVo;
import com.piggy.api.flowable.domain.vo.WfTaskVo;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.domain.PageQuery;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.satoken.utils.SecurityUtils;
import com.piggy.flowable.service.IWfCopyService;
import com.piggy.flowable.service.IWfProcessService;
import com.piggy.common.core.domain.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.Map;

/**
 * 工作流流程管理
 *
 * @author KonBAI
 * @createTime 2022/3/24 18:54
 */
@Slf4j
@Api(tags = "工作流流程管理")
@RequiredArgsConstructor
@RestController
@RequestMapping("/workflow/process")
public class WfProcessController extends BaseController {

    private final IWfProcessService processService;
    private final IWfCopyService copyService;

    @GetMapping(value = "/list")
    @ApiOperation(value = "查询可发起流程列表", response = WfDefinitionVo.class)
    public TableDataInfo<WfDefinitionVo> list(PageQuery pageQuery) {
        return processService.processList(pageQuery);
    }

    @GetMapping("/getProcessForm")
    @ApiOperation(value = "查询流程部署关联表单信息")
    public R<?> getForm(@ApiParam(value = "流程定义id") @RequestParam(value = "definitionId") String definitionId,
                        @ApiParam(value = "流程部署id") @RequestParam(value = "deployId") String deployId) {
        String formContent = processService.selectFormContent(definitionId, deployId);
        return R.ok(JSONUtil.toBean(formContent, Map.class));
    }

    @ApiOperation(value = "根据流程定义id启动流程实例")
    @PostMapping("/start/{processDefId}")
    public R start(@ApiParam(value = "流程定义id") @PathVariable(value = "processDefId") String processDefId,
                         @ApiParam(value = "变量集合,json对象") @RequestBody Map<String, Object> variables) {
        processService.startProcess(processDefId, variables);
        return R.ok("流程启动成功");

    }

    @ApiOperation(value = "查询流程详情信息")
    @GetMapping("/detail")
    public R detail(@RequestParam("procInsId") @NotBlank(message = "流程实例不能为空") String procInsId) {
        return R.ok(processService.queryProcessDetail(procInsId));
    }

    @ApiOperation(value = "我拥有的流程", response = WfTaskVo.class)
    @GetMapping(value = "/ownList")
    public TableDataInfo<WfTaskVo> ownProcess(PageQuery pageQuery) {
        return processService.queryPageOwnProcessList(pageQuery);
    }

    @ApiOperation(value = "获取待办列表", response = WfTaskVo.class)
    @GetMapping(value = "/todoList")
    public TableDataInfo<WfTaskVo> todoProcess(PageQuery pageQuery) {
        return processService.queryPageTodoProcessList(pageQuery);
    }

    @ApiOperation(value = "获取待签列表", response = WfTaskVo.class)
    @PostMapping(value = "/claimList")
    public TableDataInfo<WfTaskVo> claimProcess(@RequestBody WfProcessBo processBo) {
        return processService.queryPageClaimProcessList(processBo, processBo.getPageQuery());
    }

    @ApiOperation(value = "获取已办列表", response = WfTaskVo.class)
    @GetMapping(value = "/finishedList")
    public TableDataInfo<WfTaskVo> finishedProcess(PageQuery pageQuery) {
        return processService.queryPageFinishedProcessList(pageQuery);
    }

    @ApiOperation(value = "获取抄送列表", response = WfTaskVo.class)
    @PostMapping(value = "/copyList")
    public TableDataInfo<WfCopyVo> copyProcess(@RequestBody WfCopyBo copyBo) {
        copyBo.setUserId(SecurityUtils.getUserId());
        return copyService.queryPageList(copyBo, copyBo.getPageQuery());
    }

    /**
     * 根据流程key. businesskey，启动实列
     */
    @ApiOperation(value = "根据流程key. businesskey启动流程实例")
//    @Inner
    @PostMapping("/start")
    public R<WfTaskVo> start(@ApiParam(value = "流程业务对象") @RequestBody WfProcessBo processBo) {
        return processService.startProcessByDefKey(processBo.getProcessKey(), processBo.getBusinessKey(), processBo.getVariables());
    }

    @ApiOperation(value = "获取待办列表", response = WfTaskVo.class)
    @PostMapping (value = "/userTodoList")
    public TableDataInfo<WfTaskVo> userTodoList(@ApiParam(value = "流程业务对象") @RequestBody WfProcessBo processBo) {
        return processService.userTodoList(processBo, processBo.getPageQuery());
    }

    @ApiOperation(value = "根据procInstId获取流程图")
    @GetMapping("/readXml")
    public R readXml(@RequestParam("procInsId") @NotBlank(message = "流程实例不能为空") String procInsId) {
        try {
            return R.ok(processService.readXml(procInsId));
        } catch (Exception e) {
            return R.fail("加载xml文件异常");
        }
    }

}
