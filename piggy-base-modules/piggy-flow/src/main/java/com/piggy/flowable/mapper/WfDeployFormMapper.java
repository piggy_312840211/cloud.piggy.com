package com.piggy.flowable.mapper;

import com.piggy.api.flowable.domain.vo.WfDeployFormVo;
import com.piggy.common.core.web.page.BaseMapperPlusEx;
import com.piggy.flowable.domain.WfDeployForm;

/**
 * 流程实例关联表单Mapper接口
 *
 * @author KonBAI
 * @createTime 2022/3/7 22:07
 */
public interface WfDeployFormMapper extends BaseMapperPlusEx<WfDeployFormMapper, WfDeployForm, WfDeployFormVo> {

}
