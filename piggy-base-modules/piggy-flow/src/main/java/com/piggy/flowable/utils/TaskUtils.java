package com.piggy.flowable.utils;

import cn.hutool.core.util.ObjectUtil;
import com.piggy.common.satoken.utils.SecurityUtils;
import com.piggy.flowable.common.constant.TaskConstants;
import com.piggy.sys.api.model.LoginUser;

import java.util.ArrayList;
import java.util.List;

/**
 * 工作流任务工具类
 *
 * @author konbai
 * @createTime 2022/4/24 12:42
 */
public class TaskUtils {

    public static String getUserId() {
        return String.valueOf(SecurityUtils.getUserId());
    }

    /**
     * 获取用户组信息
     *
     * @return candidateGroup
     */
    public static List<String> getCandidateGroup() {
        List<String> list = new ArrayList<>();
        LoginUser user = SecurityUtils.getOauthUser();
        if (ObjectUtil.isNotNull(user)) {
            if (ObjectUtil.isNotEmpty(user.getRoles())) {
                user.getRoles().forEach(role -> list.add(TaskConstants.ROLE_GROUP_PREFIX + role));
            }
            if (ObjectUtil.isNotNull(user.getSysUser().getDeptId())) {
                list.add(TaskConstants.DEPT_GROUP_PREFIX + user.getSysUser().getDeptId());
            }
        }
        return list;
    }
}
