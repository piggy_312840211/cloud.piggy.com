package com.piggy.flowable.service;

import com.piggy.api.flowable.domain.bo.WfProcessBo;
import com.piggy.api.flowable.domain.vo.WfDefinitionVo;
import com.piggy.api.flowable.domain.vo.WfDetailVo;
import com.piggy.api.flowable.domain.vo.WfTaskVo;
import com.piggy.common.core.web.domain.PageQuery;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.core.domain.R;

import java.io.IOException;
import java.util.Map;

/**
 * @author KonBAI
 * @createTime 2022/3/24 18:57
 */
public interface IWfProcessService {

    /**
     * 查询可发起流程列表
     * @param pageQuery 分页参数
     * @return
     */
    TableDataInfo<WfDefinitionVo> processList(PageQuery pageQuery);

    /**
     * 查询流程部署关联表单信息
     * @param definitionId 流程定义ID
     * @param deployId 部署ID
     */
    String selectFormContent(String definitionId, String deployId);

    /**
     * 启动流程实例
     * @param procDefId 流程定义ID
     * @param variables 扩展参数
     */
    void startProcess(String procDefId, Map<String, Object> variables);

    /**
     * 通过DefinitionKey启动流程
     * @param procDefKey 流程定义Key
     * @param variables 扩展参数
     */
    void startProcessByDefKey(String procDefKey, Map<String, Object> variables);

    /**
     * 查询流程任务详情信息
     * @param procInsId 流程实例ID
     */
    WfDetailVo queryProcessDetail(String procInsId);

    /**
     * 查询我的流程列表
     * @param pageQuery 分页参数
     */
    TableDataInfo<WfTaskVo> queryPageOwnProcessList(PageQuery pageQuery);

    /**
     * 查询代办任务列表
     * @param pageQuery 分页参数
     */
    TableDataInfo<WfTaskVo> queryPageTodoProcessList(PageQuery pageQuery);

    /**
     * 查询待签任务列表
     * @param pageQuery 分页参数
     */
    TableDataInfo<WfTaskVo> queryPageClaimProcessList(WfProcessBo processBo, PageQuery pageQuery);

    /**
     * 查询已办任务列表
     * @param pageQuery 分页参数
     */
    TableDataInfo<WfTaskVo> queryPageFinishedProcessList(PageQuery pageQuery);

    R<WfTaskVo> startProcessByDefKey(String procDefKey, String businessKey, Map<String, Object> variables);

    TableDataInfo<WfTaskVo> userTodoList(WfProcessBo processBo, PageQuery pageQuery);

    String readXml(String procInsId) throws IOException;

}
