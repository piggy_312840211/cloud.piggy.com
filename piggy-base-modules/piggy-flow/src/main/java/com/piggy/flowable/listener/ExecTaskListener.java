package com.piggy.flowable.listener;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;

/**
 * @author Xuan xuan
 * @date 2021/4/20
 */
public class ExecTaskListener implements ExecutionListener {

    @Override
    public void notify(DelegateExecution execution) {
        execution.getId();
    }
}
