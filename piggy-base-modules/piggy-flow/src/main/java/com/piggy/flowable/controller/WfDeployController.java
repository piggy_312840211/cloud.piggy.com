package com.piggy.flowable.controller;

import com.alibaba.fastjson2.JSON;
import com.piggy.api.flowable.domain.bo.WfProcessBo;
import com.piggy.api.flowable.domain.vo.WfDeployVo;
import com.piggy.api.flowable.domain.vo.WfFormVo;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.domain.PageQuery;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.flowable.service.IWfDeployFormService;
import com.piggy.flowable.service.IWfDeployService;
import com.piggy.common.core.domain.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

/**
 * @author KonBAI
 * @createTime 2022/3/24 20:57
 */
@Slf4j
@Api(tags = "流程部署")
@RequiredArgsConstructor
@RestController
@RequestMapping("/workflow/deploy")
public class WfDeployController extends BaseController {

    private final IWfDeployService deployService;
    private final IWfDeployFormService deployFormService;

    /**
     * 查询流程部署列表
     */
    @GetMapping("/list")
    public TableDataInfo<WfDeployVo> list(WfProcessBo processBo, PageQuery pageQuery) {
        return deployService.queryPageList(processBo, pageQuery);
    }

    /**
     * 查询流程部署版本列表
     */
    @GetMapping("/publishList")
    public TableDataInfo<WfDeployVo> publishList(@ApiParam(value = "流程定义Key", required = true) @RequestParam String processKey,
                                                 PageQuery pageQuery) {
        return deployService.queryPublishList(processKey, pageQuery);
    }

    /**
     *
     * @param state
     * @param definitionId
     * @return
     */
    @ApiOperation(value = "激活或挂起流程")
    @PutMapping(value = "/changeState")
    public R<Void> changeState(@ApiParam(value = "状态（active:激活 suspended:挂起）", required = true) @RequestParam String state,
                               @ApiParam(value = "流程定义ID", required = true) @RequestParam String definitionId) {
        deployService.updateState(definitionId, state);
        return R.ok();
    }

    @ApiOperation(value = "读取xml文件")
    @GetMapping("/bpmnXml/{definitionId}")
    public R<String> getBpmnXml(@ApiParam(value = "流程定义ID") @PathVariable(value = "definitionId") String definitionId) {
        return R.ok(null, deployService.queryBpmnXmlById(definitionId));
    }

    /**
     * 删除流程模型
     */
    @ApiOperation("删除流程部署")
    @DeleteMapping("/{deployIds}")
    public R<String> remove(@ApiParam(value = "流程部署ids") @NotEmpty(message = "主键不能为空") @PathVariable String[] deployIds) {
        deployService.deleteByIds(Arrays.asList(deployIds));
        return R.ok();
    }

    /**
     *
     * @param deployId
     * @return
     */
    @ApiOperation(value = "查询流程部署关联表单信息")
    @GetMapping("/form/{deployId}")
    public R<?> start(@ApiParam(value = "流程部署id") @PathVariable(value = "deployId") String deployId) {
        WfFormVo formVo = deployFormService.selectDeployFormByDeployId(deployId);
        if (Objects.isNull(formVo)) {
            return R.fail("请先配置流程表单");
        }
        return R.ok(JSON.parseObject(formVo.getContent(), Map.class));
    }
}
