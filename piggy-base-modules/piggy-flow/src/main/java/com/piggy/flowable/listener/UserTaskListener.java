package com.piggy.flowable.listener;

import org.flowable.engine.delegate.TaskListener;
import org.flowable.task.service.delegate.DelegateTask;

/**
 * @author Xuan xuan
 * @date 2021/4/20
 */
public class UserTaskListener implements TaskListener{

    @Override
    public void notify(DelegateTask delegateTask) {
        delegateTask.getId();
        delegateTask.getName();
        if (delegateTask.getTaskDefinitionKey().equals("cg.order.confirm")) {
            delegateTask.setVariable("cg-order-confirm", "Y");
        }
    }

}
