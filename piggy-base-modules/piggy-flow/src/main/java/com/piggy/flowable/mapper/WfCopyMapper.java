package com.piggy.flowable.mapper;

import com.piggy.api.flowable.domain.vo.WfCopyVo;
import com.piggy.common.core.web.page.BaseMapperPlusEx;
import com.piggy.flowable.domain.WfCopy;

/**
 * 流程抄送Mapper接口
 *
 * @author KonBAI
 * @date 2022-05-19
 */
public interface WfCopyMapper extends BaseMapperPlusEx<WfCopyMapper, WfCopy, WfCopyVo> {

}
