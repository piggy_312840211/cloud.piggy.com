package com.piggy.flowable;

import com.piggy.common.satoken.annotation.EnableCustomConfig;
import com.piggy.common.satoken.annotation.EnableCustomFeignClients;
import com.piggy.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 工作流服务
 *
 * @author shark
 */
@EnableCustomSwagger2
@EnableCustomFeignClients
@EnableCustomConfig
@SpringBootApplication
public class PIGYFlowApplication {
    public static void main(String[] args) {
        SpringApplication.run(PIGYFlowApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  FLOWABLE工作流服务模块启动成功   ლ(´ڡ`ლ)ﾞ");
    }
}
