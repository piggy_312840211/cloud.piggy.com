package com.piggy.flowable.controller;

import com.piggy.api.flowable.domain.bo.WfModelBo;
import com.piggy.api.flowable.domain.vo.WfModelVo;
import com.piggy.common.core.annotation.RepeatSubmit;
import com.piggy.common.core.validate.AddGroup;
import com.piggy.common.core.validate.EditGroup;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.domain.PageQuery;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.flowable.service.IWfModelService;
import com.piggy.common.core.domain.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

/**
 * @author KonBAI
 * @createTime 2022/6/21 9:09
 */
@Slf4j
@Api(tags = "工作流流程模型管理")
@RequiredArgsConstructor
@RestController
@RequestMapping("/workflow/model")
public class WfModelController extends BaseController {

    private final IWfModelService modelService;

    @ApiOperation(value = "查询流程模型列表")
    @GetMapping("/list")
    public TableDataInfo<WfModelVo> list(WfModelBo modelBo, PageQuery pageQuery) {
        return modelService.list(modelBo, pageQuery);
    }

    @ApiOperation(value = "查询流程模型列表")
    @GetMapping("/historyList")
    public TableDataInfo<WfModelVo> historyList(WfModelBo modelBo, PageQuery pageQuery) {
        return modelService.historyList(modelBo, pageQuery);
    }

    /**
     * 获取流程模型详细信息
     */
    @ApiOperation(value = "查询流程模型详情信息")
    @GetMapping(value = "/{modelId}")
    public R<WfModelVo> getInfo(@ApiParam("主键") @NotNull(message = "主键不能为空") @PathVariable("modelId") String modelId) {
        return R.ok(modelService.getModel(modelId));
    }

    /**
     * 获取流程表单详细信息
     */
    @ApiOperation("查询流程表单详细信息")
    @GetMapping(value = "/bpmnXml/{modelId}")
    public R<String> getBpmnXml(@ApiParam("主键") @NotNull(message = "主键不能为空") @PathVariable("modelId") String modelId) {
        return R.ok(modelService.queryBpmnXmlById(modelId),"操作成功");
    }

    /**
     * 新增流程模型
     */
    @ApiOperation("新增流程模型")
    @PostMapping
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WfModelBo modelBo) {
        modelService.insertModel(modelBo);
        return R.ok();
    }

    /**
     * 修改流程模型
     */
    @ApiOperation("修改流程模型")
    @PutMapping
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WfModelBo modelBo) {
        modelService.updateModel(modelBo);
        return R.ok();
    }

    /**
     * 保存流程模型
     */
    @ApiOperation("保存流程模型")
    @RepeatSubmit()
    @PostMapping("/save")
    public R<String> save(@RequestBody WfModelBo modelBo) {
        modelService.saveModel(modelBo);
        return R.ok();
    }

    @ApiOperation("设为最新流程模型")
    @RepeatSubmit()
    @PostMapping("/latest")
    public R<?> latest(@RequestParam String modelId) {
        modelService.latestModel(modelId);
        return R.ok();
    }

    /**
     * 删除流程模型
     */
    @ApiOperation("删除流程模型")
    @DeleteMapping("/{modelIds}")
    public R<String> remove(@ApiParam("主键串") @NotEmpty(message = "主键不能为空") @PathVariable String[] modelIds) {
        modelService.deleteByIds(Arrays.asList(modelIds));
        return R.ok();
    }

    @ApiOperation("部署流程模型")
    @RepeatSubmit()
    @PostMapping("/deploy")
    public R deployModel(@RequestParam String modelId) {
        return toAjax(modelService.deployModel(modelId));
    }

}
