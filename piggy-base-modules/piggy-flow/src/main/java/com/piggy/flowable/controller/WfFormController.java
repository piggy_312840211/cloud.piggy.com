package com.piggy.flowable.controller;

import com.piggy.api.flowable.domain.bo.WfFormBo;
import com.piggy.api.flowable.domain.vo.WfFormVo;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.validate.QueryGroup;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.domain.PageQuery;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.flowable.domain.WfDeployForm;
import com.piggy.flowable.service.IWfDeployFormService;
import com.piggy.flowable.service.IWfFormService;
import com.piggy.common.core.domain.R;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 流程表单Controller
 *
 * @author KonBAI
 * @createTime 2022/3/7 22:07
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/workflow/form")
public class WfFormController extends BaseController {

    private final IWfFormService formService;

    private final IWfDeployFormService deployFormService;

    /**
     * 查询流程表单列表
     */
    @GetMapping("/list")
    public TableDataInfo<WfFormVo> list(@Validated(QueryGroup.class) WfFormBo bo, PageQuery pageQuery) {
        return formService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出流程表单列表
     */
    @GetMapping("/export")
    public void export(@Validated WfFormBo bo, HttpServletResponse response) throws IOException {
        List<WfFormVo> list = formService.queryList(bo);
        ExcelUtil<WfFormVo> util = new ExcelUtil<WfFormVo>(WfFormVo.class);
        util.exportExcel(response, list, "流程表单");
    }

    /**
     * 获取流程表单详细信息
     */
    @GetMapping(value = "/{formId}")
    public R<WfFormVo> getInfo(@ApiParam("主键") @NotNull(message = "主键不能为空") @PathVariable("formId") Long formId) {
        return R.ok(formService.queryById(formId));
    }

    /**
     * 新增流程表单
     */
    @PostMapping
    public R add(@RequestBody WfFormBo bo) {
        return toAjax(formService.insertForm(bo));
    }

    /**
     * 修改流程表单
     */
    @PutMapping
    public R edit(@RequestBody WfFormBo bo) {
        return toAjax(formService.updateForm(bo));
    }

    /**
     * 删除流程表单
     */
    @DeleteMapping("/{formIds}")
    public R remove(@ApiParam("主键串") @NotEmpty(message = "主键不能为空") @PathVariable Long[] formIds) {
        return toAjax(formService.deleteWithValidByIds(Arrays.asList(formIds)) ? 1 : 0);
    }


    /**
     * 挂载流程表单
     */
    @PostMapping("/addDeployForm")
    public R addDeployForm(@RequestBody WfDeployForm deployForm) {
        return toAjax(deployFormService.insertWfDeployForm(deployForm));
    }
}
