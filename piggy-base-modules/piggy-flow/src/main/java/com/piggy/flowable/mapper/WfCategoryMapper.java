package com.piggy.flowable.mapper;

import com.piggy.api.flowable.domain.vo.WfCategoryVo;
import com.piggy.common.core.web.page.BaseMapperPlusEx;
import com.piggy.flowable.domain.WfCategory;

/**
 * 流程分类Mapper接口
 *
 * @author KonBAI
 * @date 2022-01-15
 */
public interface WfCategoryMapper extends BaseMapperPlusEx<WfCategoryMapper, WfCategory, WfCategoryVo> {

}
