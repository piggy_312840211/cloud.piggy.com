package com.piggy.flowable.handler;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * 多实例处理类
 *
 * @author KonBAI
 */
@AllArgsConstructor
@Component("multiInstanceHandler")
public class MultiInstanceHandler {
/*
    @Resource
    private RemoteUserService userService;

    public HashSet<String> getUserIds(DelegateExecution execution) {
        HashSet<String> candidateUserIds = new LinkedHashSet<>();
        FlowElement flowElement = execution.getCurrentFlowElement();
        if (ObjectUtil.isNotEmpty(flowElement) && flowElement instanceof UserTask) {
            UserTask userTask = (UserTask) flowElement;
            String dataType = userTask.getAttributeValue(ProcessConstants.NAMASPASE, ProcessConstants.PROCESS_CUSTOM_DATA_TYPE);
            if ("USERS".equals(dataType) && CollUtil.isNotEmpty(userTask.getCandidateUsers())) {
                candidateUserIds.addAll(userTask.getCandidateUsers());
            } else if (CollUtil.isNotEmpty(userTask.getCandidateGroups())) {
                List<String> groups = userTask.getCandidateGroups()
                    .stream().map(item -> item.substring(4)).collect(Collectors.toList());
                if ("ROLES".equals(dataType)) {
                    SysUserRoleMapper userRoleMapper = SpringUtils.getBean(SysUserRoleMapper.class);
                    groups.forEach(item -> {
                        List<String> userIds = userRoleMapper.selectUserIdsByRoleId(Long.parseLong(item))
                            .stream().map(String::valueOf).collect(Collectors.toList());
                        candidateUserIds.addAll(userIds);
                    });
                } else if ("DEPTS".equals(dataType)) {
                    SysUserMapper userMapper = SpringUtils.getBean(SysUserMapper.class);
                    LambdaQueryWrapper<SysUser> lambdaQueryWrapper = new LambdaQueryWrapper<SysUser>()
                        .select(SysUser::getUserId).in(SysUser::getDeptId, groups);
                    List<String> userIds = userMapper.selectList(lambdaQueryWrapper)
                        .stream().map(k -> String.valueOf(k.getUserId())).collect(Collectors.toList());
                    candidateUserIds.addAll(userIds);
                }
            }
        }
        return candidateUserIds;
    }*/
}
