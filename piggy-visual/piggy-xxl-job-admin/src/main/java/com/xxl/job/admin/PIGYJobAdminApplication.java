package com.xxl.job.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xuxueli 2018-10-28 00:38:13
 */
@SpringBootApplication
public class PIGYJobAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(PIGYJobAdminApplication.class, args);
		System.out.println("(♥◠‿◠)ﾉﾞ  xxl-job模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" );
	}

}
