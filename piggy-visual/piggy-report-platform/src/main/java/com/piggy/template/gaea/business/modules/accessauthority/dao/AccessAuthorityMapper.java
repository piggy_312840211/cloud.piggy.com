package com.piggy.template.gaea.business.modules.accessauthority.dao;

import org.apache.ibatis.annotations.Mapper;
import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.piggy.template.gaea.business.modules.accessauthority.dao.entity.AccessAuthority;

/**
* AccessAuthority Mapper
* @author 木子李·De <lide1202@hotmail.com>
* @date 2019-02-17 08:50:10.009
**/
@Mapper
public interface AccessAuthorityMapper extends GaeaBaseMapper<AccessAuthority> {

}