package com.piggy.template.gaea.business.modules.report.service.impl;

import com.anji.plus.gaea.constant.BaseOperationEnum;
import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anji.plus.gaea.exception.BusinessException;
import com.piggy.template.gaea.business.modules.report.controller.dto.ReportDto;
import com.piggy.template.gaea.business.modules.report.dao.ReportMapper;
import com.piggy.template.gaea.business.modules.report.dao.entity.Report;
import com.piggy.template.gaea.business.modules.report.service.ReportService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 *
 * @author chenkening
 * @date 2021/3/26 10:35
 */
@Service
public class ReportServiceImpl implements ReportService {

    @Resource
    private ReportMapper reportMapper;

    @Override
    public GaeaBaseMapper<Report> getMapper() {
        return reportMapper;
    }


    @Override
    public void delReport(ReportDto reportDto) {
        deleteById(reportDto.getId());
        //删除gaea_report_excel、gaea_report_dashboard、gaea_report_dashboard_widget
        //...
    }

    @Override
    public void processBeforeOperation(Report entity, BaseOperationEnum operationEnum) throws BusinessException {

    }
}
