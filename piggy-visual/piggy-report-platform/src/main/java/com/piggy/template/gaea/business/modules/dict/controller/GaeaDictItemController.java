package com.piggy.template.gaea.business.modules.dict.controller;

import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.piggy.template.gaea.business.modules.dict.controller.dto.GaeaDictItemDTO;
import com.piggy.template.gaea.business.modules.dict.controller.param.GaeaDictItemParam;
import com.piggy.template.gaea.business.modules.dict.dao.entity.GaeaDictItem;
import com.piggy.template.gaea.business.modules.dict.service.GaeaDictItemService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 数据字典项(GaeaDictItem)实体类
 *
 * @author lirui
 * @since 2021-03-10 13:05:59
 */
@RestController
@RequestMapping("/gaeaDictItem")
@Api(value = "/gaeaDictItem", tags = "数据字典项")
public class GaeaDictItemController extends GaeaBaseController<GaeaDictItemParam, GaeaDictItem, GaeaDictItemDTO> {
    @Resource
    private GaeaDictItemService gaeaDictItemService;
    
    @Override
    public GaeaBaseService<GaeaDictItemParam, GaeaDictItem> getService() {
        return gaeaDictItemService;
    }

    @Override
    public GaeaDictItem getEntity() {
        return new GaeaDictItem();
    }

    @Override
    public GaeaDictItemDTO getDTO() {
        return new GaeaDictItemDTO();
    }
}