package com.piggy.gateway.handler;

import com.alibaba.fastjson2.JSON;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.exception.CaptchaException;
import com.piggy.gateway.service.ValidateCodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * 验证码获取
 *
 * @author shark
 */
@Slf4j
@Component
public class ValidateCodeHandler implements HandlerFunction<ServerResponse>
{
    @Resource
    private ValidateCodeService validateCodeService;

    @Override
    public Mono<ServerResponse> handle(ServerRequest serverRequest)
    {
        R ajax;
        try
        {
            log.error("获取验证码请求........................................asdfasdfasdfasdfasdfasdfasdf");
            ajax = validateCodeService.createCapcha();
            log.error("获取验证码请求........................................返回{}}", JSON.toJSONString(ajax));
        }
        catch (CaptchaException | IOException e)
        {
            return Mono.error(e);
        }
        return ServerResponse.status(HttpStatus.OK).body(BodyInserters.fromValue(ajax));
    }
}
