package com.piggy.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 网关启动程序
 *
 * @author shark
 */
@RefreshScope
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class PIGYGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(PIGYGatewayApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  智融云链网关启动成功   ლ(´ڡ`ლ)ﾞ  \n" );
    }
}
