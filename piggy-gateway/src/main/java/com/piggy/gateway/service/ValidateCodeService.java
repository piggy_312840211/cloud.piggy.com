package com.piggy.gateway.service;

import java.io.IOException;

import com.piggy.common.core.domain.R;
import com.piggy.common.core.exception.CaptchaException;

/**
 * 验证码处理
 *
 * @author shark
 */
public interface ValidateCodeService
{
    /**
     * 生成验证码
     */
    public R createCapcha() throws IOException, CaptchaException;

    /**
     * 校验验证码
     */
    public void checkCapcha(String key, String value) throws CaptchaException;
}
