## 平台简介

基于 PIGGY-Cloud 集成 Mybatis-Plus Lombok Hutool Redisson 等便捷开发工具

* 采用前后端分离的模式，微服务版本
* 后端采用Spring Boot、Spring Cloud & Alibaba。
* 注册中心、配置中心选型Nacos，权限认证使用Jwt+Redis。
* 流量控制框架选型Sentinel，分布式事务选型Seata。

## 修改功能

### 依赖改动

* ORM框架 使用 Mybatis-Plus 3.4.3 简化CRUD (支持主子表)
* Bean简化 使用 Lombok 简化 get set toString 等等
* 容器改动 Tomcat 改为 并发性能更好的 undertow
* 分页支持 pagehelper, Mybatis-Plus 分页两种分页
* 集成 Hutool 5.X 并重写部分功能
* 集成 Redisson 3.13.3 支持分布式锁功能
* 集成 netty ws,netty tcp,netty socketio，网络库，后续还会集成mqtt等，三方库，简化使用

### 代码改动

* 所有原生功能使用 Mybatis-Plus 与 Lombok 重写
* 增加 IServicePlus 与 BaseMapperPlus 可自定义通用方法
* 增加 分布式锁相关功能
* 代码生成模板 改为适配 Mybatis-Plus 的代码
* 代码生成模板 增加 文档注解 与 校验注解 简化通用操作

### 其他

/***/


## 系统模块

~~~
com.piggy     
├── piggy-ui              // 前端框架
├── piggy-gateway         // 网关模块 [8080]
├── piggy-auth-satoken            // 认证中心 [9200]
├── piggy-api             // 接口模块
│       └── piggy-api-sys                          // 系统接口
├── piggy-common          // 通用模块
│       └── piggy-common-core                         // 核心模块
│       └── piggy-common-datascope                    // 权限范围
│       └── piggy-common-datasource                   // 多数据源
│       └── piggy-common-log                          // 日志记录
│       └── piggy-common-job                          // 日志记录
│       └── piggy-common-redis                        // 缓存服务
│       └── piggy-common-satoken                     // 安全模块(satoken替换)
│       └── piggy-common-swagger                      // 系统接口
│       └── piggy-common-netty-ws                      // 对netty-ws封装
│       └── piggy-common-netty-socketio                      // 对netty-socketio封装
│       └── piggy-netty-kcp                      // kcp协议支持
│       └── piggy-netty-ws                      // wbsocket协议支持
│       └── piggy-netty-socketio                      // 多协议支持，有学习调试难度
├── piggy-base-modules         // 基础组件模块
│       └── piggy-flowable                              // 工作流程引擎
│       └── piggy-gen                              // 代码生成
│       └── piggy-seata-server                              // seata集成
├── piggy-modules         // 业务模块
│       └── piggy-system                              // 系统模块 [9201]
│       └── piggy-file                                // 文件服务 [9300]
│       └── piggy-msg                                 // 代码生成 [9202]
├── piggy-visual          // 图形化管理模块
│       └── piggy-monitor                      // 监控中心 [9100]
│       └── piggy-report-platform                      // 监控中心 [9100]
│       └── piggy-xxl-job-admin                      // xxl-job [9100]
├──pom.xml                // 公共依赖
~~~

## 架构图

<img alt="架构图" src="https://oscimg.oschina.net/oscnet/up-82e9722ecb846786405a904bafcf19f73f3.png"/>

## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 在线构建器：拖动表单元素生成相应的HTML代码。
17. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

## 后续
1. 做好piggy-ui集成
2. 造更多轮子，集成netty-mqtt
3. 小程序(uniapp)，app(ios,android)
4. 方便运维，后续考虑引入apache apisix做微服务API网关，得看社区成熟度
5. .........................