package com.piggy.pay.logic.mch.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.mch.domain.TMchIsvInfo;

/**
 * 服务商信息Mapper接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface TMchIsvInfoMapper extends BaseMapperPlus<TMchIsvInfo> {

}
