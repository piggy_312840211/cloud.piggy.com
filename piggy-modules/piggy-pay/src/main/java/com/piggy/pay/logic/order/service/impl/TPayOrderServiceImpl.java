package com.piggy.pay.logic.order.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.core.constants.CS;
import com.piggy.pay.core.enums.IOrderEnums;
import com.piggy.pay.logic.config.domain.TPayWay;
import com.piggy.pay.logic.config.mapper.TPayWayMapper;
import com.piggy.pay.logic.mch.domain.TMchInfo;
import com.piggy.pay.logic.mch.domain.TMchIsvInfo;
import com.piggy.pay.logic.mch.mapper.TMchInfoMapper;
import com.piggy.pay.logic.mch.mapper.TMchIsvInfoMapper;
import com.piggy.pay.logic.order.bo.TPayOrderEditBo;
import com.piggy.pay.logic.order.domain.TPayOrder;
import com.piggy.pay.logic.order.mapper.TPayOrderDivisionRecordMapperEx;
import com.piggy.pay.logic.order.mapper.TPayOrderMapper;
import com.piggy.pay.logic.order.mapper.TPayOrderMapperEx;
import com.piggy.pay.logic.order.service.ITPayOrderDivisionRecordService;
import com.piggy.pay.logic.order.service.ITPayOrderService;
import com.piggy.pay.logic.order.vo.TPayOrderVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.piggy.pay.logic.order.bo.TPayOrderQueryBo;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * 支付订单Service业务层处理
 *
 * @author piggy
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TPayOrderServiceImpl extends ServiceImpl<TPayOrderMapper, TPayOrder> implements ITPayOrderService {

    @Resource
    private TPayWayMapper payWayMapper;

    @Resource
    private TPayOrderMapper payOrderMapper;

    @Resource
    private TPayOrderMapperEx payOrderMapperEx;

    @Resource
    private TMchInfoMapper mchInfoMapper;

    @Resource
    private TMchIsvInfoMapper isvInfoMapper;

    @Resource
    private TPayOrderDivisionRecordMapperEx divisionRecordMapperEx;

    @Override
    public TPayOrderVo queryById(String payOrderId){
        return getVoById(payOrderId, TPayOrderVo.class);
    }

    @Override
    public TableDataInfo<TPayOrderVo> queryPageList(TPayOrderQueryBo bo) {
        PagePlus<TPayOrder, TPayOrderVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TPayOrderVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TPayOrderVo> queryList(TPayOrderQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TPayOrderVo.class);
    }

    private LambdaQueryWrapper<TPayOrder> buildQueryWrapper(TPayOrderQueryBo bo) {
        LambdaQueryWrapper<TPayOrder> lqw = Wrappers.lambdaQuery();
        lqw.eq(StrUtil.isNotBlank(bo.getPayOrderId()), TPayOrder::getPayOrderId, bo.getPayOrderId());
        lqw.eq(StrUtil.isNotBlank(bo.getMchNo()), TPayOrder::getMchNo, bo.getMchNo());
        lqw.eq(StrUtil.isNotBlank(bo.getIsvNo()), TPayOrder::getIsvNo, bo.getIsvNo());
        lqw.eq(StrUtil.isNotBlank(bo.getAppId()), TPayOrder::getAppId, bo.getAppId());
        lqw.like(StrUtil.isNotBlank(bo.getMchName()), TPayOrder::getMchName, bo.getMchName());
        lqw.eq(bo.getMchType() != null, TPayOrder::getMchType, bo.getMchType());
        lqw.eq(StrUtil.isNotBlank(bo.getMchOrderNo()), TPayOrder::getMchOrderNo, bo.getMchOrderNo());
        lqw.eq(StrUtil.isNotBlank(bo.getIfCode()), TPayOrder::getIfCode, bo.getIfCode());
        lqw.eq(StrUtil.isNotBlank(bo.getWayCode()), TPayOrder::getWayCode, bo.getWayCode());
        lqw.eq(bo.getAmount() != null, TPayOrder::getAmount, bo.getAmount());
        lqw.eq(bo.getMchFeeRate() != null, TPayOrder::getMchFeeRate, bo.getMchFeeRate());
        lqw.eq(bo.getMchFeeAmount() != null, TPayOrder::getMchFeeAmount, bo.getMchFeeAmount());
        lqw.eq(StrUtil.isNotBlank(bo.getCurrency()), TPayOrder::getCurrency, bo.getCurrency());
        lqw.eq(bo.getState() != null, TPayOrder::getState, bo.getState());
        lqw.eq(bo.getNotifyState() != null, TPayOrder::getNotifyState, bo.getNotifyState());
        lqw.eq(StrUtil.isNotBlank(bo.getClientIp()), TPayOrder::getClientIp, bo.getClientIp());
        lqw.eq(StrUtil.isNotBlank(bo.getSubject()), TPayOrder::getSubject, bo.getSubject());
        lqw.eq(StrUtil.isNotBlank(bo.getBody()), TPayOrder::getBody, bo.getBody());
        lqw.eq(StrUtil.isNotBlank(bo.getChannelExtra()), TPayOrder::getChannelExtra, bo.getChannelExtra());
        lqw.eq(StrUtil.isNotBlank(bo.getChannelUser()), TPayOrder::getChannelUser, bo.getChannelUser());
        lqw.eq(StrUtil.isNotBlank(bo.getChannelOrderNo()), TPayOrder::getChannelOrderNo, bo.getChannelOrderNo());
        lqw.eq(bo.getRefundState() != null, TPayOrder::getRefundState, bo.getRefundState());
        lqw.eq(bo.getRefundTimes() != null, TPayOrder::getRefundTimes, bo.getRefundTimes());
        lqw.eq(bo.getRefundAmount() != null, TPayOrder::getRefundAmount, bo.getRefundAmount());
        lqw.eq(bo.getDivisionMode() != null, TPayOrder::getDivisionMode, bo.getDivisionMode());
        lqw.eq(bo.getDivisionState() != null, TPayOrder::getDivisionState, bo.getDivisionState());
        lqw.eq(bo.getDivisionLastTime() != null, TPayOrder::getDivisionLastTime, bo.getDivisionLastTime());
        lqw.eq(StrUtil.isNotBlank(bo.getErrCode()), TPayOrder::getErrCode, bo.getErrCode());
        lqw.eq(StrUtil.isNotBlank(bo.getErrMsg()), TPayOrder::getErrMsg, bo.getErrMsg());
        lqw.eq(StrUtil.isNotBlank(bo.getExtParam()), TPayOrder::getExtParam, bo.getExtParam());
        lqw.eq(StrUtil.isNotBlank(bo.getNotifyUrl()), TPayOrder::getNotifyUrl, bo.getNotifyUrl());
        lqw.eq(StrUtil.isNotBlank(bo.getReturnUrl()), TPayOrder::getReturnUrl, bo.getReturnUrl());
        lqw.eq(bo.getExpiredTime() != null, TPayOrder::getExpiredTime, bo.getExpiredTime());
        lqw.eq(bo.getSuccessTime() != null, TPayOrder::getSuccessTime, bo.getSuccessTime());
        lqw.eq(bo.getCreatedAt() != null, TPayOrder::getCreatedAt, bo.getCreatedAt());
        lqw.eq(bo.getUpdatedAt() != null, TPayOrder::getUpdatedAt, bo.getUpdatedAt());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TPayOrderEditBo bo) {
        TPayOrder add = BeanUtil.toBean(bo, TPayOrder.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TPayOrderEditBo bo) {
        TPayOrder update = BeanUtil.toBean(bo, TPayOrder.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TPayOrder entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

    /** 更新订单状态  【订单生成】 --》 【支付中】 **/
    @Override
    public boolean updateInit2Ing(String payOrderId, TPayOrder payOrder){

        TPayOrder updateRecord = new TPayOrder();
        updateRecord.setState(IOrderEnums.OrderStateEnums.STATE_ING.getState());

        //同时更新， 未确定 --》 已确定的其他信息。  如支付接口的确认、 费率的计算。
        updateRecord.setIfCode(payOrder.getIfCode());
        updateRecord.setWayCode(payOrder.getWayCode());
        updateRecord.setMchFeeRate(payOrder.getMchFeeRate());
        updateRecord.setMchFeeAmount(payOrder.getMchFeeAmount());
        updateRecord.setChannelUser(payOrder.getChannelUser());
        updateRecord.setChannelOrderNo(payOrder.getChannelOrderNo());

        return update(updateRecord, new LambdaUpdateWrapper<TPayOrder>()
                .eq(TPayOrder::getPayOrderId, payOrderId).eq(TPayOrder::getState, IOrderEnums.OrderStateEnums.STATE_INIT.getState()));
    }

    /** 更新订单状态  【支付中】 --》 【支付成功】 **/
    @Override
    public boolean updateIng2Success(String payOrderId, String channelOrderNo, String channelUserId){

        TPayOrder updateRecord = new TPayOrder();
        updateRecord.setState(IOrderEnums.OrderStateEnums.STATE_SUCCESS.getState());
        updateRecord.setChannelOrderNo(channelOrderNo);
        updateRecord.setChannelUser(channelUserId);
        updateRecord.setSuccessTime(new Date());

        return update(updateRecord, new LambdaUpdateWrapper<TPayOrder>()
                .eq(TPayOrder::getPayOrderId, payOrderId).eq(TPayOrder::getState, IOrderEnums.OrderStateEnums.STATE_ING.getState()));
    }

    /** 更新订单状态  【支付中】 --》 【订单关闭】 **/
    @Override
    public boolean updateIng2Close(String payOrderId){

        TPayOrder updateRecord = new TPayOrder();
        updateRecord.setState(IOrderEnums.OrderStateEnums.STATE_CLOSED.getState());

        return update(updateRecord, new LambdaUpdateWrapper<TPayOrder>()
                .eq(TPayOrder::getPayOrderId, payOrderId).eq(TPayOrder::getState, IOrderEnums.OrderStateEnums.STATE_ING.getState()));
    }

    /** 更新订单状态  【订单生成】 --》 【订单关闭】 **/
    @Override
    public boolean updateInit2Close(String payOrderId){

        TPayOrder updateRecord = new TPayOrder();
        updateRecord.setState(IOrderEnums.OrderStateEnums.STATE_CLOSED.getState());

        return update(updateRecord, new LambdaUpdateWrapper<TPayOrder>()
                .eq(TPayOrder::getPayOrderId, payOrderId).eq(TPayOrder::getState, IOrderEnums.OrderStateEnums.STATE_INIT.getState()));
    }


    /** 更新订单状态  【支付中】 --》 【支付失败】 **/
    @Override
    public boolean updateIng2Fail(String payOrderId, String channelOrderNo, String channelUserId, String channelErrCode, String channelErrMsg){

        TPayOrder updateRecord = new TPayOrder();
        updateRecord.setState(IOrderEnums.OrderStateEnums.STATE_FAIL.getState());
        updateRecord.setErrCode(channelErrCode);
        updateRecord.setErrMsg(channelErrMsg);
        updateRecord.setChannelOrderNo(channelOrderNo);
        updateRecord.setChannelUser(channelUserId);

        return update(updateRecord, new LambdaUpdateWrapper<TPayOrder>()
                .eq(TPayOrder::getPayOrderId, payOrderId).eq(TPayOrder::getState, IOrderEnums.OrderStateEnums.STATE_ING.getState()));
    }


    /** 更新订单状态  【支付中】 --》 【支付成功/支付失败】 **/
    @Override
    public boolean updateIng2SuccessOrFail(String payOrderId, Integer updateState, String channelOrderNo, String channelUserId, String channelErrCode, String channelErrMsg){

        if(updateState == IOrderEnums.OrderStateEnums.STATE_ING.getState()){
            return true;
        }else if(updateState == IOrderEnums.OrderStateEnums.STATE_SUCCESS.getState()){
            return updateIng2Success(payOrderId, channelOrderNo, channelUserId);
        }else if(updateState == IOrderEnums.OrderStateEnums.STATE_FAIL.getState()){
            return updateIng2Fail(payOrderId, channelOrderNo, channelUserId, channelErrCode, channelErrMsg);
        }
        return false;
    }

    /** 查询商户订单 **/
    @Override
    public TPayOrder queryMchOrder(String mchNo, String payOrderId, String mchOrderNo){

        if(StringUtils.isNotEmpty(payOrderId)){
            return getOne(new LambdaQueryWrapper<TPayOrder>().eq(TPayOrder::getMchNo, mchNo).eq(TPayOrder::getPayOrderId, payOrderId));
        }else if(StringUtils.isNotEmpty(mchOrderNo)){
            return getOne(new LambdaQueryWrapper<TPayOrder>().eq(TPayOrder::getMchNo, mchNo).eq(TPayOrder::getMchOrderNo, mchOrderNo));
        }else{
            return null;
        }
    }

    @Override
    public Map payCount(String mchNo, Integer state, Integer refundState, String dayStart, String dayEnd) {
        TPayOrderQueryBo bo = new TPayOrderQueryBo().setState(state).setRefundState(refundState).
                setMchNo(mchNo).setCreateTimeStart(dayStart).setCreateTimeEnd(dayEnd);
        return payOrderMapperEx.payCount(bo);
    }

    @Override
    public List<Map> payTypeCount(String mchNo, Integer state, Integer refundState, String dayStart, String dayEnd) {
        TPayOrderQueryBo bo = new TPayOrderQueryBo().setState(state).setRefundState(refundState).
                setMchNo(mchNo).setCreateTimeStart(dayStart).setCreateTimeEnd(dayEnd);
        return payOrderMapperEx.payTypeCount(bo);
    }

    /** 更新订单为 超时状态 **/
    @Override
    public Integer updateOrderExpired(){

        TPayOrder payOrder = new TPayOrder();
        payOrder.setState(IOrderEnums.OrderStateEnums.STATE_CLOSED.getState());

        return baseMapper.update(payOrder,
                new LambdaQueryWrapper<TPayOrder>()
                        .in(TPayOrder::getState, Arrays.asList(IOrderEnums.OrderStateEnums.STATE_INIT.getState(),
                                IOrderEnums.OrderStateEnums.STATE_ING.getState()))
                        .le(TPayOrder::getExpiredTime, new Date())
        );
    }

    /** 更新订单 通知状态 --> 已发送 **/
    @Override
    public int updateNotifySent(String payOrderId){
        TPayOrder payOrder = new TPayOrder();
        payOrder.setNotifyState(CS.YES);
        payOrder.setPayOrderId(payOrderId);
        return baseMapper.updateById(payOrder);
    }

    /** 首页支付周统计 **/
    @Override
    public JSONObject mainPageWeekCount(String mchNo) {
        JSONObject json = new JSONObject();
        Map dayAmount = new LinkedHashMap();
        ArrayList array = new ArrayList<>();
        BigDecimal payAmount = new BigDecimal(0);    // 当日金额
        BigDecimal payWeek  = payAmount;   // 周总收益
        String todayAmount = "0.00";    // 今日金额
        String todayPayCount = "0";    // 今日交易笔数
        String yesterdayAmount = "0.00";    // 昨日金额
        Date today = new Date();
        for(int i = 0 ; i < 7 ; i++){
            Date date = DateUtil.offsetDay(today, -i).toJdkDate();
            String dayStart = DateUtil.beginOfDay(date).toString(DatePattern.NORM_DATETIME_MINUTE_PATTERN);
            String dayEnd = DateUtil.endOfDay(date).toString(DatePattern.NORM_DATETIME_MINUTE_PATTERN);
            // 每日交易金额查询
            dayAmount = payCount(mchNo, IOrderEnums.OrderStateEnums.STATE_SUCCESS.getState(), null, dayStart, dayEnd);
            if (dayAmount != null) {
                payAmount = new BigDecimal(dayAmount.get("payAmount").toString());
            }
            if (i == 0) {
                todayAmount = dayAmount.get("payAmount").toString();
                todayPayCount = dayAmount.get("payCount").toString();
            }
            if (i == 1) {
                yesterdayAmount = dayAmount.get("payAmount").toString();
            }
            payWeek = payWeek.add(payAmount);
            array.add(payAmount);
        }

        // 倒序排列
        Collections.reverse(array);
        json.put("dataArray", array);
        json.put("todayAmount", todayAmount);
        json.put("todayPayCount", todayPayCount);
        json.put("payWeek", payWeek);
        json.put("yesterdayAmount", yesterdayAmount);
        return json;
    }

    /** 首页统计总数量 **/
    @Override
    public JSONObject mainPageNumCount(String mchNo) {
        JSONObject json = new JSONObject();
        // 商户总数
        long mchCount = mchInfoMapper.selectCount(new LambdaQueryWrapper<TMchInfo>());
        // 服务商总数
        long isvCount = isvInfoMapper.selectCount(new LambdaQueryWrapper<TMchIsvInfo>());
        // 总交易金额
        Map<String, String> payCountMap = payCount(mchNo, IOrderEnums.OrderStateEnums.STATE_SUCCESS.getState(), null, null, null);
        json.put("totalMch", mchCount);
        json.put("totalIsv", isvCount);
        json.put("totalAmount", payCountMap.get("payAmount"));
        json.put("totalCount", payCountMap.get("payCount"));
        return json;
    }

    /** 首页支付统计 **/
    @Override
    public List<Map> mainPagePayCount(String mchNo, String createdStart, String createdEnd) {
        Map param = new HashMap<>(); // 条件参数
        int daySpace = 6; // 默认最近七天（含当天）
        if (StringUtils.isNotEmpty(createdStart) && StringUtils.isNotEmpty(createdEnd)) {
            createdStart = createdStart + " 00:00:00";
            createdEnd = createdEnd + " 23:59:59";
            // 计算两时间间隔天数
            daySpace = Math.toIntExact(DateUtil.betweenDay(DateUtil.parseDate(createdStart), DateUtil.parseDate(createdEnd), true));
        } else {
            Date today = new Date();
            createdStart = DateUtil.formatDate(DateUtil.offsetDay(today, -daySpace)) + " 00:00:00";
            createdEnd = DateUtil.formatDate(today) + " 23:59:59";
        }
        TPayOrderQueryBo bo = new TPayOrderQueryBo().
                setMchNo(mchNo).setCreateTimeStart(createdStart).setCreateTimeEnd(createdEnd);
        // 查询收款的记录
        List<Map> payOrderList = payOrderMapperEx.selectOrderCount(bo);
        // 查询退款的记录
        List<Map> refundOrderList = payOrderMapperEx.selectOrderCount(bo);
        // 生成前端返回参数类型
        List<Map> returnList = getReturnList(daySpace, createdEnd, payOrderList, refundOrderList);
        return returnList;
    }

    /** 首页支付类型统计 **/
    @Override
    public ArrayList mainPagePayTypeCount(String mchNo, String createdStart, String createdEnd) {
        // 返回数据列
        ArrayList array = new ArrayList<>();
        if (StringUtils.isNotEmpty(createdStart) && StringUtils.isNotEmpty(createdEnd)) {
            createdStart = createdStart + " 00:00:00";
            createdEnd = createdEnd + " 23:59:59";
        }else {
            Date endDay = new Date();    // 当前日期
            Date startDay = DateUtil.lastWeek().toJdkDate(); // 一周前日期
            String end = DateUtil.formatDate(endDay);
            String start = DateUtil.formatDate(startDay);
            createdStart = start + " 00:00:00";
            createdEnd = end + " 23:59:59";
        }
        // 统计列表
        List<Map> payCountMap = payTypeCount(mchNo, IOrderEnums.OrderStateEnums.STATE_SUCCESS.getState(), null, createdStart, createdEnd);

        // 得到所有支付方式
        Map<String, String> payWayNameMap = new HashMap<>();
        List<TPayWay> payWayList = payWayMapper.selectList(new LambdaQueryWrapper<TPayWay>());
        for (TPayWay payWay:payWayList) {
            payWayNameMap.put(payWay.getWayCode(), payWay.getWayName());
        }
        // 支付方式名称标注
        for (Map payCount:payCountMap) {
            if (StringUtils.isNotEmpty(payWayNameMap.get(payCount.get("wayCode")))) {
                payCount.put("typeName", payWayNameMap.get(payCount.get("wayCode")));
            }else {
                payCount.put("typeName", payCount.get("wayCode"));
            }
        }
        array.add(payCountMap);
        return array;
    }

    /** 生成首页交易统计数据类型 **/
    @Override
    public List<Map> getReturnList(int daySpace, String createdStart, List<Map> payOrderList, List<Map> refundOrderList) {
        List<Map> dayList = new ArrayList<>();
        DateTime endDay = DateUtil.parseDateTime(createdStart);
        // 先判断间隔天数 根据天数设置空的list
        for (int i = 0; i <= daySpace ; i++) {
            Map<String, String> map = new HashMap<>();
            map.put("date", DateUtil.format(DateUtil.offsetDay(endDay, -i), "MM-dd"));
            dayList.add(map);
        }
        // 日期倒序排列
        Collections.reverse(dayList);

        List<Map> payListMap = new ArrayList<>(); // 收款的列
        List<Map> refundListMap = new ArrayList<>(); // 退款的列
        for (Map dayMap:dayList) {
            // 为收款列和退款列赋值默认参数【payAmount字段切记不可为string，否则前端图表解析不出来】
            Map<String, Object> payMap = new HashMap<>();
            payMap.put("date", dayMap.get("date").toString());
            payMap.put("type", "收款");
            payMap.put("payAmount", 0);

            Map<String, Object> refundMap = new HashMap<>();
            refundMap.put("date", dayMap.get("date").toString());
            refundMap.put("type", "退款");
            refundMap.put("payAmount", 0);
            for (Map payOrderMap:payOrderList) {
                if (dayMap.get("date").equals(payOrderMap.get("groupDate"))) {
                    payMap.put("payAmount", payOrderMap.get("payAmount"));
                }
            }
            payListMap.add(payMap);
            for (Map refundOrderMap:refundOrderList) {
                if (dayMap.get("date").equals(refundOrderMap.get("groupDate"))) {
                    refundMap.put("payAmount", refundOrderMap.get("refundAmount"));
                }
            }
            refundListMap.add(refundMap);
        }
        payListMap.addAll(refundListMap);
        return payListMap;
    }


    /**
     *  计算支付订单商家入账金额
     * 商家订单入账金额 （支付金额 - 手续费 - 退款金额 - 总分账金额）
     * @author terrfly
     * @site https://www.jeequan.com
     * @date 2021/8/26 16:39
     */
    @Override
    public Long calMchIncomeAmount(TPayOrder dbPayOrder){

        //商家订单入账金额 （支付金额 - 手续费 - 退款金额 - 总分账金额）
        Long mchIncomeAmount = dbPayOrder.getAmount() - dbPayOrder.getMchFeeAmount() - dbPayOrder.getRefundAmount();

        //减去已分账金额
        mchIncomeAmount -= divisionRecordMapperEx.sumSuccessDivisionAmount(dbPayOrder.getPayOrderId());

        return mchIncomeAmount <= 0 ? 0 : mchIncomeAmount;

    }

    /**
     * 通用列表查询条件
     * @param iPage
     * @param payOrder
     * @param paramJSON
     * @param wrapper
     * @return
     */
    @Override
    public TableDataInfo<TPayOrder> listByPage(IPage iPage, TPayOrder payOrder, JSONObject paramJSON, LambdaQueryWrapper<TPayOrder> wrapper) {
        if (StringUtils.isNotEmpty(payOrder.getPayOrderId())) {
            wrapper.eq(TPayOrder::getPayOrderId, payOrder.getPayOrderId());
        }
        if (StringUtils.isNotEmpty(payOrder.getMchNo())) {
            wrapper.eq(TPayOrder::getMchNo, payOrder.getMchNo());
        }
        if (StringUtils.isNotEmpty(payOrder.getIsvNo())) {
            wrapper.eq(TPayOrder::getIsvNo, payOrder.getIsvNo());
        }
        if (payOrder.getMchType() != null) {
            wrapper.eq(TPayOrder::getMchType, payOrder.getMchType());
        }
        if (StringUtils.isNotEmpty(payOrder.getWayCode())) {
            wrapper.eq(TPayOrder::getWayCode, payOrder.getWayCode());
        }
        if (StringUtils.isNotEmpty(payOrder.getMchOrderNo())) {
            wrapper.eq(TPayOrder::getMchOrderNo, payOrder.getMchOrderNo());
        }
        if (payOrder.getState() != null) {
            wrapper.eq(TPayOrder::getState, payOrder.getState());
        }
        if (payOrder.getNotifyState() != null) {
            wrapper.eq(TPayOrder::getNotifyState, payOrder.getNotifyState());
        }
        if (StringUtils.isNotEmpty(payOrder.getAppId())) {
            wrapper.eq(TPayOrder::getAppId, payOrder.getAppId());
        }
        if (payOrder.getDivisionState() != null) {
            wrapper.eq(TPayOrder::getDivisionState, payOrder.getDivisionState());
        }
        if (paramJSON != null) {
            if (StringUtils.isNotEmpty(paramJSON.getString("createdStart"))) {
                wrapper.ge(TPayOrder::getCreatedAt, paramJSON.getString("createdStart"));
            }
            if (StringUtils.isNotEmpty(paramJSON.getString("createdEnd"))) {
                wrapper.le(TPayOrder::getCreatedAt, paramJSON.getString("createdEnd"));
            }
        }
        // 三合一订单
        if (paramJSON != null && StrUtil.isNotBlank(paramJSON.getString("unionOrderId"))) {
            wrapper.and(wr -> {
                wr.eq(TPayOrder::getPayOrderId, paramJSON.getString("unionOrderId"))
                        .or().eq(TPayOrder::getMchOrderNo, paramJSON.getString("unionOrderId"))
                        .or().eq(TPayOrder::getChannelOrderNo, paramJSON.getString("unionOrderId"));
            });
        }

        wrapper.orderByDesc(TPayOrder::getCreatedAt);
        PagePlus<TPayOrder, TPayOrder> result = page(PageUtils.buildPagePlus(), wrapper);
        return PageUtils.buildDataInfo(result);

    }

}

