package com.piggy.pay.logic.mch.controller;

import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import lombok.RequiredArgsConstructor;

import javax.annotation.Resource;
import javax.validation.constraints.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.piggy.pay.logic.mch.vo.TMchDivisionReceiverGroupVo;
import com.piggy.pay.logic.mch.bo.TMchDivisionReceiverGroupQueryBo;
import com.piggy.pay.logic.mch.bo.TMchDivisionReceiverGroupEditBo;
import com.piggy.pay.logic.mch.service.ITMchDivisionReceiverGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.lang.Integer;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
 * 分账账号组Controller
 *
 * @author piggy
 * @date 2023-05-25
 */
@Api(value = "分账账号组控制器", tags = {"分账账号组管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/mch/group")
public class TMchDivisionReceiverGroupController extends BaseController {

    private final ITMchDivisionReceiverGroupService iTMchDivisionReceiverGroupService;

    /**
     * 查询分账账号组列表
     */
    @ApiOperation("查询分账账号组列表")
    @SaCheckPermission("mch:group:list")
    @GetMapping("/list")
    public TableDataInfo<TMchDivisionReceiverGroupVo> list(@Validated TMchDivisionReceiverGroupQueryBo bo) {
        return iTMchDivisionReceiverGroupService.queryPageList(bo);
    }

    /**
     * 导出分账账号组列表
     */
    @ApiOperation("导出分账账号组列表")
    @SaCheckPermission("mch:group:export")
    @Log(title = "分账账号组", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TMchDivisionReceiverGroupQueryBo bo, HttpServletResponse response) throws IOException {
        List<TMchDivisionReceiverGroupVo> list = iTMchDivisionReceiverGroupService.queryList(bo);
        ExcelUtil<TMchDivisionReceiverGroupVo> util = new ExcelUtil<TMchDivisionReceiverGroupVo>(TMchDivisionReceiverGroupVo.class);
        util.exportExcel(response, list, "分账账号组");
    }

    /**
     * 获取分账账号组详细信息
     */
    @ApiOperation("获取分账账号组详细信息")
    @SaCheckPermission("mch:group:query")
    @GetMapping("/{receiverGroupId}")
    public R<TMchDivisionReceiverGroupVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("receiverGroupId") Long receiverGroupId) {
        return R.ok(iTMchDivisionReceiverGroupService.queryById(receiverGroupId));
    }

    /**
     * 新增分账账号组
     */
    @ApiOperation("新增分账账号组")
    @SaCheckPermission("mch:group:add")
    @Log(title = "分账账号组", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TMchDivisionReceiverGroupEditBo bo) {
        return R.ok(iTMchDivisionReceiverGroupService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改分账账号组
     */
    @ApiOperation("修改分账账号组")
    @SaCheckPermission("mch:group:edit")
    @Log(title = "分账账号组", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TMchDivisionReceiverGroupEditBo bo) {
        return R.ok(iTMchDivisionReceiverGroupService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除分账账号组
     */
    @ApiOperation("删除分账账号组")
    @SaCheckPermission("mch:group:remove")
    @Log(title = "分账账号组" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{receiverGroupIds}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable Long[] receiverGroupIds) {
        return R.ok(iTMchDivisionReceiverGroupService.deleteWithValidByIds(Arrays.asList(receiverGroupIds), true) ? 1 : 0);
    }
}
