package com.piggy.pay.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public interface IOrderEnums {

    @Getter
    @AllArgsConstructor
    enum OrderStateEnums {
        STATE_INIT(0, "订单生成"),
        STATE_ING(1, "支付中"),
        STATE_SUCCESS(2, "支付成功"),
        STATE_FAIL(3, "支付失败"),
        STATE_CANCEL(4, "已撤销"),
        STATE_REFUND(5, "已退款"),
        STATE_CLOSED(6, "订单关闭"),

        ;

        private int state;
        private String msg;

    }

    @Getter
    @AllArgsConstructor
    enum RefundStateEnums {
        REFUND_STATE_NONE(0, "未发生实际退款"),
        REFUND_STATE_SUB(1, "部分退款"),
        REFUND_STATE_ALL(2, "全额退款"),

        ;

        private int state;
        private String msg;

    }
}
