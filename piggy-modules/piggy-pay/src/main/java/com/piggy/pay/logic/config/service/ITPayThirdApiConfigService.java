package com.piggy.pay.logic.config.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.config.domain.TPayThirdApiConfig;
import com.piggy.pay.logic.config.vo.TPayThirdApiConfigVo;
import com.piggy.pay.logic.config.bo.TPayThirdApiConfigQueryBo;
import com.piggy.pay.logic.config.bo.TPayThirdApiConfigEditBo;

import java.util.Collection;
import java.util.List;

/**
 * 支付接口配置参数Service接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface ITPayThirdApiConfigService extends IServicePlus<TPayThirdApiConfig> {
    /**
     * 查询单个
     *
     * @return
     */
    TPayThirdApiConfigVo queryById(Long id);

    /**
     * 查询列表
     */
    TableDataInfo<TPayThirdApiConfigVo> queryPageList(TPayThirdApiConfigQueryBo bo);

    /**
     * 查询列表
     */
    List<TPayThirdApiConfigVo> queryList(TPayThirdApiConfigQueryBo bo);

    /**
     * 根据新增业务对象插入支付接口配置参数
     *
     * @param bo 支付接口配置参数新增业务对象
     * @return
     */
    Boolean insertByAddBo(TPayThirdApiConfigEditBo bo);

    /**
     * 根据编辑业务对象修改支付接口配置参数
     *
     * @param bo 支付接口配置参数编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TPayThirdApiConfigEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    TPayThirdApiConfig getByInfoIdAndIfCode(Integer infoType, String infoId, String ifCode);

    boolean mchAppHasAvailableIfCode(String appId, String ifCode);
}
