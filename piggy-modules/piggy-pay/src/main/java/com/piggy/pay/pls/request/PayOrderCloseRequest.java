package com.piggy.pay.pls.request;

import com.piggy.pay.pls.BasePay;
import com.piggy.pay.pls.model.PayOrderCloseReqModel;
import com.piggy.pay.pls.net.RequestOptions;
import com.piggy.pay.pls.response.PayOrderCloseResponse;

/**
 * Jeepay支付 订单关闭请求实现
 *
 * @author xiaoyu
 * @site https://www.jeequan.com
 * @date 2022/1/25 9:56
 */
public class PayOrderCloseRequest implements PayRequest<PayOrderCloseResponse, PayOrderCloseReqModel> {

    private String apiVersion = BasePay.VERSION;
    private String apiUri = "api/pay/close";
    private RequestOptions options;
    private PayOrderCloseReqModel bizModel = null;

    @Override
    public String getApiUri() {
        return this.apiUri;
    }

    @Override
    public String getApiVersion() {
        return this.apiVersion;
    }

    @Override
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    @Override
    public RequestOptions getRequestOptions() {
        return this.options;
    }

    @Override
    public void setRequestOptions(RequestOptions options) {
        this.options = options;
    }

    @Override
    public PayOrderCloseReqModel getBizModel() {
        return this.bizModel;
    }

    @Override
    public void setBizModel(PayOrderCloseReqModel bizModel) {
        this.bizModel = bizModel;
    }

    @Override
    public Class<PayOrderCloseResponse> getResponseClass() {
        return PayOrderCloseResponse.class;
    }

}
