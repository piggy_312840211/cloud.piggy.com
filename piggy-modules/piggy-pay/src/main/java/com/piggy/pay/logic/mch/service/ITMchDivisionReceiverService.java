package com.piggy.pay.logic.mch.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.mch.domain.TMchDivisionReceiver;
import com.piggy.pay.logic.mch.vo.TMchDivisionReceiverVo;
import com.piggy.pay.logic.mch.bo.TMchDivisionReceiverQueryBo;
import com.piggy.pay.logic.mch.bo.TMchDivisionReceiverEditBo;

import java.util.Collection;
import java.util.List;

/**
 * 商户分账接收者账号绑定关系Service接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface ITMchDivisionReceiverService extends IServicePlus<TMchDivisionReceiver> {
    /**
     * 查询单个
     *
     * @return
     */
    TMchDivisionReceiverVo queryById(Long receiverId);

    /**
     * 查询列表
     */
    TableDataInfo<TMchDivisionReceiverVo> queryPageList(TMchDivisionReceiverQueryBo bo);

    /**
     * 查询列表
     */
    List<TMchDivisionReceiverVo> queryList(TMchDivisionReceiverQueryBo bo);

    /**
     * 根据新增业务对象插入商户分账接收者账号绑定关系
     *
     * @param bo 商户分账接收者账号绑定关系新增业务对象
     * @return
     */
    Boolean insertByAddBo(TMchDivisionReceiverEditBo bo);

    /**
     * 根据编辑业务对象修改商户分账接收者账号绑定关系
     *
     * @param bo 商户分账接收者账号绑定关系编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TMchDivisionReceiverEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
