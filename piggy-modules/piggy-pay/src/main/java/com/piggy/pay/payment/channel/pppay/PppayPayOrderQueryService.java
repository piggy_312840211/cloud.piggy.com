package com.piggy.pay.payment.channel.pppay;

import com.piggy.pay.core.constants.CS;
import com.piggy.pay.logic.order.domain.TPayOrder;
import com.piggy.pay.payment.channel.IPayOrderQueryService;
import com.piggy.pay.payment.model.MchAppConfigContext;
import com.piggy.pay.payment.rqrs.msg.ChannelRetMsg;
import com.piggy.pay.payment.service.ConfigContextQueryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * none.
 *
 * @author 陈泉
 * @package com.jeequan.jeepay.pay.channel.pppay
 * @create 2021/11/15 21:02
 */
@Service
public class PppayPayOrderQueryService implements IPayOrderQueryService {

    @Override
    public String getIfCode() {
        return CS.IF_CODE.PPPAY;
    }

    @Resource
    private ConfigContextQueryService configContextQueryService;

    @Override
    public ChannelRetMsg query(TPayOrder payOrder, MchAppConfigContext mchAppConfigContext) throws Exception {
        return configContextQueryService.getPaypalWrapper(mchAppConfigContext).processOrder(null, payOrder);
    }
}
