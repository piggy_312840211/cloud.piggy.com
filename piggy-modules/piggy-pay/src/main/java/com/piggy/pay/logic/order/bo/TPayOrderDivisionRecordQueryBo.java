package com.piggy.pay.logic.order.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 分账记录分页查询对象 t_pay_order_division_record
 *
 * @author zito
 * @date 2023-05-25
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel("分账记录分页查询对象")
public class TPayOrderDivisionRecordQueryBo extends BaseEntity {

	/** 分账记录ID */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("分账记录ID")
	private Long recordId;
	/** 商户号 */
	@ApiModelProperty("商户号")
	private String mchNo;
	/** 服务商号 */
	@ApiModelProperty("服务商号")
	private String isvNo;
	/** 应用ID */
	@ApiModelProperty("应用ID")
	private String appId;
	/** 商户名称 */
	@ApiModelProperty("商户名称")
	private String mchName;
	/** 类型: 1-普通商户, 2-特约商户(服务商模式) */
	@ApiModelProperty("类型: 1-普通商户, 2-特约商户(服务商模式)")
	private Integer mchType;
	/** 支付接口代码 */
	@ApiModelProperty("支付接口代码")
	private String ifCode;
	/** 系统支付订单号 */
	@ApiModelProperty("系统支付订单号")
	private String payOrderId;
	/** 支付订单渠道支付订单号 */
	@ApiModelProperty("支付订单渠道支付订单号")
	private String payOrderChannelOrderNo;
	/** 订单金额,单位分 */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("订单金额,单位分")
	private Long payOrderAmount;
	/** 订单实际分账金额, 单位：分（订单金额 - 商户手续费 - 已退款金额） */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("订单实际分账金额, 单位：分（订单金额 - 商户手续费 - 已退款金额）")
	private Long payOrderDivisionAmount;
	/** 系统分账批次号 */
	@ApiModelProperty("系统分账批次号")
	private String batchOrderId;
	/** 上游分账批次号 */
	@ApiModelProperty("上游分账批次号")
	private String channelBatchOrderId;
	/** 状态: 0-待分账 1-分账成功（明确成功）, 2-分账失败（明确失败）, 3-分账已受理（上游受理） */
	@ApiModelProperty("状态: 0-待分账 1-分账成功（明确成功）, 2-分账失败（明确失败）, 3-分账已受理（上游受理）")
	private Integer state;
	/** 上游返回数据包 */
	@ApiModelProperty("上游返回数据包")
	private String channelRespResult;
	/** 账号快照》 分账接收者ID */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("账号快照》 分账接收者ID")
	private Long receiverId;
	/** 账号快照》 组ID（便于商户接口使用） */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("账号快照》 组ID（便于商户接口使用）")
	private Long receiverGroupId;
	/** 接收者账号别名 */
	@ApiModelProperty("接收者账号别名")
	private String receiverAlias;
	/** 账号快照》 分账接收账号类型: 0-个人 1-商户 */
	@ApiModelProperty("账号快照》 分账接收账号类型: 0-个人 1-商户")
	private Integer accType;
	/** 账号快照》 分账接收账号 */
	@ApiModelProperty("账号快照》 分账接收账号")
	private String accNo;
	/** 账号快照》 分账接收账号名称 */
	@ApiModelProperty("账号快照》 分账接收账号名称")
	private String accName;
	/** 账号快照》 分账关系类型（参考微信）， 如： SERVICE_PROVIDER 服务商等 */
	@ApiModelProperty("账号快照》 分账关系类型（参考微信）， 如： SERVICE_PROVIDER 服务商等")
	private String relationType;
	/** 账号快照》 当选择自定义时，需要录入该字段。 否则为对应的名称 */
	@ApiModelProperty("账号快照》 当选择自定义时，需要录入该字段。 否则为对应的名称")
	private String relationTypeName;
	/** 账号快照》 配置的实际分账比例 */
	@ApiModelProperty("账号快照》 配置的实际分账比例")
	private BigDecimal divisionProfit;
	/** 计算该接收方的分账金额,单位分 */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("计算该接收方的分账金额,单位分")
	private Long calDivisionAmount;
	/** 创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;
	/** 更新时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;

}
