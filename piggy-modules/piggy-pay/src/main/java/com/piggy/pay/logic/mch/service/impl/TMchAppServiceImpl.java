package com.piggy.pay.logic.mch.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.core.constants.ApiCodeEnum;
import com.piggy.pay.core.constants.CS;
import com.piggy.pay.core.utils.StringKit;
import com.piggy.pay.logic.config.domain.TPayThirdApiConfig;
import com.piggy.pay.logic.config.service.ITPayThirdApiConfigService;
import com.piggy.pay.logic.mch.bo.TMchAppEditBo;
import com.piggy.pay.logic.mch.bo.TMchAppQueryBo;
import com.piggy.pay.logic.mch.domain.TMchApp;
import com.piggy.pay.logic.mch.domain.TMchPayPassage;
import com.piggy.pay.logic.mch.mapper.TMchAppMapper;
import com.piggy.pay.logic.mch.service.ITMchAppService;
import com.piggy.pay.logic.mch.service.ITMchPayPassageService;
import com.piggy.pay.logic.mch.vo.TMchAppVo;
import com.piggy.pay.logic.order.domain.TPayOrder;
import com.piggy.pay.logic.order.service.ITPayOrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Collection;

/**
 * 商户应用Service业务层处理
 *
 * @author piggy
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TMchAppServiceImpl extends ServiceImpl<TMchAppMapper, TMchApp> implements ITMchAppService {

    @Resource
    private ITPayOrderService payOrderService;

    @Resource
    private ITMchPayPassageService mchPayPassageService;

    @Resource
    private ITPayThirdApiConfigService payThirdApiConfigService;

    @Override
    public TMchAppVo queryById(String appId) {
        return getVoById(appId, TMchAppVo.class);
    }

    @Override
    public TableDataInfo<TMchAppVo> queryPageList(TMchAppQueryBo bo) {
        PagePlus<TMchApp, TMchAppVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TMchAppVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TMchAppVo> queryList(TMchAppQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TMchAppVo.class);
    }

    private LambdaQueryWrapper<TMchApp> buildQueryWrapper(TMchAppQueryBo bo) {
        LambdaQueryWrapper<TMchApp> lqw = Wrappers.lambdaQuery();
        lqw.eq(StrUtil.isNotBlank(bo.getAppId()), TMchApp::getAppId, bo.getAppId());
        lqw.like(StrUtil.isNotBlank(bo.getAppName()), TMchApp::getAppName, bo.getAppName());
        lqw.eq(StrUtil.isNotBlank(bo.getMchNo()), TMchApp::getMchNo, bo.getMchNo());
        lqw.eq(bo.getState() != null, TMchApp::getState, bo.getState());
        lqw.eq(StrUtil.isNotBlank(bo.getAppSecret()), TMchApp::getAppSecret, bo.getAppSecret());
        lqw.eq(bo.getCreatedUid() != null, TMchApp::getCreatedUid, bo.getCreatedUid());
        lqw.eq(StrUtil.isNotBlank(bo.getCreatedBy()), TMchApp::getCreatedBy, bo.getCreatedBy());
        lqw.eq(StrUtil.isNotBlank(bo.getUpdateBy()), TMchApp::getUpdateBy, bo.getUpdateBy());
        lqw.eq(bo.getCreatedAt() != null, TMchApp::getCreatedAt, bo.getCreatedAt());
        lqw.eq(bo.getUpdatedAt() != null, TMchApp::getUpdatedAt, bo.getUpdatedAt());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TMchAppEditBo bo) {
        TMchApp add = BeanUtil.toBean(bo, TMchApp.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TMchAppEditBo bo) {
        TMchApp update = BeanUtil.toBean(bo, TMchApp.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TMchApp entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

    @Transactional(rollbackFor = Exception.class)
    public void removeByAppId(String appId) {

        // 1.查看当前应用是否存在交易数据
        long payCount = payOrderService.count(new LambdaQueryWrapper<TPayOrder>().eq(TPayOrder::getAppId, appId));
        if (payCount > 0) {
            throw new BaseException("该应用已存在交易数据，不可删除");
        }

        // 2.删除应用关联的支付通道
        mchPayPassageService.remove(new LambdaQueryWrapper<TMchPayPassage>().eq(TMchPayPassage::getAppId, appId));

        // 3.删除应用配置的支付参数
        payThirdApiConfigService.remove(new LambdaQueryWrapper<TPayThirdApiConfig>()
                .eq(TPayThirdApiConfig::getInfoId, appId)
                .eq(TPayThirdApiConfig::getInfoType, CS.INFO_TYPE_MCH_APP)
        );

        // 4.删除当前应用
        if (!removeById(appId)) {
            throw new BaseException(ApiCodeEnum.SYS_OPERATION_FAIL_DELETE.getCode(), ApiCodeEnum.SYS_OPERATION_FAIL_DELETE.getMsg());
        }

    }

    public TMchApp selectById(String appId) {
        TMchApp mchApp = this.getById(appId);
        if (mchApp == null) {
            return null;
        }
        mchApp.setAppSecret(StringKit.str2Star(mchApp.getAppSecret(), 6, 6, 6));

        return mchApp;
    }

    public IPage<TMchApp> selectPage(IPage iPage, TMchApp mchApp) {

        LambdaQueryWrapper<TMchApp> wrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(mchApp.getMchNo())) {
            wrapper.eq(TMchApp::getMchNo, mchApp.getMchNo());
        }
        if (StringUtils.isNotEmpty(mchApp.getAppId())) {
            wrapper.eq(TMchApp::getAppId, mchApp.getAppId());
        }
        if (StringUtils.isNotEmpty(mchApp.getAppName())) {
            wrapper.eq(TMchApp::getAppName, mchApp.getAppName());
        }
        if (mchApp.getState() != null) {
            wrapper.eq(TMchApp::getState, mchApp.getState());
        }
        wrapper.orderByDesc(TMchApp::getCreatedAt);

        IPage<TMchApp> pages = this.page(iPage, wrapper);

        pages.getRecords().stream().forEach(item -> item.setAppSecret(StringKit.str2Star(item.getAppSecret(), 6, 6, 6)));

        return pages;
    }

    public TMchApp getOneByMch(String mchNo, String appId) {
        return getOne(new LambdaQueryWrapper<TMchApp>().eq(TMchApp::getMchNo, mchNo).eq(TMchApp::getAppId, appId));
    }

}
