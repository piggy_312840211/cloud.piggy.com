package com.piggy.pay.logic.config.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.config.domain.TPayWay;
import com.piggy.pay.logic.config.vo.TPayWayVo;
import com.piggy.pay.logic.config.bo.TPayWayQueryBo;
import com.piggy.pay.logic.config.bo.TPayWayEditBo;

import java.util.Collection;
import java.util.List;

/**
 * 支付方式Service接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface ITPayWayService extends IServicePlus<TPayWay> {
    /**
     * 查询单个
     *
     * @return
     */
    TPayWayVo queryById(String wayCode);

    /**
     * 查询列表
     */
    TableDataInfo<TPayWayVo> queryPageList(TPayWayQueryBo bo);

    /**
     * 查询列表
     */
    List<TPayWayVo> queryList(TPayWayQueryBo bo);

    /**
     * 根据新增业务对象插入支付方式
     *
     * @param bo 支付方式新增业务对象
     * @return
     */
    Boolean insertByAddBo(TPayWayEditBo bo);

    /**
     * 根据编辑业务对象修改支付方式
     *
     * @param bo 支付方式编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TPayWayEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);
}
