package com.piggy.pay.pls.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
* 分账渠道余额提现
*
* @author terrfly
* @site https://www.jeequan.com
* @date 2022/5/11 15:14
*/
@Data
@NoArgsConstructor
public class DivisionReceiverChannelBalanceCashoutResModel implements Serializable {

    /**
     * 分账接收者ID
     */
    private Long receiverId;

    /** 提现状态: 1-成功, 0-失败  **/
    private Byte state;

    /** 渠道返回错误代码 **/
    private String errCode;

    /** 渠道返回错误信息 **/
    private String errMsg;

}
