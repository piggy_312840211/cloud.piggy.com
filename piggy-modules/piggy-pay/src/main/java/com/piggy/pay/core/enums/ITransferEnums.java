package com.piggy.pay.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public interface ITransferEnums {

    @AllArgsConstructor
    @Getter
    public enum TransferEntry {
        ENTRY_WX_CASH("WX_CASH"),
        ENTRY_ALIPAY_CASH("ALIPAY_CASH"),
        ENTRY_BANK_CARD("BANK_CARD"),
        ;

        private String entryType;
    }

    @AllArgsConstructor
    @Getter
    public enum TransferStateEnums {
        STATE_INIT(0,"订单生成"),
        STATE_ING(1,"转账中"),
        STATE_SUCCESS(2,"转账成功"),
        STATE_FAIL(3,"转账失败"),
        STATE_CLOSED(4,"转账关闭"),

        ;

        private int state;
        private String desc;
    }

}
