package com.piggy.pay.logic.order.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 分账记录视图对象 t_pay_order_division_record
 *
 * @author zito
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("分账记录视图对象")
public class TPayOrderDivisionRecordVo {

	private static final long serialVersionUID = 1L;

	/** 分账记录ID */
	@Excel(name = "分账记录ID")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("分账记录ID")
	private Long recordId;

	/** 商户号 */
	@Excel(name = "商户号")
	@ApiModelProperty("商户号")
	private String mchNo;

	/** 服务商号 */
	@Excel(name = "服务商号")
	@ApiModelProperty("服务商号")
	private String isvNo;

	/** 应用ID */
	@Excel(name = "应用ID")
	@ApiModelProperty("应用ID")
	private String appId;

	/** 商户名称 */
	@Excel(name = "商户名称")
	@ApiModelProperty("商户名称")
	private String mchName;

	/** 类型: 1-普通商户, 2-特约商户(服务商模式) */
	@Excel(name = "类型: 1-普通商户, 2-特约商户(服务商模式)")
	@ApiModelProperty("类型: 1-普通商户, 2-特约商户(服务商模式)")
	private Integer mchType;

	/** 支付接口代码 */
	@Excel(name = "支付接口代码")
	@ApiModelProperty("支付接口代码")
	private String ifCode;

	/** 系统支付订单号 */
	@Excel(name = "系统支付订单号")
	@ApiModelProperty("系统支付订单号")
	private String payOrderId;

	/** 支付订单渠道支付订单号 */
	@Excel(name = "支付订单渠道支付订单号")
	@ApiModelProperty("支付订单渠道支付订单号")
	private String payOrderChannelOrderNo;

	/** 订单金额,单位分 */
	@Excel(name = "订单金额,单位分")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("订单金额,单位分")
	private Long payOrderAmount;

	/** 订单实际分账金额, 单位：分（订单金额 - 商户手续费 - 已退款金额） */
	@Excel(name = "订单实际分账金额, 单位：分" , readConverterExp = "订=单金额,-=,商=户手续费,-=,已=退款金额")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("订单实际分账金额, 单位：分（订单金额 - 商户手续费 - 已退款金额）")
	private Long payOrderDivisionAmount;

	/** 系统分账批次号 */
	@Excel(name = "系统分账批次号")
	@ApiModelProperty("系统分账批次号")
	private String batchOrderId;

	/** 上游分账批次号 */
	@Excel(name = "上游分账批次号")
	@ApiModelProperty("上游分账批次号")
	private String channelBatchOrderId;

	/** 状态: 0-待分账 1-分账成功（明确成功）, 2-分账失败（明确失败）, 3-分账已受理（上游受理） */
	@Excel(name = "状态: 0-待分账 1-分账成功" , readConverterExp = "明=确成功")
	@ApiModelProperty("状态: 0-待分账 1-分账成功（明确成功）, 2-分账失败（明确失败）, 3-分账已受理（上游受理）")
	private Integer state;

	/** 上游返回数据包 */
	@Excel(name = "上游返回数据包")
	@ApiModelProperty("上游返回数据包")
	private String channelRespResult;

	/** 账号快照》 分账接收者ID */
	@Excel(name = "账号快照》 分账接收者ID")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("账号快照》 分账接收者ID")
	private Long receiverId;

	/** 账号快照》 组ID（便于商户接口使用） */
	@Excel(name = "账号快照》 组ID" , readConverterExp = "便=于商户接口使用")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("账号快照》 组ID（便于商户接口使用）")
	private Long receiverGroupId;

	/** 接收者账号别名 */
	@Excel(name = "接收者账号别名")
	@ApiModelProperty("接收者账号别名")
	private String receiverAlias;

	/** 账号快照》 分账接收账号类型: 0-个人 1-商户 */
	@Excel(name = "账号快照》 分账接收账号类型: 0-个人 1-商户")
	@ApiModelProperty("账号快照》 分账接收账号类型: 0-个人 1-商户")
	private Integer accType;

	/** 账号快照》 分账接收账号 */
	@Excel(name = "账号快照》 分账接收账号")
	@ApiModelProperty("账号快照》 分账接收账号")
	private String accNo;

	/** 账号快照》 分账接收账号名称 */
	@Excel(name = "账号快照》 分账接收账号名称")
	@ApiModelProperty("账号快照》 分账接收账号名称")
	private String accName;

	/** 账号快照》 分账关系类型（参考微信）， 如： SERVICE_PROVIDER 服务商等 */
	@Excel(name = "账号快照》 分账关系类型" , readConverterExp = "参=考微信")
	@ApiModelProperty("账号快照》 分账关系类型（参考微信）， 如： SERVICE_PROVIDER 服务商等")
	private String relationType;

	/** 账号快照》 当选择自定义时，需要录入该字段。 否则为对应的名称 */
	@Excel(name = "账号快照》 当选择自定义时，需要录入该字段。 否则为对应的名称")
	@ApiModelProperty("账号快照》 当选择自定义时，需要录入该字段。 否则为对应的名称")
	private String relationTypeName;

	/** 账号快照》 配置的实际分账比例 */
	@Excel(name = "账号快照》 配置的实际分账比例")
	@ApiModelProperty("账号快照》 配置的实际分账比例")
	private BigDecimal divisionProfit;

	/** 计算该接收方的分账金额,单位分 */
	@Excel(name = "计算该接收方的分账金额,单位分")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("计算该接收方的分账金额,单位分")
	private Long calDivisionAmount;

	/** 创建时间 */
	@Excel(name = "创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;

	/** 更新时间 */
	@Excel(name = "更新时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;


}
