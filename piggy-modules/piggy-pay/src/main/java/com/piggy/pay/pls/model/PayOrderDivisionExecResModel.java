package com.piggy.pay.pls.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/***
* 分账响应结果
*
* @author terrfly
* @site https://www.jeepay.vip
* @date 2021/8/27 10:19
*/
@Data
@NoArgsConstructor
public class PayOrderDivisionExecResModel implements Serializable {

    /**
     * 分账状态 1-分账成功, 2-分账失败
     */
    private Byte state;

    /**
     * 上游分账批次号
     */
    private String channelBatchOrderId;

    /**
     * 支付渠道错误码
     */
    private String errCode;

    /**
     * 支付渠道错误信息
     */
    private String errMsg;

}
