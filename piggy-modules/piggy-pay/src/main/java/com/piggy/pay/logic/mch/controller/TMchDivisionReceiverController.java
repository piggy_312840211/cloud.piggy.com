package com.piggy.pay.logic.mch.controller;

import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import lombok.RequiredArgsConstructor;

import javax.annotation.Resource;
import javax.validation.constraints.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.piggy.pay.logic.mch.vo.TMchDivisionReceiverVo;
import com.piggy.pay.logic.mch.bo.TMchDivisionReceiverQueryBo;
import com.piggy.pay.logic.mch.bo.TMchDivisionReceiverEditBo;
import com.piggy.pay.logic.mch.service.ITMchDivisionReceiverService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.lang.Integer;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
 * 商户分账接收者账号绑定关系Controller
 *
 * @author piggy
 * @date 2023-05-25
 */
@Api(value = "商户分账接收者账号绑定关系控制器", tags = {"商户分账接收者账号绑定关系管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/mch/receiver")
public class TMchDivisionReceiverController extends BaseController {

    private final ITMchDivisionReceiverService iTMchDivisionReceiverService;

    /**
     * 查询商户分账接收者账号绑定关系列表
     */
    @ApiOperation("查询商户分账接收者账号绑定关系列表")
    @SaCheckPermission("mch:receiver:list")
    @GetMapping("/list")
    public TableDataInfo<TMchDivisionReceiverVo> list(@Validated TMchDivisionReceiverQueryBo bo) {
        return iTMchDivisionReceiverService.queryPageList(bo);
    }

    /**
     * 导出商户分账接收者账号绑定关系列表
     */
    @ApiOperation("导出商户分账接收者账号绑定关系列表")
    @SaCheckPermission("mch:receiver:export")
    @Log(title = "商户分账接收者账号绑定关系", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TMchDivisionReceiverQueryBo bo, HttpServletResponse response) throws IOException {
        List<TMchDivisionReceiverVo> list = iTMchDivisionReceiverService.queryList(bo);
        ExcelUtil<TMchDivisionReceiverVo> util = new ExcelUtil<TMchDivisionReceiverVo>(TMchDivisionReceiverVo.class);
        util.exportExcel(response, list, "商户分账接收者账号绑定关系");
    }

    /**
     * 获取商户分账接收者账号绑定关系详细信息
     */
    @ApiOperation("获取商户分账接收者账号绑定关系详细信息")
    @SaCheckPermission("mch:receiver:query")
    @GetMapping("/{receiverId}")
    public R<TMchDivisionReceiverVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("receiverId") Long receiverId) {
        return R.ok(iTMchDivisionReceiverService.queryById(receiverId));
    }

    /**
     * 新增商户分账接收者账号绑定关系
     */
    @ApiOperation("新增商户分账接收者账号绑定关系")
    @SaCheckPermission("mch:receiver:add")
    @Log(title = "商户分账接收者账号绑定关系", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TMchDivisionReceiverEditBo bo) {
        return R.ok(iTMchDivisionReceiverService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改商户分账接收者账号绑定关系
     */
    @ApiOperation("修改商户分账接收者账号绑定关系")
    @SaCheckPermission("mch:receiver:edit")
    @Log(title = "商户分账接收者账号绑定关系", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TMchDivisionReceiverEditBo bo) {
        return R.ok(iTMchDivisionReceiverService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除商户分账接收者账号绑定关系
     */
    @ApiOperation("删除商户分账接收者账号绑定关系")
    @SaCheckPermission("mch:receiver:remove")
    @Log(title = "商户分账接收者账号绑定关系" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{receiverIds}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable Long[] receiverIds) {
        return R.ok(iTMchDivisionReceiverService.deleteWithValidByIds(Arrays.asList(receiverIds), true) ? 1 : 0);
    }
}
