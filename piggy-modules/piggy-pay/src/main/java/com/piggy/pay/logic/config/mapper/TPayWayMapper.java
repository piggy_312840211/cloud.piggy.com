package com.piggy.pay.logic.config.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.config.domain.TPayWay;

/**
 * 支付方式Mapper接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface TPayWayMapper extends BaseMapperPlus<TPayWay> {

}
