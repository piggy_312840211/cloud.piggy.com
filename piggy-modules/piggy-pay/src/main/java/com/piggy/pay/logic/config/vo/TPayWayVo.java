package com.piggy.pay.logic.config.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 支付方式视图对象 t_pay_way
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("支付方式视图对象")
public class TPayWayVo {

	private static final long serialVersionUID = 1L;

	/** 支付方式代码  例如： wxpay_jsapi */
	@Excel(name = "支付方式代码  例如： wxpay_jsapi")
	@ApiModelProperty("支付方式代码  例如： wxpay_jsapi")
	private String wayCode;

	/** 支付方式名称 */
	@Excel(name = "支付方式名称")
	@ApiModelProperty("支付方式名称")
	private String wayName;

	/** 创建时间 */
	@Excel(name = "创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;

	/** 更新时间 */
	@Excel(name = "更新时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;


}
