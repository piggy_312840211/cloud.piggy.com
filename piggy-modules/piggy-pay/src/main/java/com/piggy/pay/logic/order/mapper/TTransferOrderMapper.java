package com.piggy.pay.logic.order.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.order.domain.TTransferOrder;

/**
 * 转账订单Mapper接口
 *
 * @author zito
 * @date 2023-05-25
 */
public interface TTransferOrderMapper extends BaseMapperPlus<TTransferOrder> {

}
