package com.piggy.pay.logic.config.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.config.bo.TPayThirdApiDefineEditBo;
import com.piggy.pay.logic.config.bo.TPayThirdApiDefineQueryBo;
import com.piggy.pay.logic.config.domain.TPayThirdApiDefine;
import com.piggy.pay.logic.config.mapper.TPayThirdApiDefineMapper;
import com.piggy.pay.logic.config.vo.TPayThirdApiDefineVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.piggy.pay.logic.config.service.ITPayThirdApiDefineService;

import java.util.List;
import java.util.Collection;

/**
 * 支付接口定义Service业务层处理
 *
 * @author piggy
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TPayThirdApiDefineServiceImpl extends ServiceImpl<TPayThirdApiDefineMapper, TPayThirdApiDefine> implements ITPayThirdApiDefineService {

    @Override
    public TPayThirdApiDefineVo queryById(String ifCode){
        return getVoById(ifCode, TPayThirdApiDefineVo.class);
    }

    @Override
    public TableDataInfo<TPayThirdApiDefineVo> queryPageList(TPayThirdApiDefineQueryBo bo) {
        PagePlus<TPayThirdApiDefine, TPayThirdApiDefineVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TPayThirdApiDefineVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TPayThirdApiDefineVo> queryList(TPayThirdApiDefineQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TPayThirdApiDefineVo.class);
    }

    private LambdaQueryWrapper<TPayThirdApiDefine> buildQueryWrapper(TPayThirdApiDefineQueryBo bo) {
        LambdaQueryWrapper<TPayThirdApiDefine> lqw = Wrappers.lambdaQuery();
        lqw.eq(StrUtil.isNotBlank(bo.getIfCode()), TPayThirdApiDefine::getIfCode, bo.getIfCode());
        lqw.like(StrUtil.isNotBlank(bo.getIfName()), TPayThirdApiDefine::getIfName, bo.getIfName());
        lqw.eq(bo.getIsMchMode() != null, TPayThirdApiDefine::getIsMchMode, bo.getIsMchMode());
        lqw.eq(bo.getIsIsvMode() != null, TPayThirdApiDefine::getIsIsvMode, bo.getIsIsvMode());
        lqw.eq(bo.getConfigPageType() != null, TPayThirdApiDefine::getConfigPageType, bo.getConfigPageType());
        lqw.eq(StrUtil.isNotBlank(bo.getIsvParams()), TPayThirdApiDefine::getIsvParams, bo.getIsvParams());
        lqw.eq(StrUtil.isNotBlank(bo.getIsvsubMchParams()), TPayThirdApiDefine::getIsvsubMchParams, bo.getIsvsubMchParams());
        lqw.eq(StrUtil.isNotBlank(bo.getNormalMchParams()), TPayThirdApiDefine::getNormalMchParams, bo.getNormalMchParams());
        lqw.eq(StrUtil.isNotBlank(bo.getWayCodes()), TPayThirdApiDefine::getWayCodes, bo.getWayCodes());
        lqw.eq(StrUtil.isNotBlank(bo.getIcon()), TPayThirdApiDefine::getIcon, bo.getIcon());
        lqw.eq(StrUtil.isNotBlank(bo.getBgColor()), TPayThirdApiDefine::getBgColor, bo.getBgColor());
        lqw.eq(bo.getState() != null, TPayThirdApiDefine::getState, bo.getState());
        lqw.eq(bo.getCreatedAt() != null, TPayThirdApiDefine::getCreatedAt, bo.getCreatedAt());
        lqw.eq(bo.getUpdatedAt() != null, TPayThirdApiDefine::getUpdatedAt, bo.getUpdatedAt());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TPayThirdApiDefineEditBo bo) {
        TPayThirdApiDefine add = BeanUtil.toBean(bo, TPayThirdApiDefine.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TPayThirdApiDefineEditBo bo) {
        TPayThirdApiDefine update = BeanUtil.toBean(bo, TPayThirdApiDefine.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TPayThirdApiDefine entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
