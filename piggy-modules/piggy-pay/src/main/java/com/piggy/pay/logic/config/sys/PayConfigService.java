package com.piggy.pay.logic.config.sys;

import cn.hutool.core.util.ObjectUtil;
import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.FeignResultUtils;
import com.piggy.sys.api.RemoteConfigService;
import com.piggy.sys.api.domain.SysConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

@Slf4j
@Component
public class PayConfigService {
    public static boolean IS_USE_CACHE = false;

    private final static String GROUP_KEY = "pay.config";
    private final static String MgrSite_KEY = "mgrSiteUrl";
    private final static String MchSite_KEY = "mchSiteUrl";
    private final static String PaySite_KEY = "paySiteUrl";
    private final static String CLOUD_GROUP_KEY = "cloud.config";
    private final static String CloudSite_KEY = "cloud.site.url";

    private static DBApplicationConfig dbPayConfig = null;

    @Resource
    private RemoteConfigService configService;

    public DBApplicationConfig getDBApplicationConfig() {

        if (ObjectUtil.isNotNull(dbPayConfig)) {
            return dbPayConfig;
        }

        R<Map<String, SysConfig>> configMapR = configService.getConfigByGroupKey(GROUP_KEY, SecurityConstants.INNER);
        FeignResultUtils.throwIfFailed(configMapR);

        R<Map<String, SysConfig>> configMapR1 = configService.getConfigByGroupKey(CLOUD_GROUP_KEY, SecurityConstants.INNER);
        FeignResultUtils.throwIfFailed(configMapR1);

        Map<String, SysConfig> configMap = configMapR.getData();
        Map<String, SysConfig> configMap1 = configMapR1.getData();
        if (configMap.containsKey(MchSite_KEY) &&
                configMap.containsKey(MgrSite_KEY) &&
                configMap.containsKey(PaySite_KEY) &&
                configMap1.containsKey(CloudSite_KEY)) {

            dbPayConfig = new DBApplicationConfig();
            dbPayConfig.setPaySiteUrl(configMap.get(PaySite_KEY).getConfigValue());
            dbPayConfig.setMgrSiteUrl(configMap.get(MgrSite_KEY).getConfigValue());
            dbPayConfig.setMchSiteUrl(configMap.get(MchSite_KEY).getConfigValue());
            dbPayConfig.setOssPublicSiteUrl(configMap1.get(CloudSite_KEY).getConfigKey());

        }

        return dbPayConfig;

    }
}
