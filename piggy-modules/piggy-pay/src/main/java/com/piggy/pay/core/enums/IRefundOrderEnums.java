package com.piggy.pay.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public interface IRefundOrderEnums {

    @Getter
    @AllArgsConstructor
    enum StateEnums {
        STATE_INIT(0, "订单生成"),
        STATE_ING(1, "退款中"),
        STATE_SUCCESS(2, "退款成功"),
        STATE_FAIL(3, "退款失败"),
        STATE_CLOSED(4, "退款任务关闭"),

        ;

        private int state;
        private String msg;

    }

}
