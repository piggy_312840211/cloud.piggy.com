package com.piggy.pay.logic.mch.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.mch.domain.TMchIsvInfo;
import com.piggy.pay.logic.mch.vo.TMchIsvInfoVo;
import com.piggy.pay.logic.mch.bo.TMchIsvInfoQueryBo;
import com.piggy.pay.logic.mch.bo.TMchIsvInfoEditBo;

import java.util.Collection;
import java.util.List;

/**
 * 服务商信息Service接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface ITMchIsvInfoService extends IServicePlus<TMchIsvInfo> {
    /**
     * 查询单个
     *
     * @return
     */
    TMchIsvInfoVo queryById(String isvNo);

    /**
     * 查询列表
     */
    TableDataInfo<TMchIsvInfoVo> queryPageList(TMchIsvInfoQueryBo bo);

    /**
     * 查询列表
     */
    List<TMchIsvInfoVo> queryList(TMchIsvInfoQueryBo bo);

    /**
     * 根据新增业务对象插入服务商信息
     *
     * @param bo 服务商信息新增业务对象
     * @return
     */
    Boolean insertByAddBo(TMchIsvInfoEditBo bo);

    /**
     * 根据编辑业务对象修改服务商信息
     *
     * @param bo 服务商信息编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TMchIsvInfoEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);

    void removeByIsvNo(String isvNo);

}
