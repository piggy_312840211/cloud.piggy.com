package com.piggy.pay.pls.request;

import com.piggy.pay.pls.BasePay;
import com.piggy.pay.pls.model.PayOrderCreateReqModel;
import com.piggy.pay.pls.net.RequestOptions;
import com.piggy.pay.pls.response.PayOrderCreateResponse;

/**
 * Jeepay支付下单请求实现
 * @author jmdhappy
 * @site https://www.jeepay.vip
 * @date 2021-06-08 11:00
 */
public class PayOrderCreateRequest implements PayRequest<PayOrderCreateResponse, PayOrderCreateReqModel> {

    private String apiVersion = BasePay.VERSION;
    private String apiUri = "api/pay/unifiedOrder";
    private RequestOptions options;
    private PayOrderCreateReqModel bizModel = null;

    @Override
    public String getApiUri() {
        return this.apiUri;
    }

    @Override
    public String getApiVersion() {
        return this.apiVersion;
    }

    @Override
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    @Override
    public RequestOptions getRequestOptions() {
        return this.options;
    }

    @Override
    public void setRequestOptions(RequestOptions options) {
        this.options = options;
    }

    @Override
    public PayOrderCreateReqModel getBizModel() {
        return this.bizModel;
    }

    @Override
    public void setBizModel(PayOrderCreateReqModel bizModel) {
        this.bizModel = bizModel;
    }

    @Override
    public Class<PayOrderCreateResponse> getResponseClass() {
        return PayOrderCreateResponse.class;
    }

}
