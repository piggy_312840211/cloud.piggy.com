package com.piggy.pay.pls.model;

import com.piggy.pay.pls.ApiField;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/**
 * 支付下单请求实体类
 * @author jmdhappy
 * @site https://www.jeepay.vip
 * @date 2021-06-08 11:00
 */
@Data
@NoArgsConstructor
public class PayOrderCreateReqModel implements Serializable {

    private static final long serialVersionUID = -3998573128290306948L;

    @ApiField("mchNo")
    private String mchNo;      // 商户号
    @ApiField("appId")
    private String appId;      // 应用ID
    @ApiField("mchOrderNo")
    String mchOrderNo;          // 商户订单号
    @ApiField("wayCode")
    String wayCode;             // 支付方式
    @ApiField("amount")
    Long amount;                // 支付金额
    @ApiField("currency")
    String currency;            // 货币代码，当前只支持cny
    @ApiField("clientIp")
    String clientIp;            // 客户端IP
    @ApiField("subject")
    String subject;             // 商品标题
    @ApiField("body")
    String body;                // 商品描述
    @ApiField("notifyUrl")
    String notifyUrl;           // 异步通知地址
    @ApiField("returnUrl")
    String returnUrl;           // 跳转通知地址
    @ApiField("expiredTime")
    String expiredTime;         // 订单失效时间
    @ApiField("channelExtra")
    String channelExtra;        // 特定渠道额外支付参数
    @ApiField("extParam")
    String extParam;            // 商户扩展参数
    @ApiField("divisionMode")
    private Byte divisionMode;   // 分账模式： 0-该笔订单不允许分账[默认], 1-支付成功按配置自动完成分账, 2-商户手动分账(解冻商户金额)

    @ApiField("qrcId")
    private Long qrcId;   // 码牌ID

    @ApiField("deviceType")
    private String deviceType;   // 设备类型

    @ApiField("deviceNo")
    private String deviceNo;   // 设备号，对应值参考设备类型

    @ApiField("channelBizData")
    private String channelBizData;   // 渠道特殊参数

}
