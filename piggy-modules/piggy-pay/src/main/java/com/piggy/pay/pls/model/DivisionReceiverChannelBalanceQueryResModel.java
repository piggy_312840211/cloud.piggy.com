package com.piggy.pay.pls.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/***
 * 分账渠道余额查询接口
 *
 * @author terrfly
 * @site https://www.jeequan.com
 * @date 2022/5/11 15:03
 */
@Data
@NoArgsConstructor
public class DivisionReceiverChannelBalanceQueryResModel implements Serializable {

    /**
     * 分账接收者ID
     */
    private Long receiverId;

    /**
     * 余额, 单位：分
     */
    private Long balanceAmount;

}
