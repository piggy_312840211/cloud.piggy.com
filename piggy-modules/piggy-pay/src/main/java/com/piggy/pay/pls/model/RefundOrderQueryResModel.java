package com.piggy.pay.pls.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 退款查单响应实体类
 * @author jmdhappy
 * @site https://www.jeepay.vip
 * @date 2021-06-18 10:00
 */
@Data
@NoArgsConstructor
public class RefundOrderQueryResModel implements Serializable {

    private static final long serialVersionUID = -5184554341263929245L;

    /**
     * 退款订单号（支付系统生成订单号）
     */
    private String refundOrderId;

    /**
     * 支付订单号
     */
    private String payOrderId;

    /**
     * 商户号
     */
    private String mchNo;

    /**
     * 应用ID
     */
    private String appId;

    /**
     * 商户退款单号
     */
    private String mchRefundNo;

    /**
     * 支付金额,单位分
     */
    private Long payAmount;

    /**
     * 退款金额,单位分
     */
    private Long refundAmount;

    /**
     * 三位货币代码,人民币:cny
     */
    private String currency;

    /**
     * 退款状态
     * 0-订单生成
     * 1-退款中
     * 2-退款成功
     * 3-退款失败
     * 4-退款关闭
     */
    private Byte state;

    /**
     * 渠道订单号
     */
    private String channelOrderNo;

    /**
     * 渠道错误码
     */
    private String errCode;

    /**
     * 渠道错误描述
     */
    private String errMsg;

    /**
     * 扩展参数
     */
    private String extParam;

    /**
     * 订单创建时间,13位时间戳
     */
    private Long createdAt;

    /**
     * 订单支付成功时间,13位时间戳
     */
    private Long successTime;

}
