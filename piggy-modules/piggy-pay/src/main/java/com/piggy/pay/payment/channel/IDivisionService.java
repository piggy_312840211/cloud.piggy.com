/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.piggy.pay.payment.channel;


import com.piggy.pay.logic.mch.domain.TMchDivisionReceiver;
import com.piggy.pay.logic.order.domain.TPayOrder;
import com.piggy.pay.logic.order.domain.TPayOrderDivisionRecord;
import com.piggy.pay.payment.model.MchAppConfigContext;
import com.piggy.pay.payment.rqrs.msg.ChannelRetMsg;

import java.util.HashMap;
import java.util.List;

/**
* 分账接口
*
* @author terrfly
* @site https://www.jeequan.com
* @date 2021/8/22 08:59
*/
public interface IDivisionService {

    /** 获取到接口code **/
    String getIfCode();

    /** 是否支持该分账 */
    boolean isSupport();

    /** 绑定关系 **/
    ChannelRetMsg bind(TMchDivisionReceiver mchDivisionReceiver, MchAppConfigContext mchAppConfigContext);

    /** 单次分账 （无需调用完结接口，或自动解冻商户资金)  **/
    ChannelRetMsg singleDivision(TPayOrder payOrder, List<TPayOrderDivisionRecord> recordList, MchAppConfigContext mchAppConfigContext);

    /** 查询分账结果  **/
    HashMap<Long, ChannelRetMsg> queryDivision(TPayOrder payOrder, List<TPayOrderDivisionRecord> recordList, MchAppConfigContext mchAppConfigContext);

}
