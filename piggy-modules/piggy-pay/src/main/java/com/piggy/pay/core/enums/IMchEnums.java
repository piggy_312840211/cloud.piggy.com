package com.piggy.pay.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public interface IMchEnums {

    @AllArgsConstructor
    @Getter
    enum NotifyStateEnums {
        STATE_ING( 1, "进行中"),
        STATE_SUCCESS(2, "成功"),
        STATE_FAIL(3, "失败"),

        ;

        private int state;
        private String desc;

    }

    @AllArgsConstructor
    @Getter
    enum NotifyTypeEnums {
        TYPE_PAY_ORDER( 1, "支付"),
        TYPE_REFUND_ORDER( 2, "退款"),
        TYPE_TRANSFER_ORDER( 3, "转账"),

        ;

        private int type;
        private String desc;

    }

    @Getter
    @AllArgsConstructor
    enum MchTypeEnums {

        TYPE_NORMAL(1, "普通商户"),
        TYPE_ISVSUB(2,"特约商户"),
        ;

        private int type;
        private String desc;

    }
}
