package com.piggy.pay.logic.config.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import com.piggy.pay.logic.config.bo.TPayThirdApiDefineEditBo;
import com.piggy.pay.logic.config.bo.TPayThirdApiDefineQueryBo;
import com.piggy.pay.logic.config.service.ITPayThirdApiDefineService;
import com.piggy.pay.logic.config.vo.TPayThirdApiDefineVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 支付接口定义Controller
 *
 * @author piggy
 * @date 2023-05-25
 */
@Api(value = "支付接口定义控制器", tags = {"支付接口定义管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/config/apidefine")
public class TPayThirdApiDefineController extends BaseController {

    private final ITPayThirdApiDefineService iTPayThirdApiDefineService;

    /**
     * 查询支付接口定义列表
     */
    @ApiOperation("查询支付接口定义列表")
    @SaCheckPermission("config:apidefine:list")
    @GetMapping("/list")
    public TableDataInfo<TPayThirdApiDefineVo> list(@Validated TPayThirdApiDefineQueryBo bo) {
        return iTPayThirdApiDefineService.queryPageList(bo);
    }

    /**
     * 导出支付接口定义列表
     */
    @ApiOperation("导出支付接口定义列表")
    @SaCheckPermission("config:apidefine:export")
    @Log(title = "支付接口定义", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TPayThirdApiDefineQueryBo bo, HttpServletResponse response) throws IOException {
        List<TPayThirdApiDefineVo> list = iTPayThirdApiDefineService.queryList(bo);
        ExcelUtil<TPayThirdApiDefineVo> util = new ExcelUtil<TPayThirdApiDefineVo>(TPayThirdApiDefineVo.class);
        util.exportExcel(response, list, "支付接口定义");
    }

    /**
     * 获取支付接口定义详细信息
     */
    @ApiOperation("获取支付接口定义详细信息")
    @SaCheckPermission("config:apidefine:query")
    @GetMapping("/{ifCode}")
    public R<TPayThirdApiDefineVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("ifCode") String ifCode) {
        return R.ok(iTPayThirdApiDefineService.queryById(ifCode));
    }

    /**
     * 新增支付接口定义
     */
    @ApiOperation("新增支付接口定义")
    @SaCheckPermission("config:apidefine:add")
    @Log(title = "支付接口定义", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TPayThirdApiDefineEditBo bo) {
        return R.ok(iTPayThirdApiDefineService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改支付接口定义
     */
    @ApiOperation("修改支付接口定义")
    @SaCheckPermission("config:apidefine:edit")
    @Log(title = "支付接口定义", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TPayThirdApiDefineEditBo bo) {
        return R.ok(iTPayThirdApiDefineService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除支付接口定义
     */
    @ApiOperation("删除支付接口定义")
    @SaCheckPermission("config:apidefine:remove")
    @Log(title = "支付接口定义" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ifCodes}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable String[] ifCodes) {
        return R.ok(iTPayThirdApiDefineService.deleteWithValidByIds(Arrays.asList(ifCodes), true) ? 1 : 0);
    }
}
