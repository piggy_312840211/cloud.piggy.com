package com.piggy.pay.logic.mch.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.mch.domain.TMchApp;

/**
 * 商户应用Mapper接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface TMchAppMapper extends BaseMapperPlus<TMchApp> {

}
