package com.piggy.pay.logic.order.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.order.bo.TPayOrderQueryBo;
import com.piggy.pay.logic.order.domain.TPayOrder;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 支付订单Mapper接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface TPayOrderMapperEx extends BaseMapperPlus<TPayOrder> {

    @MapKey("payAmount")
    Map payCount(TPayOrderQueryBo bo);

    @MapKey("typeCount")
    List<Map> payTypeCount(TPayOrderQueryBo bo);

    @MapKey("groupDate")
    List<Map> selectOrderCount(TPayOrderQueryBo bo);

    /** 更新订单退款金额和次数 **/
    int updateRefundAmountAndCount(@Param("payOrderId") String payOrderId, @Param("currentRefundAmount") Long currentRefundAmount);

}
