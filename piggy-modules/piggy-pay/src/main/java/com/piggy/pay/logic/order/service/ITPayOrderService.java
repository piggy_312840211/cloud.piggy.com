package com.piggy.pay.logic.order.service;


import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.core.constants.CS;
import com.piggy.pay.core.enums.IOrderEnums;
import com.piggy.pay.logic.config.domain.TPayWay;
import com.piggy.pay.logic.mch.domain.TMchInfo;
import com.piggy.pay.logic.mch.domain.TMchIsvInfo;
import com.piggy.pay.logic.order.domain.TPayOrder;
import com.piggy.pay.logic.order.vo.TPayOrderVo;
import com.piggy.pay.logic.order.bo.TPayOrderQueryBo;
import com.piggy.pay.logic.order.bo.TPayOrderEditBo;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * 支付订单Service接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface ITPayOrderService extends IServicePlus<TPayOrder> {
    /**
     * 查询单个
     *
     * @return
     */
    TPayOrderVo queryById(String payOrderId);

    /**
     * 查询列表
     */
    TableDataInfo<TPayOrderVo> queryPageList(TPayOrderQueryBo bo);

    /**
     * 查询列表
     */
    List<TPayOrderVo> queryList(TPayOrderQueryBo bo);

    /**
     * 根据新增业务对象插入支付订单
     *
     * @param bo 支付订单新增业务对象
     * @return
     */
    Boolean insertByAddBo(TPayOrderEditBo bo);

    /**
     * 根据编辑业务对象修改支付订单
     *
     * @param bo 支付订单编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TPayOrderEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);

    /** 更新订单状态  【订单生成】 --》 【支付中】 **/
    boolean updateInit2Ing(String payOrderId, TPayOrder payOrder);

    /** 更新订单状态  【支付中】 --》 【支付成功】 **/
    boolean updateIng2Success(String payOrderId, String channelOrderNo, String channelUserId);

    /** 更新订单状态  【支付中】 --》 【订单关闭】 **/
    boolean updateIng2Close(String payOrderId);

    /** 更新订单状态  【订单生成】 --》 【订单关闭】 **/
    boolean updateInit2Close(String payOrderId);

    /** 更新订单状态  【支付中】 --》 【支付失败】 **/
    boolean updateIng2Fail(String payOrderId, String channelOrderNo, String channelUserId, String channelErrCode, String channelErrMsg);


    /** 更新订单状态  【支付中】 --》 【支付成功/支付失败】 **/
    boolean updateIng2SuccessOrFail(String payOrderId, Integer updateState, String channelOrderNo, String channelUserId, String channelErrCode, String channelErrMsg);

    /** 查询商户订单 **/
    TPayOrder queryMchOrder(String mchNo, String payOrderId, String mchOrderNo);

    Map payCount(String mchNo, Integer state, Integer refundState, String dayStart, String dayEnd);

    List<Map> payTypeCount(String mchNo, Integer state, Integer refundState, String dayStart, String dayEnd);

    /** 更新订单为 超时状态 **/
    Integer updateOrderExpired();

    /** 更新订单 通知状态 --> 已发送 **/
    int updateNotifySent(String payOrderId);

    /** 首页支付周统计 **/
    JSONObject mainPageWeekCount(String mchNo);

    /** 首页统计总数量 **/
    JSONObject mainPageNumCount(String mchNo);

    /** 首页支付统计 **/
    List<Map> mainPagePayCount(String mchNo, String createdStart, String createdEnd);

    /** 首页支付类型统计 **/
    ArrayList mainPagePayTypeCount(String mchNo, String createdStart, String createdEnd);

    /** 生成首页交易统计数据类型 **/
    List<Map> getReturnList(int daySpace, String createdStart, List<Map> payOrderList, List<Map> refundOrderList);

    /**
     *  计算支付订单商家入账金额
     * 商家订单入账金额 （支付金额 - 手续费 - 退款金额 - 总分账金额）
     * @author terrfly
     * @site https://www.jeequan.com
     * @date 2021/8/26 16:39
     */
    Long calMchIncomeAmount(TPayOrder dbPayOrder);

    /**
     * 通用列表查询条件
     * @param iPage
     * @param payOrder
     * @param paramJSON
     * @param wrapper
     * @return
     */
    TableDataInfo<TPayOrder> listByPage(IPage iPage, TPayOrder payOrder, JSONObject paramJSON, LambdaQueryWrapper<TPayOrder> wrapper);

}
