/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.piggy.pay.payment.ctrl.transfer;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.pay.core.model.ApiRes;
import com.piggy.pay.logic.order.domain.TTransferOrder;
import com.piggy.pay.logic.order.service.ITTransferOrderService;
import com.piggy.pay.payment.ctrl.ApiController;
import com.piggy.pay.payment.rqrs.transfer.QueryTransferOrderRQ;
import com.piggy.pay.payment.rqrs.transfer.QueryTransferOrderRS;
import com.piggy.pay.payment.service.ConfigContextQueryService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
* 商户转账单查询controller
*
* @author terrfly
* @site https://www.jeequan.com
* @date 2021/8/13 15:20
*/
@Slf4j
@RestController
public class QueryTransferOrderController extends ApiController {

    @Resource
    private ITTransferOrderService transferOrderService;
    @Resource
    private ConfigContextQueryService configContextQueryService;

    /**
     * 查单接口
     * **/
    @RequestMapping("/api/transfer/query")
    public ApiRes queryTransferOrder(){

        //获取参数 & 验签
        QueryTransferOrderRQ rq = getRQByWithMchSign(QueryTransferOrderRQ.class);

        if(StringUtils.isAllEmpty(rq.getMchOrderNo(), rq.getTransferId())){
            throw new BaseException("mchOrderNo 和 transferId不能同时为空");
        }

        TTransferOrder refundOrder = transferOrderService.queryMchOrder(rq.getMchNo(), rq.getMchOrderNo(), rq.getTransferId());
        if(refundOrder == null){
            throw new BaseException("订单不存在");
        }

        QueryTransferOrderRS bizRes = QueryTransferOrderRS.buildByRecord(refundOrder);
        return ApiRes.okWithSign(bizRes, configContextQueryService.queryMchApp(rq.getMchNo(), rq.getAppId()).getAppSecret());
    }
}
