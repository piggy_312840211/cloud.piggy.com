package com.piggy.pay.logic.mch.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.mch.domain.TMchApp;
import com.piggy.pay.logic.mch.vo.TMchAppVo;
import com.piggy.pay.logic.mch.bo.TMchAppQueryBo;
import com.piggy.pay.logic.mch.bo.TMchAppEditBo;

import java.util.Collection;
import java.util.List;

/**
 * 商户应用Service接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface ITMchAppService extends IServicePlus<TMchApp> {
    /**
     * 查询单个
     *
     * @return
     */
    TMchAppVo queryById(String appId);

    /**
     * 查询列表
     */
    TableDataInfo<TMchAppVo> queryPageList(TMchAppQueryBo bo);

    /**
     * 查询列表
     */
    List<TMchAppVo> queryList(TMchAppQueryBo bo);

    /**
     * 根据新增业务对象插入商户应用
     *
     * @param bo 商户应用新增业务对象
     * @return
     */
    Boolean insertByAddBo(TMchAppEditBo bo);

    /**
     * 根据编辑业务对象修改商户应用
     *
     * @param bo 商户应用编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TMchAppEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);

    TMchApp getOneByMch(String mchNo, String appId);
}
