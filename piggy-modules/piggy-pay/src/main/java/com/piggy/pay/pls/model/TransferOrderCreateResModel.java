package com.piggy.pay.pls.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/***
 * 转账下单响应实体类
 *
 * @author terrfly
 * @site https://www.jeepay.vip
 * @date 2021/8/13 16:08
 */
@Data
@NoArgsConstructor
public class TransferOrderCreateResModel implements Serializable {

    /**
     * 转账单号(网关生成)
     */
    private String transferId;

    /**
     * 商户发起的转账订单号
     */
    private String mchOrderNo;

    /**
     * 订单转账金额
     */
    private Long amount;

    /**
     * 收款账号
     */
    private String accountNo;

    /**
     * 收款人姓名
     */
    private String accountName;

    /**
     * 收款人开户行名称
     */
    private String bankName;


    /**
     * 转账状态
     * 0-订单生成
     * 1-转账中
     * 2-转账成功
     * 3-转账失败
     * 4-转账关闭
     */
    private Integer state;

    /**
     * 渠道转账单号
     */
    private String channelOrderNo;

    /**
     * 支付渠道错误码
     */
    private String errCode;

    /**
     * 支付渠道错误信息
     */
    private String errMsg;

}
