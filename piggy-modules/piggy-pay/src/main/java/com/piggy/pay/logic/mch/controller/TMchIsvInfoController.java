package com.piggy.pay.logic.mch.controller;

import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import lombok.RequiredArgsConstructor;

import javax.annotation.Resource;
import javax.validation.constraints.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.piggy.pay.logic.mch.vo.TMchIsvInfoVo;
import com.piggy.pay.logic.mch.bo.TMchIsvInfoQueryBo;
import com.piggy.pay.logic.mch.bo.TMchIsvInfoEditBo;
import com.piggy.pay.logic.mch.service.ITMchIsvInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.lang.Integer;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
 * 服务商信息Controller
 *
 * @author piggy
 * @date 2023-05-25
 */
@Api(value = "服务商信息控制器", tags = {"服务商信息管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/mch/Isvinfo")
public class TMchIsvInfoController extends BaseController {

    private final ITMchIsvInfoService iTMchIsvInfoService;

    /**
     * 查询服务商信息列表
     */
    @ApiOperation("查询服务商信息列表")
    @SaCheckPermission("mch:Isvinfo:list")
    @GetMapping("/list")
    public TableDataInfo<TMchIsvInfoVo> list(@Validated TMchIsvInfoQueryBo bo) {
        return iTMchIsvInfoService.queryPageList(bo);
    }

    /**
     * 导出服务商信息列表
     */
    @ApiOperation("导出服务商信息列表")
    @SaCheckPermission("mch:Isvinfo:export")
    @Log(title = "服务商信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TMchIsvInfoQueryBo bo, HttpServletResponse response) throws IOException {
        List<TMchIsvInfoVo> list = iTMchIsvInfoService.queryList(bo);
        ExcelUtil<TMchIsvInfoVo> util = new ExcelUtil<TMchIsvInfoVo>(TMchIsvInfoVo.class);
        util.exportExcel(response, list, "服务商信息");
    }

    /**
     * 获取服务商信息详细信息
     */
    @ApiOperation("获取服务商信息详细信息")
    @SaCheckPermission("mch:Isvinfo:query")
    @GetMapping("/{isvNo}")
    public R<TMchIsvInfoVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("isvNo") String isvNo) {
        return R.ok(iTMchIsvInfoService.queryById(isvNo));
    }

    /**
     * 新增服务商信息
     */
    @ApiOperation("新增服务商信息")
    @SaCheckPermission("mch:Isvinfo:add")
    @Log(title = "服务商信息", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TMchIsvInfoEditBo bo) {
        return R.ok(iTMchIsvInfoService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改服务商信息
     */
    @ApiOperation("修改服务商信息")
    @SaCheckPermission("mch:Isvinfo:edit")
    @Log(title = "服务商信息", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TMchIsvInfoEditBo bo) {
        return R.ok(iTMchIsvInfoService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除服务商信息
     */
    @ApiOperation("删除服务商信息")
    @SaCheckPermission("mch:Isvinfo:remove")
    @Log(title = "服务商信息" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{isvNos}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable String[] isvNos) {
        return R.ok(iTMchIsvInfoService.deleteWithValidByIds(Arrays.asList(isvNos), true) ? 1 : 0);
    }
}
