package com.piggy.pay.logic.mch.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 商户应用视图对象 t_mch_app
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("商户应用视图对象")
public class TMchAppVo {

	private static final long serialVersionUID = 1L;

	/** 应用ID */
	@Excel(name = "应用ID")
	@ApiModelProperty("应用ID")
	private String appId;

	/** 应用名称 */
	@Excel(name = "应用名称")
	@ApiModelProperty("应用名称")
	private String appName;

	/** 商户号 */
	@Excel(name = "商户号")
	@ApiModelProperty("商户号")
	private String mchNo;

	/** 应用状态: 0-停用, 1-正常 */
	@Excel(name = "应用状态: 0-停用, 1-正常")
	@ApiModelProperty("应用状态: 0-停用, 1-正常")
	private Integer state;

	/** 应用私钥 */
	@Excel(name = "应用私钥")
	@ApiModelProperty("应用私钥")
	private String appSecret;

	/** 备注 */
	@Excel(name = "备注")
	@ApiModelProperty("备注")
	private String remark;

	/** 创建者用户ID */
	@Excel(name = "创建者用户ID")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("创建者用户ID")
	private Long createdUid;

	/** 创建者姓名 */
	@Excel(name = "创建者姓名")
	@ApiModelProperty("创建者姓名")
	private String createdBy;

	/** 更新人 */
	@Excel(name = "更新人")
	@ApiModelProperty("更新人")
	private String updateBy;

	/** 创建时间 */
	@Excel(name = "创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;

	/** 更新时间 */
	@Excel(name = "更新时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;


}
