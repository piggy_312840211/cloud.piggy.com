package com.piggy.pay.logic.order.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.order.domain.TOrderSnapshot;
import com.piggy.pay.logic.order.vo.TOrderSnapshotVo;
import com.piggy.pay.logic.order.bo.TOrderSnapshotQueryBo;
import com.piggy.pay.logic.order.bo.TOrderSnapshotEditBo;

import java.util.Collection;
import java.util.List;

/**
 * 订单接口数据快照Service接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface ITOrderSnapshotService extends IServicePlus<TOrderSnapshot> {
    /**
     * 查询单个
     *
     * @return
     */
    TOrderSnapshotVo queryById(String orderId);

    /**
     * 查询列表
     */
    TableDataInfo<TOrderSnapshotVo> queryPageList(TOrderSnapshotQueryBo bo);

    /**
     * 查询列表
     */
    List<TOrderSnapshotVo> queryList(TOrderSnapshotQueryBo bo);

    /**
     * 根据新增业务对象插入订单接口数据快照
     *
     * @param bo 订单接口数据快照新增业务对象
     * @return
     */
    Boolean insertByAddBo(TOrderSnapshotEditBo bo);

    /**
     * 根据编辑业务对象修改订单接口数据快照
     *
     * @param bo 订单接口数据快照编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TOrderSnapshotEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);
}
