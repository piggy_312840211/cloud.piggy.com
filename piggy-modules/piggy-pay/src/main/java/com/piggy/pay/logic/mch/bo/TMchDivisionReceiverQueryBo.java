package com.piggy.pay.logic.mch.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 商户分账接收者账号绑定关系分页查询对象 t_mch_division_receiver
 *
 * @author piggy
 * @date 2023-05-25
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel("商户分账接收者账号绑定关系分页查询对象")
public class TMchDivisionReceiverQueryBo extends BaseEntity {

	/** 分账接收者ID */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("分账接收者ID")
	private Long receiverId;
	/** 接收者账号别名 */
	@ApiModelProperty("接收者账号别名")
	private String receiverAlias;
	/** 组ID（便于商户接口使用） */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("组ID（便于商户接口使用）")
	private Long receiverGroupId;
	/** 组名称 */
	@ApiModelProperty("组名称")
	private String receiverGroupName;
	/** 商户号 */
	@ApiModelProperty("商户号")
	private String mchNo;
	/** 服务商号 */
	@ApiModelProperty("服务商号")
	private String isvNo;
	/** 应用ID */
	@ApiModelProperty("应用ID")
	private String appId;
	/** 支付接口代码 */
	@ApiModelProperty("支付接口代码")
	private String ifCode;
	/** 分账接收账号类型: 0-个人(对私) 1-商户(对公) */
	@ApiModelProperty("分账接收账号类型: 0-个人(对私) 1-商户(对公)")
	private Integer accType;
	/** 分账接收账号 */
	@ApiModelProperty("分账接收账号")
	private String accNo;
	/** 分账接收账号名称 */
	@ApiModelProperty("分账接收账号名称")
	private String accName;
	/** 分账关系类型（参考微信）， 如： SERVICE_PROVIDER 服务商等 */
	@ApiModelProperty("分账关系类型（参考微信）， 如： SERVICE_PROVIDER 服务商等")
	private String relationType;
	/** 当选择自定义时，需要录入该字段。 否则为对应的名称 */
	@ApiModelProperty("当选择自定义时，需要录入该字段。 否则为对应的名称")
	private String relationTypeName;
	/** 分账比例 */
	@ApiModelProperty("分账比例")
	private BigDecimal divisionProfit;
	/** 分账状态（本系统状态，并不调用上游关联关系）: 1-正常分账, 0-暂停分账 */
	@ApiModelProperty("分账状态（本系统状态，并不调用上游关联关系）: 1-正常分账, 0-暂停分账")
	private Integer state;
	/** 上游绑定返回信息，一般用作查询账号异常时的记录 */
	@ApiModelProperty("上游绑定返回信息，一般用作查询账号异常时的记录")
	private String channelBindResult;
	/** 渠道特殊信息 */
	@ApiModelProperty("渠道特殊信息")
	private String channelExtInfo;
	/** 绑定成功时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("绑定成功时间")
	private Date bindSuccessTime;
	/** 创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;
	/** 更新时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;

}
