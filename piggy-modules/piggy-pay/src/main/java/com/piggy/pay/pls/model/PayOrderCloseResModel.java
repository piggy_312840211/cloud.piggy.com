package com.piggy.pay.pls.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/**
 * 关闭订单响应结果
 *
 * @author xiaoyu
 * @site https://www.jeequan.com
 * @date 2022/1/25 9:55
 */
@Data
@NoArgsConstructor
public class PayOrderCloseResModel implements Serializable {

    /**
     * 支付渠道错误码
     */
    private String errCode;

    /**
     * 支付渠道错误信息
     */
    private String errMsg;

}
