package com.piggy.pay.logic.config.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 支付方式编辑对象 t_pay_way
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("支付方式编辑对象")
public class TPayWayEditBo {


    /** 支付方式代码  例如： wxpay_jsapi */
    @ApiModelProperty("支付方式代码  例如： wxpay_jsapi")
    private String wayCode;

    /** 支付方式名称 */
    @ApiModelProperty("支付方式名称")
    @NotBlank(message = "支付方式名称不能为空")
    private String wayName;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @NotNull(message = "创建时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @NotNull(message = "更新时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;
}
