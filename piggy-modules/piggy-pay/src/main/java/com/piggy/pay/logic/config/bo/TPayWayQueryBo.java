package com.piggy.pay.logic.config.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * 支付方式分页查询对象 t_pay_way
 *
 * @author piggy
 * @date 2023-05-25
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel("支付方式分页查询对象")
public class TPayWayQueryBo extends BaseEntity {

	/** 支付方式代码  例如： wxpay_jsapi */
	@ApiModelProperty("支付方式代码  例如： wxpay_jsapi")
	private String wayCode;
	/** 支付方式名称 */
	@ApiModelProperty("支付方式名称")
	private String wayName;
	/** 创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;
	/** 更新时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;

}
