package com.piggy.pay.pls.request;

import com.piggy.pay.pls.net.RequestOptions;
import com.piggy.pay.pls.response.PayResponse;

/**
 * Jeepay请求接口
 * @author jmdhappy
 * @site https://www.jeepay.vip
 * @date 2021-06-08 11:00
 */
public interface PayRequest<T extends PayResponse, M> {

    /**
     * 获取当前接口的路径
     * @return
     */
    String getApiUri();

    /**
     * 获取当前接口的版本
     * @return
     */
    String getApiVersion();

    /**
     * 设置当前接口的版本
     * @return
     */
    void setApiVersion(String apiVersion);

    RequestOptions getRequestOptions();

    void setRequestOptions(RequestOptions options);

    M getBizModel();

    void setBizModel(M bizModel);

    Class<T> getResponseClass();

}
