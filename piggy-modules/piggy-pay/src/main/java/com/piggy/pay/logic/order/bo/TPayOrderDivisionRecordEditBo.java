package com.piggy.pay.logic.order.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;

/**
 * 分账记录编辑对象 t_pay_order_division_record
 *
 * @author zito
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("分账记录编辑对象")
public class TPayOrderDivisionRecordEditBo {


    /** 分账记录ID */
    @ApiModelProperty("分账记录ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long recordId;

    /** 商户号 */
    @ApiModelProperty("商户号")
    @NotBlank(message = "商户号不能为空")
    private String mchNo;

    /** 服务商号 */
    @ApiModelProperty("服务商号")
    private String isvNo;

    /** 应用ID */
    @ApiModelProperty("应用ID")
    @NotBlank(message = "应用ID不能为空")
    private String appId;

    /** 商户名称 */
    @ApiModelProperty("商户名称")
    @NotBlank(message = "商户名称不能为空")
    private String mchName;

    /** 类型: 1-普通商户, 2-特约商户(服务商模式) */
    @ApiModelProperty("类型: 1-普通商户, 2-特约商户(服务商模式)")
    @NotNull(message = "类型: 1-普通商户, 2-特约商户(服务商模式)不能为空")
    private Integer mchType;

    /** 支付接口代码 */
    @ApiModelProperty("支付接口代码")
    @NotBlank(message = "支付接口代码不能为空")
    private String ifCode;

    /** 系统支付订单号 */
    @ApiModelProperty("系统支付订单号")
    @NotBlank(message = "系统支付订单号不能为空")
    private String payOrderId;

    /** 支付订单渠道支付订单号 */
    @ApiModelProperty("支付订单渠道支付订单号")
    private String payOrderChannelOrderNo;

    /** 订单金额,单位分 */
    @ApiModelProperty("订单金额,单位分")
    @NotNull(message = "订单金额,单位分不能为空")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long payOrderAmount;

    /** 订单实际分账金额, 单位：分（订单金额 - 商户手续费 - 已退款金额） */
    @ApiModelProperty("订单实际分账金额, 单位：分（订单金额 - 商户手续费 - 已退款金额）")
    @NotNull(message = "订单实际分账金额, 单位：分（订单金额 - 商户手续费 - 已退款金额）不能为空")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long payOrderDivisionAmount;

    /** 系统分账批次号 */
    @ApiModelProperty("系统分账批次号")
    @NotBlank(message = "系统分账批次号不能为空")
    private String batchOrderId;

    /** 上游分账批次号 */
    @ApiModelProperty("上游分账批次号")
    private String channelBatchOrderId;

    /** 状态: 0-待分账 1-分账成功（明确成功）, 2-分账失败（明确失败）, 3-分账已受理（上游受理） */
    @ApiModelProperty("状态: 0-待分账 1-分账成功（明确成功）, 2-分账失败（明确失败）, 3-分账已受理（上游受理）")
    @NotNull(message = "状态: 0-待分账 1-分账成功（明确成功）, 2-分账失败（明确失败）, 3-分账已受理（上游受理）不能为空")
    private Integer state;

    /** 上游返回数据包 */
    @ApiModelProperty("上游返回数据包")
    private String channelRespResult;

    /** 账号快照》 分账接收者ID */
    @ApiModelProperty("账号快照》 分账接收者ID")
    @NotNull(message = "账号快照》 分账接收者ID不能为空")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long receiverId;

    /** 账号快照》 组ID（便于商户接口使用） */
    @ApiModelProperty("账号快照》 组ID（便于商户接口使用）")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long receiverGroupId;

    /** 接收者账号别名 */
    @ApiModelProperty("接收者账号别名")
    private String receiverAlias;

    /** 账号快照》 分账接收账号类型: 0-个人 1-商户 */
    @ApiModelProperty("账号快照》 分账接收账号类型: 0-个人 1-商户")
    @NotNull(message = "账号快照》 分账接收账号类型: 0-个人 1-商户不能为空")
    private Integer accType;

    /** 账号快照》 分账接收账号 */
    @ApiModelProperty("账号快照》 分账接收账号")
    @NotBlank(message = "账号快照》 分账接收账号不能为空")
    private String accNo;

    /** 账号快照》 分账接收账号名称 */
    @ApiModelProperty("账号快照》 分账接收账号名称")
    @NotBlank(message = "账号快照》 分账接收账号名称不能为空")
    private String accName;

    /** 账号快照》 分账关系类型（参考微信）， 如： SERVICE_PROVIDER 服务商等 */
    @ApiModelProperty("账号快照》 分账关系类型（参考微信）， 如： SERVICE_PROVIDER 服务商等")
    @NotBlank(message = "账号快照》 分账关系类型（参考微信）， 如： SERVICE_PROVIDER 服务商等不能为空")
    private String relationType;

    /** 账号快照》 当选择自定义时，需要录入该字段。 否则为对应的名称 */
    @ApiModelProperty("账号快照》 当选择自定义时，需要录入该字段。 否则为对应的名称")
    @NotBlank(message = "账号快照》 当选择自定义时，需要录入该字段。 否则为对应的名称不能为空")
    private String relationTypeName;

    /** 账号快照》 配置的实际分账比例 */
    @ApiModelProperty("账号快照》 配置的实际分账比例")
    @NotNull(message = "账号快照》 配置的实际分账比例不能为空")
    private BigDecimal divisionProfit;

    /** 计算该接收方的分账金额,单位分 */
    @ApiModelProperty("计算该接收方的分账金额,单位分")
    @NotNull(message = "计算该接收方的分账金额,单位分不能为空")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long calDivisionAmount;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @NotNull(message = "创建时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @NotNull(message = "更新时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;
}
