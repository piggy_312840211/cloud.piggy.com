package com.piggy.pay.pls.net;

import com.alibaba.fastjson2.JSONException;
import com.alibaba.fastjson2.JSONObject;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.pay.pls.exception.APIException;
import com.piggy.pay.pls.exception.InvalidRequestException;
import com.piggy.pay.pls.request.PayRequest;
import com.piggy.pay.pls.response.PayResponse;
import com.piggy.pay.pls.util.JSONWriter;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * API资源抽象类
 *
 * @author jmdhappy
 * @site https://www.jeepay.vip
 * @date 2021-06-08 11:00
 */
@Slf4j
public abstract class APIResource {

    public static final Charset CHARSET = StandardCharsets.UTF_8;

    private static HttpClient httpClient = new HttpURLConnectionClient();

    protected enum RequestMethod {
        GET,
        POST,
        DELETE,
        PUT
    }

    public static Class<?> getSelfClass() {
        return APIResource.class;
    }

    protected static String urlEncode(String str) {
        if (str == null) {
            return null;
        }

        try {
            return URLEncoder.encode(str, CHARSET.name());
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError("UTF-8 is unknown");
        }
    }

    public <T extends PayResponse, M> T execute(
            PayRequest<T, M> request,
            RequestMethod method,
            String url) throws BaseException {

        String jsonParam = new JSONWriter().write(request.getBizModel(), true);

        JSONObject params = JSONObject.parseObject(jsonParam);
        request.getRequestOptions();
        APIPayRequest apiJeepayRequest = new APIPayRequest(method, url, params, request.getRequestOptions());
        if (log.isDebugEnabled())
            log.debug("Jeepay_SDK_REQ：url={}, data={}", apiJeepayRequest.getUrl(), JSONObject.toJSONString(apiJeepayRequest.getParams()));
        APIPayResponse response = httpClient.requestWithRetries(apiJeepayRequest);
        int responseCode = response.getResponseCode();
        String responseBody = response.getResponseBody();
        if (log.isDebugEnabled()) log.debug("Jeepay_SDK_RES：code={}, body={}", responseCode, responseBody);
        if (responseCode != 200) {
            handleAPIError(response);
        }

        T resource = null;

        try {
            resource = JSONObject.parseObject(responseBody, request.getResponseClass());
        } catch (JSONException e) {
            raiseMalformedJsonError(responseBody, responseCode);
        }

        return resource;
    }

    /**
     * 错误处理
     *
     * @param response
     */
    private static void handleAPIError(APIPayResponse response)
            throws BaseException {

        String rBody = response.getResponseBody();
        int rCode = response.getResponseCode();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = JSONObject.parseObject(rBody);

        } catch (JSONException e) {
            raiseMalformedJsonError(rBody, rCode);
        }

        if (rCode == 404) {
            throw new InvalidRequestException(jsonObject.getString("status") + ", "
                    + jsonObject.getString("error") + ", "
                    + jsonObject.getString("path")
                    , rCode, null);
        }

    }

    private static void raiseMalformedJsonError(
            String responseBody, int responseCode) throws APIException {
        throw new APIException(
                String.format(
                        "Invalid response object from API: %s. (HTTP response code was %d)",
                        responseBody, responseCode),
                responseCode,
                null);
    }
}
