package com.piggy.pay.pls.request;

import com.piggy.pay.pls.BasePay;
import com.piggy.pay.pls.model.DivisionReceiverChannelBalanceCashoutReqModel;
import com.piggy.pay.pls.net.RequestOptions;
import com.piggy.pay.pls.response.DivisionReceiverChannelBalanceCashoutResponse;
import lombok.Data;

/**
 * 分账渠道余额提现
 *
 * @author terrfly
 * @site https://www.jeequan.com
 * @date 2022/5/11 15:13
 */
@Data
public class DivisionReceiverChannelBalanceCashoutRequest implements PayRequest<DivisionReceiverChannelBalanceCashoutResponse, DivisionReceiverChannelBalanceCashoutReqModel> {

    private String apiVersion = BasePay.VERSION;
    private String apiUri = "api/division/receiver/channelBalanceCashout";
    private RequestOptions options;
    private DivisionReceiverChannelBalanceCashoutReqModel bizModel = null;

    @Override
    public String getApiUri() {
        return this.apiUri;
    }

    @Override
    public String getApiVersion() {
        return this.apiVersion;
    }

    @Override
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    @Override
    public RequestOptions getRequestOptions() {
        return this.options;
    }

    @Override
    public void setRequestOptions(RequestOptions options) {
        this.options = options;
    }

    @Override
    public DivisionReceiverChannelBalanceCashoutReqModel getBizModel() {
        return this.bizModel;
    }

    @Override
    public void setBizModel(DivisionReceiverChannelBalanceCashoutReqModel bizModel) {
        this.bizModel = bizModel;
    }

    @Override
    public Class<DivisionReceiverChannelBalanceCashoutResponse> getResponseClass() {
        return DivisionReceiverChannelBalanceCashoutResponse.class;
    }

}
