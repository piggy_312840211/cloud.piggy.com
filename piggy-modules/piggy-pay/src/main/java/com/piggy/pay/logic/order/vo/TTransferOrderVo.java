package com.piggy.pay.logic.order.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 转账订单视图对象 t_transfer_order
 *
 * @author zito
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("转账订单视图对象")
public class TTransferOrderVo {

	private static final long serialVersionUID = 1L;

	/** 转账订单号 */
	@Excel(name = "转账订单号")
	@ApiModelProperty("转账订单号")
	private String transferId;

	/** 商户号 */
	@Excel(name = "商户号")
	@ApiModelProperty("商户号")
	private String mchNo;

	/** 服务商号 */
	@Excel(name = "服务商号")
	@ApiModelProperty("服务商号")
	private String isvNo;

	/** 应用ID */
	@Excel(name = "应用ID")
	@ApiModelProperty("应用ID")
	private String appId;

	/** 商户名称 */
	@Excel(name = "商户名称")
	@ApiModelProperty("商户名称")
	private String mchName;

	/** 类型: 1-普通商户, 2-特约商户(服务商模式) */
	@Excel(name = "类型: 1-普通商户, 2-特约商户(服务商模式)")
	@ApiModelProperty("类型: 1-普通商户, 2-特约商户(服务商模式)")
	private Integer mchType;

	/** 商户订单号 */
	@Excel(name = "商户订单号")
	@ApiModelProperty("商户订单号")
	private String mchOrderNo;

	/** 支付接口代码 */
	@Excel(name = "支付接口代码")
	@ApiModelProperty("支付接口代码")
	private String ifCode;

	/** 入账方式： WX_CASH-微信零钱; ALIPAY_CASH-支付宝转账; BANK_CARD-银行卡 */
	@Excel(name = "入账方式： WX_CASH-微信零钱; ALIPAY_CASH-支付宝转账; BANK_CARD-银行卡")
	@ApiModelProperty("入账方式： WX_CASH-微信零钱; ALIPAY_CASH-支付宝转账; BANK_CARD-银行卡")
	private String entryType;

	/** 转账金额,单位分 */
	@Excel(name = "转账金额,单位分")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("转账金额,单位分")
	private Long amount;

	/** 三位货币代码,人民币:cny */
	@Excel(name = "三位货币代码,人民币:cny")
	@ApiModelProperty("三位货币代码,人民币:cny")
	private String currency;

	/** 收款账号 */
	@Excel(name = "收款账号")
	@ApiModelProperty("收款账号")
	private String accountNo;

	/** 收款人姓名 */
	@Excel(name = "收款人姓名")
	@ApiModelProperty("收款人姓名")
	private String accountName;

	/** 收款人开户行名称 */
	@Excel(name = "收款人开户行名称")
	@ApiModelProperty("收款人开户行名称")
	private String bankName;

	/** 转账备注信息 */
	@Excel(name = "转账备注信息")
	@ApiModelProperty("转账备注信息")
	private String transferDesc;

	/** 客户端IP */
	@Excel(name = "客户端IP")
	@ApiModelProperty("客户端IP")
	private String clientIp;

	/** 支付状态: 0-订单生成, 1-转账中, 2-转账成功, 3-转账失败, 4-订单关闭 */
	@Excel(name = "支付状态: 0-订单生成, 1-转账中, 2-转账成功, 3-转账失败, 4-订单关闭")
	@ApiModelProperty("支付状态: 0-订单生成, 1-转账中, 2-转账成功, 3-转账失败, 4-订单关闭")
	private Integer state;

	/** 特定渠道发起额外参数 */
	@Excel(name = "特定渠道发起额外参数")
	@ApiModelProperty("特定渠道发起额外参数")
	private String channelExtra;

	/** 渠道订单号 */
	@Excel(name = "渠道订单号")
	@ApiModelProperty("渠道订单号")
	private String channelOrderNo;

	/** 渠道支付错误码 */
	@Excel(name = "渠道支付错误码")
	@ApiModelProperty("渠道支付错误码")
	private String errCode;

	/** 渠道支付错误描述 */
	@Excel(name = "渠道支付错误描述")
	@ApiModelProperty("渠道支付错误描述")
	private String errMsg;

	/** 商户扩展参数 */
	@Excel(name = "商户扩展参数")
	@ApiModelProperty("商户扩展参数")
	private String extParam;

	/** 异步通知地址 */
	@Excel(name = "异步通知地址")
	@ApiModelProperty("异步通知地址")
	private String notifyUrl;

	/** 转账成功时间 */
	@Excel(name = "转账成功时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("转账成功时间")
	private Date successTime;

	/** 创建时间 */
	@Excel(name = "创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;

	/** 更新时间 */
	@Excel(name = "更新时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;


}
