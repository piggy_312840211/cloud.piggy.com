package com.piggy.pay.pls.exception;

import com.piggy.common.core.exception.base.BaseException;

/**
 * 无效请求异常
 * @author jmdhappy
 * @site https://www.jeepay.vip
 * @date 2021-06-08 11:00
 */
public class InvalidRequestException extends BaseException {

    private static final long serialVersionUID = 3163726141488238321L;

    public InvalidRequestException(String message, int statusCode, Throwable e) {
        super(message, statusCode, e);
    }

}
