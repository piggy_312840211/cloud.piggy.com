package com.piggy.pay.logic.mch.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.mch.domain.TMchNotifyRecord;
import org.apache.ibatis.annotations.Param;

/**
 * 商户通知记录Mapper接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface TMchNotifyRecordMapperEx extends BaseMapperPlus<TMchNotifyRecord> {

    Integer updateNotifyResult(@Param("notifyId") Long notifyId, @Param("state") Integer state, @Param("resResult") String resResult);

    /*
     * 功能描述: 更改为通知中 & 增加允许重发通知次数
     * @param notifyId
     * @Author: terrfly
     * @Date: 2021/6/21 17:38
     */
    Integer updateIngAndAddNotifyCountLimit(@Param("notifyId") Long notifyId);
}
