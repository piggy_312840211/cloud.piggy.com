package com.piggy.pay.pls.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 退款下单响应实体类
 * @author jmdhappy
 * @site https://www.jeepay.vip
 * @date 2021-06-18 09:00
 */
@Data
@NoArgsConstructor
public class RefundOrderCreateResModel implements Serializable {

    /**
     * 退款单号(网关生成)
     */
    private String refundOrderId;

    /**
     * 商户发起的退款订单号
     */
    private String mchRefundNo;

    /**
     * 订单支付金额
     */
    private Long payAmount;

    /**
     * 申请退款金额
     */
    private Long refundAmount;

    /**
     * 退款状态
     * 0-订单生成
     * 1-退款中
     * 2-退款成功
     * 3-退款失败
     * 4-退款关闭
     */
    private Integer state;

    /**
     * 渠道退款单号
     */
    private String channelOrderNo;

    /**
     * 支付渠道错误码
     */
    private String errCode;

    /**
     * 支付渠道错误信息
     */
    private String errMsg;

}
