package com.piggy.pay.logic.order.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 支付订单分页查询对象 t_pay_order
 *
 * @author piggy
 * @date 2023-05-25
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel("支付订单分页查询对象")
public class TPayOrderQueryBo extends BaseEntity {

	/** 支付订单号 */
	@ApiModelProperty("支付订单号")
	private String payOrderId;
	/** 商户号 */
	@ApiModelProperty("商户号")
	private String mchNo;
	/** 服务商号 */
	@ApiModelProperty("服务商号")
	private String isvNo;
	/** 应用ID */
	@ApiModelProperty("应用ID")
	private String appId;
	/** 商户名称 */
	@ApiModelProperty("商户名称")
	private String mchName;
	/** 类型: 1-普通商户, 2-特约商户(服务商模式) */
	@ApiModelProperty("类型: 1-普通商户, 2-特约商户(服务商模式)")
	private Integer mchType;
	/** 商户订单号 */
	@ApiModelProperty("商户订单号")
	private String mchOrderNo;
	/** 支付接口代码 */
	@ApiModelProperty("支付接口代码")
	private String ifCode;
	/** 支付方式代码 */
	@ApiModelProperty("支付方式代码")
	private String wayCode;
	/** 支付金额,单位分 */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("支付金额,单位分")
	private Long amount;
	/** 商户手续费费率快照 */
	@ApiModelProperty("商户手续费费率快照")
	private BigDecimal mchFeeRate;
	/** 商户手续费,单位分 */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("商户手续费,单位分")
	private Long mchFeeAmount;
	/** 三位货币代码,人民币:cny */
	@ApiModelProperty("三位货币代码,人民币:cny")
	private String currency;
	/** 支付状态: 0-订单生成, 1-支付中, 2-支付成功, 3-支付失败, 4-已撤销, 5-已退款, 6-订单关闭 */
	@ApiModelProperty("支付状态: 0-订单生成, 1-支付中, 2-支付成功, 3-支付失败, 4-已撤销, 5-已退款, 6-订单关闭")
	private Integer state;
	/** 向下游回调状态, 0-未发送,  1-已发送 */
	@ApiModelProperty("向下游回调状态, 0-未发送,  1-已发送")
	private Integer notifyState;
	/** 客户端IP */
	@ApiModelProperty("客户端IP")
	private String clientIp;
	/** 商品标题 */
	@ApiModelProperty("商品标题")
	private String subject;
	/** 商品描述信息 */
	@ApiModelProperty("商品描述信息")
	private String body;
	/** 特定渠道发起额外参数 */
	@ApiModelProperty("特定渠道发起额外参数")
	private String channelExtra;
	/** 渠道用户标识,如微信openId,支付宝账号 */
	@ApiModelProperty("渠道用户标识,如微信openId,支付宝账号")
	private String channelUser;
	/** 渠道订单号 */
	@ApiModelProperty("渠道订单号")
	private String channelOrderNo;
	/** 退款状态: 0-未发生实际退款, 1-部分退款, 2-全额退款 */
	@ApiModelProperty("退款状态: 0-未发生实际退款, 1-部分退款, 2-全额退款")
	private Integer refundState;
	/** 退款次数 */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("退款次数")
	private Long refundTimes;
	/** 退款总金额,单位分 */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("退款总金额,单位分")
	private Long refundAmount;
	/** 订单分账模式：0-该笔订单不允许分账, 1-支付成功按配置自动完成分账, 2-商户手动分账(解冻商户金额) */
	@ApiModelProperty("订单分账模式：0-该笔订单不允许分账, 1-支付成功按配置自动完成分账, 2-商户手动分账(解冻商户金额)")
	private Integer divisionMode;
	/** 订单分账状态：0-未发生分账, 1-等待分账任务处理, 2-分账处理中, 3-分账任务已结束(不体现状态) */
	@ApiModelProperty("订单分账状态：0-未发生分账, 1-等待分账任务处理, 2-分账处理中, 3-分账任务已结束(不体现状态)")
	private Integer divisionState;
	/** 最新分账时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("最新分账时间")
	private Date divisionLastTime;
	/** 渠道支付错误码 */
	@ApiModelProperty("渠道支付错误码")
	private String errCode;
	/** 渠道支付错误描述 */
	@ApiModelProperty("渠道支付错误描述")
	private String errMsg;
	/** 商户扩展参数 */
	@ApiModelProperty("商户扩展参数")
	private String extParam;
	/** 异步通知地址 */
	@ApiModelProperty("异步通知地址")
	private String notifyUrl;
	/** 页面跳转地址 */
	@ApiModelProperty("页面跳转地址")
	private String returnUrl;
	/** 订单失效时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("订单失效时间")
	private Date expiredTime;
	/** 订单支付成功时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("订单支付成功时间")
	private Date successTime;
	/** 创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;

	@ApiModelProperty("创建时间-开始")
	private String createTimeStart;

	@ApiModelProperty("创建时间-结束")
	private String createTimeEnd;

	/** 更新时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;

}
