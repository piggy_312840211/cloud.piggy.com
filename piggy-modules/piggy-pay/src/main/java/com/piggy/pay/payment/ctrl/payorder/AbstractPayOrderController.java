/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.piggy.pay.payment.ctrl.payorder;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.mq.vender.IMQSender;
import com.piggy.pay.components.mq.model.PayOrderReissueMQ;
import com.piggy.pay.core.beans.RequestKitBean;
import com.piggy.pay.core.constants.CS;
import com.piggy.pay.core.enums.ChannelEnums;
import com.piggy.pay.core.enums.IDivisionEnums;
import com.piggy.pay.core.enums.IMchEnums;
import com.piggy.pay.core.enums.IOrderEnums;
import com.piggy.pay.core.exception.ChannelException;
import com.piggy.pay.core.model.ApiRes;
import com.piggy.pay.core.utils.AmountUtil;
import com.piggy.pay.core.utils.SeqKit;
import cn.hutool.extra.spring.SpringUtil;
import com.piggy.pay.core.utils.StringKit;
import com.piggy.pay.logic.config.sys.DBApplicationConfig;
import com.piggy.pay.logic.config.sys.PayConfigService;
import com.piggy.pay.logic.mch.domain.TMchApp;
import com.piggy.pay.logic.mch.domain.TMchInfo;
import com.piggy.pay.logic.mch.domain.TMchPayPassage;
import com.piggy.pay.logic.mch.service.ITMchPayPassageService;
import com.piggy.pay.logic.order.domain.TPayOrder;
import com.piggy.pay.logic.order.service.ITPayOrderService;
import com.piggy.pay.payment.channel.IPaymentService;
import com.piggy.pay.payment.ctrl.ApiController;
import com.piggy.pay.payment.model.MchAppConfigContext;
import com.piggy.pay.payment.rqrs.msg.ChannelRetMsg;
import com.piggy.pay.payment.rqrs.payorder.UnifiedOrderRQ;
import com.piggy.pay.payment.rqrs.payorder.UnifiedOrderRS;
import com.piggy.pay.payment.rqrs.payorder.payway.QrCashierOrderRQ;
import com.piggy.pay.payment.rqrs.payorder.payway.QrCashierOrderRS;
import com.piggy.pay.payment.service.ConfigContextQueryService;
import com.piggy.pay.payment.service.PayOrderProcessService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;

/*
 * 创建支付订单抽象类
 *
 * @author terrfly
 * @site https://www.jeequan.com
 * @date 2021/6/8 17:26
 */
@Slf4j
public abstract class AbstractPayOrderController extends ApiController {

    @Resource
    private ITMchPayPassageService mchPayPassageService;
    @Resource
    private ITPayOrderService payOrderService;
    @Resource
    private ConfigContextQueryService configContextQueryService;
    @Resource
    private PayOrderProcessService payOrderProcessService;
    @Resource
    private IMQSender mqSender;

    @Resource
    private PayConfigService sysConfigService;

    /**
     * 统一下单 (新建订单模式)
     **/
    protected ApiRes unifiedOrder(String wayCode, UnifiedOrderRQ bizRQ) {
        return unifiedOrder(wayCode, bizRQ, null);
    }

    /**
     * 统一下单
     **/
    protected ApiRes unifiedOrder(String wayCode, UnifiedOrderRQ bizRQ, TPayOrder payOrder) {

        // 响应数据
        UnifiedOrderRS bizRS = null;

        //是否新订单模式 [  一般接口都为新订单模式，  由于QR_CASHIER支付方式，需要先 在DB插入一个新订单， 导致此处需要特殊判断下。 如果已存在则直接更新，否则为插入。  ]
        boolean isNewOrder = payOrder == null;

        try {

            if (payOrder != null) { //当订单存在时，封装公共参数。

                if (payOrder.getState() != IOrderEnums.OrderStateEnums.STATE_INIT.getState()) {
                    throw new BaseException("订单状态异常");
                }

                payOrder.setWayCode(wayCode); // 需要将订单更新 支付方式
                payOrder.setChannelUser(bizRQ.getChannelUserId()); //更新渠道用户信息
                bizRQ.setMchNo(payOrder.getMchNo());
                bizRQ.setAppId(payOrder.getAppId());
                bizRQ.setMchOrderNo(payOrder.getMchOrderNo());
                bizRQ.setWayCode(wayCode);
                bizRQ.setAmount(payOrder.getAmount());
                bizRQ.setCurrency(payOrder.getCurrency());
                bizRQ.setClientIp(payOrder.getClientIp());
                bizRQ.setSubject(payOrder.getSubject());
                bizRQ.setNotifyUrl(payOrder.getNotifyUrl());
                bizRQ.setReturnUrl(payOrder.getReturnUrl());
                bizRQ.setChannelExtra(payOrder.getChannelExtra());
                bizRQ.setExtParam(payOrder.getExtParam());
                bizRQ.setDivisionMode(payOrder.getDivisionMode());
            }

            String mchNo = bizRQ.getMchNo();
            String appId = bizRQ.getAppId();

            // 只有新订单模式，进行校验
            if (isNewOrder && payOrderService.count(new LambdaQueryWrapper<TPayOrder>().eq(TPayOrder::getMchNo, mchNo).eq(TPayOrder::getMchOrderNo, bizRQ.getMchOrderNo())) > 0) {
                throw new BaseException("商户订单[" + bizRQ.getMchOrderNo() + "]已存在");
            }

            if (StringUtils.isNotEmpty(bizRQ.getNotifyUrl()) && !StringKit.isAvailableUrl(bizRQ.getNotifyUrl())) {
                throw new BaseException("异步通知地址协议仅支持http:// 或 https:// !");
            }

            if (StringUtils.isNotEmpty(bizRQ.getReturnUrl()) && !StringKit.isAvailableUrl(bizRQ.getReturnUrl())) {
                throw new BaseException("同步通知地址协议仅支持http:// 或 https:// !");
            }

            //获取支付参数 (缓存数据) 和 商户信息
            MchAppConfigContext mchAppConfigContext = configContextQueryService.queryMchInfoAndAppInfo(mchNo, appId);
            if (mchAppConfigContext == null) {
                throw new BaseException("获取商户应用信息失败");
            }

            TMchInfo mchInfo = mchAppConfigContext.getMchInfo();
            TMchApp mchApp = mchAppConfigContext.getMchApp();

            //收银台支付并且只有新订单需要走这里，  收银台二次下单的wayCode应该为实际支付方式。
            if (isNewOrder && CS.PAY_WAY_CODE.QR_CASHIER.equals(wayCode)) {

                //生成订单
                payOrder = genPayOrder(bizRQ, mchInfo, mchApp, null, null);
                String payOrderId = payOrder.getPayOrderId();
                //订单入库 订单状态： 生成状态  此时没有和任何上游渠道产生交互。
                payOrderService.save(payOrder);

                QrCashierOrderRS qrCashierOrderRS = new QrCashierOrderRS();
                QrCashierOrderRQ qrCashierOrderRQ = (QrCashierOrderRQ) bizRQ;

                DBApplicationConfig dbApplicationConfig = sysConfigService.getDBApplicationConfig();

                String payUrl = dbApplicationConfig.genUniJsapiPayUrl(payOrderId);
                if (CS.PAY_DATA_TYPE.CODE_IMG_URL.equals(qrCashierOrderRQ.getPayDataType())) { //二维码地址
                    qrCashierOrderRS.setCodeImgUrl(dbApplicationConfig.genScanImgUrl(payUrl));

                } else { //默认都为跳转地址方式
                    qrCashierOrderRS.setPayUrl(payUrl);
                }

                return packageApiResByPayOrder(bizRQ, qrCashierOrderRS, payOrder);
            }

            // 根据支付方式， 查询出 该商户 可用的支付接口
            TMchPayPassage mchPayPassage = mchPayPassageService.findMchPayPassage(mchAppConfigContext.getMchNo(), mchAppConfigContext.getAppId(), wayCode);
            if (mchPayPassage == null) {
                throw new BaseException("商户应用不支持该支付方式");
            }

            //获取支付接口
            IPaymentService paymentService = checkMchWayCodeAndGetService(mchAppConfigContext, mchPayPassage);
            String ifCode = paymentService.getIfCode();

            //生成订单
            if (isNewOrder) {
                payOrder = genPayOrder(bizRQ, mchInfo, mchApp, ifCode, mchPayPassage);
            } else {
                payOrder.setIfCode(ifCode);

                // 查询支付方式的费率，并 在更新ing时更新费率信息
                payOrder.setMchFeeRate(mchPayPassage.getRate());
                payOrder.setMchFeeAmount(AmountUtil.calPercentageFee(payOrder.getAmount(), payOrder.getMchFeeRate())); //商户手续费,单位分
            }

            //预先校验
            String errMsg = paymentService.preCheck(bizRQ, payOrder);
            if (StringUtils.isNotEmpty(errMsg)) {
                throw new BaseException(errMsg);
            }

            String newPayOrderId = paymentService.customPayOrderId(bizRQ, payOrder, mchAppConfigContext);


            if (isNewOrder) {
                if (StringUtils.isNotBlank(newPayOrderId)) { // 自定义订单号
                    payOrder.setPayOrderId(newPayOrderId);
                }
                //订单入库 订单状态： 生成状态  此时没有和任何上游渠道产生交互。
                payOrderService.save(payOrder);
            }

            //调起上游支付接口
            bizRS = (UnifiedOrderRS) paymentService.pay(bizRQ, payOrder, mchAppConfigContext);

            //处理上游返回数据
            this.processChannelMsg(bizRS.getChannelRetMsg(), payOrder);

            return packageApiResByPayOrder(bizRQ, bizRS, payOrder);

        } catch (BaseException e) {
            return ApiRes.customFail(e.getMessage());

        } catch (ChannelException e) {

            //处理上游返回数据
            this.processChannelMsg(e.getChannelRetMsg(), payOrder);

            if (e.getChannelRetMsg().getChannelState() == ChannelEnums.SYS_ERROR) {
                return ApiRes.customFail(e.getMessage());
            }

            return this.packageApiResByPayOrder(bizRQ, bizRS, payOrder);


        } catch (Exception e) {
            log.error("系统异常：{}", e);
            return ApiRes.customFail("系统异常");
        }
    }

    private TPayOrder genPayOrder(UnifiedOrderRQ rq, TMchInfo mchInfo, TMchApp mchApp, String ifCode, TMchPayPassage mchPayPassage) {

        TPayOrder payOrder = new TPayOrder();
        payOrder.setPayOrderId(SeqKit.genPayOrderId()); //生成订单ID
        payOrder.setMchNo(mchInfo.getMchNo()); //商户号
        payOrder.setIsvNo(mchInfo.getIsvNo()); //服务商号
        payOrder.setMchName(mchInfo.getMchShortName()); //商户名称（简称）
        payOrder.setMchType(mchInfo.getType()); //商户类型
        payOrder.setMchOrderNo(rq.getMchOrderNo()); //商户订单号
        payOrder.setAppId(mchApp.getAppId()); //商户应用appId
        payOrder.setIfCode(ifCode); //接口代码
        payOrder.setWayCode(rq.getWayCode()); //支付方式
        payOrder.setAmount(rq.getAmount()); //订单金额

        if (mchPayPassage != null) {
            payOrder.setMchFeeRate(mchPayPassage.getRate()); //商户手续费费率快照
        } else {
            payOrder.setMchFeeRate(BigDecimal.ZERO); //预下单模式， 按照0计算入库， 后续进行更新
        }

        payOrder.setMchFeeAmount(AmountUtil.calPercentageFee(payOrder.getAmount(), payOrder.getMchFeeRate())); //商户手续费,单位分

        payOrder.setCurrency(rq.getCurrency()); //币种
        payOrder.setState(IOrderEnums.OrderStateEnums.STATE_INIT.getState()); //订单状态, 默认订单生成状态
        payOrder.setClientIp(StrUtil.emptyToDefault(rq.getClientIp(), getClientIp())); //客户端IP
        payOrder.setSubject(rq.getSubject()); //商品标题
        payOrder.setBody(rq.getBody()); //商品描述信息
//        payOrder.setChannelExtra(rq.getChannelExtra()); //特殊渠道发起的附件额外参数,  是否应该删除该字段了？？ 比如authCode不应该记录， 只是在传输阶段存在的吧？  之前的为了在payOrder对象需要传参。
        payOrder.setChannelUser(rq.getChannelUserId()); //渠道用户标志
        payOrder.setExtParam(rq.getExtParam()); //商户扩展参数
        payOrder.setNotifyUrl(rq.getNotifyUrl()); //异步通知地址
        payOrder.setReturnUrl(rq.getReturnUrl()); //页面跳转地址

        // 分账模式
        payOrder.setDivisionMode(ObjectUtils.defaultIfNull(rq.getDivisionMode(), IDivisionEnums.DivisionModeEnums.DIVISION_MODE_FORBID.getMode()));

        Date nowDate = new Date();

        //订单过期时间 单位： 秒
        if (rq.getExpiredTime() != null) {
            payOrder.setExpiredTime(DateUtil.offsetSecond(nowDate, rq.getExpiredTime()));
        } else {
            payOrder.setExpiredTime(DateUtil.offsetHour(nowDate, 2)); //订单过期时间 默认两个小时
        }

        payOrder.setCreatedAt(nowDate); //订单创建时间
        return payOrder;
    }


    /**
     * 校验： 商户的支付方式是否可用
     * 返回： 支付接口
     **/
    private IPaymentService checkMchWayCodeAndGetService(MchAppConfigContext mchAppConfigContext, TMchPayPassage mchPayPassage) {

        // 接口代码
        String ifCode = mchPayPassage.getIfCode();
        IPaymentService paymentService = SpringUtil.getBean(ifCode + "PaymentService", IPaymentService.class);
        if (paymentService == null) {
            throw new BaseException("无此支付通道接口");
        }

        if (!paymentService.isSupport(mchPayPassage.getWayCode())) {
            throw new BaseException("接口不支持该支付方式");
        }

        if (mchAppConfigContext.getMchType() == IMchEnums.MchTypeEnums.TYPE_NORMAL.getType()) { //普通商户

            if (configContextQueryService.queryNormalMchParams(mchAppConfigContext.getMchNo(), mchAppConfigContext.getAppId(), ifCode) == null) {
                throw new BaseException("商户应用参数未配置");
            }
        } else if (mchAppConfigContext.getMchType() == IMchEnums.MchTypeEnums.TYPE_ISVSUB.getType()) { //特约商户

            if (configContextQueryService.queryIsvsubMchParams(mchAppConfigContext.getMchNo(), mchAppConfigContext.getAppId(), ifCode) == null) {
                throw new BaseException("特约商户参数未配置");
            }

            if (configContextQueryService.queryIsvParams(mchAppConfigContext.getMchInfo().getIsvNo(), ifCode) == null) {
                throw new BaseException("服务商参数未配置");
            }
        }

        return paymentService;

    }


    /**
     * 处理返回的渠道信息，并更新订单状态
     * payOrder将对部分信息进行 赋值操作。
     **/
    private void processChannelMsg(ChannelRetMsg channelRetMsg, TPayOrder payOrder) {

        //对象为空 || 上游返回状态为空， 则无需操作
        if (channelRetMsg == null || channelRetMsg.getChannelState() == null) {
            return;
        }

        String payOrderId = payOrder.getPayOrderId();

        //明确成功
        if (ChannelEnums.CONFIRM_SUCCESS == channelRetMsg.getChannelState()) {

            this.updateInitOrderStateThrowException(IOrderEnums.OrderStateEnums.STATE_SUCCESS.getState(), payOrder, channelRetMsg);

            //订单支付成功，其他业务逻辑
            payOrderProcessService.confirmSuccess(payOrder);

            //明确失败
        } else if (ChannelEnums.CONFIRM_FAIL == channelRetMsg.getChannelState()) {

            this.updateInitOrderStateThrowException(IOrderEnums.OrderStateEnums.STATE_FAIL.getState(), payOrder, channelRetMsg);

            // 上游处理中 || 未知 || 上游接口返回异常  订单为支付中状态
        } else if (ChannelEnums.WAITING == channelRetMsg.getChannelState() ||
                ChannelEnums.UNKNOWN == channelRetMsg.getChannelState() ||
                ChannelEnums.API_RET_ERROR == channelRetMsg.getChannelState()) {
            this.updateInitOrderStateThrowException(IOrderEnums.OrderStateEnums.STATE_ING.getState(), payOrder, channelRetMsg);

            // 系统异常：  订单不再处理。  为： 生成状态
        } else if (ChannelEnums.SYS_ERROR == channelRetMsg.getChannelState()) {

        } else {

            throw new BaseException("ChannelState 返回异常！");
        }

        //判断是否需要轮询查单
        if (channelRetMsg.isNeedQuery()) {
            mqSender.send(PayOrderReissueMQ.build(payOrderId, 1), 5);
        }

    }


    /**
     * 更新订单状态 --》 订单生成--》 其他状态  (向外抛出异常)
     **/
    private void updateInitOrderStateThrowException(int orderState, TPayOrder payOrder, ChannelRetMsg channelRetMsg) {

        payOrder.setState(orderState);
        payOrder.setChannelOrderNo(channelRetMsg.getChannelOrderId());
        payOrder.setErrCode(channelRetMsg.getChannelErrCode());
        payOrder.setErrMsg(channelRetMsg.getChannelErrMsg());

        // 聚合码场景 订单对象存在会员信息， 不可全部以上游为准。
        if (StringUtils.isNotEmpty(channelRetMsg.getChannelUserId())) {
            payOrder.setChannelUser(channelRetMsg.getChannelUserId());
        }

        boolean isSuccess = payOrderService.updateInit2Ing(payOrder.getPayOrderId(), payOrder);
        if (!isSuccess) {
            throw new BaseException("更新订单异常!");
        }

        isSuccess = payOrderService.updateIng2SuccessOrFail(payOrder.getPayOrderId(), payOrder.getState(),
                channelRetMsg.getChannelOrderId(), channelRetMsg.getChannelUserId(), channelRetMsg.getChannelErrCode(), channelRetMsg.getChannelErrMsg());
        if (!isSuccess) {
            throw new BaseException("更新订单异常!");
        }

    }


    /**
     * 统一封装订单数据
     **/
    private ApiRes packageApiResByPayOrder(UnifiedOrderRQ bizRQ, UnifiedOrderRS bizRS, TPayOrder payOrder) {

        // 返回接口数据
        bizRS.setPayOrderId(payOrder.getPayOrderId());
        bizRS.setOrderState(payOrder.getState());
        bizRS.setMchOrderNo(payOrder.getMchOrderNo());

        if (payOrder.getState() == IOrderEnums.OrderStateEnums.STATE_FAIL.getState()) {
            bizRS.setErrCode(bizRS.getChannelRetMsg() != null ? bizRS.getChannelRetMsg().getChannelErrCode() : null);
            bizRS.setErrMsg(bizRS.getChannelRetMsg() != null ? bizRS.getChannelRetMsg().getChannelErrMsg() : null);
        }

        return ApiRes.okWithSign(bizRS, configContextQueryService.queryMchApp(bizRQ.getMchNo(), bizRQ.getAppId()).getAppSecret());
    }


}
