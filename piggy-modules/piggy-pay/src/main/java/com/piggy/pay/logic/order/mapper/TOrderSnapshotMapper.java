package com.piggy.pay.logic.order.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.order.domain.TOrderSnapshot;

/**
 * 订单接口数据快照Mapper接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface TOrderSnapshotMapper extends BaseMapperPlus<TOrderSnapshot> {

}
