package com.piggy.pay.logic.order.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.core.enums.ITransferEnums;
import com.piggy.pay.logic.order.bo.TTransferOrderQueryBo;
import com.piggy.pay.logic.order.domain.TTransferOrder;
import com.piggy.pay.logic.order.mapper.TTransferOrderMapper;
import com.piggy.pay.logic.order.service.ITTransferOrderService;
import com.piggy.pay.logic.order.vo.TTransferOrderVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.piggy.pay.logic.order.bo.TTransferOrderEditBo;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Collection;

/**
 * 转账订单Service业务层处理
 *
 * @author zito
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TTransferOrderServiceImpl extends ServiceImpl<TTransferOrderMapper, TTransferOrder> implements ITTransferOrderService {

    @Override
    public TTransferOrderVo queryById(String transferId){
        return getVoById(transferId, TTransferOrderVo.class);
    }

    @Override
    public TableDataInfo<TTransferOrderVo> queryPageList(TTransferOrderQueryBo bo) {
        PagePlus<TTransferOrder, TTransferOrderVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TTransferOrderVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TTransferOrderVo> queryList(TTransferOrderQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TTransferOrderVo.class);
    }

    private LambdaQueryWrapper<TTransferOrder> buildQueryWrapper(TTransferOrderQueryBo bo) {
        LambdaQueryWrapper<TTransferOrder> lqw = Wrappers.lambdaQuery();
        lqw.eq(StrUtil.isNotBlank(bo.getTransferId()), TTransferOrder::getTransferId, bo.getTransferId());
        lqw.eq(StrUtil.isNotBlank(bo.getMchNo()), TTransferOrder::getMchNo, bo.getMchNo());
        lqw.eq(StrUtil.isNotBlank(bo.getIsvNo()), TTransferOrder::getIsvNo, bo.getIsvNo());
        lqw.eq(StrUtil.isNotBlank(bo.getAppId()), TTransferOrder::getAppId, bo.getAppId());
        lqw.like(StrUtil.isNotBlank(bo.getMchName()), TTransferOrder::getMchName, bo.getMchName());
        lqw.eq(bo.getMchType() != null, TTransferOrder::getMchType, bo.getMchType());
        lqw.eq(StrUtil.isNotBlank(bo.getMchOrderNo()), TTransferOrder::getMchOrderNo, bo.getMchOrderNo());
        lqw.eq(StrUtil.isNotBlank(bo.getIfCode()), TTransferOrder::getIfCode, bo.getIfCode());
        lqw.eq(StrUtil.isNotBlank(bo.getEntryType()), TTransferOrder::getEntryType, bo.getEntryType());
        lqw.eq(bo.getAmount() != null, TTransferOrder::getAmount, bo.getAmount());
        lqw.eq(StrUtil.isNotBlank(bo.getCurrency()), TTransferOrder::getCurrency, bo.getCurrency());
        lqw.eq(StrUtil.isNotBlank(bo.getAccountNo()), TTransferOrder::getAccountNo, bo.getAccountNo());
        lqw.like(StrUtil.isNotBlank(bo.getAccountName()), TTransferOrder::getAccountName, bo.getAccountName());
        lqw.like(StrUtil.isNotBlank(bo.getBankName()), TTransferOrder::getBankName, bo.getBankName());
        lqw.eq(StrUtil.isNotBlank(bo.getTransferDesc()), TTransferOrder::getTransferDesc, bo.getTransferDesc());
        lqw.eq(StrUtil.isNotBlank(bo.getClientIp()), TTransferOrder::getClientIp, bo.getClientIp());
        lqw.eq(bo.getState() != null, TTransferOrder::getState, bo.getState());
        lqw.eq(StrUtil.isNotBlank(bo.getChannelExtra()), TTransferOrder::getChannelExtra, bo.getChannelExtra());
        lqw.eq(StrUtil.isNotBlank(bo.getChannelOrderNo()), TTransferOrder::getChannelOrderNo, bo.getChannelOrderNo());
        lqw.eq(StrUtil.isNotBlank(bo.getErrCode()), TTransferOrder::getErrCode, bo.getErrCode());
        lqw.eq(StrUtil.isNotBlank(bo.getErrMsg()), TTransferOrder::getErrMsg, bo.getErrMsg());
        lqw.eq(StrUtil.isNotBlank(bo.getExtParam()), TTransferOrder::getExtParam, bo.getExtParam());
        lqw.eq(StrUtil.isNotBlank(bo.getNotifyUrl()), TTransferOrder::getNotifyUrl, bo.getNotifyUrl());
        lqw.eq(bo.getSuccessTime() != null, TTransferOrder::getSuccessTime, bo.getSuccessTime());
        lqw.eq(bo.getCreatedAt() != null, TTransferOrder::getCreatedAt, bo.getCreatedAt());
        lqw.eq(bo.getUpdatedAt() != null, TTransferOrder::getUpdatedAt, bo.getUpdatedAt());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TTransferOrderEditBo bo) {
        TTransferOrder add = BeanUtil.toBean(bo, TTransferOrder.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TTransferOrderEditBo bo) {
        TTransferOrder update = BeanUtil.toBean(bo, TTransferOrder.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TTransferOrder entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

    /** 更新转账订单状态  【转账订单生成】 --》 【转账中】 **/
    @Override
    public boolean updateInit2Ing(String transferId){

        TTransferOrder updateRecord = new TTransferOrder();
        updateRecord.setState(ITransferEnums.TransferStateEnums.STATE_ING.getState());

        return update(updateRecord, new LambdaUpdateWrapper<TTransferOrder>()
                .eq(TTransferOrder::getTransferId, transferId).eq(TTransferOrder::getState, ITransferEnums.TransferStateEnums.STATE_INIT.getState()));
    }


    /** 更新转账订单状态  【转账中】 --》 【转账成功】 **/
    @Override
    @Transactional
    public boolean updateIng2Success(String transferId, String channelOrderNo){

        TTransferOrder updateRecord = new TTransferOrder();
        updateRecord.setState(ITransferEnums.TransferStateEnums.STATE_SUCCESS.getState());
        updateRecord.setChannelOrderNo(channelOrderNo);
        updateRecord.setSuccessTime(new Date());

        //更新转账订单表数据
        if(! update(updateRecord, new LambdaUpdateWrapper<TTransferOrder>()
                .eq(TTransferOrder::getTransferId, transferId).eq(TTransferOrder::getState, ITransferEnums.TransferStateEnums.STATE_ING.getState()))
        ){
            return false;
        }

        return true;
    }


    /** 更新转账订单状态  【转账中】 --》 【转账失败】 **/
    @Override
    @Transactional
    public boolean updateIng2Fail(String transferId, String channelOrderNo, String channelErrCode, String channelErrMsg){

        TTransferOrder updateRecord = new TTransferOrder();
        updateRecord.setState(ITransferEnums.TransferStateEnums.STATE_FAIL.getState());
        updateRecord.setErrCode(channelErrCode);
        updateRecord.setErrMsg(channelErrMsg);
        updateRecord.setChannelOrderNo(channelOrderNo);

        return update(updateRecord, new LambdaUpdateWrapper<TTransferOrder>()
                .eq(TTransferOrder::getTransferId, transferId).eq(TTransferOrder::getState, ITransferEnums.TransferStateEnums.STATE_ING.getState()));
    }


    /** 更新转账订单状态  【转账中】 --》 【转账成功/转账失败】 **/
    @Override
    @Transactional
    public boolean updateIng2SuccessOrFail(String transferId, Integer updateState, String channelOrderNo, String channelErrCode, String channelErrMsg){

        if(updateState == ITransferEnums.TransferStateEnums.STATE_ING.getState()){
            return true;
        }else if(updateState == ITransferEnums.TransferStateEnums.STATE_SUCCESS.getState()){
            return updateIng2Success(transferId, channelOrderNo);
        }else if(updateState == ITransferEnums.TransferStateEnums.STATE_FAIL.getState()){
            return updateIng2Fail(transferId, channelOrderNo, channelErrCode, channelErrMsg);
        }
        return false;
    }

    /** 查询商户订单 **/
    @Override
    public TTransferOrder queryMchOrder(String mchNo, String mchOrderNo, String transferId){

        if(StringUtils.isNotEmpty(transferId)){
            return getOne(new LambdaQueryWrapper<TTransferOrder>().eq(TTransferOrder::getMchNo, mchNo).eq(TTransferOrder::getTransferId, transferId));
        }else if(StringUtils.isNotEmpty(mchOrderNo)){
            return getOne(new LambdaQueryWrapper<TTransferOrder>().eq(TTransferOrder::getMchNo, mchNo).eq(TTransferOrder::getMchOrderNo, mchOrderNo));
        }else{
            return null;
        }
    }

    @Override
    public TableDataInfo<TTransferOrder> pageList(LambdaQueryWrapper<TTransferOrder> wrapper, TTransferOrder transferOrder, JSONObject paramJSON) {
        if (StringUtils.isNotEmpty(transferOrder.getTransferId())) {
            wrapper.eq(TTransferOrder::getTransferId, transferOrder.getTransferId());
        }
        if (StringUtils.isNotEmpty(transferOrder.getMchOrderNo())) {
            wrapper.eq(TTransferOrder::getMchOrderNo, transferOrder.getMchOrderNo());
        }
        if (StringUtils.isNotEmpty(transferOrder.getChannelOrderNo())) {
            wrapper.eq(TTransferOrder::getChannelOrderNo, transferOrder.getChannelOrderNo());
        }
        if (StringUtils.isNotEmpty(transferOrder.getMchNo())) {
            wrapper.eq(TTransferOrder::getMchNo, transferOrder.getMchNo());
        }
        if (transferOrder.getState() != null) {
            wrapper.eq(TTransferOrder::getState, transferOrder.getState());
        }
        if (StringUtils.isNotEmpty(transferOrder.getAppId())) {
            wrapper.eq(TTransferOrder::getAppId, transferOrder.getAppId());
        }
        if (paramJSON != null) {
            if (StringUtils.isNotEmpty(paramJSON.getString("createdStart"))) {
                wrapper.ge(TTransferOrder::getCreatedAt, paramJSON.getString("createdStart"));
            }
            if (StringUtils.isNotEmpty(paramJSON.getString("createdEnd"))) {
                wrapper.le(TTransferOrder::getCreatedAt, paramJSON.getString("createdEnd"));
            }
        }
        // 三合一订单
        if (paramJSON != null && StringUtils.isNotEmpty(paramJSON.getString("unionOrderId"))) {
            wrapper.and(wr -> {
                wr.eq(TTransferOrder::getTransferId, paramJSON.getString("unionOrderId"))
                        .or().eq(TTransferOrder::getMchOrderNo, paramJSON.getString("unionOrderId"))
                        .or().eq(TTransferOrder::getChannelOrderNo, paramJSON.getString("unionOrderId"));
            });
        }
        wrapper.orderByDesc(TTransferOrder::getCreatedAt);

        PagePlus<TTransferOrder, TTransferOrder> result = page(PageUtils.buildPagePlus(), wrapper);
        return PageUtils.buildDataInfo(result);

    }
    
}
