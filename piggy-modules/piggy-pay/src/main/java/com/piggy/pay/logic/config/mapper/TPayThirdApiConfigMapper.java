package com.piggy.pay.logic.config.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.config.domain.TPayThirdApiConfig;

/**
 * 支付接口配置参数Mapper接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface TPayThirdApiConfigMapper extends BaseMapperPlus<TPayThirdApiConfig> {

}
