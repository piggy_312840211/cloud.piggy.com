package com.piggy.pay.pls.model;

import com.piggy.pay.pls.ApiField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/***
* 转账查单请求实体类
*
* @author terrfly
* @site https://www.jeepay.vip
* @date 2021/8/16 16:08
*/
@Data
@NoArgsConstructor
public class TransferOrderQueryReqModel implements Serializable {

    private static final long serialVersionUID = -3998573128290306948L;

    @ApiField("mchNo")
    private String mchNo;      // 商户号
    @ApiField("appId")
    private String appId;      // 应用ID
    @ApiField("mchOrderNo")
    String mchOrderNo;          // 商户订单号
    @ApiField("transferId")
    String transferId;          // 支付平台订单号

}
