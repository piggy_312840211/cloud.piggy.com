package com.piggy.pay;

import com.piggy.common.satoken.annotation.EnableCustomConfig;
import com.piggy.common.satoken.annotation.EnableCustomFeignClients;
import com.piggy.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;


/**
 * @author liuyadu
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableCustomFeignClients
@RefreshScope
@SpringBootApplication
public class PIGYPayApplication {

    public static void main(String[] args) {
        SpringApplication.run(PIGYPayApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  支付结算中心启动成功   ლ(´ڡ`ლ)ﾞ  \n" );
    }

}
