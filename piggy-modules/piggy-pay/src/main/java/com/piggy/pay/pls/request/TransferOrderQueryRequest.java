package com.piggy.pay.pls.request;

import com.piggy.pay.pls.BasePay;
import com.piggy.pay.pls.model.TransferOrderQueryReqModel;
import com.piggy.pay.pls.net.RequestOptions;
import com.piggy.pay.pls.response.TransferOrderQueryResponse;

/***
* Jeepay转账查询请求实现
*
* @author terrfly
* @site https://www.jeepay.vip
* @date 2021/8/16 16:26
*/
public class TransferOrderQueryRequest implements PayRequest<TransferOrderQueryResponse, TransferOrderQueryReqModel> {

    private String apiVersion = BasePay.VERSION;
    private String apiUri = "api/transfer/query";
    private RequestOptions options;
    private TransferOrderQueryReqModel bizModel = null;

    @Override
    public String getApiUri() {
        return this.apiUri;
    }

    @Override
    public String getApiVersion() {
        return this.apiVersion;
    }

    @Override
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    @Override
    public RequestOptions getRequestOptions() {
        return this.options;
    }

    @Override
    public void setRequestOptions(RequestOptions options) {
        this.options = options;
    }

    @Override
    public TransferOrderQueryReqModel getBizModel() {
        return this.bizModel;
    }

    @Override
    public void setBizModel(TransferOrderQueryReqModel bizModel) {
        this.bizModel = bizModel;
    }

    @Override
    public Class<TransferOrderQueryResponse> getResponseClass() {
        return TransferOrderQueryResponse.class;
    }

}
