package com.piggy.pay.logic.order.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.core.enums.IDivisionEnums;
import com.piggy.pay.core.utils.SeqKit;
import com.piggy.pay.logic.order.bo.TPayOrderDivisionRecordEditBo;
import com.piggy.pay.logic.order.bo.TPayOrderDivisionRecordQueryBo;
import com.piggy.pay.logic.order.domain.TPayOrder;
import com.piggy.pay.logic.order.domain.TPayOrderDivisionRecord;
import com.piggy.pay.logic.order.mapper.TPayOrderDivisionRecordMapper;
import com.piggy.pay.logic.order.mapper.TPayOrderDivisionRecordMapperEx;
import com.piggy.pay.logic.order.service.ITPayOrderDivisionRecordService;
import com.piggy.pay.logic.order.service.ITPayOrderService;
import com.piggy.pay.logic.order.vo.TPayOrderDivisionRecordVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;

/**
 * 分账记录Service业务层处理
 *
 * @author zito
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TPayOrderDivisionRecordServiceImpl extends ServiceImpl<TPayOrderDivisionRecordMapper, TPayOrderDivisionRecord> implements ITPayOrderDivisionRecordService {

    @Resource
    private ITPayOrderService payOrderService;

    @Resource
    private TPayOrderDivisionRecordMapperEx divisionRecordMapperEx;

    @Override
    public TPayOrderDivisionRecordVo queryById(Long recordId){
        return getVoById(recordId, TPayOrderDivisionRecordVo.class);
    }

    @Override
    public TableDataInfo<TPayOrderDivisionRecordVo> queryPageList(TPayOrderDivisionRecordQueryBo bo) {
        PagePlus<TPayOrderDivisionRecord, TPayOrderDivisionRecordVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TPayOrderDivisionRecordVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TPayOrderDivisionRecordVo> queryList(TPayOrderDivisionRecordQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TPayOrderDivisionRecordVo.class);
    }

    private LambdaQueryWrapper<TPayOrderDivisionRecord> buildQueryWrapper(TPayOrderDivisionRecordQueryBo bo) {
        LambdaQueryWrapper<TPayOrderDivisionRecord> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getRecordId() != null, TPayOrderDivisionRecord::getRecordId, bo.getRecordId());
        lqw.eq(StrUtil.isNotBlank(bo.getMchNo()), TPayOrderDivisionRecord::getMchNo, bo.getMchNo());
        lqw.eq(StrUtil.isNotBlank(bo.getIsvNo()), TPayOrderDivisionRecord::getIsvNo, bo.getIsvNo());
        lqw.eq(StrUtil.isNotBlank(bo.getAppId()), TPayOrderDivisionRecord::getAppId, bo.getAppId());
        lqw.like(StrUtil.isNotBlank(bo.getMchName()), TPayOrderDivisionRecord::getMchName, bo.getMchName());
        lqw.eq(bo.getMchType() != null, TPayOrderDivisionRecord::getMchType, bo.getMchType());
        lqw.eq(StrUtil.isNotBlank(bo.getIfCode()), TPayOrderDivisionRecord::getIfCode, bo.getIfCode());
        lqw.eq(StrUtil.isNotBlank(bo.getPayOrderId()), TPayOrderDivisionRecord::getPayOrderId, bo.getPayOrderId());
        lqw.eq(StrUtil.isNotBlank(bo.getPayOrderChannelOrderNo()), TPayOrderDivisionRecord::getPayOrderChannelOrderNo, bo.getPayOrderChannelOrderNo());
        lqw.eq(bo.getPayOrderAmount() != null, TPayOrderDivisionRecord::getPayOrderAmount, bo.getPayOrderAmount());
        lqw.eq(bo.getPayOrderDivisionAmount() != null, TPayOrderDivisionRecord::getPayOrderDivisionAmount, bo.getPayOrderDivisionAmount());
        lqw.eq(StrUtil.isNotBlank(bo.getBatchOrderId()), TPayOrderDivisionRecord::getBatchOrderId, bo.getBatchOrderId());
        lqw.eq(StrUtil.isNotBlank(bo.getChannelBatchOrderId()), TPayOrderDivisionRecord::getChannelBatchOrderId, bo.getChannelBatchOrderId());
        lqw.eq(bo.getState() != null, TPayOrderDivisionRecord::getState, bo.getState());
        lqw.eq(StrUtil.isNotBlank(bo.getChannelRespResult()), TPayOrderDivisionRecord::getChannelRespResult, bo.getChannelRespResult());
        lqw.eq(bo.getReceiverId() != null, TPayOrderDivisionRecord::getReceiverId, bo.getReceiverId());
        lqw.eq(bo.getReceiverGroupId() != null, TPayOrderDivisionRecord::getReceiverGroupId, bo.getReceiverGroupId());
        lqw.eq(StrUtil.isNotBlank(bo.getReceiverAlias()), TPayOrderDivisionRecord::getReceiverAlias, bo.getReceiverAlias());
        lqw.eq(bo.getAccType() != null, TPayOrderDivisionRecord::getAccType, bo.getAccType());
        lqw.eq(StrUtil.isNotBlank(bo.getAccNo()), TPayOrderDivisionRecord::getAccNo, bo.getAccNo());
        lqw.like(StrUtil.isNotBlank(bo.getAccName()), TPayOrderDivisionRecord::getAccName, bo.getAccName());
        lqw.eq(StrUtil.isNotBlank(bo.getRelationType()), TPayOrderDivisionRecord::getRelationType, bo.getRelationType());
        lqw.like(StrUtil.isNotBlank(bo.getRelationTypeName()), TPayOrderDivisionRecord::getRelationTypeName, bo.getRelationTypeName());
        lqw.eq(bo.getDivisionProfit() != null, TPayOrderDivisionRecord::getDivisionProfit, bo.getDivisionProfit());
        lqw.eq(bo.getCalDivisionAmount() != null, TPayOrderDivisionRecord::getCalDivisionAmount, bo.getCalDivisionAmount());
        lqw.eq(bo.getCreatedAt() != null, TPayOrderDivisionRecord::getCreatedAt, bo.getCreatedAt());
        lqw.eq(bo.getUpdatedAt() != null, TPayOrderDivisionRecord::getUpdatedAt, bo.getUpdatedAt());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TPayOrderDivisionRecordEditBo bo) {
        TPayOrderDivisionRecord add = BeanUtil.toBean(bo, TPayOrderDivisionRecord.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TPayOrderDivisionRecordEditBo bo) {
        TPayOrderDivisionRecord update = BeanUtil.toBean(bo, TPayOrderDivisionRecord.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TPayOrderDivisionRecord entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

    /** 更新分账记录为分账成功  ( 单条 )  将：  已受理 更新为： 其他状态    **/
    @Override
    public void updateRecordSuccessOrFailBySingleItem(Long recordId, Integer state, String channelRespResult){

        TPayOrderDivisionRecord updateRecord = new TPayOrderDivisionRecord();
        updateRecord.setState(state);
        updateRecord.setChannelRespResult( state == IDivisionEnums.OrderDivisionStateEnums.STATE_SUCCESS.getState() ? "" : channelRespResult); // 若明确成功，清空错误信息。
        update(updateRecord, new LambdaQueryWrapper<TPayOrderDivisionRecord>().eq(TPayOrderDivisionRecord::getRecordId, recordId).eq(TPayOrderDivisionRecord::getState, IDivisionEnums.OrderDivisionStateEnums.STATE_ACCEPT.getState()));

    }


    /** 更新分账记录为分账成功**/
    @Override
    public void updateRecordSuccessOrFail(List<TPayOrderDivisionRecord> records, Integer state, String channelBatchOrderId, String channelRespResult){

        if(records == null || records.isEmpty()){
            return ;
        }

        List<Long> recordIds = new ArrayList<>();
        records.stream().forEach(r -> recordIds.add(r.getRecordId()));

        TPayOrderDivisionRecord updateRecord = new TPayOrderDivisionRecord();
        updateRecord.setState(state);
        updateRecord.setChannelBatchOrderId(channelBatchOrderId);
        updateRecord.setChannelRespResult(channelRespResult);
        update(updateRecord, new LambdaQueryWrapper<TPayOrderDivisionRecord>().in(TPayOrderDivisionRecord::getRecordId, recordIds).eq(TPayOrderDivisionRecord::getState, IDivisionEnums.OrderDivisionStateEnums.STATE_WAIT.getState()));

    }

    /** 更新分账订单为： 等待分账中的状态  **/
    @Override
    @Transactional
    public void updateResendState(String payOrderId){

        TPayOrder updateRecord = new TPayOrder();
        updateRecord.setDivisionState(IDivisionEnums.DivisionStateEnums.DIVISION_STATE_WAIT_TASK.getState());

        // 更新订单
        boolean payOrderUpdateRow = payOrderService.update(updateRecord, new LambdaQueryWrapper<TPayOrder>().eq(TPayOrder::getPayOrderId, payOrderId).eq(TPayOrder::getDivisionState, IDivisionEnums.DivisionStateEnums.DIVISION_STATE_FINISH.getState()));

        if(!payOrderUpdateRow){
            throw new BaseException("更新订单分账状态失败");
        }

        TPayOrderDivisionRecord updateRecordByDiv = new TPayOrderDivisionRecord();
        updateRecordByDiv.setBatchOrderId(SeqKit.genDivisionBatchId()); // 重新生成batchOrderId, 避免部分失败导致： out_trade_no重复。
        updateRecordByDiv.setState(IDivisionEnums.OrderDivisionStateEnums.STATE_WAIT.getState()); //待分账
        updateRecordByDiv.setChannelRespResult("");
        updateRecordByDiv.setChannelBatchOrderId("");
        boolean recordUpdateFlag = update(updateRecordByDiv,
                new LambdaQueryWrapper<TPayOrderDivisionRecord>().eq(TPayOrderDivisionRecord::getPayOrderId, payOrderId).eq(TPayOrderDivisionRecord::getState, IDivisionEnums.OrderDivisionStateEnums.STATE_FAIL.getState())
        );

        if(!recordUpdateFlag){
            throw new BaseException("更新分账记录状态失败");
        }
    }

    @Override
    public TPayOrderDivisionRecordMapperEx getBaseMapperEx() {
        return divisionRecordMapperEx;
    }

}
