package com.piggy.pay.logic.order.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.order.domain.TPayOrder;

/**
 * 支付订单Mapper接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface TPayOrderMapper extends BaseMapperPlus<TPayOrder> {

}
