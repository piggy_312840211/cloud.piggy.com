package com.piggy.pay.logic.config.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 支付接口定义视图对象 t_pay_third_api_define
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("支付接口定义视图对象")
public class TPayThirdApiDefineVo {

	private static final long serialVersionUID = 1L;

	/** 接口代码 全小写  wxpay alipay  */
	@Excel(name = "接口代码 全小写  wxpay alipay ")
	@ApiModelProperty("接口代码 全小写  wxpay alipay ")
	private String ifCode;

	/** 接口名称 */
	@Excel(name = "接口名称")
	@ApiModelProperty("接口名称")
	private String ifName;

	/** 是否支持普通商户模式: 0-不支持, 1-支持 */
	@Excel(name = "是否支持普通商户模式: 0-不支持, 1-支持")
	@ApiModelProperty("是否支持普通商户模式: 0-不支持, 1-支持")
	private Integer isMchMode;

	/** 是否支持服务商子商户模式: 0-不支持, 1-支持 */
	@Excel(name = "是否支持服务商子商户模式: 0-不支持, 1-支持")
	@ApiModelProperty("是否支持服务商子商户模式: 0-不支持, 1-支持")
	private Integer isIsvMode;

	/** 支付参数配置页面类型:1-JSON渲染,2-自定义 */
	@Excel(name = "支付参数配置页面类型:1-JSON渲染,2-自定义")
	@ApiModelProperty("支付参数配置页面类型:1-JSON渲染,2-自定义")
	private Integer configPageType;

	/** ISV接口配置定义描述,json字符串 */
	@Excel(name = "ISV接口配置定义描述,json字符串")
	@ApiModelProperty("ISV接口配置定义描述,json字符串")
	private String isvParams;

	/** 特约商户接口配置定义描述,json字符串 */
	@Excel(name = "特约商户接口配置定义描述,json字符串")
	@ApiModelProperty("特约商户接口配置定义描述,json字符串")
	private String isvsubMchParams;

	/** 普通商户接口配置定义描述,json字符串 */
	@Excel(name = "普通商户接口配置定义描述,json字符串")
	@ApiModelProperty("普通商户接口配置定义描述,json字符串")
	private String normalMchParams;

	/** 支持的支付方式 ["wxpay_jsapi", "wxpay_bar"] */
	@Excel(name = "支持的支付方式 [wxpay_jsapi, wxpay_bar]")
	@ApiModelProperty("支持的支付方式 [wxpay_jsapi, wxpay_bar]")
	private String wayCodes;

	/** 页面展示：卡片-图标 */
	@Excel(name = "页面展示：卡片-图标")
	@ApiModelProperty("页面展示：卡片-图标")
	private String icon;

	/** 页面展示：卡片-背景色 */
	@Excel(name = "页面展示：卡片-背景色")
	@ApiModelProperty("页面展示：卡片-背景色")
	private String bgColor;

	/** 状态: 0-停用, 1-启用 */
	@Excel(name = "状态: 0-停用, 1-启用")
	@ApiModelProperty("状态: 0-停用, 1-启用")
	private Integer state;

	/** 备注 */
	@Excel(name = "备注")
	@ApiModelProperty("备注")
	private String remark;

	/** 创建时间 */
	@Excel(name = "创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;

	/** 更新时间 */
	@Excel(name = "更新时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;


}
