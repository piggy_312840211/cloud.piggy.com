package com.piggy.pay.logic.order.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import com.piggy.pay.logic.config.bo.TRefundOrderEditBo;
import com.piggy.pay.logic.config.bo.TRefundOrderQueryBo;
import com.piggy.pay.logic.order.service.ITRefundOrderService;
import com.piggy.pay.logic.order.vo.TRefundOrderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 退款订单Controller
 *
 * @author piggy
 * @date 2023-05-25
 */
@Api(value = "退款订单控制器", tags = {"退款订单管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/refundorder")
public class TRefundOrderController extends BaseController {

    private final ITRefundOrderService iTRefundOrderService;

    /**
     * 查询退款订单列表
     */
    @ApiOperation("查询退款订单列表")
    @SaCheckPermission("order:refundorder:list")
    @GetMapping("/list")
    public TableDataInfo<TRefundOrderVo> list(@Validated TRefundOrderQueryBo bo) {
        return iTRefundOrderService.queryPageList(bo);
    }

    /**
     * 导出退款订单列表
     */
    @ApiOperation("导出退款订单列表")
    @SaCheckPermission("order:refundorder:export")
    @Log(title = "退款订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TRefundOrderQueryBo bo, HttpServletResponse response) throws IOException {
        List<TRefundOrderVo> list = iTRefundOrderService.queryList(bo);
        ExcelUtil<TRefundOrderVo> util = new ExcelUtil<TRefundOrderVo>(TRefundOrderVo.class);
        util.exportExcel(response, list, "退款订单");
    }

    /**
     * 获取退款订单详细信息
     */
    @ApiOperation("获取退款订单详细信息")
    @SaCheckPermission("order:refundorder:query")
    @GetMapping("/{refundOrderId}")
    public R<TRefundOrderVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("refundOrderId") String refundOrderId) {
        return R.ok(iTRefundOrderService.queryById(refundOrderId));
    }

    /**
     * 新增退款订单
     */
    @ApiOperation("新增退款订单")
    @SaCheckPermission("order:refundorder:add")
    @Log(title = "退款订单", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TRefundOrderEditBo bo) {
        return R.ok(iTRefundOrderService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改退款订单
     */
    @ApiOperation("修改退款订单")
    @SaCheckPermission("order:refundorder:edit")
    @Log(title = "退款订单", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TRefundOrderEditBo bo) {
        return R.ok(iTRefundOrderService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除退款订单
     */
    @ApiOperation("删除退款订单")
    @SaCheckPermission("order:refundorder:remove")
    @Log(title = "退款订单" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{refundOrderIds}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable String[] refundOrderIds) {
        return R.ok(iTRefundOrderService.deleteWithValidByIds(Arrays.asList(refundOrderIds), true) ? 1 : 0);
    }
}
