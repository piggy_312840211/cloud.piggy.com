package com.piggy.pay.logic.config.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.core.constants.CS;
import com.piggy.pay.logic.config.bo.TPayThirdApiConfigEditBo;
import com.piggy.pay.logic.config.bo.TPayThirdApiConfigQueryBo;
import com.piggy.pay.logic.config.domain.TPayThirdApiConfig;
import com.piggy.pay.logic.config.mapper.TPayThirdApiConfigMapper;
import com.piggy.pay.logic.config.vo.TPayThirdApiConfigVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.piggy.pay.logic.config.service.ITPayThirdApiConfigService;

import java.util.List;
import java.util.Collection;

/**
 * 支付接口配置参数Service业务层处理
 *
 * @author piggy
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TPayThirdApiConfigServiceImpl extends ServiceImpl<TPayThirdApiConfigMapper, TPayThirdApiConfig> implements ITPayThirdApiConfigService {

    @Override
    public TPayThirdApiConfigVo queryById(Long id){
        return getVoById(id, TPayThirdApiConfigVo.class);
    }

    @Override
    public TableDataInfo<TPayThirdApiConfigVo> queryPageList(TPayThirdApiConfigQueryBo bo) {
        PagePlus<TPayThirdApiConfig, TPayThirdApiConfigVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TPayThirdApiConfigVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TPayThirdApiConfigVo> queryList(TPayThirdApiConfigQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TPayThirdApiConfigVo.class);
    }

    private LambdaQueryWrapper<TPayThirdApiConfig> buildQueryWrapper(TPayThirdApiConfigQueryBo bo) {
        LambdaQueryWrapper<TPayThirdApiConfig> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getId() != null, TPayThirdApiConfig::getId, bo.getId());
        lqw.eq(bo.getInfoType() != null, TPayThirdApiConfig::getInfoType, bo.getInfoType());
        lqw.eq(StrUtil.isNotBlank(bo.getInfoId()), TPayThirdApiConfig::getInfoId, bo.getInfoId());
        lqw.eq(StrUtil.isNotBlank(bo.getIfCode()), TPayThirdApiConfig::getIfCode, bo.getIfCode());
        lqw.eq(StrUtil.isNotBlank(bo.getIfParams()), TPayThirdApiConfig::getIfParams, bo.getIfParams());
        lqw.eq(bo.getIfRate() != null, TPayThirdApiConfig::getIfRate, bo.getIfRate());
        lqw.eq(bo.getState() != null, TPayThirdApiConfig::getState, bo.getState());
        lqw.eq(bo.getCreatedUid() != null, TPayThirdApiConfig::getCreatedUid, bo.getCreatedUid());
        lqw.eq(StrUtil.isNotBlank(bo.getCreatedBy()), TPayThirdApiConfig::getCreatedBy, bo.getCreatedBy());
        lqw.eq(bo.getCreatedAt() != null, TPayThirdApiConfig::getCreatedAt, bo.getCreatedAt());
        lqw.eq(bo.getUpdatedUid() != null, TPayThirdApiConfig::getUpdatedUid, bo.getUpdatedUid());
        lqw.eq(StrUtil.isNotBlank(bo.getUpdatedBy()), TPayThirdApiConfig::getUpdatedBy, bo.getUpdatedBy());
        lqw.eq(bo.getUpdatedAt() != null, TPayThirdApiConfig::getUpdatedAt, bo.getUpdatedAt());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TPayThirdApiConfigEditBo bo) {
        TPayThirdApiConfig add = BeanUtil.toBean(bo, TPayThirdApiConfig.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TPayThirdApiConfigEditBo bo) {
        TPayThirdApiConfig update = BeanUtil.toBean(bo, TPayThirdApiConfig.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TPayThirdApiConfig entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

    @Override
    public TPayThirdApiConfig getByInfoIdAndIfCode(Integer infoType, String infoId, String ifCode) {
        return getOne(new LambdaQueryWrapper<TPayThirdApiConfig>()
                .eq(TPayThirdApiConfig::getInfoType, infoType)
                .eq(TPayThirdApiConfig::getInfoId, infoId)
                .eq(TPayThirdApiConfig::getIfCode, ifCode));
    }

    /** 查询商户app使用已正确配置了通道信息 */
    @Override
    public boolean mchAppHasAvailableIfCode(String appId, String ifCode){

        return this.count(new LambdaQueryWrapper<TPayThirdApiConfig>()
                        .eq(TPayThirdApiConfig::getIfCode, ifCode)
                        .eq(TPayThirdApiConfig::getState, CS.PUB_USABLE)
                        .eq(TPayThirdApiConfig::getInfoId, appId)
                        .eq(TPayThirdApiConfig::getInfoType, CS.INFO_TYPE_MCH_APP)
        ) > 0;

    }

}
