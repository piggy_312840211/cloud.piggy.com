/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.piggy.pay.payment.service;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.piggy.mq.vender.IMQSender;
import com.piggy.pay.components.mq.model.PayOrderDivisionMQ;
import com.piggy.pay.core.constants.CS;
import com.piggy.pay.core.enums.IDivisionEnums;
import com.piggy.pay.core.enums.IOrderEnums;
import com.piggy.pay.logic.order.domain.TPayOrder;
import com.piggy.pay.logic.order.service.ITPayOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/***
 * 订单处理通用逻辑
 *
 * @author terrfly
 * @site https://www.jeequan.com
 * @date 2021/8/22 16:50
 */
@Service
@Slf4j
public class PayOrderProcessService {


    @Resource
    private ITPayOrderService payOrderService;
    @Resource
    private PayMchNotifyService payMchNotifyService;
    @Resource
    private IMQSender mqSender;

    /**
     * 明确成功的处理逻辑（除更新订单其他业务）
     **/
    public void confirmSuccess(TPayOrder payOrder) {

        // 查询查询订单详情
        payOrder = payOrderService.getById(payOrder.getPayOrderId());

        //设置订单状态
        payOrder.setState(IOrderEnums.OrderStateEnums.STATE_SUCCESS.getState());

        //自动分账 处理逻辑， 不影响主订单任务
        this.updatePayOrderAutoDivision(payOrder);

        //发送商户通知
        payMchNotifyService.payOrderNotify(payOrder);

    }

    /**
     * 更新订单自动分账业务
     **/
    private void updatePayOrderAutoDivision(TPayOrder payOrder) {

        try {

            //默认不分账  || 其他非【自动分账】逻辑时， 不处理
            if (payOrder == null || payOrder.getDivisionMode() == null || payOrder.getDivisionMode() != IDivisionEnums.DivisionModeEnums.DIVISION_MODE_AUTO.getMode()) {
                return;
            }

            //更新订单表分账状态为： 等待分账任务处理
            boolean updDivisionState = payOrderService.update(new LambdaUpdateWrapper<TPayOrder>()
                    .set(TPayOrder::getDivisionState, IDivisionEnums.DivisionStateEnums.DIVISION_STATE_WAIT_TASK.getState())
                    .eq(TPayOrder::getPayOrderId, payOrder.getPayOrderId())
                    .eq(TPayOrder::getDivisionState, IDivisionEnums.DivisionStateEnums.DIVISION_STATE_UNHAPPEN.getState())
            );

            if (updDivisionState) {
                //推送到分账MQ
                mqSender.send(PayOrderDivisionMQ.build(payOrder.getPayOrderId(), CS.YES, null), 80); //80s 后执行
            }

        } catch (Exception e) {
            log.error("订单[{}]自动分账逻辑异常：", payOrder.getPayOrderId(), e);
        }
    }

}
