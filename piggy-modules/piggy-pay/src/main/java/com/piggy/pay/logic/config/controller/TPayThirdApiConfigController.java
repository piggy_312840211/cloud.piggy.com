package com.piggy.pay.logic.config.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import com.piggy.pay.logic.config.bo.TPayThirdApiConfigEditBo;
import com.piggy.pay.logic.config.bo.TPayThirdApiConfigQueryBo;
import com.piggy.pay.logic.config.service.ITPayThirdApiConfigService;
import com.piggy.pay.logic.config.vo.TPayThirdApiConfigVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 支付接口配置参数Controller
 *
 * @author piggy
 * @date 2023-05-25
 */
@Api(value = "支付接口配置参数控制器", tags = {"支付接口配置参数管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/config/apiconfig")
public class TPayThirdApiConfigController extends BaseController {

    private final ITPayThirdApiConfigService iTPayThirdApiConfigService;

    /**
     * 查询支付接口配置参数列表
     */
    @ApiOperation("查询支付接口配置参数列表")
    @SaCheckPermission("config:apiconfig:list")
    @GetMapping("/list")
    public TableDataInfo<TPayThirdApiConfigVo> list(@Validated TPayThirdApiConfigQueryBo bo) {
        return iTPayThirdApiConfigService.queryPageList(bo);
    }

    /**
     * 导出支付接口配置参数列表
     */
    @ApiOperation("导出支付接口配置参数列表")
    @SaCheckPermission("config:apiconfig:export")
    @Log(title = "支付接口配置参数", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TPayThirdApiConfigQueryBo bo, HttpServletResponse response) throws IOException {
        List<TPayThirdApiConfigVo> list = iTPayThirdApiConfigService.queryList(bo);
        ExcelUtil<TPayThirdApiConfigVo> util = new ExcelUtil<TPayThirdApiConfigVo>(TPayThirdApiConfigVo.class);
        util.exportExcel(response, list, "支付接口配置参数");
    }

    /**
     * 获取支付接口配置参数详细信息
     */
    @ApiOperation("获取支付接口配置参数详细信息")
    @SaCheckPermission("config:apiconfig:query")
    @GetMapping("/{id}")
    public R<TPayThirdApiConfigVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("id") Long id) {
        return R.ok(iTPayThirdApiConfigService.queryById(id));
    }

    /**
     * 新增支付接口配置参数
     */
    @ApiOperation("新增支付接口配置参数")
    @SaCheckPermission("config:apiconfig:add")
    @Log(title = "支付接口配置参数", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TPayThirdApiConfigEditBo bo) {
        return R.ok(iTPayThirdApiConfigService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改支付接口配置参数
     */
    @ApiOperation("修改支付接口配置参数")
    @SaCheckPermission("config:apiconfig:edit")
    @Log(title = "支付接口配置参数", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TPayThirdApiConfigEditBo bo) {
        return R.ok(iTPayThirdApiConfigService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除支付接口配置参数
     */
    @ApiOperation("删除支付接口配置参数")
    @SaCheckPermission("config:apiconfig:remove")
    @Log(title = "支付接口配置参数" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable Long[] ids) {
        return R.ok(iTPayThirdApiConfigService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
