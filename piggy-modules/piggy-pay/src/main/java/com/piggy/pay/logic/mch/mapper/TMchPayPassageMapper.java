package com.piggy.pay.logic.mch.mapper;


import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.mch.domain.TMchPayPassage;

/**
 * 商户支付通道Mapper接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface TMchPayPassageMapper extends BaseMapperPlus<TMchPayPassage> {

}
