package com.piggy.pay.logic.config.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.config.domain.TPayThirdApiDefine;
import com.piggy.pay.logic.config.vo.TPayThirdApiDefineVo;
import com.piggy.pay.logic.config.bo.TPayThirdApiDefineQueryBo;
import com.piggy.pay.logic.config.bo.TPayThirdApiDefineEditBo;

import java.util.Collection;
import java.util.List;

/**
 * 支付接口定义Service接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface ITPayThirdApiDefineService extends IServicePlus<TPayThirdApiDefine> {
    /**
     * 查询单个
     *
     * @return
     */
    TPayThirdApiDefineVo queryById(String ifCode);

    /**
     * 查询列表
     */
    TableDataInfo<TPayThirdApiDefineVo> queryPageList(TPayThirdApiDefineQueryBo bo);

    /**
     * 查询列表
     */
    List<TPayThirdApiDefineVo> queryList(TPayThirdApiDefineQueryBo bo);

    /**
     * 根据新增业务对象插入支付接口定义
     *
     * @param bo 支付接口定义新增业务对象
     * @return
     */
    Boolean insertByAddBo(TPayThirdApiDefineEditBo bo);

    /**
     * 根据编辑业务对象修改支付接口定义
     *
     * @param bo 支付接口定义编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TPayThirdApiDefineEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);
}
