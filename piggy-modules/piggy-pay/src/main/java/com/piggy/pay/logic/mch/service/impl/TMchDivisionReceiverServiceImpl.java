package com.piggy.pay.logic.mch.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.mch.bo.TMchDivisionReceiverEditBo;
import com.piggy.pay.logic.mch.bo.TMchDivisionReceiverQueryBo;
import com.piggy.pay.logic.mch.domain.TMchDivisionReceiver;
import com.piggy.pay.logic.mch.mapper.TMchDivisionReceiverMapper;
import com.piggy.pay.logic.mch.service.ITMchDivisionReceiverService;
import com.piggy.pay.logic.mch.vo.TMchDivisionReceiverVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
import java.util.Collection;

/**
 * 商户分账接收者账号绑定关系Service业务层处理
 *
 * @author piggy
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TMchDivisionReceiverServiceImpl extends ServiceImpl<TMchDivisionReceiverMapper, TMchDivisionReceiver> implements ITMchDivisionReceiverService {

    @Override
    public TMchDivisionReceiverVo queryById(Long receiverId){
        return getVoById(receiverId, TMchDivisionReceiverVo.class);
    }

    @Override
    public TableDataInfo<TMchDivisionReceiverVo> queryPageList(TMchDivisionReceiverQueryBo bo) {
        PagePlus<TMchDivisionReceiver, TMchDivisionReceiverVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TMchDivisionReceiverVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TMchDivisionReceiverVo> queryList(TMchDivisionReceiverQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TMchDivisionReceiverVo.class);
    }

    private LambdaQueryWrapper<TMchDivisionReceiver> buildQueryWrapper(TMchDivisionReceiverQueryBo bo) {
        LambdaQueryWrapper<TMchDivisionReceiver> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getReceiverId() != null, TMchDivisionReceiver::getReceiverId, bo.getReceiverId());
        lqw.eq(StrUtil.isNotBlank(bo.getReceiverAlias()), TMchDivisionReceiver::getReceiverAlias, bo.getReceiverAlias());
        lqw.eq(bo.getReceiverGroupId() != null, TMchDivisionReceiver::getReceiverGroupId, bo.getReceiverGroupId());
        lqw.like(StrUtil.isNotBlank(bo.getReceiverGroupName()), TMchDivisionReceiver::getReceiverGroupName, bo.getReceiverGroupName());
        lqw.eq(StrUtil.isNotBlank(bo.getMchNo()), TMchDivisionReceiver::getMchNo, bo.getMchNo());
        lqw.eq(StrUtil.isNotBlank(bo.getIsvNo()), TMchDivisionReceiver::getIsvNo, bo.getIsvNo());
        lqw.eq(StrUtil.isNotBlank(bo.getAppId()), TMchDivisionReceiver::getAppId, bo.getAppId());
        lqw.eq(StrUtil.isNotBlank(bo.getIfCode()), TMchDivisionReceiver::getIfCode, bo.getIfCode());
        lqw.eq(bo.getAccType() != null, TMchDivisionReceiver::getAccType, bo.getAccType());
        lqw.eq(StrUtil.isNotBlank(bo.getAccNo()), TMchDivisionReceiver::getAccNo, bo.getAccNo());
        lqw.like(StrUtil.isNotBlank(bo.getAccName()), TMchDivisionReceiver::getAccName, bo.getAccName());
        lqw.eq(StrUtil.isNotBlank(bo.getRelationType()), TMchDivisionReceiver::getRelationType, bo.getRelationType());
        lqw.like(StrUtil.isNotBlank(bo.getRelationTypeName()), TMchDivisionReceiver::getRelationTypeName, bo.getRelationTypeName());
        lqw.eq(bo.getDivisionProfit() != null, TMchDivisionReceiver::getDivisionProfit, bo.getDivisionProfit());
        lqw.eq(bo.getState() != null, TMchDivisionReceiver::getState, bo.getState());
        lqw.eq(StrUtil.isNotBlank(bo.getChannelBindResult()), TMchDivisionReceiver::getChannelBindResult, bo.getChannelBindResult());
        lqw.eq(StrUtil.isNotBlank(bo.getChannelExtInfo()), TMchDivisionReceiver::getChannelExtInfo, bo.getChannelExtInfo());
        lqw.eq(bo.getBindSuccessTime() != null, TMchDivisionReceiver::getBindSuccessTime, bo.getBindSuccessTime());
        lqw.eq(bo.getCreatedAt() != null, TMchDivisionReceiver::getCreatedAt, bo.getCreatedAt());
        lqw.eq(bo.getUpdatedAt() != null, TMchDivisionReceiver::getUpdatedAt, bo.getUpdatedAt());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TMchDivisionReceiverEditBo bo) {
        TMchDivisionReceiver add = BeanUtil.toBean(bo, TMchDivisionReceiver.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TMchDivisionReceiverEditBo bo) {
        TMchDivisionReceiver update = BeanUtil.toBean(bo, TMchDivisionReceiver.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TMchDivisionReceiver entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
