package com.piggy.pay.logic.mch.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 商户支付通道分页查询对象 t_mch_pay_passage
 *
 * @author piggy
 * @date 2023-05-25
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel("商户支付通道分页查询对象")
public class TMchPayPassageQueryBo extends BaseEntity {

	/** ID */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("ID")
	private Long id;
	/** 商户号 */
	@ApiModelProperty("商户号")
	private String mchNo;
	/** 应用ID */
	@ApiModelProperty("应用ID")
	private String appId;
	/** 支付接口 */
	@ApiModelProperty("支付接口")
	private String ifCode;
	/** 支付方式 */
	@ApiModelProperty("支付方式")
	private String wayCode;
	/** 支付方式费率 */
	@ApiModelProperty("支付方式费率")
	private BigDecimal rate;
	/** 风控数据 */
	@ApiModelProperty("风控数据")
	private String riskConfig;
	/** 状态: 0-停用, 1-启用 */
	@ApiModelProperty("状态: 0-停用, 1-启用")
	private Integer state;
	/** 创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;
	/** 更新时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;

}
