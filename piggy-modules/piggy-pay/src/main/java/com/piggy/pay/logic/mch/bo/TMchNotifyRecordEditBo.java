package com.piggy.pay.logic.mch.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 商户通知记录编辑对象 t_mch_notify_record
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("商户通知记录编辑对象")
public class TMchNotifyRecordEditBo {


    /** 商户通知记录ID */
    @ApiModelProperty("商户通知记录ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long notifyId;

    /** 订单ID */
    @ApiModelProperty("订单ID")
    @NotBlank(message = "订单ID不能为空")
    private String orderId;

    /** 订单类型:1-支付,2-退款 */
    @ApiModelProperty("订单类型:1-支付,2-退款")
    @NotNull(message = "订单类型:1-支付,2-退款不能为空")
    private Integer orderType;

    /** 商户订单号 */
    @ApiModelProperty("商户订单号")
    @NotBlank(message = "商户订单号不能为空")
    private String mchOrderNo;

    /** 商户号 */
    @ApiModelProperty("商户号")
    @NotBlank(message = "商户号不能为空")
    private String mchNo;

    /** 服务商号 */
    @ApiModelProperty("服务商号")
    private String isvNo;

    /** 应用ID */
    @ApiModelProperty("应用ID")
    @NotBlank(message = "应用ID不能为空")
    private String appId;

    /** 通知地址 */
    @ApiModelProperty("通知地址")
    @NotBlank(message = "通知地址不能为空")
    private String notifyUrl;

    /** 通知响应结果 */
    @ApiModelProperty("通知响应结果")
    private String resResult;

    /** 通知次数 */
    @ApiModelProperty("通知次数")
    @NotNull(message = "通知次数不能为空")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long notifyCount;

    /** 最大通知次数, 默认6次 */
    @ApiModelProperty("最大通知次数, 默认6次")
    @NotNull(message = "最大通知次数, 默认6次不能为空")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long notifyCountLimit;

    /** 通知状态,1-通知中,2-通知成功,3-通知失败 */
    @ApiModelProperty("通知状态,1-通知中,2-通知成功,3-通知失败")
    @NotNull(message = "通知状态,1-通知中,2-通知成功,3-通知失败不能为空")
    private Integer state;

    /** 最后一次通知时间 */
    @ApiModelProperty("最后一次通知时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastNotifyTime;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @NotNull(message = "创建时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @NotNull(message = "更新时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;
}
