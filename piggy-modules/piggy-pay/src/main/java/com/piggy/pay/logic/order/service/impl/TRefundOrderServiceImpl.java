package com.piggy.pay.logic.order.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.core.enums.IOrderEnums;
import com.piggy.pay.core.enums.IRefundOrderEnums;
import com.piggy.pay.logic.config.bo.TRefundOrderEditBo;
import com.piggy.pay.logic.config.bo.TRefundOrderQueryBo;
import com.piggy.pay.logic.order.domain.TRefundOrder;
import com.piggy.pay.logic.order.mapper.TPayOrderMapperEx;
import com.piggy.pay.logic.order.mapper.TRefundOrderMapper;
import com.piggy.pay.logic.order.mapper.TRefundOrderMapperEx;
import com.piggy.pay.logic.order.service.ITRefundOrderService;
import com.piggy.pay.logic.order.vo.TRefundOrderVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * 退款订单Service业务层处理
 *
 * @author piggy
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TRefundOrderServiceImpl extends ServiceImpl<TRefundOrderMapper, TRefundOrder> implements ITRefundOrderService {

    @Resource
    private TPayOrderMapperEx payOrderMapperEx;

    @Resource
    private TRefundOrderMapperEx refundOrderMapperEx;

    @Override
    public TRefundOrderVo queryById(String refundOrderId) {
        return getVoById(refundOrderId, TRefundOrderVo.class);
    }

    @Override
    public TableDataInfo<TRefundOrderVo> queryPageList(TRefundOrderQueryBo bo) {
        PagePlus<TRefundOrder, TRefundOrderVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TRefundOrderVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TRefundOrderVo> queryList(TRefundOrderQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TRefundOrderVo.class);
    }

    private LambdaQueryWrapper<TRefundOrder> buildQueryWrapper(TRefundOrderQueryBo bo) {
        LambdaQueryWrapper<TRefundOrder> lqw = Wrappers.lambdaQuery();
        lqw.eq(StrUtil.isNotBlank(bo.getRefundOrderId()), TRefundOrder::getRefundOrderId, bo.getRefundOrderId());
        lqw.eq(StrUtil.isNotBlank(bo.getPayOrderId()), TRefundOrder::getPayOrderId, bo.getPayOrderId());
        lqw.eq(StrUtil.isNotBlank(bo.getChannelPayOrderNo()), TRefundOrder::getChannelPayOrderNo, bo.getChannelPayOrderNo());
        lqw.eq(StrUtil.isNotBlank(bo.getMchNo()), TRefundOrder::getMchNo, bo.getMchNo());
        lqw.eq(StrUtil.isNotBlank(bo.getIsvNo()), TRefundOrder::getIsvNo, bo.getIsvNo());
        lqw.eq(StrUtil.isNotBlank(bo.getAppId()), TRefundOrder::getAppId, bo.getAppId());
        lqw.like(StrUtil.isNotBlank(bo.getMchName()), TRefundOrder::getMchName, bo.getMchName());
        lqw.eq(bo.getMchType() != null, TRefundOrder::getMchType, bo.getMchType());
        lqw.eq(StrUtil.isNotBlank(bo.getMchRefundNo()), TRefundOrder::getMchRefundNo, bo.getMchRefundNo());
        lqw.eq(StrUtil.isNotBlank(bo.getWayCode()), TRefundOrder::getWayCode, bo.getWayCode());
        lqw.eq(StrUtil.isNotBlank(bo.getIfCode()), TRefundOrder::getIfCode, bo.getIfCode());
        lqw.eq(bo.getPayAmount() != null, TRefundOrder::getPayAmount, bo.getPayAmount());
        lqw.eq(bo.getRefundAmount() != null, TRefundOrder::getRefundAmount, bo.getRefundAmount());
        lqw.eq(StrUtil.isNotBlank(bo.getCurrency()), TRefundOrder::getCurrency, bo.getCurrency());
        lqw.eq(bo.getState() != null, TRefundOrder::getState, bo.getState());
        lqw.eq(StrUtil.isNotBlank(bo.getClientIp()), TRefundOrder::getClientIp, bo.getClientIp());
        lqw.eq(StrUtil.isNotBlank(bo.getRefundReason()), TRefundOrder::getRefundReason, bo.getRefundReason());
        lqw.eq(StrUtil.isNotBlank(bo.getChannelOrderNo()), TRefundOrder::getChannelOrderNo, bo.getChannelOrderNo());
        lqw.eq(StrUtil.isNotBlank(bo.getErrCode()), TRefundOrder::getErrCode, bo.getErrCode());
        lqw.eq(StrUtil.isNotBlank(bo.getErrMsg()), TRefundOrder::getErrMsg, bo.getErrMsg());
        lqw.eq(StrUtil.isNotBlank(bo.getChannelExtra()), TRefundOrder::getChannelExtra, bo.getChannelExtra());
        lqw.eq(StrUtil.isNotBlank(bo.getNotifyUrl()), TRefundOrder::getNotifyUrl, bo.getNotifyUrl());
        lqw.eq(StrUtil.isNotBlank(bo.getExtParam()), TRefundOrder::getExtParam, bo.getExtParam());
        lqw.eq(bo.getSuccessTime() != null, TRefundOrder::getSuccessTime, bo.getSuccessTime());
        lqw.eq(bo.getExpiredTime() != null, TRefundOrder::getExpiredTime, bo.getExpiredTime());
        lqw.eq(bo.getCreatedAt() != null, TRefundOrder::getCreatedAt, bo.getCreatedAt());
        lqw.eq(bo.getUpdatedAt() != null, TRefundOrder::getUpdatedAt, bo.getUpdatedAt());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TRefundOrderEditBo bo) {
        TRefundOrder add = BeanUtil.toBean(bo, TRefundOrder.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TRefundOrderEditBo bo) {
        TRefundOrder update = BeanUtil.toBean(bo, TRefundOrder.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TRefundOrder entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

    @Override
    /** 更新退款单状态  【退款单生成】 --》 【退款中】 **/
    public boolean updateInit2Ing(String refundOrderId, String channelOrderNo){

        TRefundOrder updateRecord = new TRefundOrder();
        updateRecord.setState(IOrderEnums.OrderStateEnums.STATE_ING.getState());
        updateRecord.setChannelOrderNo(channelOrderNo);

        return update(updateRecord, new LambdaUpdateWrapper<TRefundOrder>()
                .eq(TRefundOrder::getRefundOrderId, refundOrderId).eq(TRefundOrder::getState, IOrderEnums.OrderStateEnums.STATE_INIT.getState()));
    }

    /** 更新退款单状态  【退款中】 --》 【退款成功】 **/
    @Override
    @Transactional
    public boolean updateIng2Success(String refundOrderId, String channelOrderNo){

        TRefundOrder updateRecord = new TRefundOrder();
        updateRecord.setState(IOrderEnums.OrderStateEnums.STATE_SUCCESS.getState());
        updateRecord.setChannelOrderNo(channelOrderNo);
        updateRecord.setSuccessTime(new Date());

        //1. 更新退款订单表数据
        if(! update(updateRecord, new LambdaUpdateWrapper<TRefundOrder>()
                .eq(TRefundOrder::getRefundOrderId, refundOrderId).eq(TRefundOrder::getState, IOrderEnums.OrderStateEnums.STATE_ING.getState()))){
            return false;
        }

        //2. 更新订单表数据（更新退款次数,退款状态,如全额退款更新支付状态为已退款）
        TRefundOrder refundOrder = getOne(new LambdaQueryWrapper<TRefundOrder>().select(TRefundOrder::getPayOrderId, TRefundOrder::getRefundAmount).eq(TRefundOrder::getRefundOrderId, refundOrderId));
        int updateCount = payOrderMapperEx.updateRefundAmountAndCount(refundOrder.getPayOrderId(), refundOrder.getRefundAmount());
        if(updateCount <= 0){
            throw new BaseException("更新订单数据异常");
        }

        return true;
    }

    /** 更新退款单状态  【退款中】 --》 【退款失败】 **/
    @Override
    @Transactional
    public boolean updateIng2Fail(String refundOrderId, String channelOrderNo, String channelErrCode, String channelErrMsg){

        TRefundOrder updateRecord = new TRefundOrder();
        updateRecord.setState(IRefundOrderEnums.StateEnums.STATE_FAIL.getState());
        updateRecord.setErrCode(channelErrCode);
        updateRecord.setErrMsg(channelErrMsg);
        updateRecord.setChannelOrderNo(channelOrderNo);

        return update(updateRecord, new LambdaUpdateWrapper<TRefundOrder>()
                .eq(TRefundOrder::getRefundOrderId, refundOrderId).eq(TRefundOrder::getState, IRefundOrderEnums.StateEnums.STATE_ING.getState()));
    }

    /** 更新退款单状态  【退款中】 --》 【退款成功/退款失败】 **/
    @Override
    @Transactional
    public boolean updateIng2SuccessOrFail(String refundOrderId, Integer updateState, String channelOrderNo, String channelErrCode, String channelErrMsg){

        if(updateState == IRefundOrderEnums.StateEnums.STATE_ING.getState()){
            return true;
        }else if(updateState == IRefundOrderEnums.StateEnums.STATE_SUCCESS.getState()){
            return updateIng2Success(refundOrderId, channelOrderNo);
        }else if(updateState == IRefundOrderEnums.StateEnums.STATE_FAIL.getState()){
            return updateIng2Fail(refundOrderId, channelOrderNo, channelErrCode, channelErrMsg);
        }
        return false;
    }

    /** 更新退款单为 关闭状态 **/
    @Override
    public Integer updateOrderExpired(){

        TRefundOrder refundOrder = new TRefundOrder();
        refundOrder.setState(IRefundOrderEnums.StateEnums.STATE_CLOSED.getState());

        return baseMapper.update(refundOrder,
                new LambdaQueryWrapper<TRefundOrder>()
                        .in(TRefundOrder::getState, Arrays.asList(IRefundOrderEnums.StateEnums.STATE_INIT.getState(),
                                IRefundOrderEnums.StateEnums.STATE_ING.getState()))
                        .le(TRefundOrder::getExpiredTime, new Date())

        );
    }

    /** 查询商户订单 **/
    @Override
    public TRefundOrder queryMchOrder(String mchNo, String mchRefundNo, String refundOrderId){

        if(StringUtils.isNotEmpty(refundOrderId)){
            return getOne(new LambdaQueryWrapper<TRefundOrder>().eq(TRefundOrder::getMchNo, mchNo).eq(TRefundOrder::getRefundOrderId, refundOrderId));
        }else if(StringUtils.isNotEmpty(mchRefundNo)){
            return getOne(new LambdaQueryWrapper<TRefundOrder>().eq(TRefundOrder::getMchNo, mchNo).eq(TRefundOrder::getMchRefundNo, mchRefundNo));
        }else{
            return null;
        }
    }

    @Override
    public TRefundOrderMapperEx getBaseMapperEx() {
        return this.refundOrderMapperEx;
    }
}
