package com.piggy.pay.pls.request;

import com.piggy.pay.pls.BasePay;
import com.piggy.pay.pls.model.DivisionReceiverChannelBalanceQueryReqModel;
import com.piggy.pay.pls.net.RequestOptions;
import com.piggy.pay.pls.response.DivisionReceiverChannelBalanceQueryResponse;

/***
 * 分账渠道余额查询接口
 *
 * @author terrfly
 * @site https://www.jeequan.com
 * @date 2022/5/11 15:03
 */
public class DivisionReceiverChannelBalanceQueryRequest implements PayRequest<DivisionReceiverChannelBalanceQueryResponse, DivisionReceiverChannelBalanceQueryReqModel> {

    private String apiVersion = BasePay.VERSION;
    private String apiUri = "api/division/receiver/channelBalanceQuery";
    private RequestOptions options;
    private DivisionReceiverChannelBalanceQueryReqModel bizModel = null;

    @Override
    public String getApiUri() {
        return this.apiUri;
    }

    @Override
    public String getApiVersion() {
        return this.apiVersion;
    }

    @Override
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    @Override
    public RequestOptions getRequestOptions() {
        return this.options;
    }

    @Override
    public void setRequestOptions(RequestOptions options) {
        this.options = options;
    }

    @Override
    public DivisionReceiverChannelBalanceQueryReqModel getBizModel() {
        return this.bizModel;
    }

    @Override
    public void setBizModel(DivisionReceiverChannelBalanceQueryReqModel bizModel) {
        this.bizModel = bizModel;
    }

    @Override
    public Class<DivisionReceiverChannelBalanceQueryResponse> getResponseClass() {
        return DivisionReceiverChannelBalanceQueryResponse.class;
    }

}
