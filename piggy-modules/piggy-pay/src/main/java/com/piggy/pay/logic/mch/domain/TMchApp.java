package com.piggy.pay.logic.mch.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import com.baomidou.mybatisplus.annotation.*;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;


/**
 * 商户应用对象 t_mch_app
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("t_mch_app")
public class TMchApp implements Serializable {

    private static final long serialVersionUID=1L;

    /** 应用ID */
    @ApiModelProperty("应用ID")
    @TableId(value = "app_id")
    private String appId;

    /** 应用名称 */
    @ApiModelProperty("应用名称")
    private String appName;

    /** 商户号 */
    @ApiModelProperty("商户号")
    private String mchNo;

    /** 应用状态: 0-停用, 1-正常 */
    @ApiModelProperty("应用状态: 0-停用, 1-正常")
    private Integer state;

    /** 应用私钥 */
    @ApiModelProperty("应用私钥")
    private String appSecret;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;

    /** 创建者用户ID */
    @ApiModelProperty("创建者用户ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long createdUid;

    /** 创建者姓名 */
    @ApiModelProperty("创建者姓名")
    private String createdBy;

    /** 更新人 */
    @ApiModelProperty("更新人")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

}
