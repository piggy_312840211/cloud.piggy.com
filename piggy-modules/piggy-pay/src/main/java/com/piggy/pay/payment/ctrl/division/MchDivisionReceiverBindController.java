/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.piggy.pay.payment.ctrl.division;

import com.piggy.common.core.exception.base.BaseException;
import com.piggy.pay.core.beans.RequestKitBean;
import com.piggy.pay.core.constants.CS;
import com.piggy.pay.core.enums.ChannelEnums;
import com.piggy.pay.core.model.ApiRes;
import cn.hutool.extra.spring.SpringUtil;
import com.piggy.pay.logic.config.service.ITPayThirdApiConfigService;
import com.piggy.pay.logic.mch.domain.TMchDivisionReceiver;
import com.piggy.pay.logic.mch.domain.TMchDivisionReceiverGroup;
import com.piggy.pay.logic.mch.domain.TMchInfo;
import com.piggy.pay.logic.mch.service.ITMchDivisionReceiverGroupService;
import com.piggy.pay.logic.mch.service.ITMchDivisionReceiverService;
import com.piggy.pay.payment.channel.IDivisionService;
import com.piggy.pay.payment.ctrl.ApiController;
import com.piggy.pay.payment.model.MchAppConfigContext;
import com.piggy.pay.payment.rqrs.division.DivisionReceiverBindRQ;
import com.piggy.pay.payment.rqrs.division.DivisionReceiverBindRS;
import com.piggy.pay.payment.rqrs.msg.ChannelRetMsg;
import com.piggy.pay.payment.service.ConfigContextQueryService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;

/**
* 分账账号绑定
*
* @author terrfly
* @site https://www.jeequan.com
* @date 2021/8/25 9:07
*/
@Slf4j
@RestController
public class MchDivisionReceiverBindController extends ApiController {

    @Resource
    private ConfigContextQueryService configContextQueryService;
    @Resource
    private ITPayThirdApiConfigService payInterfaceConfigService;
    @Resource
    private ITMchDivisionReceiverService mchDivisionReceiverService;
    @Resource
    private ITMchDivisionReceiverGroupService mchDivisionReceiverGroupService;

    /** 分账账号绑定 **/
    @PostMapping("/api/division/receiver/bind")
    public ApiRes bind(){

        //获取参数 & 验签
        DivisionReceiverBindRQ bizRQ = getRQByWithMchSign(DivisionReceiverBindRQ.class);

        try {

            //检查商户应用是否存在该接口
            String ifCode = bizRQ.getIfCode();


            // 商户配置信息
            MchAppConfigContext mchAppConfigContext = configContextQueryService.queryMchInfoAndAppInfo(bizRQ.getMchNo(), bizRQ.getAppId());
            if(mchAppConfigContext == null){
                throw new BaseException("获取商户应用信息失败");
            }

            TMchInfo mchInfo = mchAppConfigContext.getMchInfo();

            if(!payInterfaceConfigService.mchAppHasAvailableIfCode(bizRQ.getAppId(), ifCode)){
                throw new BaseException("商户应用的支付配置不存在或已关闭");
            }

            TMchDivisionReceiverGroup group = mchDivisionReceiverGroupService.findByIdAndMchNo(bizRQ.getReceiverGroupId(), bizRQ.getMchNo());
            if(group == null){
                throw new BaseException("商户分账账号组不存在，请检查或进入商户平台进行创建操作");
            }

            BigDecimal divisionProfit = new BigDecimal(bizRQ.getDivisionProfit());
            if(divisionProfit.compareTo(BigDecimal.ZERO) <= 0  || divisionProfit.compareTo(BigDecimal.ONE) > 1){
                throw new BaseException("账号分账比例有误, 配置值为[0.0001~1.0000]");
            }


            //生成数据库对象信息 （数据不完成， 暂时不可入库操作）
            TMchDivisionReceiver receiver = genRecord(bizRQ, group, mchInfo, divisionProfit);

            //调起上游接口

            IDivisionService divisionService = SpringUtil.getBean(ifCode + "DivisionService", IDivisionService.class);
            if(divisionService == null){
                throw new BaseException("系统不支持该分账接口");
            }


            ChannelRetMsg retMsg = divisionService.bind(receiver, mchAppConfigContext);
            if(retMsg.getChannelState() == ChannelEnums.CONFIRM_SUCCESS){

                receiver.setState((int) CS.YES);
                receiver.setBindSuccessTime(new Date());
                mchDivisionReceiverService.save(receiver);

            }else{

                receiver.setState(CS.NO);
                receiver.setChannelBindResult(retMsg.getChannelErrMsg());
            }

            DivisionReceiverBindRS bizRes = DivisionReceiverBindRS.buildByRecord(receiver);

            if(retMsg.getChannelState() == ChannelEnums.CONFIRM_SUCCESS){

                bizRes.setBindState(CS.YES);

            }else{
                bizRes.setBindState(CS.NO);
                bizRes.setErrCode(retMsg.getChannelErrCode());
                bizRes.setErrMsg(retMsg.getChannelErrMsg());
            }

            return ApiRes.okWithSign(bizRes, mchAppConfigContext.getMchApp().getAppSecret());

        }  catch (BaseException e) {
            return ApiRes.customFail(e.getMessage());

        } catch (Exception e) {
            log.error("系统异常：{}", e);
            return ApiRes.customFail("系统异常");
        }
    }

    private TMchDivisionReceiver genRecord(DivisionReceiverBindRQ bizRQ, TMchDivisionReceiverGroup group, TMchInfo mchInfo, BigDecimal divisionProfit){

        TMchDivisionReceiver receiver = new TMchDivisionReceiver();
        receiver.setReceiverAlias(StringUtils.defaultIfEmpty(bizRQ.getReceiverAlias(), bizRQ.getAccNo())); //别名
        receiver.setReceiverGroupId(bizRQ.getReceiverGroupId()); //分组ID
        receiver.setReceiverGroupName(group.getReceiverGroupName()); //组名称
        receiver.setMchNo(bizRQ.getMchNo()); //商户号
        receiver.setIsvNo(mchInfo.getIsvNo()); //isvNo
        receiver.setAppId(bizRQ.getAppId()); //appId
        receiver.setIfCode(bizRQ.getIfCode()); //接口代码
        receiver.setAccType(bizRQ.getAccType()); //账号类型
        receiver.setAccNo(bizRQ.getAccNo()); //账号
        receiver.setAccName(bizRQ.getAccName()); //账号名称
        receiver.setRelationType(bizRQ.getRelationType()); //关系

        receiver.setRelationTypeName(getRelationTypeName(bizRQ.getRelationType())); //关系名称

        if(receiver.getRelationTypeName() == null){
            receiver.setRelationTypeName(bizRQ.getRelationTypeName());
        }

        receiver.setDivisionProfit(divisionProfit); //分账比例
        receiver.setChannelExtInfo(bizRQ.getChannelExtInfo()); //渠道信息

        return receiver;
    }

    public String getRelationTypeName(String relationType){

        if("PARTNER".equals(relationType)){
            return "合作伙伴";
        }else if("SERVICE_PROVIDER".equals(relationType)){
            return "服务商";
        }else if("STORE".equals(relationType)){
            return "门店";
        }else if("STAFF".equals(relationType)){
            return "员工";
        }else if("STORE_OWNER".equals(relationType)){
            return "店主";
        }else if("HEADQUARTER".equals(relationType)){
            return "总部";
        }else if("BRAND".equals(relationType)){
            return "品牌方";
        }else if("DISTRIBUTOR".equals(relationType)){
            return "分销商";
        }else if("USER".equals(relationType)){
            return "用户";
        }else if("SUPPLIER".equals(relationType)){
            return "供应商";
        }
        return null;
    }



}
