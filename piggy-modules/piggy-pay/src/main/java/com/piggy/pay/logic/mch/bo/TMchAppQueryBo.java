package com.piggy.pay.logic.mch.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * 商户应用分页查询对象 t_mch_app
 *
 * @author piggy
 * @date 2023-05-25
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel("商户应用分页查询对象")
public class TMchAppQueryBo extends BaseEntity {

	/** 应用ID */
	@ApiModelProperty("应用ID")
	private String appId;
	/** 应用名称 */
	@ApiModelProperty("应用名称")
	private String appName;
	/** 商户号 */
	@ApiModelProperty("商户号")
	private String mchNo;
	/** 应用状态: 0-停用, 1-正常 */
	@ApiModelProperty("应用状态: 0-停用, 1-正常")
	private Integer state;
	/** 应用私钥 */
	@ApiModelProperty("应用私钥")
	private String appSecret;
	/** 创建者用户ID */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("创建者用户ID")
	private Long createdUid;
	/** 创建者姓名 */
	@ApiModelProperty("创建者姓名")
	private String createdBy;
	/** 创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;
	/** 更新时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;

}
