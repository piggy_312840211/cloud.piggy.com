package com.piggy.pay.logic.mch.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.mch.bo.TMchDivisionReceiverGroupEditBo;
import com.piggy.pay.logic.mch.bo.TMchDivisionReceiverGroupQueryBo;
import com.piggy.pay.logic.mch.domain.TMchDivisionReceiverGroup;
import com.piggy.pay.logic.mch.mapper.TMchDivisionReceiverGroupMapper;
import com.piggy.pay.logic.mch.service.ITMchDivisionReceiverGroupService;
import com.piggy.pay.logic.mch.vo.TMchDivisionReceiverGroupVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import java.util.List;
import java.util.Collection;

/**
 * 分账账号组Service业务层处理
 *
 * @author piggy
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TMchDivisionReceiverGroupServiceImpl extends ServiceImpl<TMchDivisionReceiverGroupMapper, TMchDivisionReceiverGroup> implements ITMchDivisionReceiverGroupService {

    //gw
    public static final LambdaQueryWrapper<TMchDivisionReceiverGroup> gw(){
        return new LambdaQueryWrapper<>();
    }

    @Override
    public TMchDivisionReceiverGroupVo queryById(Long receiverGroupId){
        return getVoById(receiverGroupId, TMchDivisionReceiverGroupVo.class);
    }

    @Override
    public TableDataInfo<TMchDivisionReceiverGroupVo> queryPageList(TMchDivisionReceiverGroupQueryBo bo) {
        PagePlus<TMchDivisionReceiverGroup, TMchDivisionReceiverGroupVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TMchDivisionReceiverGroupVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TMchDivisionReceiverGroupVo> queryList(TMchDivisionReceiverGroupQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TMchDivisionReceiverGroupVo.class);
    }

    private LambdaQueryWrapper<TMchDivisionReceiverGroup> buildQueryWrapper(TMchDivisionReceiverGroupQueryBo bo) {
        LambdaQueryWrapper<TMchDivisionReceiverGroup> lqw = gw();
        lqw.eq(bo.getReceiverGroupId() != null, TMchDivisionReceiverGroup::getReceiverGroupId, bo.getReceiverGroupId());
        lqw.like(StrUtil.isNotBlank(bo.getReceiverGroupName()), TMchDivisionReceiverGroup::getReceiverGroupName, bo.getReceiverGroupName());
        lqw.eq(StrUtil.isNotBlank(bo.getMchNo()), TMchDivisionReceiverGroup::getMchNo, bo.getMchNo());
        lqw.eq(bo.getAutoDivisionFlag() != null, TMchDivisionReceiverGroup::getAutoDivisionFlag, bo.getAutoDivisionFlag());
        lqw.eq(bo.getCreatedUid() != null, TMchDivisionReceiverGroup::getCreatedUid, bo.getCreatedUid());
        lqw.eq(StrUtil.isNotBlank(bo.getCreatedBy()), TMchDivisionReceiverGroup::getCreatedBy, bo.getCreatedBy());
        lqw.eq(bo.getCreatedAt() != null, TMchDivisionReceiverGroup::getCreatedAt, bo.getCreatedAt());
        lqw.eq(bo.getUpdatedAt() != null, TMchDivisionReceiverGroup::getUpdatedAt, bo.getUpdatedAt());
        lqw.eq(StrUtil.isNotBlank(bo.getUpdateBy()), TMchDivisionReceiverGroup::getUpdateBy, bo.getUpdateBy());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TMchDivisionReceiverGroupEditBo bo) {
        TMchDivisionReceiverGroup add = BeanUtil.toBean(bo, TMchDivisionReceiverGroup.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TMchDivisionReceiverGroupEditBo bo) {
        TMchDivisionReceiverGroup update = BeanUtil.toBean(bo, TMchDivisionReceiverGroup.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TMchDivisionReceiverGroup entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

    /** 根据ID和商户号查询 **/
    public TMchDivisionReceiverGroup findByIdAndMchNo(Long groupId, String mchNo){
        return getOne(gw().eq(TMchDivisionReceiverGroup::getReceiverGroupId, groupId).eq(TMchDivisionReceiverGroup::getMchNo, mchNo));
    }


}
