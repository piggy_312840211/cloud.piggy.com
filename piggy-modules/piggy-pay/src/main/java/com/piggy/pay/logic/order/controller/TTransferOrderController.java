package com.piggy.pay.logic.order.controller;

import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import lombok.RequiredArgsConstructor;

import javax.annotation.Resource;
import javax.validation.constraints.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.piggy.pay.logic.order.vo.TTransferOrderVo;
import com.piggy.pay.logic.order.bo.TTransferOrderQueryBo;
import com.piggy.pay.logic.order.bo.TTransferOrderEditBo;
import com.piggy.pay.logic.order.service.ITTransferOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.lang.Integer;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
 * 转账订单Controller
 *
 * @author zito
 * @date 2023-05-25
 */
@Api(value = "转账订单控制器", tags = {"转账订单管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/order/transferorder")
public class TTransferOrderController extends BaseController {

    private final ITTransferOrderService iTTransferOrderService;

    /**
     * 查询转账订单列表
     */
    @ApiOperation("查询转账订单列表")
    @SaCheckPermission("order:transferorder:list")
    @GetMapping("/list")
    public TableDataInfo<TTransferOrderVo> list(@Validated TTransferOrderQueryBo bo) {
        return iTTransferOrderService.queryPageList(bo);
    }

    /**
     * 导出转账订单列表
     */
    @ApiOperation("导出转账订单列表")
    @SaCheckPermission("order:transferorder:export")
    @Log(title = "转账订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TTransferOrderQueryBo bo, HttpServletResponse response) throws IOException {
        List<TTransferOrderVo> list = iTTransferOrderService.queryList(bo);
        ExcelUtil<TTransferOrderVo> util = new ExcelUtil<TTransferOrderVo>(TTransferOrderVo.class);
        util.exportExcel(response, list, "转账订单");
    }

    /**
     * 获取转账订单详细信息
     */
    @ApiOperation("获取转账订单详细信息")
    @SaCheckPermission("order:transferorder:query")
    @GetMapping("/{transferId}")
    public R<TTransferOrderVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("transferId") String transferId) {
        return R.ok(iTTransferOrderService.queryById(transferId));
    }

    /**
     * 新增转账订单
     */
    @ApiOperation("新增转账订单")
    @SaCheckPermission("order:transferorder:add")
    @Log(title = "转账订单", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TTransferOrderEditBo bo) {
        return R.ok(iTTransferOrderService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改转账订单
     */
    @ApiOperation("修改转账订单")
    @SaCheckPermission("order:transferorder:edit")
    @Log(title = "转账订单", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TTransferOrderEditBo bo) {
        return R.ok(iTTransferOrderService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除转账订单
     */
    @ApiOperation("删除转账订单")
    @SaCheckPermission("order:transferorder:remove")
    @Log(title = "转账订单" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{transferIds}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable String[] transferIds) {
        return R.ok(iTTransferOrderService.deleteWithValidByIds(Arrays.asList(transferIds), true) ? 1 : 0);
    }
}
