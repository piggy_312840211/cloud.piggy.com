package com.piggy.pay.logic.order.service;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.core.enums.IOrderEnums;
import com.piggy.pay.core.enums.IRefundOrderEnums;
import com.piggy.pay.logic.config.bo.TRefundOrderEditBo;
import com.piggy.pay.logic.config.bo.TRefundOrderQueryBo;
import com.piggy.pay.logic.order.domain.TRefundOrder;
import com.piggy.pay.logic.order.mapper.TRefundOrderMapperEx;
import com.piggy.pay.logic.order.vo.TRefundOrderVo;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * 退款订单Service接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface ITRefundOrderService extends IServicePlus<TRefundOrder> {
    /**
     * 查询单个
     *
     * @return
     */
    TRefundOrderVo queryById(String refundOrderId);

    /**
     * 查询列表
     */
    TableDataInfo<TRefundOrderVo> queryPageList(TRefundOrderQueryBo bo);

    /**
     * 查询列表
     */
    List<TRefundOrderVo> queryList(TRefundOrderQueryBo bo);

    /**
     * 根据新增业务对象插入退款订单
     *
     * @param bo 退款订单新增业务对象
     * @return
     */
    Boolean insertByAddBo(TRefundOrderEditBo bo);

    /**
     * 根据编辑业务对象修改退款订单
     *
     * @param bo 退款订单编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TRefundOrderEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);

    /**
     * 更新退款单状态  【退款单生成】 --》 【退款中】
     **/
    boolean updateInit2Ing(String refundOrderId, String channelOrderNo);

    /**
     * 更新退款单状态  【退款中】 --》 【退款成功】
     **/
    boolean updateIng2Success(String refundOrderId, String channelOrderNo);


    /**
     * 更新退款单状态  【退款中】 --》 【退款失败】
     **/
    boolean updateIng2Fail(String refundOrderId, String channelOrderNo, String channelErrCode, String channelErrMsg);

    /**
     * 更新退款单状态  【退款中】 --》 【退款成功/退款失败】
     **/
    boolean updateIng2SuccessOrFail(String refundOrderId, Integer updateState, String channelOrderNo, String channelErrCode, String channelErrMsg);

    /**
     * 更新退款单为 关闭状态
     **/
    Integer updateOrderExpired();

    TRefundOrder queryMchOrder(String mchNo, String mchRefundNo, String refundOrderId);

    TRefundOrderMapperEx getBaseMapperEx();
}