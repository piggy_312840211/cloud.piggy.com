package com.piggy.pay.pls.request;

import com.piggy.pay.pls.BasePay;
import com.piggy.pay.pls.model.DivisionReceiverBindReqModel;
import com.piggy.pay.pls.net.RequestOptions;
import com.piggy.pay.pls.response.DivisionReceiverBindResponse;

/***
* 分账绑定接口
*
* @author terrfly
* @site https://www.jeepay.vip
* @date 2021/8/25 10:34
*/
public class DivisionReceiverBindRequest implements PayRequest<DivisionReceiverBindResponse, DivisionReceiverBindReqModel> {

    private String apiVersion = BasePay.VERSION;
    private String apiUri = "api/division/receiver/bind";
    private RequestOptions options;
    private DivisionReceiverBindReqModel bizModel = null;

    @Override
    public String getApiUri() {
        return this.apiUri;
    }

    @Override
    public String getApiVersion() {
        return this.apiVersion;
    }

    @Override
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    @Override
    public RequestOptions getRequestOptions() {
        return this.options;
    }

    @Override
    public void setRequestOptions(RequestOptions options) {
        this.options = options;
    }

    @Override
    public DivisionReceiverBindReqModel getBizModel() {
        return this.bizModel;
    }

    @Override
    public void setBizModel(DivisionReceiverBindReqModel bizModel) {
        this.bizModel = bizModel;
    }

    @Override
    public Class<DivisionReceiverBindResponse> getResponseClass() {
        return DivisionReceiverBindResponse.class;
    }

}
