package com.piggy.pay.logic.order.controller;

import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import lombok.RequiredArgsConstructor;

import javax.annotation.Resource;
import javax.validation.constraints.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.piggy.pay.logic.order.vo.TPayOrderVo;
import com.piggy.pay.logic.order.bo.TPayOrderQueryBo;
import com.piggy.pay.logic.order.bo.TPayOrderEditBo;
import com.piggy.pay.logic.order.service.ITPayOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.lang.Integer;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
 * 支付订单Controller
 *
 * @author piggy
 * @date 2023-05-25
 */
@Api(value = "支付订单控制器", tags = {"支付订单管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/order/order")
public class TPayOrderController extends BaseController {

    private final ITPayOrderService iTPayOrderService;

    /**
     * 查询支付订单列表
     */
    @ApiOperation("查询支付订单列表")
    @SaCheckPermission("order:order:list")
    @GetMapping("/list")
    public TableDataInfo<TPayOrderVo> list(@Validated TPayOrderQueryBo bo) {
        return iTPayOrderService.queryPageList(bo);
    }

    /**
     * 导出支付订单列表
     */
    @ApiOperation("导出支付订单列表")
    @SaCheckPermission("order:order:export")
    @Log(title = "支付订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TPayOrderQueryBo bo, HttpServletResponse response) throws IOException {
        List<TPayOrderVo> list = iTPayOrderService.queryList(bo);
        ExcelUtil<TPayOrderVo> util = new ExcelUtil<TPayOrderVo>(TPayOrderVo.class);
        util.exportExcel(response, list, "支付订单");
    }

    /**
     * 获取支付订单详细信息
     */
    @ApiOperation("获取支付订单详细信息")
    @SaCheckPermission("order:order:query")
    @GetMapping("/{payOrderId}")
    public R<TPayOrderVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("payOrderId") String payOrderId) {
        return R.ok(iTPayOrderService.queryById(payOrderId));
    }

    /**
     * 新增支付订单
     */
    @ApiOperation("新增支付订单")
    @SaCheckPermission("order:order:add")
    @Log(title = "支付订单", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TPayOrderEditBo bo) {
        return R.ok(iTPayOrderService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改支付订单
     */
    @ApiOperation("修改支付订单")
    @SaCheckPermission("order:order:edit")
    @Log(title = "支付订单", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TPayOrderEditBo bo) {
        return R.ok(iTPayOrderService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除支付订单
     */
    @ApiOperation("删除支付订单")
    @SaCheckPermission("order:order:remove")
    @Log(title = "支付订单" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{payOrderIds}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable String[] payOrderIds) {
        return R.ok(iTPayOrderService.deleteWithValidByIds(Arrays.asList(payOrderIds), true) ? 1 : 0);
    }
}
