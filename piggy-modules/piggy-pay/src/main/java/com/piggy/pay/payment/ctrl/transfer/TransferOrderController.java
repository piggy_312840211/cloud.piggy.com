/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.piggy.pay.payment.ctrl.transfer;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.pay.core.enums.ChannelEnums;
import com.piggy.pay.core.enums.ITransferEnums;
import com.piggy.pay.core.exception.ChannelException;
import com.piggy.pay.core.model.ApiRes;
import com.piggy.pay.core.utils.SeqKit;
import cn.hutool.extra.spring.SpringUtil;
import com.piggy.pay.core.utils.StringKit;
import com.piggy.pay.logic.config.service.ITPayThirdApiConfigService;
import com.piggy.pay.logic.mch.domain.TMchApp;
import com.piggy.pay.logic.mch.domain.TMchInfo;
import com.piggy.pay.logic.order.domain.TTransferOrder;
import com.piggy.pay.logic.order.service.ITTransferOrderService;
import com.piggy.pay.payment.channel.ITransferService;
import com.piggy.pay.payment.ctrl.ApiController;
import com.piggy.pay.payment.model.MchAppConfigContext;
import com.piggy.pay.payment.rqrs.msg.ChannelRetMsg;
import com.piggy.pay.payment.rqrs.transfer.TransferOrderRQ;
import com.piggy.pay.payment.rqrs.transfer.TransferOrderRS;
import com.piggy.pay.payment.service.ConfigContextQueryService;
import com.piggy.pay.payment.service.PayMchNotifyService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;

/**
* 转账接口
*
* @author terrfly
* @site https://www.jeequan.com
* @date 2021/8/11 11:07
*/
@Slf4j
@RestController
public class TransferOrderController extends ApiController {

    @Resource
    private ConfigContextQueryService configContextQueryService;
    @Resource
    private ITTransferOrderService transferOrderService;
    @Resource
    private ITPayThirdApiConfigService payInterfaceConfigService;
    @Resource
    private PayMchNotifyService payMchNotifyService;

    /**
     * 转账
     * **/
    @PostMapping("/api/transferOrder")
    public ApiRes transferOrder(){

        TTransferOrder transferOrder = null;

        //获取参数 & 验签
        TransferOrderRQ bizRQ = getRQByWithMchSign(TransferOrderRQ.class);

        try {


            String mchNo = bizRQ.getMchNo();
            String appId = bizRQ.getAppId();
            String ifCode = bizRQ.getIfCode();

            // 商户订单号是否重复
            if(transferOrderService.count(new LambdaQueryWrapper<TTransferOrder>().eq(TTransferOrder::getMchNo, mchNo).eq(TTransferOrder::getMchOrderNo, bizRQ.getMchOrderNo())) > 0){
                throw new BaseException("商户订单["+bizRQ.getMchOrderNo()+"]已存在");
            }

            if(StringUtils.isNotEmpty(bizRQ.getNotifyUrl()) && !StringKit.isAvailableUrl(bizRQ.getNotifyUrl())){
                throw new BaseException("异步通知地址协议仅支持http:// 或 https:// !");
            }

            // 商户配置信息
            MchAppConfigContext mchAppConfigContext = configContextQueryService.queryMchInfoAndAppInfo(mchNo, appId);
            if(mchAppConfigContext == null){
                throw new BaseException("获取商户应用信息失败");
            }

            TMchInfo mchInfo = mchAppConfigContext.getMchInfo();
            TMchApp mchApp = mchAppConfigContext.getMchApp();

            // 是否已正确配置
            if(!payInterfaceConfigService.mchAppHasAvailableIfCode(appId, ifCode)){
                throw new BaseException("应用未开通此接口配置!");
            }

            ITransferService transferService = SpringUtil.getBean(ifCode + "TransferService", ITransferService.class);
            if(transferService == null){
                throw new BaseException("无此转账通道接口");
            }

            if(!transferService.isSupport(bizRQ.getEntryType())){
                throw new BaseException("该接口不支持该入账方式");
            }

            transferOrder = genTransferOrder(bizRQ, mchInfo, mchApp, ifCode);

            //预先校验
            String errMsg = transferService.preCheck(bizRQ, transferOrder);
            if(StringUtils.isNotEmpty(errMsg)){
                throw new BaseException(errMsg);
            }

            // 入库
            transferOrderService.save(transferOrder);

            // 调起上游接口
            ChannelRetMsg channelRetMsg = transferService.transfer(bizRQ, transferOrder, mchAppConfigContext);

            //处理退款单状态
            this.processChannelMsg(channelRetMsg, transferOrder);

            TransferOrderRS bizRes = TransferOrderRS.buildByRecord(transferOrder);
            return ApiRes.okWithSign(bizRes, mchApp.getAppSecret());

        }  catch (BaseException e) {
            return ApiRes.customFail(e.getMessage());

        } catch (ChannelException e) {

            //处理上游返回数据
            this.processChannelMsg(e.getChannelRetMsg(), transferOrder);

            if(e.getChannelRetMsg().getChannelState() == ChannelEnums.SYS_ERROR ){
                return ApiRes.customFail(e.getMessage());
            }

            TransferOrderRS bizRes = TransferOrderRS.buildByRecord(transferOrder);
            return ApiRes.okWithSign(bizRes, configContextQueryService.queryMchApp(bizRQ.getMchNo(), bizRQ.getAppId()).getAppSecret());

        } catch (Exception e) {
            log.error("系统异常：{}", e);
            return ApiRes.customFail("系统异常");
        }
    }


    private TTransferOrder genTransferOrder(TransferOrderRQ rq, TMchInfo mchInfo, TMchApp mchApp, String ifCode){

        TTransferOrder transferOrder = new TTransferOrder();
        transferOrder.setTransferId(SeqKit.genTransferId()); //生成转账订单号
        transferOrder.setMchNo(mchInfo.getMchNo()); //商户号
        transferOrder.setIsvNo(mchInfo.getIsvNo()); //服务商号
        transferOrder.setAppId(mchApp.getAppId()); //商户应用appId
        transferOrder.setMchName(mchInfo.getMchShortName()); //商户名称（简称）
        transferOrder.setMchType(mchInfo.getType()); //商户类型
        transferOrder.setMchOrderNo(rq.getMchOrderNo()); //商户订单号
        transferOrder.setIfCode(ifCode); //接口代码
        transferOrder.setEntryType(rq.getEntryType()); //入账方式
        transferOrder.setAmount(rq.getAmount()); //订单金额
        transferOrder.setCurrency(rq.getCurrency()); //币种
        transferOrder.setClientIp(StringUtils.defaultIfEmpty(rq.getClientIp(), getClientIp())); //客户端IP
        transferOrder.setState(ITransferEnums.TransferStateEnums.STATE_INIT.getState()); //订单状态, 默认订单生成状态
        transferOrder.setAccountNo(rq.getAccountNo()); //收款账号
        transferOrder.setAccountName(rq.getAccountName()); //账户姓名
        transferOrder.setBankName(rq.getBankName()); //银行名称
        transferOrder.setTransferDesc(rq.getTransferDesc()); //转账备注
        transferOrder.setExtParam(rq.getExtParam()); //商户扩展参数
        transferOrder.setNotifyUrl(rq.getNotifyUrl()); //异步通知地址
        transferOrder.setCreatedAt(new Date()); //订单创建时间
        return transferOrder;

    }


    /**
     * 处理返回的渠道信息，并更新订单状态
     *  TransferOrder将对部分信息进行 赋值操作。
     * **/
    private void processChannelMsg(ChannelRetMsg channelRetMsg, TTransferOrder transferOrder){

        //对象为空 || 上游返回状态为空， 则无需操作
        if(channelRetMsg == null || channelRetMsg.getChannelState() == null){
            return ;
        }

        String transferId = transferOrder.getTransferId();

        //明确成功
        if(ChannelEnums.CONFIRM_SUCCESS == channelRetMsg.getChannelState()) {

            this.updateInitOrderStateThrowException(ITransferEnums.TransferStateEnums.STATE_SUCCESS.getState(), transferOrder, channelRetMsg);
            payMchNotifyService.transferOrderNotify(transferOrder);

            //明确失败
        }else if(ChannelEnums.CONFIRM_FAIL == channelRetMsg.getChannelState()) {

            this.updateInitOrderStateThrowException(ITransferEnums.TransferStateEnums.STATE_FAIL.getState(), transferOrder, channelRetMsg);
            payMchNotifyService.transferOrderNotify(transferOrder);

            // 上游处理中 || 未知 || 上游接口返回异常  订单为支付中状态
        }else if( ChannelEnums.WAITING == channelRetMsg.getChannelState() ||
                ChannelEnums.UNKNOWN == channelRetMsg.getChannelState() ||
                ChannelEnums.API_RET_ERROR == channelRetMsg.getChannelState()

        ){
            this.updateInitOrderStateThrowException(ITransferEnums.TransferStateEnums.STATE_ING.getState(), transferOrder, channelRetMsg);

            // 系统异常：  订单不再处理。  为： 生成状态
        }else if( ChannelEnums.SYS_ERROR == channelRetMsg.getChannelState()){

        }else{

            throw new BaseException("ChannelState 返回异常！");
        }

    }


    /** 更新订单状态 --》 订单生成--》 其他状态  (向外抛出异常) **/
    private void updateInitOrderStateThrowException(int orderState, TTransferOrder transferOrder, ChannelRetMsg channelRetMsg){

        transferOrder.setState(orderState);
        transferOrder.setChannelOrderNo(channelRetMsg.getChannelOrderId());
        transferOrder.setErrCode(channelRetMsg.getChannelErrCode());
        transferOrder.setErrMsg(channelRetMsg.getChannelErrMsg());


        boolean isSuccess = transferOrderService.updateInit2Ing(transferOrder.getTransferId());
        if(!isSuccess){
            throw new BaseException("更新转账订单异常!");
        }

        isSuccess = transferOrderService.updateIng2SuccessOrFail(transferOrder.getTransferId(), transferOrder.getState(),
                channelRetMsg.getChannelOrderId(), channelRetMsg.getChannelErrCode(), channelRetMsg.getChannelErrMsg());
        if(!isSuccess){
            throw new BaseException("更新转账订单异常!");
        }
    }


}
