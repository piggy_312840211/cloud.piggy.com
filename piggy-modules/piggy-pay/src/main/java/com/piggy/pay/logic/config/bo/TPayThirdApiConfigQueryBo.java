package com.piggy.pay.logic.config.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 支付接口配置参数分页查询对象 t_pay_third_api_config
 *
 * @author piggy
 * @date 2023-05-25
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel("支付接口配置参数分页查询对象")
public class TPayThirdApiConfigQueryBo extends BaseEntity {

	/** ID */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("ID")
	private Long id;
	/** 账号类型:1-服务商 2-商户 3-商户应用 */
	@ApiModelProperty("账号类型:1-服务商 2-商户 3-商户应用")
	private Integer infoType;
	/** 服务商号/商户号/应用ID */
	@ApiModelProperty("服务商号/商户号/应用ID")
	private String infoId;
	/** 支付接口代码 */
	@ApiModelProperty("支付接口代码")
	private String ifCode;
	/** 接口配置参数,json字符串 */
	@ApiModelProperty("接口配置参数,json字符串")
	private String ifParams;
	/** 支付接口费率 */
	@ApiModelProperty("支付接口费率")
	private BigDecimal ifRate;
	/** 状态: 0-停用, 1-启用 */
	@ApiModelProperty("状态: 0-停用, 1-启用")
	private Integer state;
	/** 创建者用户ID */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("创建者用户ID")
	private Long createdUid;
	/** 创建者姓名 */
	@ApiModelProperty("创建者姓名")
	private String createdBy;
	/** 创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;
	/** 更新者用户ID */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("更新者用户ID")
	private Long updatedUid;
	/** 更新者姓名 */
	@ApiModelProperty("更新者姓名")
	private String updatedBy;
	/** 更新时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;

}
