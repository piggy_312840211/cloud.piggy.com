package com.piggy.pay.logic.mch.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 商户应用编辑对象 t_mch_app
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("商户应用编辑对象")
public class TMchAppEditBo {


    /** 应用ID */
    @ApiModelProperty("应用ID")
    private String appId;

    /** 应用名称 */
    @ApiModelProperty("应用名称")
    @NotBlank(message = "应用名称不能为空")
    private String appName;

    /** 商户号 */
    @ApiModelProperty("商户号")
    @NotBlank(message = "商户号不能为空")
    private String mchNo;

    /** 应用状态: 0-停用, 1-正常 */
    @ApiModelProperty("应用状态: 0-停用, 1-正常")
    @NotNull(message = "应用状态: 0-停用, 1-正常不能为空")
    private Integer state;

    /** 应用私钥 */
    @ApiModelProperty("应用私钥")
    @NotBlank(message = "应用私钥不能为空")
    private String appSecret;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;

    /** 创建者用户ID */
    @ApiModelProperty("创建者用户ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long createdUid;

    /** 创建者姓名 */
    @ApiModelProperty("创建者姓名")
    private String createdBy;

    /** 更新人 */
    @ApiModelProperty("更新人")
    private String updateBy;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @NotNull(message = "创建时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @NotNull(message = "更新时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;
}
