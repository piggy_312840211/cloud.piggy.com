package com.piggy.pay.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public interface IDivisionEnums {

    @Getter
    @AllArgsConstructor
    enum DivisionModeEnums {
        DIVISION_MODE_FORBID((byte) 0, "该笔订单不允许分账"),
        DIVISION_MODE_AUTO((byte) 1, "支付成功按配置自动完成分账"),
        DIVISION_MODE_MANUAL((byte) 2, "商户手动分账(解冻商户金额)"),

        ;

        private int mode;
        private String msg;

    }

    @Getter
    @AllArgsConstructor
    enum DivisionStateEnums {
        DIVISION_STATE_UNHAPPEN(0, "未发生分账"),
        DIVISION_STATE_WAIT_TASK(1, "等待分账任务处理"),
        DIVISION_STATE_ING(2, "分账处理中"),
        DIVISION_STATE_FINISH(2, "分账任务已结束"),

        ;

        private int state;
        private String msg;

    }

    @Getter
    @AllArgsConstructor
    enum OrderDivisionStateEnums {
        STATE_WAIT(0, "待分账"),
        STATE_SUCCESS(1, "分账成功（明确成功）"),
        STATE_FAIL(2, "分账失败（明确失败）"),
        STATE_ACCEPT(2, "分账已受理（上游受理）"),

        ;

        private int state;
        private String msg;
    }

}
