package com.piggy.pay.logic.mch.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.mch.domain.TMchDivisionReceiverGroup;

/**
 * 分账账号组Mapper接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface TMchDivisionReceiverGroupMapper extends BaseMapperPlus<TMchDivisionReceiverGroup> {

}
