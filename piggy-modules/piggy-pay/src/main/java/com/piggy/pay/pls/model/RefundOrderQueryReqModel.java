package com.piggy.pay.pls.model;

import com.piggy.pay.pls.ApiField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 退款查单请求实体类
 * @author jmdhappy
 * @site https://www.jeepay.vip
 * @date 2021-06-18 10:00
 */
@Data
@NoArgsConstructor
public class RefundOrderQueryReqModel implements Serializable {

    private static final long serialVersionUID = -5184554341263929245L;

    /**
     * 商户号
     */
    @ApiField("mchNo")
    private String mchNo;
    /**
     * 应用ID
     */
    @ApiField("appId")
    private String appId;
    /**
     * 商户退款单号
     */
    @ApiField("mchRefundNo")
    String mchRefundNo;
    /**
     * 支付系统退款订单号
     */
    @ApiField("refundOrderId")
    String refundOrderId;

}
