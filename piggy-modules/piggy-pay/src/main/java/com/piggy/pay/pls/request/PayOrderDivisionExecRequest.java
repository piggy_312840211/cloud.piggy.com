package com.piggy.pay.pls.request;

import com.piggy.pay.pls.BasePay;
import com.piggy.pay.pls.model.PayOrderDivisionExecReqModel;
import com.piggy.pay.pls.net.RequestOptions;
import com.piggy.pay.pls.response.PayOrderDivisionExecResponse;

/***
* 分账发起
*
* @author terrfly
* @site https://www.jeepay.vip
* @date 2021/8/27 10:19
*/
public class PayOrderDivisionExecRequest implements PayRequest<PayOrderDivisionExecResponse, PayOrderDivisionExecReqModel> {

    private String apiVersion = BasePay.VERSION;
    private String apiUri = "api/division/exec";
    private RequestOptions options;
    private PayOrderDivisionExecReqModel bizModel = null;

    @Override
    public String getApiUri() {
        return this.apiUri;
    }

    @Override
    public String getApiVersion() {
        return this.apiVersion;
    }

    @Override
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    @Override
    public RequestOptions getRequestOptions() {
        return this.options;
    }

    @Override
    public void setRequestOptions(RequestOptions options) {
        this.options = options;
    }

    @Override
    public PayOrderDivisionExecReqModel getBizModel() {
        return this.bizModel;
    }

    @Override
    public void setBizModel(PayOrderDivisionExecReqModel bizModel) {
        this.bizModel = bizModel;
    }

    @Override
    public Class<PayOrderDivisionExecResponse> getResponseClass() {
        return PayOrderDivisionExecResponse.class;
    }

}
