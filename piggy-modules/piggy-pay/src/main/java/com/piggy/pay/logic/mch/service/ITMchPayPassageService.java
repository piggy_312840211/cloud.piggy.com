package com.piggy.pay.logic.mch.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.mch.domain.TMchPayPassage;
import com.piggy.pay.logic.mch.vo.TMchPayPassageVo;
import com.piggy.pay.logic.mch.bo.TMchPayPassageQueryBo;
import com.piggy.pay.logic.mch.bo.TMchPayPassageEditBo;

import java.util.Collection;
import java.util.List;

/**
 * 商户支付通道Service接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface ITMchPayPassageService extends IServicePlus<TMchPayPassage> {
    /**
     * 查询单个
     *
     * @return
     */
    TMchPayPassageVo queryById(Long id);

    /**
     * 查询列表
     */
    TableDataInfo<TMchPayPassageVo> queryPageList(TMchPayPassageQueryBo bo);

    /**
     * 查询列表
     */
    List<TMchPayPassageVo> queryList(TMchPayPassageQueryBo bo);

    /**
     * 根据新增业务对象插入商户支付通道
     *
     * @param bo 商户支付通道新增业务对象
     * @return
     */
    Boolean insertByAddBo(TMchPayPassageEditBo bo);

    /**
     * 根据编辑业务对象修改商户支付通道
     *
     * @param bo 商户支付通道编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TMchPayPassageEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    TMchPayPassage findMchPayPassage(String mchNo, String appId, String wayCode);

}
