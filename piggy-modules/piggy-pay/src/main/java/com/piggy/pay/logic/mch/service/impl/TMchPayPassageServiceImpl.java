package com.piggy.pay.logic.mch.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.core.constants.CS;
import com.piggy.pay.logic.config.domain.TPayThirdApiDefine;
import com.piggy.pay.logic.config.service.ITPayThirdApiDefineService;
import com.piggy.pay.logic.mch.bo.TMchPayPassageEditBo;
import com.piggy.pay.logic.mch.bo.TMchPayPassageQueryBo;
import com.piggy.pay.logic.mch.domain.TMchPayPassage;
import com.piggy.pay.logic.mch.mapper.TMchPayPassageMapper;
import com.piggy.pay.logic.mch.service.ITMchPayPassageService;
import com.piggy.pay.logic.mch.vo.TMchPayPassageVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Collection;

/**
 * 商户支付通道Service业务层处理
 *
 * @author piggy
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TMchPayPassageServiceImpl extends ServiceImpl<TMchPayPassageMapper, TMchPayPassage> implements ITMchPayPassageService {

    @Resource
    private ITPayThirdApiDefineService payInterfaceDefineService;

    @Override
    public TMchPayPassageVo queryById(Long id) {
        return getVoById(id, TMchPayPassageVo.class);
    }

    @Override
    public TableDataInfo<TMchPayPassageVo> queryPageList(TMchPayPassageQueryBo bo) {
        PagePlus<TMchPayPassage, TMchPayPassageVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TMchPayPassageVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TMchPayPassageVo> queryList(TMchPayPassageQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TMchPayPassageVo.class);
    }

    private LambdaQueryWrapper<TMchPayPassage> buildQueryWrapper(TMchPayPassageQueryBo bo) {
        LambdaQueryWrapper<TMchPayPassage> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getId() != null, TMchPayPassage::getId, bo.getId());
        lqw.eq(StrUtil.isNotBlank(bo.getMchNo()), TMchPayPassage::getMchNo, bo.getMchNo());
        lqw.eq(StrUtil.isNotBlank(bo.getAppId()), TMchPayPassage::getAppId, bo.getAppId());
        lqw.eq(StrUtil.isNotBlank(bo.getIfCode()), TMchPayPassage::getIfCode, bo.getIfCode());
        lqw.eq(StrUtil.isNotBlank(bo.getWayCode()), TMchPayPassage::getWayCode, bo.getWayCode());
        lqw.eq(bo.getRate() != null, TMchPayPassage::getRate, bo.getRate());
        lqw.eq(StrUtil.isNotBlank(bo.getRiskConfig()), TMchPayPassage::getRiskConfig, bo.getRiskConfig());
        lqw.eq(bo.getState() != null, TMchPayPassage::getState, bo.getState());
        lqw.eq(bo.getCreatedAt() != null, TMchPayPassage::getCreatedAt, bo.getCreatedAt());
        lqw.eq(bo.getUpdatedAt() != null, TMchPayPassage::getUpdatedAt, bo.getUpdatedAt());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TMchPayPassageEditBo bo) {
        TMchPayPassage add = BeanUtil.toBean(bo, TMchPayPassage.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TMchPayPassageEditBo bo) {
        TMchPayPassage update = BeanUtil.toBean(bo, TMchPayPassage.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TMchPayPassage entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }



    /** 根据应用ID 和 支付方式， 查询出商户可用的支付接口 **/
    public TMchPayPassage findMchPayPassage(String mchNo, String appId, String wayCode){

        List<TMchPayPassage> list = list(new LambdaQueryWrapper<TMchPayPassage>()
                .eq(TMchPayPassage::getMchNo, mchNo)
                .eq(TMchPayPassage::getAppId, appId)
                .eq(TMchPayPassage::getState, CS.YES)
                .eq(TMchPayPassage::getWayCode, wayCode)
        );

        if (list.isEmpty()) {
            return null;
        }else { // 返回一个可用通道

            HashMap<String, TMchPayPassage> mchPayPassageMap = new HashMap<>();

            for (TMchPayPassage mchPayPassage:list) {
                mchPayPassageMap.put(mchPayPassage.getIfCode(), mchPayPassage);
            }
            // 查询ifCode所有接口
            TPayThirdApiDefine interfaceDefine = payInterfaceDefineService
                    .getOne(new LambdaQueryWrapper<TPayThirdApiDefine>()
                            .select(TPayThirdApiDefine::getIfCode, TPayThirdApiDefine::getState)
                            .eq(TPayThirdApiDefine::getState, CS.YES)
                            .in(TPayThirdApiDefine::getIfCode, mchPayPassageMap.keySet()), false);

            if (interfaceDefine != null) {
                return mchPayPassageMap.get(interfaceDefine.getIfCode());
            }
        }
        return null;
    }

}
