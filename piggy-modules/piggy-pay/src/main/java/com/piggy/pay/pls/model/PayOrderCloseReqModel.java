package com.piggy.pay.pls.model;

import com.piggy.pay.pls.ApiField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 支付关闭请求实体类
 *
 * @author xiaoyu
 * @site https://www.jeequan.com
 * @date 2022/1/25 9:53
 */
@Data
@NoArgsConstructor
public class PayOrderCloseReqModel implements Serializable {

    private static final long serialVersionUID = -5184554341263929245L;

    /**
     * 商户号
     */
    @ApiField("mchNo")
    private String mchNo;
    /**
     * 应用ID
     */
    @ApiField("appId")
    private String appId;
    /**
     * 商户订单号
     */
    @ApiField("mchOrderNo")
    String mchOrderNo;
    /**
     * 支付订单号
     */
    @ApiField("payOrderId")
    String payOrderId;

}
