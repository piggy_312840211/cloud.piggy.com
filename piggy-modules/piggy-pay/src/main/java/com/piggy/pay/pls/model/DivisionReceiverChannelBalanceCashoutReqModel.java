package com.piggy.pay.pls.model;

import com.piggy.pay.pls.ApiField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/***
* 分账渠道余额提现
*
* @author terrfly
* @site https://www.jeequan.com
* @date 2022/5/11 15:03
*/
@Data
@NoArgsConstructor
public class DivisionReceiverChannelBalanceCashoutReqModel implements Serializable {

    @ApiField("mchNo")
    private String mchNo;      // 商户号

    @ApiField("appId")
    private String appId;      // 应用ID

    /** receiverId   **/
    @ApiField("receiverId")
    private Long receiverId;

    /** 提现金额： 单位：分   **/
    @ApiField("cashoutAmount")
    private Long cashoutAmount;

}
