package com.piggy.pay.logic.mch.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 分账账号组编辑对象 t_mch_division_receiver_group
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("分账账号组编辑对象")
public class TMchDivisionReceiverGroupEditBo {


    /** 组ID */
    @ApiModelProperty("组ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long receiverGroupId;

    /** 组名称 */
    @ApiModelProperty("组名称")
    @NotBlank(message = "组名称不能为空")
    private String receiverGroupName;

    /** 商户号 */
    @ApiModelProperty("商户号")
    @NotBlank(message = "商户号不能为空")
    private String mchNo;

    /** 自动分账组（当订单分账模式为自动分账，改组将完成分账逻辑） 0-否 1-是 */
    @ApiModelProperty("自动分账组（当订单分账模式为自动分账，改组将完成分账逻辑） 0-否 1-是")
    @NotNull(message = "自动分账组（当订单分账模式为自动分账，改组将完成分账逻辑） 0-否 1-是不能为空")
    private Integer autoDivisionFlag;

    /** 创建者用户ID */
    @ApiModelProperty("创建者用户ID")
    @NotNull(message = "创建者用户ID不能为空")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long createdUid;

    /** 创建者姓名 */
    @ApiModelProperty("创建者姓名")
    @NotBlank(message = "创建者姓名不能为空")
    private String createdBy;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @NotNull(message = "创建时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @NotNull(message = "更新时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    /** 更新人 */
    @ApiModelProperty("更新人")
    @NotBlank(message = "更新人不能为空")
    private String updateBy;
}
