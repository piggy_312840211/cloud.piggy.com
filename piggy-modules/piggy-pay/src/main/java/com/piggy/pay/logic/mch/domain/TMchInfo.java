package com.piggy.pay.logic.mch.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import com.baomidou.mybatisplus.annotation.*;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;


/**
 * 商户信息对象 t_mch_info
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("t_mch_info")
public class TMchInfo implements Serializable {

    private static final long serialVersionUID=1L;

    /** 商户号 */
    @ApiModelProperty("商户号")
    @TableId(value = "mch_no")
    private String mchNo;

    /** 商户名称 */
    @ApiModelProperty("商户名称")
    private String mchName;

    /** 商户简称 */
    @ApiModelProperty("商户简称")
    private String mchShortName;

    /** 类型: 1-普通商户, 2-特约商户(服务商模式) */
    @ApiModelProperty("类型: 1-普通商户, 2-特约商户(服务商模式)")
    private Integer type;

    /** 服务商号 */
    @ApiModelProperty("服务商号")
    private String isvNo;

    /** 联系人姓名 */
    @ApiModelProperty("联系人姓名")
    private String contactName;

    /** 联系人手机号 */
    @ApiModelProperty("联系人手机号")
    private String contactTel;

    /** 联系人邮箱 */
    @ApiModelProperty("联系人邮箱")
    private String contactEmail;

    /** 商户状态: 0-停用, 1-正常 */
    @ApiModelProperty("商户状态: 0-停用, 1-正常")
    private Integer state;

    /** 商户备注 */
    @ApiModelProperty("商户备注")
    private String remark;

    /** 初始用户ID（创建商户时，允许商户登录的用户） */
    @ApiModelProperty("初始用户ID（创建商户时，允许商户登录的用户）")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long initUserId;

    /** 创建者用户ID */
    @ApiModelProperty("创建者用户ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long createdUid;

    /** 创建者姓名 */
    @ApiModelProperty("创建者姓名")
    private String createdBy;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新人 */
    @ApiModelProperty("更新人")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

}
