package com.piggy.pay.logic.order.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 退款订单对象 t_refund_order
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("t_refund_order")
public class TRefundOrder implements Serializable {

    private static final long serialVersionUID=1L;

    /** 退款订单号（支付系统生成订单号） */
    @ApiModelProperty("退款订单号（支付系统生成订单号）")
    @TableId(value = "refund_order_id")
    private String refundOrderId;

    /** 支付订单号（与t_pay_order对应） */
    @ApiModelProperty("支付订单号（与t_pay_order对应）")
    private String payOrderId;

    /** 渠道支付单号（与t_pay_order channel_order_no对应） */
    @ApiModelProperty("渠道支付单号（与t_pay_order channel_order_no对应）")
    private String channelPayOrderNo;

    /** 商户号 */
    @ApiModelProperty("商户号")
    private String mchNo;

    /** 服务商号 */
    @ApiModelProperty("服务商号")
    private String isvNo;

    /** 应用ID */
    @ApiModelProperty("应用ID")
    private String appId;

    /** 商户名称 */
    @ApiModelProperty("商户名称")
    private String mchName;

    /** 类型: 1-普通商户, 2-特约商户(服务商模式) */
    @ApiModelProperty("类型: 1-普通商户, 2-特约商户(服务商模式)")
    private Integer mchType;

    /** 商户退款单号（商户系统的订单号） */
    @ApiModelProperty("商户退款单号（商户系统的订单号）")
    private String mchRefundNo;

    /** 支付方式代码 */
    @ApiModelProperty("支付方式代码")
    private String wayCode;

    /** 支付接口代码 */
    @ApiModelProperty("支付接口代码")
    private String ifCode;

    /** 支付金额,单位分 */
    @ApiModelProperty("支付金额,单位分")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long payAmount;

    /** 退款金额,单位分 */
    @ApiModelProperty("退款金额,单位分")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long refundAmount;

    /** 三位货币代码,人民币:cny */
    @ApiModelProperty("三位货币代码,人民币:cny")
    private String currency;

    /** 退款状态:0-订单生成,1-退款中,2-退款成功,3-退款失败,4-退款任务关闭 */
    @ApiModelProperty("退款状态:0-订单生成,1-退款中,2-退款成功,3-退款失败,4-退款任务关闭")
    private Integer state;

    /** 客户端IP */
    @ApiModelProperty("客户端IP")
    private String clientIp;

    /** 退款原因 */
    @ApiModelProperty("退款原因")
    private String refundReason;

    /** 渠道订单号 */
    @ApiModelProperty("渠道订单号")
    private String channelOrderNo;

    /** 渠道错误码 */
    @ApiModelProperty("渠道错误码")
    private String errCode;

    /** 渠道错误描述 */
    @ApiModelProperty("渠道错误描述")
    private String errMsg;

    /** 特定渠道发起时额外参数 */
    @ApiModelProperty("特定渠道发起时额外参数")
    private String channelExtra;

    /** 通知地址 */
    @ApiModelProperty("通知地址")
    private String notifyUrl;

    /** 扩展参数 */
    @ApiModelProperty("扩展参数")
    private String extParam;

    /** 订单退款成功时间 */
    @ApiModelProperty("订单退款成功时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date successTime;

    /** 退款失效时间（失效后系统更改为退款任务关闭状态） */
    @ApiModelProperty("退款失效时间（失效后系统更改为退款任务关闭状态）")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date expiredTime;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

}
