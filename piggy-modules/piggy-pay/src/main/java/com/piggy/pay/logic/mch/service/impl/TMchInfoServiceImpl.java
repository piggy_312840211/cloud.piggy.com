package com.piggy.pay.logic.mch.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.mch.bo.TMchInfoEditBo;
import com.piggy.pay.logic.mch.bo.TMchInfoQueryBo;
import com.piggy.pay.logic.mch.domain.TMchInfo;
import com.piggy.pay.logic.mch.mapper.TMchInfoMapper;
import com.piggy.pay.logic.mch.service.ITMchInfoService;
import com.piggy.pay.logic.mch.vo.TMchInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
import java.util.Collection;

/**
 * 商户信息Service业务层处理
 *
 * @author piggy
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TMchInfoServiceImpl extends ServiceImpl<TMchInfoMapper, TMchInfo> implements ITMchInfoService {

    @Override
    public TMchInfoVo queryById(String mchNo){
        return getVoById(mchNo, TMchInfoVo.class);
    }

    @Override
    public TableDataInfo<TMchInfoVo> queryPageList(TMchInfoQueryBo bo) {
        PagePlus<TMchInfo, TMchInfoVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TMchInfoVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TMchInfoVo> queryList(TMchInfoQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TMchInfoVo.class);
    }

    private LambdaQueryWrapper<TMchInfo> buildQueryWrapper(TMchInfoQueryBo bo) {
        LambdaQueryWrapper<TMchInfo> lqw = Wrappers.lambdaQuery();
        lqw.eq(StrUtil.isNotBlank(bo.getMchNo()), TMchInfo::getMchNo, bo.getMchNo());
        lqw.like(StrUtil.isNotBlank(bo.getMchName()), TMchInfo::getMchName, bo.getMchName());
        lqw.like(StrUtil.isNotBlank(bo.getMchShortName()), TMchInfo::getMchShortName, bo.getMchShortName());
        lqw.eq(bo.getType() != null, TMchInfo::getType, bo.getType());
        lqw.eq(StrUtil.isNotBlank(bo.getIsvNo()), TMchInfo::getIsvNo, bo.getIsvNo());
        lqw.like(StrUtil.isNotBlank(bo.getContactName()), TMchInfo::getContactName, bo.getContactName());
        lqw.eq(StrUtil.isNotBlank(bo.getContactTel()), TMchInfo::getContactTel, bo.getContactTel());
        lqw.eq(StrUtil.isNotBlank(bo.getContactEmail()), TMchInfo::getContactEmail, bo.getContactEmail());
        lqw.eq(bo.getState() != null, TMchInfo::getState, bo.getState());
        lqw.eq(bo.getInitUserId() != null, TMchInfo::getInitUserId, bo.getInitUserId());
        lqw.eq(bo.getCreatedUid() != null, TMchInfo::getCreatedUid, bo.getCreatedUid());
        lqw.eq(StrUtil.isNotBlank(bo.getCreatedBy()), TMchInfo::getCreatedBy, bo.getCreatedBy());
        lqw.eq(bo.getCreatedAt() != null, TMchInfo::getCreatedAt, bo.getCreatedAt());
        lqw.eq(StrUtil.isNotBlank(bo.getUpdateBy()), TMchInfo::getUpdateBy, bo.getUpdateBy());
        lqw.eq(bo.getUpdatedAt() != null, TMchInfo::getUpdatedAt, bo.getUpdatedAt());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TMchInfoEditBo bo) {
        TMchInfo add = BeanUtil.toBean(bo, TMchInfo.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TMchInfoEditBo bo) {
        TMchInfo update = BeanUtil.toBean(bo, TMchInfo.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TMchInfo entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
