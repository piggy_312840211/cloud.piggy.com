package com.piggy.pay.logic.order.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 订单接口数据快照编辑对象 t_order_snapshot
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("订单接口数据快照编辑对象")
public class TOrderSnapshotEditBo {


    /** 订单ID */
    @ApiModelProperty("订单ID")
    private String orderId;

    /** 订单类型: 1-支付, 2-退款 */
    @ApiModelProperty("订单类型: 1-支付, 2-退款")
    private Integer orderType;

    /** 下游请求数据 */
    @ApiModelProperty("下游请求数据")
    private String mchReqData;

    /** 下游请求时间 */
    @ApiModelProperty("下游请求时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date mchReqTime;

    /** 向下游响应数据 */
    @ApiModelProperty("向下游响应数据")
    private String mchRespData;

    /** 向下游响应时间 */
    @ApiModelProperty("向下游响应时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date mchRespTime;

    /** 向上游请求数据 */
    @ApiModelProperty("向上游请求数据")
    private String channelReqData;

    /** 向上游请求时间 */
    @ApiModelProperty("向上游请求时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date channelReqTime;

    /** 上游响应数据 */
    @ApiModelProperty("上游响应数据")
    private String channelRespData;

    /** 上游响应时间 */
    @ApiModelProperty("上游响应时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date channelRespTime;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @NotNull(message = "创建时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @NotNull(message = "更新时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;
}
