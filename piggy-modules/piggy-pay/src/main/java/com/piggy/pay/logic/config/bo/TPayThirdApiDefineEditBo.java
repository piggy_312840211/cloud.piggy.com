package com.piggy.pay.logic.config.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 支付接口定义编辑对象 t_pay_third_api_define
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("支付接口定义编辑对象")
public class TPayThirdApiDefineEditBo {


    /** 接口代码 全小写  wxpay alipay  */
    @ApiModelProperty("接口代码 全小写  wxpay alipay ")
    private String ifCode;

    /** 接口名称 */
    @ApiModelProperty("接口名称")
    @NotBlank(message = "接口名称不能为空")
    private String ifName;

    /** 是否支持普通商户模式: 0-不支持, 1-支持 */
    @ApiModelProperty("是否支持普通商户模式: 0-不支持, 1-支持")
    @NotNull(message = "是否支持普通商户模式: 0-不支持, 1-支持不能为空")
    private Integer isMchMode;

    /** 是否支持服务商子商户模式: 0-不支持, 1-支持 */
    @ApiModelProperty("是否支持服务商子商户模式: 0-不支持, 1-支持")
    @NotNull(message = "是否支持服务商子商户模式: 0-不支持, 1-支持不能为空")
    private Integer isIsvMode;

    /** 支付参数配置页面类型:1-JSON渲染,2-自定义 */
    @ApiModelProperty("支付参数配置页面类型:1-JSON渲染,2-自定义")
    @NotNull(message = "支付参数配置页面类型:1-JSON渲染,2-自定义不能为空")
    private Integer configPageType;

    /** ISV接口配置定义描述,json字符串 */
    @ApiModelProperty("ISV接口配置定义描述,json字符串")
    private String isvParams;

    /** 特约商户接口配置定义描述,json字符串 */
    @ApiModelProperty("特约商户接口配置定义描述,json字符串")
    private String isvsubMchParams;

    /** 普通商户接口配置定义描述,json字符串 */
    @ApiModelProperty("普通商户接口配置定义描述,json字符串")
    private String normalMchParams;

    /** 支持的支付方式 ["wxpay_jsapi", "wxpay_bar"] */
    @ApiModelProperty("支持的支付方式 [wxpay_jsapi, wxpay_bar]")
    @NotBlank(message = "支持的支付方式 [wxpay_jsapi, wxpay_bar]不能为空")
    private String wayCodes;

    /** 页面展示：卡片-图标 */
    @ApiModelProperty("页面展示：卡片-图标")
    private String icon;

    /** 页面展示：卡片-背景色 */
    @ApiModelProperty("页面展示：卡片-背景色")
    private String bgColor;

    /** 状态: 0-停用, 1-启用 */
    @ApiModelProperty("状态: 0-停用, 1-启用")
    @NotNull(message = "状态: 0-停用, 1-启用不能为空")
    private Integer state;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @NotNull(message = "创建时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @NotNull(message = "更新时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;
}
