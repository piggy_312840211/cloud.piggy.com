package com.piggy.pay.logic.order.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.order.domain.TPayOrderDivisionRecord;
import org.apache.ibatis.annotations.Param;

/**
 * 分账记录Mapper接口
 *
 * @author zito
 * @date 2023-05-25
 */
public interface TPayOrderDivisionRecordMapperEx extends BaseMapperPlus<TPayOrderDivisionRecord> {

    /** 查询全部分账成功金额 **/
    Long sumSuccessDivisionAmount(String payOrderId);

    /**  batch_order_id 去重， 查询出所有的 分账已受理状态的订单， 支持分页。 */
    IPage<TPayOrderDivisionRecord> distinctBatchOrderIdList(IPage<?> page, @Param("ew") Wrapper<TPayOrderDivisionRecord> wrapper);

}
