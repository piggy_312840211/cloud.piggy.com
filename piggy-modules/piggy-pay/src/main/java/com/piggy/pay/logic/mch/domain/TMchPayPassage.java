package com.piggy.pay.logic.mch.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import com.baomidou.mybatisplus.annotation.*;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;


/**
 * 商户支付通道对象 t_mch_pay_passage
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("t_mch_pay_passage")
public class TMchPayPassage implements Serializable {

    private static final long serialVersionUID=1L;

    /** ID */
    @ApiModelProperty("ID")
    @TableId(value = "id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 商户号 */
    @ApiModelProperty("商户号")
    private String mchNo;

    /** 应用ID */
    @ApiModelProperty("应用ID")
    private String appId;

    /** 支付接口 */
    @ApiModelProperty("支付接口")
    private String ifCode;

    /** 支付方式 */
    @ApiModelProperty("支付方式")
    private String wayCode;

    /** 支付方式费率 */
    @ApiModelProperty("支付方式费率")
    private BigDecimal rate;

    /** 风控数据 */
    @ApiModelProperty("风控数据")
    private String riskConfig;

    /** 状态: 0-停用, 1-启用 */
    @ApiModelProperty("状态: 0-停用, 1-启用")
    private Integer state;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

}
