package com.piggy.pay.logic.mch.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import com.piggy.pay.logic.mch.bo.TMchPayPassageEditBo;
import com.piggy.pay.logic.mch.bo.TMchPayPassageQueryBo;
import com.piggy.pay.logic.mch.service.ITMchPayPassageService;
import com.piggy.pay.logic.mch.vo.TMchPayPassageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 商户支付通道Controller
 *
 * @author piggy
 * @date 2023-05-25
 */
@Api(value = "商户支付通道控制器", tags = {"商户支付通道管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/mch/paypassage")
public class TMchPayPassageController extends BaseController {

    private final ITMchPayPassageService iTMchPayPassageService;

    /**
     * 查询商户支付通道列表
     */
    @ApiOperation("查询商户支付通道列表")
    @SaCheckPermission("mch:paypassage:list")
    @GetMapping("/list")
    public TableDataInfo<TMchPayPassageVo> list(@Validated TMchPayPassageQueryBo bo) {
        return iTMchPayPassageService.queryPageList(bo);
    }

    /**
     * 导出商户支付通道列表
     */
    @ApiOperation("导出商户支付通道列表")
    @SaCheckPermission("mch:paypassage:export")
    @Log(title = "商户支付通道", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TMchPayPassageQueryBo bo, HttpServletResponse response) throws IOException {
        List<TMchPayPassageVo> list = iTMchPayPassageService.queryList(bo);
        ExcelUtil<TMchPayPassageVo> util = new ExcelUtil<TMchPayPassageVo>(TMchPayPassageVo.class);
        util.exportExcel(response, list, "商户支付通道");
    }

    /**
     * 获取商户支付通道详细信息
     */
    @ApiOperation("获取商户支付通道详细信息")
    @SaCheckPermission("mch:paypassage:query")
    @GetMapping("/{id}")
    public R<TMchPayPassageVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("id") Long id) {
        return R.ok(iTMchPayPassageService.queryById(id));
    }

    /**
     * 新增商户支付通道
     */
    @ApiOperation("新增商户支付通道")
    @SaCheckPermission("mch:paypassage:add")
    @Log(title = "商户支付通道", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TMchPayPassageEditBo bo) {
        return R.ok(iTMchPayPassageService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改商户支付通道
     */
    @ApiOperation("修改商户支付通道")
    @SaCheckPermission("mch:paypassage:edit")
    @Log(title = "商户支付通道", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TMchPayPassageEditBo bo) {
        return R.ok(iTMchPayPassageService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除商户支付通道
     */
    @ApiOperation("删除商户支付通道")
    @SaCheckPermission("mch:paypassage:remove")
    @Log(title = "商户支付通道" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable Long[] ids) {
        return R.ok(iTMchPayPassageService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
