package com.piggy.pay.logic.mch.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import com.piggy.pay.logic.mch.bo.TMchAppEditBo;
import com.piggy.pay.logic.mch.bo.TMchAppQueryBo;
import com.piggy.pay.logic.mch.service.ITMchAppService;
import com.piggy.pay.logic.mch.vo.TMchAppVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 商户应用Controller
 *
 * @author piggy
 * @date 2023-05-25
 */
@Api(value = "商户应用控制器", tags = {"商户应用管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/mch/app")
public class TMchAppController extends BaseController {

    private final ITMchAppService iTMchAppService;

    /**
     * 查询商户应用列表
     */
    @ApiOperation("查询商户应用列表")
    @SaCheckPermission("mch:app:list")
    @GetMapping("/list")
    public TableDataInfo<TMchAppVo> list(@Validated TMchAppQueryBo bo) {
        return iTMchAppService.queryPageList(bo);
    }

    /**
     * 导出商户应用列表
     */
    @ApiOperation("导出商户应用列表")
    @SaCheckPermission("mch:app:export")
    @Log(title = "商户应用", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TMchAppQueryBo bo, HttpServletResponse response) throws IOException {
        List<TMchAppVo> list = iTMchAppService.queryList(bo);
        ExcelUtil<TMchAppVo> util = new ExcelUtil<TMchAppVo>(TMchAppVo.class);
        util.exportExcel(response, list, "商户应用");
    }

    /**
     * 获取商户应用详细信息
     */
    @ApiOperation("获取商户应用详细信息")
    @SaCheckPermission("mch:app:query")
    @GetMapping("/{appId}")
    public R<TMchAppVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("appId") String appId) {
        return R.ok(iTMchAppService.queryById(appId));
    }

    /**
     * 新增商户应用
     */
    @ApiOperation("新增商户应用")
    @SaCheckPermission("mch:app:add")
    @Log(title = "商户应用", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TMchAppEditBo bo) {
        return R.ok(iTMchAppService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改商户应用
     */
    @ApiOperation("修改商户应用")
    @SaCheckPermission("mch:app:edit")
    @Log(title = "商户应用", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TMchAppEditBo bo) {
        return R.ok(iTMchAppService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除商户应用
     */
    @ApiOperation("删除商户应用")
    @SaCheckPermission("mch:app:remove")
    @Log(title = "商户应用" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{appIds}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable String[] appIds) {
        return R.ok(iTMchAppService.deleteWithValidByIds(Arrays.asList(appIds), true) ? 1 : 0);
    }
}
