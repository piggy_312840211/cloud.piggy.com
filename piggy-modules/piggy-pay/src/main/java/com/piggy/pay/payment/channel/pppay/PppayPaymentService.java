package com.piggy.pay.payment.channel.pppay;

import com.piggy.pay.core.constants.CS;
import com.piggy.pay.logic.order.domain.TPayOrder;
import com.piggy.pay.payment.channel.AbstractPaymentService;
import com.piggy.pay.payment.model.MchAppConfigContext;
import com.piggy.pay.payment.rqrs.AbstractRS;
import com.piggy.pay.payment.rqrs.payorder.UnifiedOrderRQ;
import com.piggy.pay.payment.service.ConfigContextQueryService;
import com.piggy.pay.payment.util.PaywayUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * none.
 *
 * @author 陈泉
 * @package com.jeequan.jeepay.pay.channel.pppay
 * @create 2021/11/15 18:17
 */
@Service
public class PppayPaymentService extends AbstractPaymentService {

    @Resource
    public ConfigContextQueryService configContextQueryService;

    @Override
    public String getIfCode() {
        return CS.IF_CODE.PPPAY;
    }

    @Override
    public boolean isSupport(String wayCode) {
        return true;
    }

    @Override
    public String preCheck(UnifiedOrderRQ bizRQ, TPayOrder payOrder) {
        return PaywayUtil.getRealPaywayService(this, payOrder.getWayCode()).preCheck(bizRQ, payOrder);
    }

    @Override
    public AbstractRS pay(UnifiedOrderRQ bizRQ, TPayOrder payOrder, MchAppConfigContext mchAppConfigContext) throws
            Exception {
        return PaywayUtil.getRealPaywayService(this, payOrder.getWayCode()).pay(bizRQ, payOrder, mchAppConfigContext);
    }
}
