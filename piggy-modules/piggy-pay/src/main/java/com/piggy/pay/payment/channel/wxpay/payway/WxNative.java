/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.piggy.pay.payment.channel.wxpay.payway;

import com.github.binarywang.wxpay.bean.order.WxPayNativeOrderResult;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.constant.WxPayConstants;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.piggy.pay.core.constants.CS;
import com.piggy.pay.core.enums.ChannelEnums;
import com.piggy.pay.logic.config.sys.PayConfigService;
import com.piggy.pay.logic.order.domain.TPayOrder;
import com.piggy.pay.payment.channel.wxpay.WxpayPaymentService;
import com.piggy.pay.payment.channel.wxpay.kits.WxpayKit;
import com.piggy.pay.payment.model.MchAppConfigContext;
import com.piggy.pay.payment.model.WxServiceWrapper;
import com.piggy.pay.payment.rqrs.AbstractRS;
import com.piggy.pay.payment.rqrs.msg.ChannelRetMsg;
import com.piggy.pay.payment.rqrs.payorder.UnifiedOrderRQ;
import com.piggy.pay.payment.rqrs.payorder.payway.WxNativeOrderRQ;
import com.piggy.pay.payment.rqrs.payorder.payway.WxNativeOrderRS;
import com.piggy.pay.payment.util.ApiResBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/*
 * 微信 native支付
 *
 * @author zhuxiao
 * @site https://www.jeequan.com
 * @date 2021/6/8 18:08
 */
@Service("wxpayPaymentByNativeService") //Service Name需保持全局唯一性
public class WxNative extends WxpayPaymentService {

    @Resource
    private PayConfigService sysConfigService;

    @Override
    public String preCheck(UnifiedOrderRQ rq, TPayOrder payOrder) {

        return null;
    }

    @Override
    public AbstractRS pay(UnifiedOrderRQ rq, TPayOrder payOrder, MchAppConfigContext mchAppConfigContext) throws Exception{

        WxNativeOrderRQ bizRQ = (WxNativeOrderRQ) rq;

        WxPayUnifiedOrderRequest req = buildUnifiedOrderRequest(payOrder, mchAppConfigContext);
        req.setTradeType(WxPayConstants.TradeType.NATIVE);

        // 构造函数响应数据
        WxNativeOrderRS res = ApiResBuilder.buildSuccess(WxNativeOrderRS.class);
        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        res.setChannelRetMsg(channelRetMsg);

        // 调起上游接口：
        // 1. 如果抛异常，则订单状态为： 生成状态，此时没有查单处理操作。 订单将超时关闭
        // 2. 接口调用成功， 后续异常需进行捕捉， 如果 逻辑代码出现异常则需要走完正常流程，此时订单状态为： 支付中， 需要查单处理。
        WxServiceWrapper wxServiceWrapper = configContextQueryService.getWxServiceWrapper(mchAppConfigContext);
        WxPayService wxPayService = wxServiceWrapper.getWxPayService();
        try {
            WxPayNativeOrderResult wxPayNativeOrderResult = wxPayService.createOrder(req);

            String codeUrl = wxPayNativeOrderResult.getCodeUrl();
            if (CS.PAY_DATA_TYPE.CODE_IMG_URL.equals(bizRQ.getPayDataType())){ //二维码图片地址

                res.setCodeImgUrl(sysConfigService.getDBApplicationConfig().genScanImgUrl(codeUrl));
            }else{

                res.setCodeUrl(codeUrl);
            }

            // 支付中
            channelRetMsg.setChannelState(ChannelEnums.WAITING);

        } catch (WxPayException e) {
            channelRetMsg.setChannelState(ChannelEnums.CONFIRM_FAIL);
            WxpayKit.commonSetErrInfo(channelRetMsg, e);
        }

        return res;
    }

}
