package com.piggy.pay.logic.config.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import com.baomidou.mybatisplus.annotation.*;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

/**
 * 支付接口定义对象 t_pay_third_api_define
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("t_pay_third_api_define")
public class TPayThirdApiDefine implements Serializable {

    private static final long serialVersionUID=1L;

    /** 接口代码 全小写  wxpay alipay  */
    @ApiModelProperty("接口代码 全小写  wxpay alipay ")
    @TableId(value = "if_code")
    private String ifCode;

    /** 接口名称 */
    @ApiModelProperty("接口名称")
    private String ifName;

    /** 是否支持普通商户模式: 0-不支持, 1-支持 */
    @ApiModelProperty("是否支持普通商户模式: 0-不支持, 1-支持")
    private Integer isMchMode;

    /** 是否支持服务商子商户模式: 0-不支持, 1-支持 */
    @ApiModelProperty("是否支持服务商子商户模式: 0-不支持, 1-支持")
    private Integer isIsvMode;

    /** 支付参数配置页面类型:1-JSON渲染,2-自定义 */
    @ApiModelProperty("支付参数配置页面类型:1-JSON渲染,2-自定义")
    private Integer configPageType;

    /** ISV接口配置定义描述,json字符串 */
    @ApiModelProperty("ISV接口配置定义描述,json字符串")
    private String isvParams;

    /** 特约商户接口配置定义描述,json字符串 */
    @ApiModelProperty("特约商户接口配置定义描述,json字符串")
    private String isvsubMchParams;

    /** 普通商户接口配置定义描述,json字符串 */
    @ApiModelProperty("普通商户接口配置定义描述,json字符串")
    private String normalMchParams;

    /** 支持的支付方式 ["wxpay_jsapi", "wxpay_bar"] */
    @ApiModelProperty("支持的支付方式 [wxpay_jsapi, wxpay_bar]")
    private String wayCodes;

    /** 页面展示：卡片-图标 */
    @ApiModelProperty("页面展示：卡片-图标")
    private String icon;

    /** 页面展示：卡片-背景色 */
    @ApiModelProperty("页面展示：卡片-背景色")
    private String bgColor;

    /** 状态: 0-停用, 1-启用 */
    @ApiModelProperty("状态: 0-停用, 1-启用")
    private Integer state;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

}
