package com.piggy.pay.logic.mch.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.core.constants.CS;
import com.piggy.pay.logic.config.domain.TPayThirdApiConfig;
import com.piggy.pay.logic.config.service.ITPayThirdApiConfigService;
import com.piggy.pay.logic.mch.bo.TMchIsvInfoEditBo;
import com.piggy.pay.logic.mch.bo.TMchIsvInfoQueryBo;
import com.piggy.pay.logic.mch.domain.TMchInfo;
import com.piggy.pay.logic.mch.domain.TMchIsvInfo;
import com.piggy.pay.logic.mch.mapper.TMchIsvInfoMapper;
import com.piggy.pay.logic.mch.service.ITMchInfoService;
import com.piggy.pay.logic.mch.service.ITMchIsvInfoService;
import com.piggy.pay.logic.mch.vo.TMchIsvInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;

/**
 * 服务商信息Service业务层处理
 *
 * @author piggy
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TMchIsvInfoServiceImpl extends ServiceImpl<TMchIsvInfoMapper, TMchIsvInfo> implements ITMchIsvInfoService {

    @Resource
    private ITMchInfoService mchInfoService;

    @Resource
    private ITPayThirdApiConfigService payThirdApiConfigService;

    @Override
    public TMchIsvInfoVo queryById(String isvNo){
        return getVoById(isvNo, TMchIsvInfoVo.class);
    }

    @Override
    public TableDataInfo<TMchIsvInfoVo> queryPageList(TMchIsvInfoQueryBo bo) {
        PagePlus<TMchIsvInfo, TMchIsvInfoVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TMchIsvInfoVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TMchIsvInfoVo> queryList(TMchIsvInfoQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TMchIsvInfoVo.class);
    }

    private LambdaQueryWrapper<TMchIsvInfo> buildQueryWrapper(TMchIsvInfoQueryBo bo) {
        LambdaQueryWrapper<TMchIsvInfo> lqw = Wrappers.lambdaQuery();
        lqw.eq(StrUtil.isNotBlank(bo.getIsvNo()), TMchIsvInfo::getIsvNo, bo.getIsvNo());
        lqw.like(StrUtil.isNotBlank(bo.getIsvName()), TMchIsvInfo::getIsvName, bo.getIsvName());
        lqw.like(StrUtil.isNotBlank(bo.getIsvShortName()), TMchIsvInfo::getIsvShortName, bo.getIsvShortName());
        lqw.like(StrUtil.isNotBlank(bo.getContactName()), TMchIsvInfo::getContactName, bo.getContactName());
        lqw.eq(StrUtil.isNotBlank(bo.getContactTel()), TMchIsvInfo::getContactTel, bo.getContactTel());
        lqw.eq(StrUtil.isNotBlank(bo.getContactEmail()), TMchIsvInfo::getContactEmail, bo.getContactEmail());
        lqw.eq(bo.getState() != null, TMchIsvInfo::getState, bo.getState());
        lqw.eq(bo.getCreatedUid() != null, TMchIsvInfo::getCreatedUid, bo.getCreatedUid());
        lqw.eq(StrUtil.isNotBlank(bo.getCreatedBy()), TMchIsvInfo::getCreatedBy, bo.getCreatedBy());
        lqw.eq(bo.getCreatedAt() != null, TMchIsvInfo::getCreatedAt, bo.getCreatedAt());
        lqw.eq(bo.getUpdatedAt() != null, TMchIsvInfo::getUpdatedAt, bo.getUpdatedAt());
        lqw.eq(StrUtil.isNotBlank(bo.getUpdateBy()), TMchIsvInfo::getUpdateBy, bo.getUpdateBy());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TMchIsvInfoEditBo bo) {
        TMchIsvInfo add = BeanUtil.toBean(bo, TMchIsvInfo.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TMchIsvInfoEditBo bo) {
        TMchIsvInfo update = BeanUtil.toBean(bo, TMchIsvInfo.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TMchIsvInfo entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

    @Transactional
    public void removeByIsvNo(String isvNo) {
        // 0.当前服务商是否存在
        TMchIsvInfo isvInfo = this.getById(isvNo);
        if (isvInfo == null) {
            throw new BaseException("该服务商不存在");
        }

        // 1.查询当前服务商下是否存在商户
        long mchCount = mchInfoService.count(new LambdaQueryWrapper<TMchInfo>().eq(TMchInfo::getIsvNo, isvNo).eq(TMchInfo::getType, CS.MCH_TYPE_ISVSUB));
        if (mchCount > 0) {
            throw new BaseException("该服务商下存在商户，不可删除");
        }

        // 2.删除当前服务商支付接口配置参数
        payThirdApiConfigService.remove(new LambdaQueryWrapper<TPayThirdApiConfig>()
                .eq(TPayThirdApiConfig::getInfoId, isvNo)
                .eq(TPayThirdApiConfig::getInfoType, CS.INFO_TYPE_ISV)
        );

        // 3.删除该服务商
        boolean remove = this.removeById(isvNo);
        if (!remove) {
            throw new BaseException("删除服务商失败");
        }

    }
}
