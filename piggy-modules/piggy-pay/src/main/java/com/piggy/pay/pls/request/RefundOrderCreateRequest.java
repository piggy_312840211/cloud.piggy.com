package com.piggy.pay.pls.request;

import com.piggy.pay.pls.BasePay;
import com.piggy.pay.pls.model.RefundOrderCreateReqModel;
import com.piggy.pay.pls.net.RequestOptions;
import com.piggy.pay.pls.response.RefundOrderCreateResponse;

/**
 * Jeepay退款请求实现
 * @author jmdhappy
 * @site https://www.jeepay.vip
 * @date 2021-06-18 09:00
 */
public class RefundOrderCreateRequest implements PayRequest<RefundOrderCreateResponse, RefundOrderCreateReqModel> {

    private String apiVersion = BasePay.VERSION;
    private String apiUri = "api/refund/refundOrder";
    private RequestOptions options;
    private RefundOrderCreateReqModel bizModel = null;

    @Override
    public String getApiUri() {
        return this.apiUri;
    }

    @Override
    public String getApiVersion() {
        return this.apiVersion;
    }

    @Override
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    @Override
    public RequestOptions getRequestOptions() {
        return this.options;
    }

    @Override
    public void setRequestOptions(RequestOptions options) {
        this.options = options;
    }

    @Override
    public RefundOrderCreateReqModel getBizModel() {
        return this.bizModel;
    }

    @Override
    public void setBizModel(RefundOrderCreateReqModel bizModel) {
        this.bizModel = bizModel;
    }

    @Override
    public Class<RefundOrderCreateResponse> getResponseClass() {
        return RefundOrderCreateResponse.class;
    }

}
