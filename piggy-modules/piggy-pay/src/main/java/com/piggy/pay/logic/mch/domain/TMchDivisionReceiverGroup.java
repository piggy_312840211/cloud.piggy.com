package com.piggy.pay.logic.mch.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import com.baomidou.mybatisplus.annotation.*;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;


/**
 * 分账账号组对象 t_mch_division_receiver_group
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("t_mch_division_receiver_group")
public class TMchDivisionReceiverGroup implements Serializable {

    private static final long serialVersionUID=1L;

    /** 组ID */
    @ApiModelProperty("组ID")
    @TableId(value = "receiver_group_id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long receiverGroupId;

    /** 组名称 */
    @ApiModelProperty("组名称")
    private String receiverGroupName;

    /** 商户号 */
    @ApiModelProperty("商户号")
    private String mchNo;

    /** 自动分账组（当订单分账模式为自动分账，改组将完成分账逻辑） 0-否 1-是 */
    @ApiModelProperty("自动分账组（当订单分账模式为自动分账，改组将完成分账逻辑） 0-否 1-是")
    private Integer autoDivisionFlag;

    /** 创建者用户ID */
    @ApiModelProperty("创建者用户ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long createdUid;

    /** 创建者姓名 */
    @ApiModelProperty("创建者姓名")
    private String createdBy;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    /** 更新人 */
    @ApiModelProperty("更新人")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

}
