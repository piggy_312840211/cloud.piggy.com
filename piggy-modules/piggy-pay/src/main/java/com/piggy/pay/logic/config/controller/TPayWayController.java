package com.piggy.pay.logic.config.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import com.piggy.pay.logic.config.bo.TPayWayEditBo;
import com.piggy.pay.logic.config.bo.TPayWayQueryBo;
import com.piggy.pay.logic.config.service.ITPayWayService;
import com.piggy.pay.logic.config.vo.TPayWayVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 支付方式Controller
 *
 * @author piggy
 * @date 2023-05-25
 */
@Api(value = "支付方式控制器", tags = {"支付方式管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/config/payway")
public class TPayWayController extends BaseController {

    private final ITPayWayService iTPayWayService;

    /**
     * 查询支付方式列表
     */
    @ApiOperation("查询支付方式列表")
    @SaCheckPermission("config:payway:list")
    @GetMapping("/list")
    public TableDataInfo<TPayWayVo> list(@Validated TPayWayQueryBo bo) {
        return iTPayWayService.queryPageList(bo);
    }

    /**
     * 导出支付方式列表
     */
    @ApiOperation("导出支付方式列表")
    @SaCheckPermission("config:payway:export")
    @Log(title = "支付方式", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TPayWayQueryBo bo, HttpServletResponse response) throws IOException {
        List<TPayWayVo> list = iTPayWayService.queryList(bo);
        ExcelUtil<TPayWayVo> util = new ExcelUtil<TPayWayVo>(TPayWayVo.class);
        util.exportExcel(response, list, "支付方式");
    }

    /**
     * 获取支付方式详细信息
     */
    @ApiOperation("获取支付方式详细信息")
    @SaCheckPermission("config:payway:query")
    @GetMapping("/{wayCode}")
    public R<TPayWayVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("wayCode") String wayCode) {
        return R.ok(iTPayWayService.queryById(wayCode));
    }

    /**
     * 新增支付方式
     */
    @ApiOperation("新增支付方式")
    @SaCheckPermission("config:payway:add")
    @Log(title = "支付方式", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TPayWayEditBo bo) {
        return R.ok(iTPayWayService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改支付方式
     */
    @ApiOperation("修改支付方式")
    @SaCheckPermission("config:payway:edit")
    @Log(title = "支付方式", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TPayWayEditBo bo) {
        return R.ok(iTPayWayService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除支付方式
     */
    @ApiOperation("删除支付方式")
    @SaCheckPermission("config:payway:remove")
    @Log(title = "支付方式" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{wayCodes}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable String[] wayCodes) {
        return R.ok(iTPayWayService.deleteWithValidByIds(Arrays.asList(wayCodes), true) ? 1 : 0);
    }
}
