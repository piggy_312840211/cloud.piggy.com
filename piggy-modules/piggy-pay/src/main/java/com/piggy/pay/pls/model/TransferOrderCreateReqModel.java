package com.piggy.pay.pls.model;

import com.piggy.pay.pls.ApiField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/***
* 转账下单请求实体类
*
* @author terrfly
* @site https://www.jeepay.vip
* @date 2021/8/13 16:08
*/
@Data
@NoArgsConstructor
public class TransferOrderCreateReqModel implements Serializable {

    private static final long serialVersionUID = -3998573128290306948L;

    @ApiField("mchNo")
    private String mchNo;      // 商户号
    @ApiField("appId")
    private String appId;      // 应用ID
    @ApiField("mchOrderNo")
    String mchOrderNo;          // 商户订单号
    @ApiField("ifCode")
    String ifCode;          // 支付接口代码
    @ApiField("entryType")
    String entryType;         // 入账方式
    @ApiField("amount")
    Long amount;          // 转账金额
    @ApiField("currency")
    String currency;            // 货币代码，当前只支持cny
    @ApiField("accountNo")
    String accountNo;        // 收款账号
    @ApiField("accountName")
    String accountName;        // 收款人姓名
    @ApiField("bankName")
    String bankName;        // 收款人开户行名称
    @ApiField("clientIp")
    String clientIp;            // 客户端IP
    @ApiField("transferDesc")
    String transferDesc;            // 转账备注信息
    @ApiField("notifyUrl")
    String notifyUrl;           // 异步通知地址
    @ApiField("channelExtra")
    String channelExtra;        // 特定渠道额外支付参数
    @ApiField("extParam")
    String extParam;            // 商户扩展参数

}
