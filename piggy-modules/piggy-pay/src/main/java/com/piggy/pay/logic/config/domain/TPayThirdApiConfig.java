package com.piggy.pay.logic.config.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import com.baomidou.mybatisplus.annotation.*;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;


/**
 * 支付接口配置参数对象 t_pay_third_api_config
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("t_pay_third_api_config")
public class TPayThirdApiConfig implements Serializable {

    private static final long serialVersionUID=1L;

    /** ID */
    @ApiModelProperty("ID")
    @TableId(value = "id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 账号类型:1-服务商 2-商户 3-商户应用 */
    @ApiModelProperty("账号类型:1-服务商 2-商户 3-商户应用")
    private Integer infoType;

    /** 服务商号/商户号/应用ID */
    @ApiModelProperty("服务商号/商户号/应用ID")
    private String infoId;

    /** 支付接口代码 */
    @ApiModelProperty("支付接口代码")
    private String ifCode;

    /** 接口配置参数,json字符串 */
    @ApiModelProperty("接口配置参数,json字符串")
    private String ifParams;

    /** 支付接口费率 */
    @ApiModelProperty("支付接口费率")
    private BigDecimal ifRate;

    /** 状态: 0-停用, 1-启用 */
    @ApiModelProperty("状态: 0-停用, 1-启用")
    private Integer state;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;

    /** 创建者用户ID */
    @ApiModelProperty("创建者用户ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long createdUid;

    /** 创建者姓名 */
    @ApiModelProperty("创建者姓名")
    private String createdBy;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新者用户ID */
    @ApiModelProperty("更新者用户ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long updatedUid;

    /** 更新者姓名 */
    @ApiModelProperty("更新者姓名")
    private String updatedBy;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

}
