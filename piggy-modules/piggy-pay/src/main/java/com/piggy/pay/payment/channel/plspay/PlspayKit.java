package com.piggy.pay.payment.channel.plspay;

import com.piggy.common.core.exception.base.BaseException;
import com.piggy.pay.core.constants.CS;
import com.piggy.pay.core.model.params.plspay.PlspayConfig;
import com.piggy.pay.core.model.params.plspay.PlspayNormalMchParams;
import cn.hutool.extra.spring.SpringUtil;
import com.piggy.pay.logic.order.domain.TPayOrder;
import com.piggy.pay.payment.model.MchAppConfigContext;
import com.piggy.pay.payment.service.ConfigContextQueryService;
import com.piggy.pay.pls.BasePay;
import com.piggy.pay.pls.PayClient;
import com.piggy.pay.pls.model.PayOrderCreateReqModel;
import com.piggy.pay.pls.request.PayOrderCreateRequest;
import com.piggy.pay.pls.response.PayResponse;
import com.piggy.pay.pls.response.PayOrderCreateResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/*
 * 工具类
 *
 * @author xiaoyu
 * @site https://www.jeequan.com
 * @date 2022/8/23 16:29
 */
@Slf4j
public class PlspayKit {

	public static PayOrderCreateResponse payRequest(TPayOrder payOrder, MchAppConfigContext mchAppConfigContext, PayOrderCreateReqModel model) throws BaseException {

		// 发起统一下单
		PayOrderCreateResponse response = new PayOrderCreateResponse();
		ConfigContextQueryService configContextQueryService = SpringUtil.getBean(ConfigContextQueryService.class);
		PlspayNormalMchParams normalMchParams = (PlspayNormalMchParams) configContextQueryService.queryNormalMchParams(mchAppConfigContext.getMchNo(), mchAppConfigContext.getAppId(), CS.IF_CODE.PLSPAY);

		// 构建请求数据
		PayOrderCreateRequest request = new PayOrderCreateRequest();
		model.setMchNo(normalMchParams.getMerchantNo());        // 商户号
		model.setAppId(normalMchParams.getAppId());             // 应用ID
		model.setMchOrderNo(payOrder.getPayOrderId());          // 商户订单号
		model.setAmount(payOrder.getAmount());                  // 金额，单位分
		model.setCurrency(payOrder.getCurrency());              // 币种，目前只支持cny
		model.setClientIp(payOrder.getClientIp());              // 发起支付请求客户端的IP地址
		model.setSubject(payOrder.getSubject());                // 商品标题
		model.setBody(payOrder.getBody());                      // 商品描述
		request.setBizModel(model);

		if (normalMchParams.getSignType().equals(PlspayConfig.DEFAULT_SIGN_TYPE) || StringUtils.isEmpty(normalMchParams.getSignType())) {
			PayClient payClient = PayClient.getInstance(normalMchParams.getAppId(), normalMchParams.getAppSecret(), BasePay.getApiBase());
			response = payClient.execute(request);

		} else if (normalMchParams.getSignType().equals(PlspayConfig.SIGN_TYPE_RSA2)) {
			PayClient payClient = PayClient.getInstance(normalMchParams.getAppId(), normalMchParams.getRsa2AppPrivateKey(), BasePay.getApiBase());
			response = payClient.executeByRSA2(request);
		}
		return response;
	}

	public static Boolean checkPayResp(PayResponse response , MchAppConfigContext mchAppConfigContext) {
		ConfigContextQueryService configContextQueryService = SpringUtil.getBean(ConfigContextQueryService.class);
		PlspayNormalMchParams normalMchParams = (PlspayNormalMchParams) configContextQueryService.queryNormalMchParams(mchAppConfigContext.getMchNo(), mchAppConfigContext.getAppId(), CS.IF_CODE.PLSPAY);

		boolean isSuccess = false;
		if (normalMchParams.getSignType().equals(PlspayConfig.DEFAULT_SIGN_TYPE) || StringUtils.isEmpty(normalMchParams.getSignType())) {
			isSuccess = response.isSuccess(normalMchParams.getAppSecret());

		} else if (normalMchParams.getSignType().equals(PlspayConfig.SIGN_TYPE_RSA2)) {
			isSuccess = response.isSuccessByRsa2(normalMchParams.getRsa2PayPublicKey());
		}

		return isSuccess;
	}

}
