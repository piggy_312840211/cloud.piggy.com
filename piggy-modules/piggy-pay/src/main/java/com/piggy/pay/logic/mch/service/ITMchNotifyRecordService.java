package com.piggy.pay.logic.mch.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.mch.domain.TMchNotifyRecord;
import com.piggy.pay.logic.mch.vo.TMchNotifyRecordVo;
import com.piggy.pay.logic.mch.bo.TMchNotifyRecordQueryBo;
import com.piggy.pay.logic.mch.bo.TMchNotifyRecordEditBo;

import java.util.Collection;
import java.util.List;

/**
 * 商户通知记录Service接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface ITMchNotifyRecordService extends IServicePlus<TMchNotifyRecord> {
    /**
     * 查询单个
     *
     * @return
     */
    TMchNotifyRecordVo queryById(Long notifyId);

    /**
     * 查询列表
     */
    TableDataInfo<TMchNotifyRecordVo> queryPageList(TMchNotifyRecordQueryBo bo);

    /**
     * 查询列表
     */
    List<TMchNotifyRecordVo> queryList(TMchNotifyRecordQueryBo bo);

    /**
     * 根据新增业务对象插入商户通知记录
     *
     * @param bo 商户通知记录新增业务对象
     * @return
     */
    Boolean insertByAddBo(TMchNotifyRecordEditBo bo);

    /**
     * 根据编辑业务对象修改商户通知记录
     *
     * @param bo 商户通知记录编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TMchNotifyRecordEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);


    /** 根据订单号和类型查询 */
    TMchNotifyRecord findByOrderAndType(String orderId, Integer orderType);

    /** 查询支付订单 */
    TMchNotifyRecord findByPayOrder(String orderId);

    /** 查询退款订单订单 */
    TMchNotifyRecord findByRefundOrder(String orderId);

    /** 查询退款订单订单 */
    TMchNotifyRecord findByTransferOrder(String transferId);

    Integer updateNotifyResult(Long notifyId, Integer state, String resResult);

}
