package com.piggy.pay.pls.exception;

import com.piggy.common.core.exception.base.BaseException;

/**
 * API异常
 * @author jmdhappy
 * @site https://www.jeepay.vip
 * @date 2021-06-08 11:00
 */
public class APIException extends BaseException {

    private static final long serialVersionUID = -2753719317464278319L;

    public APIException(String message,int statusCode, Throwable e) {
        super(message, statusCode, e);
    }
}
