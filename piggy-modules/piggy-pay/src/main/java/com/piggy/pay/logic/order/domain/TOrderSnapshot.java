package com.piggy.pay.logic.order.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import com.baomidou.mybatisplus.annotation.*;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;


/**
 * 订单接口数据快照对象 t_order_snapshot
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("t_order_snapshot")
public class TOrderSnapshot implements Serializable {

    private static final long serialVersionUID=1L;

    /** 订单ID */
    @ApiModelProperty("订单ID")
    @TableId(value = "order_id")
    private String orderId;

    /** 订单类型: 1-支付, 2-退款 */
    @ApiModelProperty("订单类型: 1-支付, 2-退款")
    @TableId(value = "order_type")
    private Integer orderType;

    /** 下游请求数据 */
    @ApiModelProperty("下游请求数据")
    private String mchReqData;

    /** 下游请求时间 */
    @ApiModelProperty("下游请求时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date mchReqTime;

    /** 向下游响应数据 */
    @ApiModelProperty("向下游响应数据")
    private String mchRespData;

    /** 向下游响应时间 */
    @ApiModelProperty("向下游响应时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date mchRespTime;

    /** 向上游请求数据 */
    @ApiModelProperty("向上游请求数据")
    private String channelReqData;

    /** 向上游请求时间 */
    @ApiModelProperty("向上游请求时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date channelReqTime;

    /** 上游响应数据 */
    @ApiModelProperty("上游响应数据")
    private String channelRespData;

    /** 上游响应时间 */
    @ApiModelProperty("上游响应时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date channelRespTime;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

}
