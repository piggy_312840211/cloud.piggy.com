package com.piggy.pay.logic.mch.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;

/**
 * 商户分账接收者账号绑定关系编辑对象 t_mch_division_receiver
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("商户分账接收者账号绑定关系编辑对象")
public class TMchDivisionReceiverEditBo {


    /** 分账接收者ID */
    @ApiModelProperty("分账接收者ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long receiverId;

    /** 接收者账号别名 */
    @ApiModelProperty("接收者账号别名")
    @NotBlank(message = "接收者账号别名不能为空")
    private String receiverAlias;

    /** 组ID（便于商户接口使用） */
    @ApiModelProperty("组ID（便于商户接口使用）")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long receiverGroupId;

    /** 组名称 */
    @ApiModelProperty("组名称")
    private String receiverGroupName;

    /** 商户号 */
    @ApiModelProperty("商户号")
    @NotBlank(message = "商户号不能为空")
    private String mchNo;

    /** 服务商号 */
    @ApiModelProperty("服务商号")
    private String isvNo;

    /** 应用ID */
    @ApiModelProperty("应用ID")
    @NotBlank(message = "应用ID不能为空")
    private String appId;

    /** 支付接口代码 */
    @ApiModelProperty("支付接口代码")
    @NotBlank(message = "支付接口代码不能为空")
    private String ifCode;

    /** 分账接收账号类型: 0-个人(对私) 1-商户(对公) */
    @ApiModelProperty("分账接收账号类型: 0-个人(对私) 1-商户(对公)")
    @NotNull(message = "分账接收账号类型: 0-个人(对私) 1-商户(对公)不能为空")
    private Integer accType;

    /** 分账接收账号 */
    @ApiModelProperty("分账接收账号")
    @NotBlank(message = "分账接收账号不能为空")
    private String accNo;

    /** 分账接收账号名称 */
    @ApiModelProperty("分账接收账号名称")
    @NotBlank(message = "分账接收账号名称不能为空")
    private String accName;

    /** 分账关系类型（参考微信）， 如： SERVICE_PROVIDER 服务商等 */
    @ApiModelProperty("分账关系类型（参考微信）， 如： SERVICE_PROVIDER 服务商等")
    @NotBlank(message = "分账关系类型（参考微信）， 如： SERVICE_PROVIDER 服务商等不能为空")
    private String relationType;

    /** 当选择自定义时，需要录入该字段。 否则为对应的名称 */
    @ApiModelProperty("当选择自定义时，需要录入该字段。 否则为对应的名称")
    @NotBlank(message = "当选择自定义时，需要录入该字段。 否则为对应的名称不能为空")
    private String relationTypeName;

    /** 分账比例 */
    @ApiModelProperty("分账比例")
    private BigDecimal divisionProfit;

    /** 分账状态（本系统状态，并不调用上游关联关系）: 1-正常分账, 0-暂停分账 */
    @ApiModelProperty("分账状态（本系统状态，并不调用上游关联关系）: 1-正常分账, 0-暂停分账")
    @NotNull(message = "分账状态（本系统状态，并不调用上游关联关系）: 1-正常分账, 0-暂停分账不能为空")
    private Integer state;

    /** 上游绑定返回信息，一般用作查询账号异常时的记录 */
    @ApiModelProperty("上游绑定返回信息，一般用作查询账号异常时的记录")
    private String channelBindResult;

    /** 渠道特殊信息 */
    @ApiModelProperty("渠道特殊信息")
    private String channelExtInfo;

    /** 绑定成功时间 */
    @ApiModelProperty("绑定成功时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date bindSuccessTime;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @NotNull(message = "创建时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @NotNull(message = "更新时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;
}
