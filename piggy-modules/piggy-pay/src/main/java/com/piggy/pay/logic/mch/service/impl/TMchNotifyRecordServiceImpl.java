package com.piggy.pay.logic.mch.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.core.enums.IMchEnums;
import com.piggy.pay.logic.mch.bo.TMchNotifyRecordEditBo;
import com.piggy.pay.logic.mch.bo.TMchNotifyRecordQueryBo;
import com.piggy.pay.logic.mch.domain.TMchNotifyRecord;
import com.piggy.pay.logic.mch.mapper.TMchNotifyRecordMapper;
import com.piggy.pay.logic.mch.mapper.TMchNotifyRecordMapperEx;
import com.piggy.pay.logic.mch.service.ITMchNotifyRecordService;
import com.piggy.pay.logic.mch.vo.TMchNotifyRecordVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import javax.annotation.Resource;
import java.util.List;
import java.util.Collection;

/**
 * 商户通知记录Service业务层处理
 *
 * @author piggy
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TMchNotifyRecordServiceImpl extends ServiceImpl<TMchNotifyRecordMapper, TMchNotifyRecord> implements ITMchNotifyRecordService {

    @Resource
    private TMchNotifyRecordMapperEx notifyRecordMapperEx;

    @Override
    public TMchNotifyRecordVo queryById(Long notifyId){
        return getVoById(notifyId, TMchNotifyRecordVo.class);
    }

    @Override
    public TableDataInfo<TMchNotifyRecordVo> queryPageList(TMchNotifyRecordQueryBo bo) {
        PagePlus<TMchNotifyRecord, TMchNotifyRecordVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TMchNotifyRecordVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TMchNotifyRecordVo> queryList(TMchNotifyRecordQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TMchNotifyRecordVo.class);
    }

    private LambdaQueryWrapper<TMchNotifyRecord> buildQueryWrapper(TMchNotifyRecordQueryBo bo) {
        LambdaQueryWrapper<TMchNotifyRecord> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getNotifyId() != null, TMchNotifyRecord::getNotifyId, bo.getNotifyId());
        lqw.eq(StrUtil.isNotBlank(bo.getOrderId()), TMchNotifyRecord::getOrderId, bo.getOrderId());
        lqw.eq(bo.getOrderType() != null, TMchNotifyRecord::getOrderType, bo.getOrderType());
        lqw.eq(StrUtil.isNotBlank(bo.getMchOrderNo()), TMchNotifyRecord::getMchOrderNo, bo.getMchOrderNo());
        lqw.eq(StrUtil.isNotBlank(bo.getMchNo()), TMchNotifyRecord::getMchNo, bo.getMchNo());
        lqw.eq(StrUtil.isNotBlank(bo.getIsvNo()), TMchNotifyRecord::getIsvNo, bo.getIsvNo());
        lqw.eq(StrUtil.isNotBlank(bo.getAppId()), TMchNotifyRecord::getAppId, bo.getAppId());
        lqw.eq(StrUtil.isNotBlank(bo.getNotifyUrl()), TMchNotifyRecord::getNotifyUrl, bo.getNotifyUrl());
        lqw.eq(StrUtil.isNotBlank(bo.getResResult()), TMchNotifyRecord::getResResult, bo.getResResult());
        lqw.eq(bo.getNotifyCount() != null, TMchNotifyRecord::getNotifyCount, bo.getNotifyCount());
        lqw.eq(bo.getNotifyCountLimit() != null, TMchNotifyRecord::getNotifyCountLimit, bo.getNotifyCountLimit());
        lqw.eq(bo.getState() != null, TMchNotifyRecord::getState, bo.getState());
        lqw.eq(bo.getLastNotifyTime() != null, TMchNotifyRecord::getLastNotifyTime, bo.getLastNotifyTime());
        lqw.eq(bo.getCreatedAt() != null, TMchNotifyRecord::getCreatedAt, bo.getCreatedAt());
        lqw.eq(bo.getUpdatedAt() != null, TMchNotifyRecord::getUpdatedAt, bo.getUpdatedAt());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TMchNotifyRecordEditBo bo) {
        TMchNotifyRecord add = BeanUtil.toBean(bo, TMchNotifyRecord.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TMchNotifyRecordEditBo bo) {
        TMchNotifyRecord update = BeanUtil.toBean(bo, TMchNotifyRecord.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TMchNotifyRecord entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

    /** 根据订单号和类型查询 */
    public TMchNotifyRecord findByOrderAndType(String orderId, Integer orderType){
        return getOne(
                new LambdaQueryWrapper<TMchNotifyRecord>().eq(TMchNotifyRecord::getOrderId, orderId).eq(TMchNotifyRecord::getOrderType, orderType)
        );
    }

    /** 查询支付订单 */
    public TMchNotifyRecord findByPayOrder(String orderId){
        return findByOrderAndType(orderId, IMchEnums.NotifyTypeEnums.TYPE_PAY_ORDER.getType());
    }

    /** 查询退款订单订单 */
    public TMchNotifyRecord findByRefundOrder(String orderId){
        return findByOrderAndType(orderId, IMchEnums.NotifyTypeEnums.TYPE_REFUND_ORDER.getType());
    }

    /** 查询退款订单订单 */
    public TMchNotifyRecord findByTransferOrder(String transferId){
        return findByOrderAndType(transferId, IMchEnums.NotifyTypeEnums.TYPE_TRANSFER_ORDER.getType());
    }

    public Integer updateNotifyResult(Long notifyId, Integer state, String resResult){
        return notifyRecordMapperEx.updateNotifyResult(notifyId, state, resResult);
    }

}
