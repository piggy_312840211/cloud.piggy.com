package com.piggy.pay.logic.mch.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 服务商信息编辑对象 t_mch_isv_info
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("服务商信息编辑对象")
public class TMchIsvInfoEditBo {


    /** 服务商号 */
    @ApiModelProperty("服务商号")
    private String isvNo;

    /** 服务商名称 */
    @ApiModelProperty("服务商名称")
    @NotBlank(message = "服务商名称不能为空")
    private String isvName;

    /** 服务商简称 */
    @ApiModelProperty("服务商简称")
    @NotBlank(message = "服务商简称不能为空")
    private String isvShortName;

    /** 联系人姓名 */
    @ApiModelProperty("联系人姓名")
    private String contactName;

    /** 联系人手机号 */
    @ApiModelProperty("联系人手机号")
    private String contactTel;

    /** 联系人邮箱 */
    @ApiModelProperty("联系人邮箱")
    private String contactEmail;

    /** 状态: 0-停用, 1-正常 */
    @ApiModelProperty("状态: 0-停用, 1-正常")
    @NotNull(message = "状态: 0-停用, 1-正常不能为空")
    private Integer state;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;

    /** 创建者用户ID */
    @ApiModelProperty("创建者用户ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long createdUid;

    /** 创建者姓名 */
    @ApiModelProperty("创建者姓名")
    private String createdBy;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @NotNull(message = "创建时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @NotNull(message = "更新时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    /** 更新人 */
    @ApiModelProperty("更新人")
    private String updateBy;
}
