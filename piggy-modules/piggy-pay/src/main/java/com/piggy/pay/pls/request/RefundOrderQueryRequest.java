package com.piggy.pay.pls.request;

import com.piggy.pay.pls.BasePay;
import com.piggy.pay.pls.model.RefundOrderQueryReqModel;
import com.piggy.pay.pls.net.RequestOptions;
import com.piggy.pay.pls.response.RefundOrderQueryResponse;

/**
 * Jeepay退款查单请求实现
 * @author jmdhappy
 * @site https://www.jeepay.vip
 * @date 2021-06-18 12:00
 */
public class RefundOrderQueryRequest implements PayRequest<RefundOrderQueryResponse, RefundOrderQueryReqModel> {

    private String apiVersion = BasePay.VERSION;
    private String apiUri = "api/refund/query";
    private RequestOptions options;
    private RefundOrderQueryReqModel bizModel = null;

    @Override
    public String getApiUri() {
        return this.apiUri;
    }

    @Override
    public String getApiVersion() {
        return this.apiVersion;
    }

    @Override
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    @Override
    public RequestOptions getRequestOptions() {
        return this.options;
    }

    @Override
    public void setRequestOptions(RequestOptions options) {
        this.options = options;
    }

    @Override
    public RefundOrderQueryReqModel getBizModel() {
        return this.bizModel;
    }

    @Override
    public void setBizModel(RefundOrderQueryReqModel bizModel) {
        this.bizModel = bizModel;
    }

    @Override
    public Class<RefundOrderQueryResponse> getResponseClass() {
        return RefundOrderQueryResponse.class;
    }

}
