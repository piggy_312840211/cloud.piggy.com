package com.piggy.pay.logic.order.service;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.core.enums.IDivisionEnums;
import com.piggy.pay.core.utils.SeqKit;
import com.piggy.pay.logic.order.domain.TPayOrder;
import com.piggy.pay.logic.order.domain.TPayOrderDivisionRecord;
import com.piggy.pay.logic.order.mapper.TPayOrderDivisionRecordMapperEx;
import com.piggy.pay.logic.order.vo.TPayOrderDivisionRecordVo;
import com.piggy.pay.logic.order.bo.TPayOrderDivisionRecordQueryBo;
import com.piggy.pay.logic.order.bo.TPayOrderDivisionRecordEditBo;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 分账记录Service接口
 *
 * @author zito
 * @date 2023-05-25
 */
public interface ITPayOrderDivisionRecordService extends IServicePlus<TPayOrderDivisionRecord> {
    /**
     * 查询单个
     *
     * @return
     */
    TPayOrderDivisionRecordVo queryById(Long recordId);

    /**
     * 查询列表
     */
    TableDataInfo<TPayOrderDivisionRecordVo> queryPageList(TPayOrderDivisionRecordQueryBo bo);

    /**
     * 查询列表
     */
    List<TPayOrderDivisionRecordVo> queryList(TPayOrderDivisionRecordQueryBo bo);

    /**
     * 根据新增业务对象插入分账记录
     *
     * @param bo 分账记录新增业务对象
     * @return
     */
    Boolean insertByAddBo(TPayOrderDivisionRecordEditBo bo);

    /**
     * 根据编辑业务对象修改分账记录
     *
     * @param bo 分账记录编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TPayOrderDivisionRecordEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /** 更新分账记录为分账成功  ( 单条 )  将：  已受理 更新为： 其他状态    **/
    void updateRecordSuccessOrFailBySingleItem(Long recordId, Integer state, String channelRespResult);

    /** 更新分账记录为分账成功**/
    void updateRecordSuccessOrFail(List<TPayOrderDivisionRecord> records, Integer state, String channelBatchOrderId, String channelRespResult);

    /** 更新分账订单为： 等待分账中的状态  **/
    void updateResendState(String payOrderId);

    TPayOrderDivisionRecordMapperEx getBaseMapperEx();

}
