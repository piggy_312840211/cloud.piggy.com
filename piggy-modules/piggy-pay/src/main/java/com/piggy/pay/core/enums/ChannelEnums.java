package com.piggy.pay.core.enums;

public enum ChannelEnums {
    //接口正确返回： 业务状态已经明确成功
    CONFIRM_SUCCESS,
    //接口正确返回： 业务状态已经明确失败
    CONFIRM_FAIL,
    //接口正确返回： 上游处理中， 需通过定时查询/回调进行下一步处理
    WAITING,
    //接口超时，或网络异常等请求， 或者返回结果的签名失败： 状态不明确 ( 上游接口变更, 暂时无法确定状态值 )
    UNKNOWN,
    //渠道侧出现异常( 接口返回了异常状态 )
    API_RET_ERROR,
    //本系统出现不可预知的异常
    SYS_ERROR
}
