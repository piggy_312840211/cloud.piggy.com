package com.piggy.pay.logic.mch.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;

/**
 * 商户支付通道编辑对象 t_mch_pay_passage
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("商户支付通道编辑对象")
public class TMchPayPassageEditBo {


    /** ID */
    @ApiModelProperty("ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 商户号 */
    @ApiModelProperty("商户号")
    @NotBlank(message = "商户号不能为空")
    private String mchNo;

    /** 应用ID */
    @ApiModelProperty("应用ID")
    @NotBlank(message = "应用ID不能为空")
    private String appId;

    /** 支付接口 */
    @ApiModelProperty("支付接口")
    @NotBlank(message = "支付接口不能为空")
    private String ifCode;

    /** 支付方式 */
    @ApiModelProperty("支付方式")
    @NotBlank(message = "支付方式不能为空")
    private String wayCode;

    /** 支付方式费率 */
    @ApiModelProperty("支付方式费率")
    @NotNull(message = "支付方式费率不能为空")
    private BigDecimal rate;

    /** 风控数据 */
    @ApiModelProperty("风控数据")
    private String riskConfig;

    /** 状态: 0-停用, 1-启用 */
    @ApiModelProperty("状态: 0-停用, 1-启用")
    @NotNull(message = "状态: 0-停用, 1-启用不能为空")
    private Integer state;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @NotNull(message = "创建时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @NotNull(message = "更新时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;
}
