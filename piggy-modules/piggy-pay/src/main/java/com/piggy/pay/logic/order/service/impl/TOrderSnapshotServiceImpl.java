package com.piggy.pay.logic.order.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.order.domain.TOrderSnapshot;
import com.piggy.pay.logic.order.mapper.TOrderSnapshotMapper;
import com.piggy.pay.logic.order.service.ITOrderSnapshotService;
import com.piggy.pay.logic.order.vo.TOrderSnapshotVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.piggy.pay.logic.order.bo.TOrderSnapshotQueryBo;
import com.piggy.pay.logic.order.bo.TOrderSnapshotEditBo;

import java.util.List;
import java.util.Collection;

/**
 * 订单接口数据快照Service业务层处理
 *
 * @author piggy
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TOrderSnapshotServiceImpl extends ServiceImpl<TOrderSnapshotMapper, TOrderSnapshot> implements ITOrderSnapshotService {

    @Override
    public TOrderSnapshotVo queryById(String orderId){
        return getVoById(orderId, TOrderSnapshotVo.class);
    }

    @Override
    public TableDataInfo<TOrderSnapshotVo> queryPageList(TOrderSnapshotQueryBo bo) {
        PagePlus<TOrderSnapshot, TOrderSnapshotVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TOrderSnapshotVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TOrderSnapshotVo> queryList(TOrderSnapshotQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TOrderSnapshotVo.class);
    }

    private LambdaQueryWrapper<TOrderSnapshot> buildQueryWrapper(TOrderSnapshotQueryBo bo) {
        LambdaQueryWrapper<TOrderSnapshot> lqw = Wrappers.lambdaQuery();
        lqw.eq(StrUtil.isNotBlank(bo.getOrderId()), TOrderSnapshot::getOrderId, bo.getOrderId());
        lqw.eq(bo.getOrderType() != null, TOrderSnapshot::getOrderType, bo.getOrderType());
        lqw.eq(StrUtil.isNotBlank(bo.getMchReqData()), TOrderSnapshot::getMchReqData, bo.getMchReqData());
        lqw.eq(bo.getMchReqTime() != null, TOrderSnapshot::getMchReqTime, bo.getMchReqTime());
        lqw.eq(StrUtil.isNotBlank(bo.getMchRespData()), TOrderSnapshot::getMchRespData, bo.getMchRespData());
        lqw.eq(bo.getMchRespTime() != null, TOrderSnapshot::getMchRespTime, bo.getMchRespTime());
        lqw.eq(StrUtil.isNotBlank(bo.getChannelReqData()), TOrderSnapshot::getChannelReqData, bo.getChannelReqData());
        lqw.eq(bo.getChannelReqTime() != null, TOrderSnapshot::getChannelReqTime, bo.getChannelReqTime());
        lqw.eq(StrUtil.isNotBlank(bo.getChannelRespData()), TOrderSnapshot::getChannelRespData, bo.getChannelRespData());
        lqw.eq(bo.getChannelRespTime() != null, TOrderSnapshot::getChannelRespTime, bo.getChannelRespTime());
        lqw.eq(bo.getCreatedAt() != null, TOrderSnapshot::getCreatedAt, bo.getCreatedAt());
        lqw.eq(bo.getUpdatedAt() != null, TOrderSnapshot::getUpdatedAt, bo.getUpdatedAt());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TOrderSnapshotEditBo bo) {
        TOrderSnapshot add = BeanUtil.toBean(bo, TOrderSnapshot.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TOrderSnapshotEditBo bo) {
        TOrderSnapshot update = BeanUtil.toBean(bo, TOrderSnapshot.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TOrderSnapshot entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
