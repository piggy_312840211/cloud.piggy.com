package com.piggy.pay.logic.config.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.config.domain.TPayThirdApiDefine;

/**
 * 支付接口定义Mapper接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface TPayThirdApiDefineMapper extends BaseMapperPlus<TPayThirdApiDefine> {

}
