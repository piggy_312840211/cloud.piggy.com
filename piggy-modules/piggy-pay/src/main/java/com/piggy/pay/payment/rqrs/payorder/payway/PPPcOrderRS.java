package com.piggy.pay.payment.rqrs.payorder.payway;

import com.piggy.pay.payment.rqrs.payorder.CommonPayDataRS;
import lombok.Data;

/**
 * none.
 *
 * @author 陈泉
 * @package com.jeequan.jeepay.pay.rqrs.payorder.payway
 * @create 2021/11/15 19:56
 */
@Data
public class PPPcOrderRS extends CommonPayDataRS {

}
