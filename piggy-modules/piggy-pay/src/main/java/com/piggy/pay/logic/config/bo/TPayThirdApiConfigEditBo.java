package com.piggy.pay.logic.config.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;

/**
 * 支付接口配置参数编辑对象 t_pay_third_api_config
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("支付接口配置参数编辑对象")
public class TPayThirdApiConfigEditBo {


    /** ID */
    @ApiModelProperty("ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 账号类型:1-服务商 2-商户 3-商户应用 */
    @ApiModelProperty("账号类型:1-服务商 2-商户 3-商户应用")
    @NotNull(message = "账号类型:1-服务商 2-商户 3-商户应用不能为空")
    private Integer infoType;

    /** 服务商号/商户号/应用ID */
    @ApiModelProperty("服务商号/商户号/应用ID")
    @NotBlank(message = "服务商号/商户号/应用ID不能为空")
    private String infoId;

    /** 支付接口代码 */
    @ApiModelProperty("支付接口代码")
    @NotBlank(message = "支付接口代码不能为空")
    private String ifCode;

    /** 接口配置参数,json字符串 */
    @ApiModelProperty("接口配置参数,json字符串")
    @NotBlank(message = "接口配置参数,json字符串不能为空")
    private String ifParams;

    /** 支付接口费率 */
    @ApiModelProperty("支付接口费率")
    private BigDecimal ifRate;

    /** 状态: 0-停用, 1-启用 */
    @ApiModelProperty("状态: 0-停用, 1-启用")
    @NotNull(message = "状态: 0-停用, 1-启用不能为空")
    private Integer state;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;

    /** 创建者用户ID */
    @ApiModelProperty("创建者用户ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long createdUid;

    /** 创建者姓名 */
    @ApiModelProperty("创建者姓名")
    private String createdBy;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @NotNull(message = "创建时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新者用户ID */
    @ApiModelProperty("更新者用户ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long updatedUid;

    /** 更新者姓名 */
    @ApiModelProperty("更新者姓名")
    private String updatedBy;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @NotNull(message = "更新时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;
}
