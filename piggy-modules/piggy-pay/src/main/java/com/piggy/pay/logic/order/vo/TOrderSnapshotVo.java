package com.piggy.pay.logic.order.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 订单接口数据快照视图对象 t_order_snapshot
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("订单接口数据快照视图对象")
public class TOrderSnapshotVo {

	private static final long serialVersionUID = 1L;

	/** 订单ID */
	@Excel(name = "订单ID")
	@ApiModelProperty("订单ID")
	private String orderId;

	/** 订单类型: 1-支付, 2-退款 */
	@Excel(name = "订单类型: 1-支付, 2-退款")
	@ApiModelProperty("订单类型: 1-支付, 2-退款")
	private Integer orderType;

	/** 下游请求数据 */
	@Excel(name = "下游请求数据")
	@ApiModelProperty("下游请求数据")
	private String mchReqData;

	/** 下游请求时间 */
	@Excel(name = "下游请求时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("下游请求时间")
	private Date mchReqTime;

	/** 向下游响应数据 */
	@Excel(name = "向下游响应数据")
	@ApiModelProperty("向下游响应数据")
	private String mchRespData;

	/** 向下游响应时间 */
	@Excel(name = "向下游响应时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("向下游响应时间")
	private Date mchRespTime;

	/** 向上游请求数据 */
	@Excel(name = "向上游请求数据")
	@ApiModelProperty("向上游请求数据")
	private String channelReqData;

	/** 向上游请求时间 */
	@Excel(name = "向上游请求时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("向上游请求时间")
	private Date channelReqTime;

	/** 上游响应数据 */
	@Excel(name = "上游响应数据")
	@ApiModelProperty("上游响应数据")
	private String channelRespData;

	/** 上游响应时间 */
	@Excel(name = "上游响应时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("上游响应时间")
	private Date channelRespTime;

	/** 创建时间 */
	@Excel(name = "创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;

	/** 更新时间 */
	@Excel(name = "更新时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;


}
