package com.piggy.pay.logic.mch.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.mch.domain.TMchDivisionReceiverGroup;
import com.piggy.pay.logic.mch.vo.TMchDivisionReceiverGroupVo;
import com.piggy.pay.logic.mch.bo.TMchDivisionReceiverGroupQueryBo;
import com.piggy.pay.logic.mch.bo.TMchDivisionReceiverGroupEditBo;

import java.util.Collection;
import java.util.List;

/**
 * 分账账号组Service接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface ITMchDivisionReceiverGroupService extends IServicePlus<TMchDivisionReceiverGroup> {
    /**
     * 查询单个
     *
     * @return
     */
    TMchDivisionReceiverGroupVo queryById(Long receiverGroupId);

    /**
     * 查询列表
     */
    TableDataInfo<TMchDivisionReceiverGroupVo> queryPageList(TMchDivisionReceiverGroupQueryBo bo);

    /**
     * 查询列表
     */
    List<TMchDivisionReceiverGroupVo> queryList(TMchDivisionReceiverGroupQueryBo bo);

    /**
     * 根据新增业务对象插入分账账号组
     *
     * @param bo 分账账号组新增业务对象
     * @return
     */
    Boolean insertByAddBo(TMchDivisionReceiverGroupEditBo bo);

    /**
     * 根据编辑业务对象修改分账账号组
     *
     * @param bo 分账账号组编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TMchDivisionReceiverGroupEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    TMchDivisionReceiverGroup findByIdAndMchNo(Long groupId, String mchNo);
}
