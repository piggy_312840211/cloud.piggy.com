package com.piggy.pay.logic.mch.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.mch.domain.TMchDivisionReceiver;

/**
 * 商户分账接收者账号绑定关系Mapper接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface TMchDivisionReceiverMapper extends BaseMapperPlus<TMchDivisionReceiver> {

}
