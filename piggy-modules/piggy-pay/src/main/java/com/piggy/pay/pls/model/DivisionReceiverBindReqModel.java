package com.piggy.pay.pls.model;

import com.piggy.pay.pls.ApiField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/***
* 分账账号的绑定
*
* @author terrfly
* @site https://www.jeepay.vip
* @date 2021/8/25 10:36
*/
@Data
@NoArgsConstructor
public class DivisionReceiverBindReqModel implements Serializable {

    private static final long serialVersionUID = -3998573128290306948L;

    @ApiField("mchNo")
    private String mchNo;      // 商户号

    @ApiField("appId")
    private String appId;      // 应用ID

    /** 支付接口代码   **/
    @ApiField("ifCode")
    private String ifCode;

    /** 接收者账号别名 **/
    @ApiField("receiverAlias")
    private String receiverAlias;

    /** 组ID  **/
    @ApiField("receiverGroupId")
    private Long receiverGroupId;

    /** 分账接收账号类型: 0-个人(对私) 1-商户(对公) **/
    @ApiField("accType")
    private Byte accType;

    /** 分账接收账号 **/
    @ApiField("accNo")
    private String accNo;

    /** 分账接收账号名称 **/
    @ApiField("accName")
    private String accName;

    /** 分账关系类型（参考微信）， 如： SERVICE_PROVIDER 服务商等 **/
    @ApiField("relationType")
    private String relationType;

    /** 当选择自定义时，需要录入该字段。 否则为对应的名称 **/
    @ApiField("relationTypeName")
    private String relationTypeName;

    /** 渠道特殊信息 */
    @ApiField("channelExtInfo")
    private String channelExtInfo;

    /** 分账比例 **/
    @ApiField("divisionProfit")
    private String divisionProfit;

}
