package com.piggy.pay.pls.request;

import com.piggy.pay.pls.BasePay;
import com.piggy.pay.pls.model.PayOrderQueryReqModel;
import com.piggy.pay.pls.net.RequestOptions;
import com.piggy.pay.pls.response.PayOrderQueryResponse;

/**
 * Jeepay支付查单请求实现
 * @author jmdhappy
 * @site https://www.jeepay.vip
 * @date 2021-06-08 11:00
 */
public class PayOrderQueryRequest implements PayRequest<PayOrderQueryResponse, PayOrderQueryReqModel> {

    private String apiVersion = BasePay.VERSION;
    private String apiUri = "api/pay/query";
    private RequestOptions options;
    private PayOrderQueryReqModel bizModel = null;

    @Override
    public String getApiUri() {
        return this.apiUri;
    }

    @Override
    public String getApiVersion() {
        return this.apiVersion;
    }

    @Override
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    @Override
    public RequestOptions getRequestOptions() {
        return this.options;
    }

    @Override
    public void setRequestOptions(RequestOptions options) {
        this.options = options;
    }

    @Override
    public PayOrderQueryReqModel getBizModel() {
        return this.bizModel;
    }

    @Override
    public void setBizModel(PayOrderQueryReqModel bizModel) {
        this.bizModel = bizModel;
    }

    @Override
    public Class<PayOrderQueryResponse> getResponseClass() {
        return PayOrderQueryResponse.class;
    }

}
