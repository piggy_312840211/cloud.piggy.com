package com.piggy.pay.logic.mch.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import com.piggy.pay.logic.mch.bo.TMchInfoEditBo;
import com.piggy.pay.logic.mch.bo.TMchInfoQueryBo;
import com.piggy.pay.logic.mch.service.ITMchInfoService;
import com.piggy.pay.logic.mch.vo.TMchInfoVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 商户信息Controller
 *
 * @author piggy
 * @date 2023-05-25
 */
@Api(value = "商户信息控制器", tags = {"商户信息管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/mch/info")
public class TMchInfoController extends BaseController {

    private final ITMchInfoService iTMchInfoService;

    /**
     * 查询商户信息列表
     */
    @ApiOperation("查询商户信息列表")
    @SaCheckPermission("mch:info:list")
    @GetMapping("/list")
    public TableDataInfo<TMchInfoVo> list(@Validated TMchInfoQueryBo bo) {
        return iTMchInfoService.queryPageList(bo);
    }

    /**
     * 导出商户信息列表
     */
    @ApiOperation("导出商户信息列表")
    @SaCheckPermission("mch:info:export")
    @Log(title = "商户信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TMchInfoQueryBo bo, HttpServletResponse response) throws IOException {
        List<TMchInfoVo> list = iTMchInfoService.queryList(bo);
        ExcelUtil<TMchInfoVo> util = new ExcelUtil<TMchInfoVo>(TMchInfoVo.class);
        util.exportExcel(response, list, "商户信息");
    }

    /**
     * 获取商户信息详细信息
     */
    @ApiOperation("获取商户信息详细信息")
    @SaCheckPermission("mch:info:query")
    @GetMapping("/{mchNo}")
    public R<TMchInfoVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("mchNo") String mchNo) {
        return R.ok(iTMchInfoService.queryById(mchNo));
    }

    /**
     * 新增商户信息
     */
    @ApiOperation("新增商户信息")
    @SaCheckPermission("mch:info:add")
    @Log(title = "商户信息", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TMchInfoEditBo bo) {
        return R.ok(iTMchInfoService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改商户信息
     */
    @ApiOperation("修改商户信息")
    @SaCheckPermission("mch:info:edit")
    @Log(title = "商户信息", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TMchInfoEditBo bo) {
        return R.ok(iTMchInfoService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除商户信息
     */
    @ApiOperation("删除商户信息")
    @SaCheckPermission("mch:info:remove")
    @Log(title = "商户信息" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{mchNos}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable String[] mchNos) {
        return R.ok(iTMchInfoService.deleteWithValidByIds(Arrays.asList(mchNos), true) ? 1 : 0);
    }
}
