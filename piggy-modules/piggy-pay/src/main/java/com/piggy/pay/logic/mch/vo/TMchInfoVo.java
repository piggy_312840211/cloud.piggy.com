package com.piggy.pay.logic.mch.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 商户信息视图对象 t_mch_info
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("商户信息视图对象")
public class TMchInfoVo {

	private static final long serialVersionUID = 1L;

	/** 商户号 */
	@Excel(name = "商户号")
	@ApiModelProperty("商户号")
	private String mchNo;

	/** 商户名称 */
	@Excel(name = "商户名称")
	@ApiModelProperty("商户名称")
	private String mchName;

	/** 商户简称 */
	@Excel(name = "商户简称")
	@ApiModelProperty("商户简称")
	private String mchShortName;

	/** 类型: 1-普通商户, 2-特约商户(服务商模式) */
	@Excel(name = "类型: 1-普通商户, 2-特约商户(服务商模式)")
	@ApiModelProperty("类型: 1-普通商户, 2-特约商户(服务商模式)")
	private Integer type;

	/** 服务商号 */
	@Excel(name = "服务商号")
	@ApiModelProperty("服务商号")
	private String isvNo;

	/** 联系人姓名 */
	@Excel(name = "联系人姓名")
	@ApiModelProperty("联系人姓名")
	private String contactName;

	/** 联系人手机号 */
	@Excel(name = "联系人手机号")
	@ApiModelProperty("联系人手机号")
	private String contactTel;

	/** 联系人邮箱 */
	@Excel(name = "联系人邮箱")
	@ApiModelProperty("联系人邮箱")
	private String contactEmail;

	/** 商户状态: 0-停用, 1-正常 */
	@Excel(name = "商户状态: 0-停用, 1-正常")
	@ApiModelProperty("商户状态: 0-停用, 1-正常")
	private Integer state;

	/** 商户备注 */
	@Excel(name = "商户备注")
	@ApiModelProperty("商户备注")
	private String remark;

	/** 初始用户ID（创建商户时，允许商户登录的用户） */
	@Excel(name = "初始用户ID" , readConverterExp = "创=建商户时，允许商户登录的用户")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("初始用户ID（创建商户时，允许商户登录的用户）")
	private Long initUserId;

	/** 创建者用户ID */
	@Excel(name = "创建者用户ID")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("创建者用户ID")
	private Long createdUid;

	/** 创建者姓名 */
	@Excel(name = "创建者姓名")
	@ApiModelProperty("创建者姓名")
	private String createdBy;

	/** 创建时间 */
	@Excel(name = "创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;

	/** 更新人 */
	@Excel(name = "更新人")
	@ApiModelProperty("更新人")
	private String updateBy;

	/** 更新时间 */
	@Excel(name = "更新时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;


}
