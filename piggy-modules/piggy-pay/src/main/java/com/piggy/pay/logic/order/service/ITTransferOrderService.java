package com.piggy.pay.logic.order.service;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.core.enums.ITransferEnums;
import com.piggy.pay.logic.order.domain.TTransferOrder;
import com.piggy.pay.logic.order.vo.TTransferOrderVo;
import com.piggy.pay.logic.order.bo.TTransferOrderQueryBo;
import com.piggy.pay.logic.order.bo.TTransferOrderEditBo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * 转账订单Service接口
 *
 * @author zito
 * @date 2023-05-25
 */
public interface ITTransferOrderService extends IServicePlus<TTransferOrder> {
    /**
     * 查询单个
     *
     * @return
     */
    TTransferOrderVo queryById(String transferId);

    /**
     * 查询列表
     */
    TableDataInfo<TTransferOrderVo> queryPageList(TTransferOrderQueryBo bo);

    /**
     * 查询列表
     */
    List<TTransferOrderVo> queryList(TTransferOrderQueryBo bo);

    /**
     * 根据新增业务对象插入转账订单
     *
     * @param bo 转账订单新增业务对象
     * @return
     */
    Boolean insertByAddBo(TTransferOrderEditBo bo);

    /**
     * 根据编辑业务对象修改转账订单
     *
     * @param bo 转账订单编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TTransferOrderEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);

    /** 更新转账订单状态  【转账订单生成】 --》 【转账中】 **/
    boolean updateInit2Ing(String transferId);

    /** 更新转账订单状态  【转账中】 --》 【转账成功】 **/
    boolean updateIng2Success(String transferId, String channelOrderNo);

    /** 更新转账订单状态  【转账中】 --》 【转账失败】 **/
    boolean updateIng2Fail(String transferId, String channelOrderNo, String channelErrCode, String channelErrMsg);

    /** 更新转账订单状态  【转账中】 --》 【转账成功/转账失败】 **/
    boolean updateIng2SuccessOrFail(String transferId, Integer updateState, String channelOrderNo, String channelErrCode, String channelErrMsg);

    /** 查询商户订单 **/
    TTransferOrder queryMchOrder(String mchNo, String mchOrderNo, String transferId);

    TableDataInfo<TTransferOrder> pageList(LambdaQueryWrapper<TTransferOrder> wrapper, TTransferOrder transferOrder, JSONObject paramJSON);

}
