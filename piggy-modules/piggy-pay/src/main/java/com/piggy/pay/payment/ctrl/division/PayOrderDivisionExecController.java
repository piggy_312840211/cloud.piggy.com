/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.piggy.pay.payment.ctrl.division;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.pay.components.mq.model.PayOrderDivisionMQ;
import com.piggy.pay.core.beans.RequestKitBean;
import com.piggy.pay.core.constants.CS;
import com.piggy.pay.core.enums.ChannelEnums;
import com.piggy.pay.core.enums.IDivisionEnums;
import com.piggy.pay.core.enums.IOrderEnums;
import com.piggy.pay.core.model.ApiRes;
import com.piggy.pay.logic.mch.domain.TMchDivisionReceiver;
import com.piggy.pay.logic.mch.domain.TMchDivisionReceiverGroup;
import com.piggy.pay.logic.mch.service.ITMchDivisionReceiverGroupService;
import com.piggy.pay.logic.mch.service.ITMchDivisionReceiverService;
import com.piggy.pay.logic.order.domain.TPayOrder;
import com.piggy.pay.logic.order.service.ITPayOrderService;
import com.piggy.pay.payment.ctrl.ApiController;
import com.piggy.pay.payment.model.MchAppConfigContext;
import com.piggy.pay.payment.rqrs.division.PayOrderDivisionExecRQ;
import com.piggy.pay.payment.rqrs.division.PayOrderDivisionExecRS;
import com.piggy.pay.payment.rqrs.msg.ChannelRetMsg;
import com.piggy.pay.payment.service.ConfigContextQueryService;
import com.piggy.pay.payment.service.PayOrderDivisionProcessService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
* 发起分账请求
*
* @author terrfly
* @site https://www.jeequan.com
* @date 2021/8/27 8:01
*/
@Slf4j
@RestController
public class PayOrderDivisionExecController extends ApiController {

    @Resource
    private ConfigContextQueryService configContextQueryService;
    @Resource
    private ITPayOrderService payOrderService;
    @Resource
    private ITMchDivisionReceiverService mchDivisionReceiverService;
    @Resource
    private ITMchDivisionReceiverGroupService mchDivisionReceiverGroupService;
    @Resource
    private PayOrderDivisionProcessService payOrderDivisionProcessService;

    /** 分账执行 **/
    @PostMapping("/api/division/exec")
    public ApiRes exec(){

        //获取参数 & 验签
        PayOrderDivisionExecRQ bizRQ = getRQByWithMchSign(PayOrderDivisionExecRQ.class);

        try {

            if(StringUtils.isAllEmpty(bizRQ.getMchOrderNo(), bizRQ.getPayOrderId())){
                throw new BaseException("mchOrderNo 和 payOrderId不能同时为空");
            }

            TPayOrder payOrder = payOrderService.queryMchOrder(bizRQ.getMchNo(), bizRQ.getPayOrderId(), bizRQ.getMchOrderNo());
            if(payOrder == null){
                throw new BaseException("订单不存在");
            }

            if(payOrder.getState() != IOrderEnums.OrderStateEnums.STATE_SUCCESS.getState() || payOrder.getDivisionState() != IDivisionEnums.DivisionStateEnums.DIVISION_STATE_UNHAPPEN.getState() || payOrder.getDivisionMode() != IDivisionEnums.DivisionModeEnums.DIVISION_MODE_MANUAL.getMode()){
                throw new BaseException("当前订单状态不支持分账");
            }

            List<PayOrderDivisionMQ.CustomerDivisionReceiver> receiverList = null;

            //不使用默认分组， 需要转换每个账号信息
            if(bizRQ.getUseSysAutoDivisionReceivers() != CS.YES && !StringUtils.isEmpty(bizRQ.getReceivers())){
                receiverList = JSON.parseArray(bizRQ.getReceivers(), PayOrderDivisionMQ.CustomerDivisionReceiver.class);
            }

            // 验证账号是否合法
            this.checkReceiverList(receiverList, payOrder.getIfCode(), bizRQ.getMchNo(), bizRQ.getAppId());

            // 商户配置信息
            MchAppConfigContext mchAppConfigContext = configContextQueryService.queryMchInfoAndAppInfo(bizRQ.getMchNo(), bizRQ.getAppId());
            if(mchAppConfigContext == null){
                throw new BaseException("获取商户应用信息失败");
            }

            //处理分账请求
            ChannelRetMsg channelRetMsg = payOrderDivisionProcessService.processPayOrderDivision(payOrder.getPayOrderId(), bizRQ.getUseSysAutoDivisionReceivers(), receiverList, false);

            PayOrderDivisionExecRS bizRS = new PayOrderDivisionExecRS();
            bizRS.setState(channelRetMsg.getChannelState() == ChannelEnums.CONFIRM_SUCCESS ? IOrderEnums.OrderStateEnums.STATE_SUCCESS.getState() : IOrderEnums.OrderStateEnums.STATE_FAIL.getState());
            bizRS.setChannelBatchOrderId(channelRetMsg.getChannelOrderId());
            bizRS.setErrCode(channelRetMsg.getChannelErrCode());
            bizRS.setErrMsg(channelRetMsg.getChannelErrMsg());

            return ApiRes.okWithSign(bizRS, mchAppConfigContext.getMchApp().getAppSecret());

        }  catch (BaseException e) {
            return ApiRes.customFail(e.getMessage());

        } catch (Exception e) {
            log.error("系统异常：{}", e);
            return ApiRes.customFail("系统异常");
        }
    }

    /** 检验账号是否合法 **/
    private void checkReceiverList(List<PayOrderDivisionMQ.CustomerDivisionReceiver> receiverList, String ifCode, String mchNo, String appId){

        if(receiverList == null || receiverList.isEmpty()){
            return ;
        }

        Set<Long> receiverIdSet = new HashSet<>();
        Set<Long> receiverGroupIdSet = new HashSet<>();

        for (PayOrderDivisionMQ.CustomerDivisionReceiver receiver : receiverList) {

            if(receiver.getReceiverId() != null){
                receiverIdSet.add(receiver.getReceiverId());
            }

            if(receiver.getReceiverGroupId() != null){
                receiverGroupIdSet.add(receiver.getReceiverGroupId());
            }

            if(receiver.getReceiverId() == null && receiver.getReceiverGroupId() == null){
                throw new BaseException("分账用户组： receiverId 和 与receiverGroupId 必填一项");
            }

            if(receiver.getDivisionProfit() != null){

                if(receiver.getDivisionProfit().compareTo(BigDecimal.ZERO) < 0){
                    throw new BaseException("分账用户receiverId=["+ ( receiver.getReceiverId() == null ? "": receiver.getReceiverId() ) +"]," +
                            "receiverGroupId=["+ (receiver.getReceiverGroupId() == null ? "": receiver.getReceiverGroupId() ) +"] 分账比例不得小于0%");
                }

                if(receiver.getDivisionProfit().compareTo(BigDecimal.ONE) > 0){
                    throw new BaseException("分账用户receiverId=["+ ( receiver.getReceiverId() == null ? "": receiver.getReceiverId() ) +"]," +
                            "receiverGroupId=["+ (receiver.getReceiverGroupId() == null ? "": receiver.getReceiverGroupId() ) +"] 分账比例不得高于100%");
                }
            }
        }

        if(!receiverIdSet.isEmpty()){

            long receiverCount = mchDivisionReceiverService.count(new LambdaQueryWrapper<TMchDivisionReceiver>()
                    .in(TMchDivisionReceiver::getReceiverId, receiverIdSet)
                    .eq(TMchDivisionReceiver::getMchNo, mchNo)
                    .eq(TMchDivisionReceiver::getAppId, appId)
                    .eq(TMchDivisionReceiver::getIfCode, ifCode)
                    .eq(TMchDivisionReceiver::getState, CS.YES));

            if(receiverCount != receiverIdSet.size()){
                throw new BaseException("分账[用户]中包含不存在或渠道不可用账号，请更改");
            }
        }

        if(!receiverGroupIdSet.isEmpty()){

            long receiverGroupCount = mchDivisionReceiverGroupService.count(new LambdaQueryWrapper<TMchDivisionReceiverGroup>()
                    .in(TMchDivisionReceiverGroup::getReceiverGroupId, receiverGroupIdSet)
                    .eq(TMchDivisionReceiverGroup::getMchNo, mchNo)
            );

            if(receiverGroupCount != receiverGroupIdSet.size()){
                throw new BaseException("分账[账号组]中包含不存在或不可用组，请更改");
            }
        }

    }

}
