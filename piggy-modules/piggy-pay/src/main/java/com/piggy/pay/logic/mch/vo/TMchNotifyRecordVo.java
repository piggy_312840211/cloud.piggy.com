package com.piggy.pay.logic.mch.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 商户通知记录视图对象 t_mch_notify_record
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("商户通知记录视图对象")
public class TMchNotifyRecordVo {

	private static final long serialVersionUID = 1L;

	/** 商户通知记录ID */
	@Excel(name = "商户通知记录ID")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("商户通知记录ID")
	private Long notifyId;

	/** 订单ID */
	@Excel(name = "订单ID")
	@ApiModelProperty("订单ID")
	private String orderId;

	/** 订单类型:1-支付,2-退款 */
	@Excel(name = "订单类型:1-支付,2-退款")
	@ApiModelProperty("订单类型:1-支付,2-退款")
	private Integer orderType;

	/** 商户订单号 */
	@Excel(name = "商户订单号")
	@ApiModelProperty("商户订单号")
	private String mchOrderNo;

	/** 商户号 */
	@Excel(name = "商户号")
	@ApiModelProperty("商户号")
	private String mchNo;

	/** 服务商号 */
	@Excel(name = "服务商号")
	@ApiModelProperty("服务商号")
	private String isvNo;

	/** 应用ID */
	@Excel(name = "应用ID")
	@ApiModelProperty("应用ID")
	private String appId;

	/** 通知地址 */
	@Excel(name = "通知地址")
	@ApiModelProperty("通知地址")
	private String notifyUrl;

	/** 通知响应结果 */
	@Excel(name = "通知响应结果")
	@ApiModelProperty("通知响应结果")
	private String resResult;

	/** 通知次数 */
	@Excel(name = "通知次数")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("通知次数")
	private Long notifyCount;

	/** 最大通知次数, 默认6次 */
	@Excel(name = "最大通知次数, 默认6次")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("最大通知次数, 默认6次")
	private Long notifyCountLimit;

	/** 通知状态,1-通知中,2-通知成功,3-通知失败 */
	@Excel(name = "通知状态,1-通知中,2-通知成功,3-通知失败")
	@ApiModelProperty("通知状态,1-通知中,2-通知成功,3-通知失败")
	private Integer state;

	/** 最后一次通知时间 */
	@Excel(name = "最后一次通知时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("最后一次通知时间")
	private Date lastNotifyTime;

	/** 创建时间 */
	@Excel(name = "创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;

	/** 更新时间 */
	@Excel(name = "更新时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;


}
