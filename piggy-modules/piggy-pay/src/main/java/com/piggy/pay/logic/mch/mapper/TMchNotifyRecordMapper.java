package com.piggy.pay.logic.mch.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.pay.logic.mch.domain.TMchNotifyRecord;

/**
 * 商户通知记录Mapper接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface TMchNotifyRecordMapper extends BaseMapperPlus<TMchNotifyRecord> {

}
