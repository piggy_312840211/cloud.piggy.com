package com.piggy.pay.logic.mch.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import com.baomidou.mybatisplus.annotation.*;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;


/**
 * 商户通知记录对象 t_mch_notify_record
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("t_mch_notify_record")
public class TMchNotifyRecord implements Serializable {

    private static final long serialVersionUID=1L;

    /** 商户通知记录ID */
    @ApiModelProperty("商户通知记录ID")
    @TableId(value = "notify_id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long notifyId;

    /** 订单ID */
    @ApiModelProperty("订单ID")
    private String orderId;

    /** 订单类型:1-支付,2-退款 */
    @ApiModelProperty("订单类型:1-支付,2-退款")
    private Integer orderType;

    /** 商户订单号 */
    @ApiModelProperty("商户订单号")
    private String mchOrderNo;

    /** 商户号 */
    @ApiModelProperty("商户号")
    private String mchNo;

    /** 服务商号 */
    @ApiModelProperty("服务商号")
    private String isvNo;

    /** 应用ID */
    @ApiModelProperty("应用ID")
    private String appId;

    /** 通知地址 */
    @ApiModelProperty("通知地址")
    private String notifyUrl;

    /** 通知响应结果 */
    @ApiModelProperty("通知响应结果")
    private String resResult;

    /** 通知次数 */
    @ApiModelProperty("通知次数")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long notifyCount;

    /** 最大通知次数, 默认6次 */
    @ApiModelProperty("最大通知次数, 默认6次")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long notifyCountLimit;

    /** 通知状态,1-通知中,2-通知成功,3-通知失败 */
    @ApiModelProperty("通知状态,1-通知中,2-通知成功,3-通知失败")
    private Integer state;

    /** 最后一次通知时间 */
    @ApiModelProperty("最后一次通知时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastNotifyTime;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

}
