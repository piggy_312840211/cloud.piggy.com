package com.piggy.pay.logic.config.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.config.bo.TPayWayEditBo;
import com.piggy.pay.logic.config.bo.TPayWayQueryBo;
import com.piggy.pay.logic.config.domain.TPayWay;
import com.piggy.pay.logic.config.mapper.TPayWayMapper;
import com.piggy.pay.logic.config.service.ITPayWayService;
import com.piggy.pay.logic.config.vo.TPayWayVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * 支付方式Service业务层处理
 *
 * @author piggy
 * @date 2023-05-25
 */
@Slf4j
@Service
public class TPayWayServiceImpl extends ServiceImpl<TPayWayMapper, TPayWay> implements ITPayWayService {

    @Override
    public TPayWayVo queryById(String wayCode){
        return getVoById(wayCode, TPayWayVo.class);
    }

    @Override
    public TableDataInfo<TPayWayVo> queryPageList(TPayWayQueryBo bo) {
        PagePlus<TPayWay, TPayWayVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), TPayWayVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<TPayWayVo> queryList(TPayWayQueryBo bo) {
        return listVo(buildQueryWrapper(bo), TPayWayVo.class);
    }

    private LambdaQueryWrapper<TPayWay> buildQueryWrapper(TPayWayQueryBo bo) {
        LambdaQueryWrapper<TPayWay> lqw = Wrappers.lambdaQuery();
        lqw.eq(StrUtil.isNotBlank(bo.getWayCode()), TPayWay::getWayCode, bo.getWayCode());
        lqw.like(StrUtil.isNotBlank(bo.getWayName()), TPayWay::getWayName, bo.getWayName());
        lqw.eq(bo.getCreatedAt() != null, TPayWay::getCreatedAt, bo.getCreatedAt());
        lqw.eq(bo.getUpdatedAt() != null, TPayWay::getUpdatedAt, bo.getUpdatedAt());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(TPayWayEditBo bo) {
        TPayWay add = BeanUtil.toBean(bo, TPayWay.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(TPayWayEditBo bo) {
        TPayWay update = BeanUtil.toBean(bo, TPayWay.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TPayWay entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
