package com.piggy.pay.logic.mch.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * 服务商信息分页查询对象 t_mch_isv_info
 *
 * @author piggy
 * @date 2023-05-25
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel("服务商信息分页查询对象")
public class TMchIsvInfoQueryBo extends BaseEntity {

	/** 服务商号 */
	@ApiModelProperty("服务商号")
	private String isvNo;
	/** 服务商名称 */
	@ApiModelProperty("服务商名称")
	private String isvName;
	/** 服务商简称 */
	@ApiModelProperty("服务商简称")
	private String isvShortName;
	/** 联系人姓名 */
	@ApiModelProperty("联系人姓名")
	private String contactName;
	/** 联系人手机号 */
	@ApiModelProperty("联系人手机号")
	private String contactTel;
	/** 联系人邮箱 */
	@ApiModelProperty("联系人邮箱")
	private String contactEmail;
	/** 状态: 0-停用, 1-正常 */
	@ApiModelProperty("状态: 0-停用, 1-正常")
	private Integer state;
	/** 创建者用户ID */
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("创建者用户ID")
	private Long createdUid;
	/** 创建者姓名 */
	@ApiModelProperty("创建者姓名")
	private String createdBy;
	/** 创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;
	/** 更新时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;

}
