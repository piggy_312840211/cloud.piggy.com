package com.piggy.pay.pls;

import com.piggy.common.core.exception.base.BaseException;
import com.piggy.pay.pls.net.APIResource;
import com.piggy.pay.pls.net.RequestOptions;
import com.piggy.pay.pls.request.PayRequest;
import com.piggy.pay.pls.response.PayResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * BasePay sdk客户端
 * @author jmdhappy
 * @site https://www.jeepay.vip
 * @date 2021-06-08 11:00
 */
public class PayClient extends APIResource {

    private static Map<String, PayClient> clientMap = new HashMap<>();

    private String appId;
    private String signType = BasePay.DEFAULT_SIGN_TYPE;
    private String apiKey = BasePay.apiKey;
    private String apiBase = BasePay.getApiBase();

    public String getAppId() {
        return appId;
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public PayClient(String apiBase, String signType, String apiKey) {
        this.apiBase = apiBase;
        this.signType = signType;
        this.apiKey = apiKey;
    }

    public PayClient(String apiBase, String apiKey) {
        this.apiBase = apiBase;
        this.apiKey = apiKey;
    }

    public PayClient(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiBase() {
        return apiBase;
    }

    public void setApiBase(String apiBase) {
        this.apiBase = apiBase;
    }

    public PayClient() {
    }

    public static synchronized PayClient getInstance(String appId, String apiKey, String apiBase) {
        PayClient client = clientMap.get(appId);
        if (client != null) {
            client.setApiKey(apiKey);
            client.setApiBase(apiBase);
            return client;
        }
        client = new PayClient();
        clientMap.put(appId, client);
        client.appId = appId;
        client.apiKey = apiKey;
        client.apiBase = apiBase;
        return client;
    }

    public static synchronized PayClient getInstance(String appId, String apiKey) {
        PayClient client = clientMap.get(appId);
        if (client != null) {
            client.setApiKey(apiKey);
            return client;
        }
        client = new PayClient();
        clientMap.put(appId, client);
        client.appId = appId;
        client.apiKey = apiKey;
        return client;
    }

    public static synchronized PayClient getInstance(String appId) {
        PayClient client = clientMap.get(appId);
        if (client != null) {
            return client;
        }
        client = new PayClient();
        clientMap.put(appId, client);
        client.appId = appId;
        return client;
    }

    public <T extends PayResponse,M> T execute(PayRequest<T,M> request) throws BaseException {

        // 支持用户自己设置RequestOptions
        if(request.getRequestOptions() == null) {
            RequestOptions options = RequestOptions.builder()
                    .setVersion(request.getApiVersion())
                    .setUri(request.getApiUri())
                    .setAppId(this.appId)
                    .setApiKey(this.apiKey)
                    .build();
            request.setRequestOptions(options);
        }

        return execute(request, RequestMethod.POST, this.apiBase);
    }

    public <T extends PayResponse, M> T executeByRSA2(PayRequest<T, M> request) throws BaseException {

        // 支持用户自己设置RequestOptions
        if(request.getRequestOptions() == null) {
            RequestOptions options = RequestOptions.builder()
                    .setVersion(request.getApiVersion())
                    .setUri(request.getApiUri())
                    .setAppId(this.appId)
                    .setApiKey(this.apiKey)
                    .setSignType(BasePay.SIGN_TYPE_RSA2)
                    .build();
            request.setRequestOptions(options);
        }

        return execute(request, RequestMethod.POST, this.apiBase);
    }

}
