package com.piggy.pay.logic.order.controller;

import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import com.piggy.pay.logic.order.service.ITPayOrderDivisionRecordService;
import lombok.RequiredArgsConstructor;

import javax.annotation.Resource;
import javax.validation.constraints.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.piggy.pay.logic.order.vo.TPayOrderDivisionRecordVo;
import com.piggy.pay.logic.order.bo.TPayOrderDivisionRecordQueryBo;
import com.piggy.pay.logic.order.bo.TPayOrderDivisionRecordEditBo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.lang.Integer;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
 * 分账记录Controller
 *
 * @author zito
 * @date 2023-05-25
 */
@Api(value = "分账记录控制器", tags = {"分账记录管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/order/divisionrecord")
public class TPayOrderDivisionRecordController extends BaseController {

    private final ITPayOrderDivisionRecordService iTPayOrderDivisionRecordService;

    /**
     * 查询分账记录列表
     */
    @ApiOperation("查询分账记录列表")
    @SaCheckPermission("order:divisionrecord:list")
    @GetMapping("/list")
    public TableDataInfo<TPayOrderDivisionRecordVo> list(@Validated TPayOrderDivisionRecordQueryBo bo) {
        return iTPayOrderDivisionRecordService.queryPageList(bo);
    }

    /**
     * 导出分账记录列表
     */
    @ApiOperation("导出分账记录列表")
    @SaCheckPermission("order:divisionrecord:export")
    @Log(title = "分账记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TPayOrderDivisionRecordQueryBo bo, HttpServletResponse response) throws IOException {
        List<TPayOrderDivisionRecordVo> list = iTPayOrderDivisionRecordService.queryList(bo);
        ExcelUtil<TPayOrderDivisionRecordVo> util = new ExcelUtil<TPayOrderDivisionRecordVo>(TPayOrderDivisionRecordVo.class);
        util.exportExcel(response, list, "分账记录");
    }

    /**
     * 获取分账记录详细信息
     */
    @ApiOperation("获取分账记录详细信息")
    @SaCheckPermission("order:divisionrecord:query")
    @GetMapping("/{recordId}")
    public R<TPayOrderDivisionRecordVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("recordId") Long recordId) {
        return R.ok(iTPayOrderDivisionRecordService.queryById(recordId));
    }

    /**
     * 新增分账记录
     */
    @ApiOperation("新增分账记录")
    @SaCheckPermission("order:divisionrecord:add")
    @Log(title = "分账记录", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TPayOrderDivisionRecordEditBo bo) {
        return R.ok(iTPayOrderDivisionRecordService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改分账记录
     */
    @ApiOperation("修改分账记录")
    @SaCheckPermission("order:divisionrecord:edit")
    @Log(title = "分账记录", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TPayOrderDivisionRecordEditBo bo) {
        return R.ok(iTPayOrderDivisionRecordService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除分账记录
     */
    @ApiOperation("删除分账记录")
    @SaCheckPermission("order:divisionrecord:remove")
    @Log(title = "分账记录" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{recordIds}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable Long[] recordIds) {
        return R.ok(iTPayOrderDivisionRecordService.deleteWithValidByIds(Arrays.asList(recordIds), true) ? 1 : 0);
    }
}
