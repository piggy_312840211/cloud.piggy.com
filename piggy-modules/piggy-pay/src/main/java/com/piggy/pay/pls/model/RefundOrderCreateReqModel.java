package com.piggy.pay.pls.model;

import com.piggy.pay.pls.ApiField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 退款下单请求实体类
 * @author jmdhappy
 * @site https://www.jeepay.vip
 * @date 2021-06-18 09:00
 */
@Data
@NoArgsConstructor
public class RefundOrderCreateReqModel implements Serializable {

    private static final long serialVersionUID = -3998573128290306948L;

    @ApiField("mchNo")
    private String mchNo;      // 商户号
    @ApiField("appId")
    private String appId;      // 应用ID
    @ApiField("mchOrderNo")
    String mchOrderNo;          // 商户订单号
    @ApiField("payOrderId")
    String payOrderId;          // 支付系统订单号
    @ApiField("mchRefundNo")
    String mchRefundNo;         // 退款单号
    @ApiField("refundAmount")
    Long refundAmount;          // 退款金额
    @ApiField("currency")
    String currency;            // 货币代码，当前只支持cny
    @ApiField("refundReason")
    String refundReason;        // 退款原因
    @ApiField("clientIp")
    String clientIp;            // 客户端IP
    @ApiField("notifyUrl")
    String notifyUrl;           // 异步通知地址
    @ApiField("channelExtra")
    String channelExtra;        // 特定渠道额外支付参数
    @ApiField("extParam")
    String extParam;            // 商户扩展参数

}
