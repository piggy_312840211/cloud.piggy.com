package com.piggy.pay.logic.mch.controller;

import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import lombok.RequiredArgsConstructor;

import javax.annotation.Resource;
import javax.validation.constraints.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.piggy.pay.logic.mch.vo.TMchNotifyRecordVo;
import com.piggy.pay.logic.mch.bo.TMchNotifyRecordQueryBo;
import com.piggy.pay.logic.mch.bo.TMchNotifyRecordEditBo;
import com.piggy.pay.logic.mch.service.ITMchNotifyRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.lang.Integer;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
 * 商户通知记录Controller
 *
 * @author piggy
 * @date 2023-05-25
 */
@Api(value = "商户通知记录控制器", tags = {"商户通知记录管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/mch/notifyrecord")
public class TMchNotifyRecordController extends BaseController {

    private final ITMchNotifyRecordService iTMchNotifyRecordService;

    /**
     * 查询商户通知记录列表
     */
    @ApiOperation("查询商户通知记录列表")
    @SaCheckPermission("mch:notifyrecord:list")
    @GetMapping("/list")
    public TableDataInfo<TMchNotifyRecordVo> list(@Validated TMchNotifyRecordQueryBo bo) {
        return iTMchNotifyRecordService.queryPageList(bo);
    }

    /**
     * 导出商户通知记录列表
     */
    @ApiOperation("导出商户通知记录列表")
    @SaCheckPermission("mch:notifyrecord:export")
    @Log(title = "商户通知记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TMchNotifyRecordQueryBo bo, HttpServletResponse response) throws IOException {
        List<TMchNotifyRecordVo> list = iTMchNotifyRecordService.queryList(bo);
        ExcelUtil<TMchNotifyRecordVo> util = new ExcelUtil<TMchNotifyRecordVo>(TMchNotifyRecordVo.class);
        util.exportExcel(response, list, "商户通知记录");
    }

    /**
     * 获取商户通知记录详细信息
     */
    @ApiOperation("获取商户通知记录详细信息")
    @SaCheckPermission("mch:notifyrecord:query")
    @GetMapping("/{notifyId}")
    public R<TMchNotifyRecordVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("notifyId") Long notifyId) {
        return R.ok(iTMchNotifyRecordService.queryById(notifyId));
    }

    /**
     * 新增商户通知记录
     */
    @ApiOperation("新增商户通知记录")
    @SaCheckPermission("mch:notifyrecord:add")
    @Log(title = "商户通知记录", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TMchNotifyRecordEditBo bo) {
        return R.ok(iTMchNotifyRecordService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改商户通知记录
     */
    @ApiOperation("修改商户通知记录")
    @SaCheckPermission("mch:notifyrecord:edit")
    @Log(title = "商户通知记录", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TMchNotifyRecordEditBo bo) {
        return R.ok(iTMchNotifyRecordService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除商户通知记录
     */
    @ApiOperation("删除商户通知记录")
    @SaCheckPermission("mch:notifyrecord:remove")
    @Log(title = "商户通知记录" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{notifyIds}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable Long[] notifyIds) {
        return R.ok(iTMchNotifyRecordService.deleteWithValidByIds(Arrays.asList(notifyIds), true) ? 1 : 0);
    }
}
