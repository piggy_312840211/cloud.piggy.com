package com.piggy.pay.logic.order.controller;

import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import lombok.RequiredArgsConstructor;

import javax.annotation.Resource;
import javax.validation.constraints.*;

import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.piggy.pay.logic.order.vo.TOrderSnapshotVo;
import com.piggy.pay.logic.order.bo.TOrderSnapshotQueryBo;
import com.piggy.pay.logic.order.bo.TOrderSnapshotEditBo;
import com.piggy.pay.logic.order.service.ITOrderSnapshotService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.lang.Integer;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
 * 订单接口数据快照Controller
 *
 * @author piggy
 * @date 2023-05-25
 */
@Api(value = "订单接口数据快照控制器", tags = {"订单接口数据快照管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/order/snapshot")
public class TOrderSnapshotController extends BaseController {

    private final ITOrderSnapshotService iTOrderSnapshotService;

    /**
     * 查询订单接口数据快照列表
     */
    @ApiOperation("查询订单接口数据快照列表")
    @SaCheckPermission("order:snapshot:list")
    @GetMapping("/list")
    public TableDataInfo<TOrderSnapshotVo> list(@Validated TOrderSnapshotQueryBo bo) {
        return iTOrderSnapshotService.queryPageList(bo);
    }

    /**
     * 导出订单接口数据快照列表
     */
    @ApiOperation("导出订单接口数据快照列表")
    @SaCheckPermission("order:snapshot:export")
    @Log(title = "订单接口数据快照", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(@Validated TOrderSnapshotQueryBo bo, HttpServletResponse response) throws IOException {
        List<TOrderSnapshotVo> list = iTOrderSnapshotService.queryList(bo);
        ExcelUtil<TOrderSnapshotVo> util = new ExcelUtil<TOrderSnapshotVo>(TOrderSnapshotVo.class);
        util.exportExcel(response, list, "订单接口数据快照");
    }

    /**
     * 获取订单接口数据快照详细信息
     */
    @ApiOperation("获取订单接口数据快照详细信息")
    @SaCheckPermission("order:snapshot:query")
    @GetMapping("/{orderId}")
    public R<TOrderSnapshotVo> getInfo(@NotNull(message = "主键不能为空")
                                       @PathVariable("orderId") String orderId) {
        return R.ok(iTOrderSnapshotService.queryById(orderId));
    }

    /**
     * 新增订单接口数据快照
     */
    @ApiOperation("新增订单接口数据快照")
    @SaCheckPermission("order:snapshot:add")
    @Log(title = "订单接口数据快照", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Integer> add(@Validated @RequestBody TOrderSnapshotEditBo bo) {
        return R.ok(iTOrderSnapshotService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改订单接口数据快照
     */
    @ApiOperation("修改订单接口数据快照")
    @SaCheckPermission("order:snapshot:edit")
    @Log(title = "订单接口数据快照", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Integer> edit(@Validated @RequestBody TOrderSnapshotEditBo bo) {
        return R.ok(iTOrderSnapshotService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除订单接口数据快照
     */
    @ApiOperation("删除订单接口数据快照")
    @SaCheckPermission("order:snapshot:remove")
    @Log(title = "订单接口数据快照", businessType = BusinessType.DELETE)
    @DeleteMapping("/{orderIds}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                             @PathVariable String[] orderIds) {
        return R.ok(iTOrderSnapshotService.deleteWithValidByIds(Arrays.asList(orderIds), true) ? 1 : 0);
    }
}
