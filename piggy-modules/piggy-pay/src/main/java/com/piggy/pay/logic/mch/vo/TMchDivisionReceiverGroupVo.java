package com.piggy.pay.logic.mch.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 分账账号组视图对象 t_mch_division_receiver_group
 *
 * @author piggy
 * @date 2023-05-25
 */
@Data
@Accessors(chain = true)
@ApiModel("分账账号组视图对象")
public class TMchDivisionReceiverGroupVo {

	private static final long serialVersionUID = 1L;

	/** 组ID */
	@Excel(name = "组ID")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("组ID")
	private Long receiverGroupId;

	/** 组名称 */
	@Excel(name = "组名称")
	@ApiModelProperty("组名称")
	private String receiverGroupName;

	/** 商户号 */
	@Excel(name = "商户号")
	@ApiModelProperty("商户号")
	private String mchNo;

	/** 自动分账组（当订单分账模式为自动分账，改组将完成分账逻辑） 0-否 1-是 */
	@Excel(name = "自动分账组" , readConverterExp = "当=订单分账模式为自动分账，改组将完成分账逻辑")
	@ApiModelProperty("自动分账组（当订单分账模式为自动分账，改组将完成分账逻辑） 0-否 1-是")
	private Integer autoDivisionFlag;

	/** 创建者用户ID */
	@Excel(name = "创建者用户ID")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@ApiModelProperty("创建者用户ID")
	private Long createdUid;

	/** 创建者姓名 */
	@Excel(name = "创建者姓名")
	@ApiModelProperty("创建者姓名")
	private String createdBy;

	/** 创建时间 */
	@Excel(name = "创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createdAt;

	/** 更新时间 */
	@Excel(name = "更新时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("更新时间")
	private Date updatedAt;

	/** 更新人 */
	@Excel(name = "更新人")
	@ApiModelProperty("更新人")
	private String updateBy;


}
