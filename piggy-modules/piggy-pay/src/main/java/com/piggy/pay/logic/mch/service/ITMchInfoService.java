package com.piggy.pay.logic.mch.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.pay.logic.mch.domain.TMchInfo;
import com.piggy.pay.logic.mch.vo.TMchInfoVo;
import com.piggy.pay.logic.mch.bo.TMchInfoQueryBo;
import com.piggy.pay.logic.mch.bo.TMchInfoEditBo;

import java.util.Collection;
import java.util.List;

/**
 * 商户信息Service接口
 *
 * @author piggy
 * @date 2023-05-25
 */
public interface ITMchInfoService extends IServicePlus<TMchInfo> {
    /**
     * 查询单个
     *
     * @return
     */
    TMchInfoVo queryById(String mchNo);

    /**
     * 查询列表
     */
    TableDataInfo<TMchInfoVo> queryPageList(TMchInfoQueryBo bo);

    /**
     * 查询列表
     */
    List<TMchInfoVo> queryList(TMchInfoQueryBo bo);

    /**
     * 根据新增业务对象插入商户信息
     *
     * @param bo 商户信息新增业务对象
     * @return
     */
    Boolean insertByAddBo(TMchInfoEditBo bo);

    /**
     * 根据编辑业务对象修改商户信息
     *
     * @param bo 商户信息编辑业务对象
     * @return
     */
    Boolean updateByEditBo(TMchInfoEditBo bo);

    /**
     * 校验并删除数据
     *
     * @param ids     主键集合
     * @param isValid 是否校验,true-删除前校验,false-不校验
     * @return
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);
}
