package com.piggy.sys.domain.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 消息模板分页查询对象 sys_msg_receiver
 *
 * @author shark
 * @date 2021-06-23
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("消息模板分页查询对象")
public class SysMsgReceiverQueryBo extends BaseEntity {

	/** 消息id */
	@ApiModelProperty("消息id")
	private Long msgId;
	/** 接收人用户id */
	@ApiModelProperty("接收人用户id")
	private Long userId;
	/** 手机或邮箱 */
	@ApiModelProperty("手机或邮箱")
	private String link;

}
