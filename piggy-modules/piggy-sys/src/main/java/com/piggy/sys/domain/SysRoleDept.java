package com.piggy.sys.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 角色和部门关联 sys_role_dept
 *
 * @author shark
 */

@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("sys_role_dept")
public class SysRoleDept {
    /**
     * 角色ID
     */
    @ApiModelProperty("角色ID")
    private Long roleId;

    /**
     * 部门ID
     */
    @ApiModelProperty("部门ID")
    private Long deptId;

}
