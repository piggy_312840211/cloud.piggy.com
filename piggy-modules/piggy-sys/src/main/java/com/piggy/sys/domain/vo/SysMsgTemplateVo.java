package com.piggy.sys.domain.vo;

import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 消息模板视图对象 sys_msg_template
 *
 * @author shark
 * @date 2021-06-23
 */
@Data
@ApiModel("消息模板视图对象")
public class SysMsgTemplateVo {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	@ApiModelProperty("主键")
	private Long id;

	/** 消息类型 0-站内信 1-邮件 2-短信 */
	@Excel(name = "消息类型 0-站内信 1-邮件 2-短信")
	@ApiModelProperty("消息类型 0-站内信 1-邮件 2-短信")
	private Integer type;

	/** 消息内容模板 */
	@Excel(name = "消息内容模板")
	@ApiModelProperty("消息内容模板")
	private String content;

	/** 变量(jsonStr) */
	@Excel(name = "变量(jsonStr)")
	@ApiModelProperty("变量(jsonStr)")
	private String param;

	/** 发送人 */
	@Excel(name = "发送人")
	@ApiModelProperty("发送人")
	private String sender;

	/** 主题 */
	@Excel(name = "subject")
	@ApiModelProperty("主题")
	private String subject;

	/** key */
	@Excel(name = "msgKey")
	@ApiModelProperty("msgKey")
	private String msgKey;

	/** secret */
	@Excel(name = "msgSecret")
	@ApiModelProperty("msgSecret")
	private String msgSecret;


}
