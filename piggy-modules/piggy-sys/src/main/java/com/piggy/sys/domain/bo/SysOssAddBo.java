package com.piggy.sys.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


/**
 * 文件上传添加对象 sys_oss
 *
 * @author piggy
 * @date 2021-07-20
 */
@Data
@ApiModel("文件上传添加对象")
public class SysOssAddBo {


    /** URL地址 */
    @ApiModelProperty("URL地址")
    private String url;

    /** 状态 */
    @ApiModelProperty("状态")
    @NotBlank(message = "状态不能为空")
    private String status;
}
