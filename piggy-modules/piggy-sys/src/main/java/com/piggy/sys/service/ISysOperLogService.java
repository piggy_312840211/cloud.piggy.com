package com.piggy.sys.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.sys.api.domain.SysOperLog;

import java.util.List;

/**
 * 操作日志 服务层
 *
 * @author shark
 */
public interface ISysOperLogService extends IServicePlus<SysOperLog> {

    TableDataInfo<SysOperLog> selectPageOperLogList(SysOperLog operLog);

    /**
     * 新增操作日志
     *
     * @param operLog 操作日志对象
     */
    public boolean insertOperlog(SysOperLog operLog);

    /**
     * 查询系统操作日志集合
     *
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    public List<SysOperLog> selectOperLogList(SysOperLog operLog);

    /**
     * 批量删除系统操作日志
     *
     * @param operIds 需要删除的操作日志ID
     * @return 结果
     */
    public int deleteOperLogByIds(Long[] operIds);

    /**
     * 查询操作日志详细
     *
     * @param operId 操作ID
     * @return 操作日志对象
     */
    public SysOperLog selectOperLogById(Long operId);

    /**
     * 清空操作日志
     */
    public void cleanOperLog();
}
