package com.piggy.sys.domain.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
//@TableName("sys_mail")
public class SysMsgContentBo {

	@ApiModelProperty(value = "模板id",required = false)
	private Long templateId;

	@ApiModelProperty(value = "接收人",required = true)
	private List<Long> userIds;

	@ApiModelProperty(value = "内容",required = false)
	private String content;

}
