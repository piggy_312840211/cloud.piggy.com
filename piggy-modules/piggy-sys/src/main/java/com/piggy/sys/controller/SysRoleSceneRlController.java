package com.piggy.sys.controller;

import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.satoken.annotation.Inner;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.domain.SysRoleSceneRl;
import com.piggy.sys.api.bo.SysRoleSceneRlQueryBo;
import com.piggy.sys.domain.vo.SysRoleSceneRlVo;
import com.piggy.sys.service.ISysRoleSceneRlService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/roleSceneRl")
public class SysRoleSceneRlController extends BaseController {
    @Resource
    private ISysRoleSceneRlService service;

    @Inner
    @GetMapping("/getSceneRole")
    public R<List<SysRoleSceneRlVo>> getSceneRole(Long id){
        return success(service.getSceneRole(id));
    }

    //@Inner
    @PostMapping("/addOrUpdateRole/{sceneId}")
    public R addOrUpdateRole(@RequestBody List<SysRoleSceneRl> roleSceneRls, @PathVariable("sceneId") Long sceneId){
        return success(service.addOrUpdateRole(roleSceneRls,sceneId));
    }

    @Inner
    @GetMapping("/activation")
    public int activation(Long sceneId){
        return service.activation(sceneId);
    }

    @Inner
    @PostMapping("/queryList")
    public R<List<SysRoleSceneRlVo>> queryList(@RequestBody SysRoleSceneRlQueryBo bo) {
        return R.ok(service.queryList(bo));
    }

}
