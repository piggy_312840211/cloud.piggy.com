package com.piggy.sys.service;

import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.sys.api.domain.SysDept;
import com.piggy.sys.domain.vo.TreeSelect;

import java.util.List;

/**
 * 部门管理 服务层
 *
 * @author shark
 */
public interface ISysDeptService extends IServicePlus<SysDept> {
    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    public List<SysDept> selectDeptList(SysDept dept);

    /**
     * 构建前端所需要树结构
     *
     * @param depts 部门列表
     * @return 树结构列表
     */
    public List<SysDept> buildDeptTree(List<SysDept> depts);

    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    public List<TreeSelect> buildDeptTreeSelect(List<SysDept> depts);

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId 角色ID
     * @return 选中部门列表
     */
    public List<Integer> selectDeptListByRoleId(Long roleId);

    /**
     * 根据部门ID查询信息
     *
     * @param deptId 部门ID
     * @return 部门信息
     */
    public SysDept selectDeptById(Long deptId);

    /**
     * 获取部门列表(如供应商、经销商、资方列表)根据企业类型
     *
     * @param busineseType 企业类型 (0核心企业1供应商2经销商3金融机构)
     * @return 部门列表
     */
    List<SysDept> getDeptListByOrgType(String sceneKey,Integer busineseType);

    /**
     * 根据ID查询所有子部门（正常状态）
     *
     * @param deptId 部门ID
     * @return 子部门数
     */
    public int selectNormalChildrenDeptById(Long deptId);

	/**
	 * 根据用户所在部门，获取该用户所属组织类型
	 **/
    Integer getOrgType(Long deptId,String sceneKey);

	/**
	 * 获取用户所属组织的部门ID(如车商部门ID、资方部门ID、中外运部门ID、平台部门ID)
	 */
	 Long getOrgDeptId(Long deptId);

    /**
     * 是否存在部门子节点
     *
     * @param deptId 部门ID
     * @return 结果
     */
    public boolean hasChildByDeptId(Long deptId);

    /**
     * 查询部门是否存在用户
     *
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    public boolean checkDeptExistUser(Long deptId);

    /**
     * 校验部门名称是否唯一
     *
     * @param dept 部门信息
     * @return 结果
     */
    public String checkDeptNameUnique(SysDept dept);

    /**
     * 新增保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    public int insertDept(SysDept dept);

    /**
     * 修改保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    public int updateDept(SysDept dept);

    /**
     * 删除部门管理信息
     *
     * @param deptId 部门ID
     * @return 结果
     */
    public int deleteDeptById(Long deptId);


    /**
     * 查询所有部门
     * @return
     */
    List<SysDept> queryDeptAll();
}
