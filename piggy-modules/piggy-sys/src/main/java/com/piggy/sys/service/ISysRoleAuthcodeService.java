package com.piggy.sys.service;

import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.sys.api.bo.SysRoleAuthcodeAddBo;
import com.piggy.sys.api.domain.SysRoleAuthcode;
import com.piggy.sys.api.bo.SysRoleAuthcodeEditBo;
import com.piggy.sys.domain.bo.SysRoleAuthcodeQueryBo;
import com.piggy.sys.domain.vo.SysRoleAuthcodeVo;

import java.util.Collection;
import java.util.List;

/**
 * 角色授权码Service接口
 *
 * @author shark
 * @date 2021-09-14
 */
public interface ISysRoleAuthcodeService extends IServicePlus<SysRoleAuthcode> {
	/**
	 * 查询单个
	 * @return
	 */
	SysRoleAuthcodeVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<SysRoleAuthcodeVo> queryPageList(SysRoleAuthcodeQueryBo bo);

	/**
	 * 查询列表
	 */
	List<SysRoleAuthcodeVo> queryList(SysRoleAuthcodeQueryBo bo);

	/**
	 * 获取授权码列表
	 */
	List<SysRoleAuthcodeVo> getAutoCodeList(String authCode);

	/**
	 * 根据新增业务对象插入角色授权码
	 * @param bo 角色授权码新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(SysRoleAuthcodeAddBo bo);

	/**
	 * 根据新增业务对象插入角色授权码
	 * @param bo 角色授权码新增业务对象
	 * @return
	 */
	Boolean sceneAddByAddBo(SysRoleAuthcodeAddBo bo);

	/**
	 * 根据编辑业务对象修改角色授权码
	 * @param bo 角色授权码编辑业务对象
	 * @return
	 */
	Boolean updateByEditBo(SysRoleAuthcodeEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
