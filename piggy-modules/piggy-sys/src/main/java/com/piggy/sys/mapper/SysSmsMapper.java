package com.piggy.sys.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.sys.domain.SysSms;

/**
 * 消息模板Mapper接口
 *
 * @author shark
 * @date 2021-08-16
 */
public interface SysSmsMapper extends BaseMapperPlus<SysSms> {

}
