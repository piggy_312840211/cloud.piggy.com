package com.piggy.sys.service;

import com.piggy.common.core.domain.R;
import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.sys.api.bo.OssFileUploadBo;
import com.piggy.sys.api.domain.SysOss;
import com.piggy.sys.api.vo.SysOssVo;
import com.piggy.sys.domain.bo.SysOssAddBo;
import com.piggy.sys.domain.bo.SysOssEditBo;
import com.piggy.sys.domain.bo.SysOssQueryBo;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.List;

/**
 * 文件上传Service接口
 *
 * @author piggy
 * @date 2021-07-20
 */
public interface ISysOssService extends IServicePlus<SysOss> {
	/**
	 * 查询单个
	 * @return
	 */
	SysOssVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<SysOssVo> queryPageList(SysOssQueryBo bo);

	/**
	 * 查询列表
	 */
	List<SysOssVo> queryList(SysOssQueryBo bo);

	/**
	 * 根据新增业务对象插入文件上传
	 * @param bo 文件上传新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(SysOssAddBo bo);

	/**
	 * 根据编辑业务对象修改文件上传
	 * @param bo 文件上传编辑业务对象
	 * @return
	 */
	Boolean updateByEditBo(SysOssEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	/**
	 * 上传文件
	 * @param file
	 * @return SysOss对象
	 *
	 */
	SysOss uploadFileToCLoud(MultipartFile file);

	/**
	 * 上传文件
	 * @param file
	 * @param path 上传指定文件夹
	 * @return SysOss对象
	 *
	 */
	SysOss uploadFileToCLoud(MultipartFile file, String path);

	void delCloudFile(String objectName);

	R<SysOssVo> getSysOssById(Long id);

	SysOss uploadToCloud(OssFileUploadBo bo);
}
