package com.piggy.sys.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 消息模板编辑对象 sys_msg_template
 *
 * @author shark
 * @date 2021-06-23
 */
@Data
@ApiModel("消息模板编辑对象")
public class SysMsgTemplateEditBo {


    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 消息类型 0-站内信 1-邮件 2-短信 */
    @ApiModelProperty("消息类型 0-站内信 1-邮件 2-短信")
    private Integer type;

    /** 消息内容模板 */
    @ApiModelProperty("消息内容模板")
    private String content;

    /** 变量(jsonStr) */
    @ApiModelProperty("变量(jsonStr)")
    private String param;

	/** 主题 */
	@ApiModelProperty("主题")
	private String subject;

    /** 发送人 */
    @ApiModelProperty("发送人")
    private String sender;

    /** key */
    @ApiModelProperty("key")
    private String msgKey;

    /** secret */
    @ApiModelProperty("secret")
    private String msgSecret;
}
