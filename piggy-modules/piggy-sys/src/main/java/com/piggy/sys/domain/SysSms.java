package com.piggy.sys.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 消息模板对象 sys_sms
 *
 * @author shark
 * @date 2021-08-16
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("sys_sms")
public class SysSms implements Serializable {

    private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "id")
    private Long id;

    /** 主题 */
    private String subject;

    /** 参数内容 */
    private String params;

    /** 接收人用户id */
    private Long userId;

    /** 模板id */
    private String templateId;

    /** 签名 */
    private String signName;

    /** 手机或邮箱 */
    private String link;

    /** 发送返回结果 */
    private String sendResult;

    /** 状态 */
    private Integer status;

    /** 消息类型 */
    private Integer type;

    /** AccessKeyID */
    private String accessKeyId;

    /** AccessKeySecret */
    private String accessKeySecret;

    /** 创建者 */
    @TableField(fill = FieldFill.INSERT)
    private Long createBy;

    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 备注 */
    private String remark;

}
