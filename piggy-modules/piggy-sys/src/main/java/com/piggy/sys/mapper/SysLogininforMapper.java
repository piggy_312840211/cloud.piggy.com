package com.piggy.sys.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.sys.api.domain.SysLogininfor;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author shark
 */
public interface SysLogininforMapper extends BaseMapperPlus<SysLogininfor> {

}
