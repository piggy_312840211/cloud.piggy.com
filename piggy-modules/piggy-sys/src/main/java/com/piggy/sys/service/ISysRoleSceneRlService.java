package com.piggy.sys.service;

import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.sys.api.domain.SysRoleSceneRl;
import com.piggy.sys.domain.bo.SysRoleSceneRlAddBo;
import com.piggy.sys.domain.bo.SysRoleSceneRlEditBo;
import com.piggy.sys.api.bo.SysRoleSceneRlQueryBo;
import com.piggy.sys.domain.vo.SysRoleSceneRlVo;

import java.util.Collection;
import java.util.List;

/**
 * 场景角色Service接口
 *
 * @author shark
 * @date 2021-10-15
 */
public interface ISysRoleSceneRlService extends IServicePlus<SysRoleSceneRl> {
	/**
	 * 查询单个
	 * @return
	 */
	SysRoleSceneRlVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<SysRoleSceneRlVo> queryPageList(SysRoleSceneRlQueryBo bo);

	/**
	 * 查询列表
	 */
	List<SysRoleSceneRlVo> queryList(SysRoleSceneRlQueryBo bo);

	/**
	 * 根据新增业务对象插入场景角色
	 * @param bo 场景角色新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(SysRoleSceneRlAddBo bo);

	/**
	 * 根据编辑业务对象修改场景角色
	 * @param bo 场景角色编辑业务对象
	 * @return
	 */
	Boolean updateByEditBo(SysRoleSceneRlEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	/**
	 * 根据场景id获取角色列表
	 * @param id
	 * @return
	 */
	List<SysRoleSceneRlVo>  getSceneRole(Long id);

	Integer addOrUpdateRole(List<SysRoleSceneRl> roleSceneRls, Long sceneId);

	int activation(Long sceneId);
}
