package com.piggy.sys.domain.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 终端配置场景部门分页查询对象 sys_client_scene
 *
 * @author piggy
 * @date 2022-09-19
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("终端配置场景部门分页查询对象")
public class SysClientSceneQueryBo extends BaseEntity {

	/** 终端编号 */
	@ApiModelProperty("终端编号")
	private String clientId;
	/** 场景id */
	@ApiModelProperty("场景id")
	private Long sceneId;
	/** 核心企业部门id */
	@ApiModelProperty("核心企业部门id")
	private Long coreDeptId;
	/** 是否停用 1 启用 0 停用 */
	@ApiModelProperty("是否停用 1 启用 0 停用")
	private Integer validFlag;

}
