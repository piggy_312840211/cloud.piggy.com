package com.piggy.sys.mapper;

import com.piggy.sys.api.domain.SysDictType;
import com.piggy.common.core.web.page.BaseMapperPlus;

/**
 * 字典表 数据层
 *
 * @author shark
 */
public interface SysDictTypeMapper extends BaseMapperPlus<SysDictType> {

}
