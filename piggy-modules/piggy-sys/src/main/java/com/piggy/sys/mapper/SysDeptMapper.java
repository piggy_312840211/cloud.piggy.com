package com.piggy.sys.mapper;
import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.sys.api.domain.SysDept;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 部门管理 数据层
 *
 * @author shark
 */
public interface SysDeptMapper extends BaseMapperPlus<SysDept> {

    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    public List<SysDept> selectDeptList(SysDept dept);

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId            角色ID
     * @param deptCheckStrictly 部门树选择项是否关联显示
     * @return 选中部门列表
     */
    public List<Integer> selectDeptListByRoleId(@Param("roleId") Long roleId, @Param("deptCheckStrictly") boolean deptCheckStrictly);

	/**
	 * 修改子元素关系
	 *
	 * @param depts 子元素
	 * @return 结果
	 */
	public int updateDeptChildren(@Param("depts") List<SysDept> depts);

	/**
	 * 根据场景字符获取场景id
	 */
	public Long getSceneIdBySceneKey(@Param("sceneKey") String sceneKey);

	/**
	 * 根据组织部门id获取企业id
	 */
	public Long getBusinessIdByDeptId(@Param("deptId") Long deptId);

	/**
	 * 获取企业场景对应组织类型
	 */
	public Integer getBusineseType(@Param("sceneKey") String sceneKey,@Param("busineseId") Long busineseId);

	/**
	 * 获取企业部门ids(根据企业场景和企业类型)
	 */
	public List<Long> getDeptIds(@Param("sceneKey") String sceneKey,@Param("busineseType") Integer busineseType);

}
