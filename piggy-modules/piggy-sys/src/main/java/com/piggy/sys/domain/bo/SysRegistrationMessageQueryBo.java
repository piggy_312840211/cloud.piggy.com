package com.piggy.sys.domain.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 注册邀请短信详情分页查询对象 tdms_registration_message
 *
 * @author piggy
 * @date 2021-10-11
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("注册邀请短信详情分页查询对象")
public class SysRegistrationMessageQueryBo extends BaseEntity {

	/** 邀请方机构ID */
	@ApiModelProperty("邀请方机构ID")
	private Long deptIdFrom;

}
