package com.piggy.sys.domain.vo;

import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 消息模板视图对象 sys_msg
 *
 * @author shark
 * @date 2021-06-23
 */
@Data
@ApiModel("消息模板视图对象")
public class SysMsgVo {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	@ApiModelProperty("主键")
	private Long id;

	/** 消息类型(冗余方便查询) 0-站内信 1-邮件 2-短信 */
	@Excel(name = "消息类型(冗余方便查询) 0-站内信 1-邮件 2-短信")
	@ApiModelProperty("消息类型(冗余方便查询) 0-站内信 1-邮件 2-短信")
	private Integer type;

	/** 模板id */
	@Excel(name = "模板id")
	@ApiModelProperty("模板id")
	private Long templateId;

	/** 主题 */
	@Excel(name = "主题")
	@ApiModelProperty("主题")
	private String subject;

	/** 内容 */
	@Excel(name = "内容")
	@ApiModelProperty("内容")
	private String params;


}
