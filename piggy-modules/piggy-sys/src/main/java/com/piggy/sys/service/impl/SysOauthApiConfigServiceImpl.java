package com.piggy.sys.service.impl;
import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.piggy.sys.api.domain.SysOauthApiConfig;
import com.piggy.sys.api.vo.SysOauthApiConfigVo;
import com.piggy.sys.mapper.SysClientDetailsMapper;
import com.piggy.sys.mapper.SysOauthApiConfigMapper;
import com.piggy.sys.service.ISysOauthApiConfigService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

@Service
public class SysOauthApiConfigServiceImpl  implements ISysOauthApiConfigService
{
    @Resource
    private SysOauthApiConfigMapper sysOauthApiConfigMapper;
    @Resource
    private SysClientDetailsMapper  sysClientDetailsMapper;

    private  final  String  clientId="625729664010158080";
    @Override
    public SysOauthApiConfigVo getApiConfigInfo()
    {
        QueryWrapper<SysOauthApiConfig>  queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SysOauthApiConfig::getClientId,clientId);
        return BeanUtil.toBean(sysOauthApiConfigMapper.selectOne(queryWrapper),SysOauthApiConfigVo.class);
    }

    @Override
    public void updateApiConfig(String callbackUrl)
    {

        sysOauthApiConfigMapper.updateByCondition (clientId,callbackUrl);
    }
}
