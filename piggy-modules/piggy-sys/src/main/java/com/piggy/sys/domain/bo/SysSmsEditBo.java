package com.piggy.sys.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 消息模板编辑对象 sys_sms
 *
 * @author shark
 * @date 2021-08-16
 */
@Data
@ApiModel("消息模板编辑对象")
public class SysSmsEditBo {


    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;
}
