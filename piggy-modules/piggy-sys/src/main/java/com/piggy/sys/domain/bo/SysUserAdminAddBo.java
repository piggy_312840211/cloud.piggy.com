package com.piggy.sys.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 角色授权码添加对象
 *
 * @author shark
 * @date 2021-09-14
 */
@Data
@ApiModel("角色授权码添加对象")
public class SysUserAdminAddBo {


    /** 手机号 */
    @ApiModelProperty("手机号")
	@NotBlank(message = "手机号不能为空")
    private String phone;


	/** 密码 */
	@ApiModelProperty("密码")
	@NotBlank(message = "密码不能为空")
	private String password;

	/** 短信验证码 */
	@ApiModelProperty("短信验证码")
	@NotBlank(message = "短信验证码不能为空")
	private String smsCode;

    /** 授权码 */
    @ApiModelProperty("授权码")
	@NotBlank(message = "授权码不能为空")
    private String authCode;





}
