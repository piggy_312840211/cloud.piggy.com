package com.piggy.sys.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.sys.domain.SysNotice;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 通知公告表 数据层
 *
 * @author shark
 */
public interface SysNoticeMapper extends BaseMapperPlus<SysNotice> {


	List<SysNotice> selectListNotice(@Param("deptId") Long deptId, @Param("title") String title);
}
