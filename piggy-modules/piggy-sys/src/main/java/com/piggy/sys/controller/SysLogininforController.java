package com.piggy.sys.controller;

import java.io.IOException;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.lang.Console;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.piggy.common.core.utils.ip.AddressUtils;
import com.piggy.common.satoken.annotation.Inner;
import com.piggy.common.core.domain.R;
import org.springframework.web.bind.annotation.*;
import com.piggy.common.core.utils.ServletUtils;
import com.piggy.common.core.utils.ip.IpUtils;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;

import com.piggy.sys.api.domain.SysLogininfor;
import com.piggy.sys.service.ISysLogininforService;

/**
 * 系统访问记录
 *
 * @author shark
 */
@RestController
@RequestMapping("/logininfor")
public class SysLogininforController extends BaseController {
    @Resource
    private ISysLogininforService logininforService;

    @SaCheckPermission("system:logininfor:list")
    @GetMapping("/list")
    public TableDataInfo list(SysLogininfor logininfor) {
        startPage();
        List<SysLogininfor> list = logininforService.selectLogininforList(logininfor);
        return getDataTable(list);
    }

    @Log(title = "登录日志", businessType = BusinessType.EXPORT)
    @SaCheckPermission("system:logininfor:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysLogininfor logininfor) throws IOException {
        List<SysLogininfor> list = logininforService.selectLogininforList(logininfor);
        ExcelUtil<SysLogininfor> util = new ExcelUtil<SysLogininfor>(SysLogininfor.class);
        util.exportExcel(response, list, "登录日志");
    }

    @SaCheckPermission("system:logininfor:remove")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{infoIds}")
    public R remove(@PathVariable Long[] infoIds) {
        return toAjax(logininforService.deleteLogininforByIds(infoIds));
    }

    @SaCheckPermission("system:logininfor:remove")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/clean")
    public R clean() {
        logininforService.cleanLogininfor();
        return R.ok();
    }

    @Inner
    @PostMapping
    public R<Boolean> add(@RequestBody SysLogininfor sysLogininfor) {
//        String ip = IpUtils.getIpAddr(ServletUtils.getRequest());

        // 封装对象
//        SysLogininfor logininfor = new SysLogininfor();
//        logininfor.setUserName(username);
//        logininfor.setIpaddr(ip);
//        logininfor.setMsg(message);
//        // 日志状态
//        if (Constants.LOGIN_SUCCESS.equals(status) || Constants.LOGOUT.equals(status)) {
//            logininfor.setStatus("0");
//        } else if (Constants.LOGIN_FAIL.equals(status)) {
//            logininfor.setStatus("1");
//        }
        UserAgent userAgent = UserAgentUtil.parse(ServletUtils.getRequest().getHeader("User-Agent"));
        String ip = IpUtils.getIpAddr(ServletUtils.getRequest());
        // 获取客户端操作系统
        String os = userAgent.getOs().getName();
        // 获取客户端浏览器
        String browser = userAgent.getBrowser().getName();
        String address = AddressUtils.getRealAddressByIP(ip);
        sysLogininfor.setLoginLocation(address);
        sysLogininfor.setBrowser(browser);
        sysLogininfor.setOs(os);

        StringBuilder s = new StringBuilder();
        s.append(getBlock(ip));
        s.append(address);
        s.append(getBlock(sysLogininfor.getUserName()));
        s.append(getBlock(sysLogininfor.getMsg()));
        s.append(getBlock(sysLogininfor.getStatus()));
        // 打印信息到日志
        Console.log(s.toString(), sysLogininfor.getMsg());

        return R.ok(logininforService.insertLogininfor(sysLogininfor));
    }


    public static String getBlock(Object msg)
    {
        if (msg == null)
        {
            msg = "";
        }
        return "[" + msg.toString() + "]";
    }
}
