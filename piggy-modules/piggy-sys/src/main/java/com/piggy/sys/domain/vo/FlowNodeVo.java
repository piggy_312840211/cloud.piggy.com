package com.piggy.sys.domain.vo;

import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 流程节点视图对象 flow_node
 *
 * @author piggy
 * @date 2021-06-16
 */
@Data
@ApiModel("流程节点视图对象")
public class FlowNodeVo {

	private static final long serialVersionUID = 1L;

	/** $pkColumn.columnComment */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private String nodeName;

	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private String nodeKey;

	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private Integer type;

	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private Long menuId;


}
