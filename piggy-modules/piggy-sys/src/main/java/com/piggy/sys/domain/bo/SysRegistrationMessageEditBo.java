package com.piggy.sys.domain.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


/**
 * 注册邀请短信详情编辑对象 tdms_registration_message
 *
 * @author piggy
 * @date 2021-10-11
 */
@Data
@ApiModel("注册邀请短信详情编辑对象")
public class SysRegistrationMessageEditBo {


    /** 邀请方机构ID */
    @ApiModelProperty("邀请方机构ID")
    private Long deptIdFrom;

    /** ID */
    @ApiModelProperty("ID")
    private Long id;

    /** 更新者 */
    @ApiModelProperty("更新者")
    private String updateBy;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
