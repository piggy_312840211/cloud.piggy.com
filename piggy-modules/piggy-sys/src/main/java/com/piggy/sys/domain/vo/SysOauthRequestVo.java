package com.piggy.sys.domain.vo;

import com.piggy.common.core.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 终端授权申请视图对象 sys_oauth_request
 *
 * @author piggy
 * @date 2022-09-21
 */
@Data
@ApiModel("终端授权申请视图对象")
public class SysOauthRequestVo {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	@ApiModelProperty("主键")
	private Long id;

	/** 核心企业id */
	@Excel(name = "核心企业id")
	@ApiModelProperty("核心企业id")
	private Long coreBusinessId;

	/** 申请状态 0 待审核 1 审核中 2 审核完成 3 审核失败 */
	@Excel(name = "申请状态 0 待审核 1 审核中 2 审核完成 3 审核失败")
	@ApiModelProperty("申请状态 0 待审核 1 审核中 2 审核完成 3 审核失败")
	private Integer requestStatus;

	/** 审核失败原因 */
	@Excel(name = "审核失败原因")
	@ApiModelProperty("审核失败原因")
	private String reason;

	/** 删除状态：0有效，1删除 */
	@Excel(name = "删除状态：0有效，1删除")
	@ApiModelProperty("删除状态：0有效，1删除")
	private Integer delFlag;

	/** 删除人 */
	@Excel(name = "删除人")
	@ApiModelProperty("删除人")
	private String deleter;

	/** 申请时间 */
	@Excel(name = "申请时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("申请时间")
	private Date requestTime;

	/** 审核时间 */
	@Excel(name = "审核时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("审核时间")
	private Date auditTime;

	/** 审核人 */
	@Excel(name = "审核人")
	@ApiModelProperty("审核人")
	private String auditor;

	/** 提交人 */
	@Excel(name = "提交人")
	@ApiModelProperty("提交人")
	private String createBy;


}
