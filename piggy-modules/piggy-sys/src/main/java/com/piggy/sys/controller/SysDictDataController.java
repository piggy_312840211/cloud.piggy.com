package com.piggy.sys.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.lang.Validator;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import com.piggy.common.satoken.annotation.Inner;
import com.piggy.common.satoken.utils.SecurityUtils;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.domain.SysDictData;
import com.piggy.sys.service.ISysDictDataService;
import com.piggy.sys.service.ISysDictTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 数据字典信息
 *
 * @author piggy
 */
@Api(value="数据字典信息",tags = {"数据字典信息"})
@RestController
@RequestMapping("/dict/data")
public class SysDictDataController extends BaseController
{
    @Resource
    private ISysDictDataService dictDataService;

    @Resource
    private ISysDictTypeService dictTypeService;

    //@SaCheckPermission("system:dict:list")
    @GetMapping("/list")
    public TableDataInfo list(SysDictData dictData)
    {
        return dictDataService.selectPageDictDataList(dictData);
    }

	/**
	 * 获取字典标签列表(dictType必输，listClass、dictKeyList选输)
	 */
	@ApiOperation("获取字典标签列表")
	@PostMapping("/getDictList")
	public R getDictList(@RequestBody SysDictData dictData)
	{
		return R.ok(dictDataService.getDictList(dictData));
	}

    @Log(title = "字典数据", businessType = BusinessType.EXPORT)
    @SaCheckPermission("system:dict:export")
    @GetMapping("/export")
    public void export(HttpServletResponse response, SysDictData dictData) throws IOException {
        List<SysDictData> list = dictDataService.selectDictDataList(dictData);
        ExcelUtil<SysDictData> util = new ExcelUtil<SysDictData>(SysDictData.class);
        util.exportExcel(response,list, "字典数据");
    }

    /**
     * 查询字典数据详细
     */
    @ApiOperation("查询字典数据详细")
    @SaCheckPermission("system:dict:query")
    @GetMapping(value = "/{dictCode}")
    public R getInfo( @ApiParam(name = "dictCode", value = "字典ID", required = true)@PathVariable Long dictCode)
    {
        return R.ok(dictDataService.selectDictDataById(dictCode));
    }

	/**
	 * 查询某个字典value
	 */
	@ApiOperation("查询某个字典value")
	@SaCheckPermission("system:dict:getDictValue")
	@GetMapping(value = "/getDictValue")
	public R getDictLabel( @ApiParam(name = "dictType", value = "字典类型", required = true)@PathVariable String dictType,@ApiParam(name = "dictValue", value = "字典key", required = true)@PathVariable String dictValue)
	{
		return R.ok(dictDataService.selectDictLabel(dictType,dictValue));
	}

    /**
     * 根据字典类型查询字典数据信息
     */
    @ApiOperation("根据字典类型查询字典数据信息")
    @GetMapping(value = "/type/{dictType}")
    public R dictType(@PathVariable String dictType)
    {
        List<SysDictData> data = dictTypeService.selectDictDataByType(dictType);
        if (Validator.isNull(data))
        {
            data = new ArrayList<SysDictData>();
        }
        return R.ok(data);
    }

    /**
     * 新增字典类型
     */
    @ApiOperation("新增字典类型")
    @SaCheckPermission("system:dict:add")
    @Log(title = "字典数据", businessType = BusinessType.INSERT)
    @PostMapping
    public R add(@Validated @RequestBody SysDictData dict)
    {
        dict.setCreateBy(SecurityUtils.getUsername());
        return toAjax(dictDataService.insertDictData(dict));
    }

    /**
     * 修改保存字典类型
     */
    @ApiOperation("修改保存字典类型")
    @SaCheckPermission("system:dict:edit")
    @Log(title = "字典数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public R edit(@Validated @RequestBody SysDictData dict)
    {
        dict.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(dictDataService.updateDictData(dict));
    }

    /**
     * 删除字典类型
     */
    @ApiOperation("删除字典类型")
    @SaCheckPermission("system:dict:remove")
    @Log(title = "字典类型", businessType = BusinessType.DELETE)
    @DeleteMapping("/{dictCodes}")
    public R remove( @ApiParam(name = "dictCodes", value = "字典ID", required = true)@PathVariable Long[] dictCodes)
    {
        dictDataService.deleteDictDataByIds(dictCodes);
        return success();
    }

    @Inner
    @PostMapping(value = "/getDictType")
    public R<List<SysDictData>> getDictType(@RequestParam("dictType")  String dictType)
    {
        List<SysDictData> data = dictTypeService.selectDictDataByType(dictType);
        if (Validator.isNull(data))
        {
            data = new ArrayList<SysDictData>();
        }
        return R.ok(data);
    }
}
