
package com.piggy.sys.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author liyc
 * @date 2021/11/15 场景下组织类型
 */
@Getter
@AllArgsConstructor
public enum BusineseTypeEnum {
	CORE(0, "核心企业"),
	SUPPELIER(1, "供应商"),
	JXS(2, "经销商"),
	BANK(3, "金融机构"),
	OTHER(4, "其它");

	/**
	 * 类型
	 */
	private final Integer code;

	/**
	 * 描述
	 */
	private final String info;

}
