package com.piggy.sys.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.*;


/**
 * 终端配置场景部门编辑对象 sys_client_scene
 *
 * @author piggy
 * @date 2022-09-19
 */
@Data
@ApiModel("终端配置场景部门编辑对象")
public class SysClientSceneEditBo {


    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 终端编号 */
    @ApiModelProperty("终端编号")
    @NotBlank(message = "终端编号不能为空")
    private String clientId;

    /** 场景id */
    @ApiModelProperty("场景id")
    @NotNull(message = "场景id不能为空")
    private Long sceneId;

    /** 核心企业部门id */
    @ApiModelProperty("核心企业部门id")
    @NotNull(message = "核心企业部门id不能为空")
    private Long coreDeptId;

    /** 是否停用 1 启用 0 停用 */
    @ApiModelProperty("是否停用 1 启用 0 停用")
    private Integer validFlag;
}
