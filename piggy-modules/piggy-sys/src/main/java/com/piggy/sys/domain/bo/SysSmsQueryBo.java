package com.piggy.sys.domain.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 消息模板分页查询对象 sys_sms
 *
 * @author shark
 * @date 2021-08-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("消息模板分页查询对象")
public class SysSmsQueryBo extends BaseEntity {

	/** 消息id */
	@ApiModelProperty("消息id")
	private Long msgId;
	/** 接收人用户id */
	@ApiModelProperty("接收人用户id")
	private Long userId;
	/** 模板id */
	@ApiModelProperty("模板id")
	private String templateId;
	/** 签名 */
	@ApiModelProperty("签名")
	private String signName;
	/** 手机或邮箱 */
	@ApiModelProperty("手机或邮箱")
	private String link;
	/** 状态 */
	@ApiModelProperty("状态")
	private Integer status;
	/** 消息类型 */
	@ApiModelProperty("消息类型")
	private Integer type;
	/** AccessKeyID */
	@ApiModelProperty("AccessKeyID")
	private String accessKeyId;
	/** AccessKeySecret */
	@ApiModelProperty("AccessKeySecret")
	private String accessKeySecret;

}
