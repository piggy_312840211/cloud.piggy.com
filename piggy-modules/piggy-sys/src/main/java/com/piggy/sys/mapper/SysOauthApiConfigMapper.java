package com.piggy.sys.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.sys.api.domain.SysOauthApiConfig;
import org.apache.ibatis.annotations.Param;

public interface SysOauthApiConfigMapper extends BaseMapperPlus<SysOauthApiConfig>
{

    int  updateByCondition(@Param(value = "clientId") String clientId,@Param(value = "callbackUrl") String callbackUrl);
}
