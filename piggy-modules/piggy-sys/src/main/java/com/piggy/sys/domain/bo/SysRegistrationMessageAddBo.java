package com.piggy.sys.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 注册邀请短信详情添加对象 tdms_registration_message
 *
 * @author piggy
 * @date 2021-10-11
 */
@Data
@ApiModel("注册邀请短信详情添加对象")
public class SysRegistrationMessageAddBo {

    /** 邀请方机构ID */
    @ApiModelProperty("邀请方机构ID")
    private Long deptIdFrom;

    /** 邀请方行号/机构号/统一社会信用代码 */
    @ApiModelProperty("邀请方行号/机构号/统一社会信用代码")
    private String orgCodeFrom;

    /** 邀请方企业名称 */
    @ApiModelProperty("邀请方企业名称")
    private String nameFrom;

    /** 接收方机构ID */
    @ApiModelProperty("接收方机构ID")
    private Long deptIdTo;

    /** 接收方行号/机构号/统一社会信用代码 */
    @ApiModelProperty("接收方行号/机构号/统一社会信用代码")
    private String orgCodeTo;

    /** 接收方企业名称 */
    @ApiModelProperty("接收方企业名称")
    private String nameTo;

    /** 手机号码 */
    @ApiModelProperty("手机号码")
    private String phone;

    /** 短信信息 */
    @ApiModelProperty("短信信息")
    private String phoneMessage;

    /** 公共邮箱 */
    @ApiModelProperty("公共邮箱")
    private String email;

    /** 邮箱内容 */
    @ApiModelProperty("邮箱内容")
    private String emailMessage;

    /** 发送状态
0、失败
1、成功 */
    @ApiModelProperty("发送状态 0、失败 1、成功")
    private Long sendStatus;
}
