package com.piggy.sys.mapper;


import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.sys.api.domain.SysOss;

/**
 * 文件上传Mapper接口
 *
 * @author piggy
 * @date 2021-07-20
 */
public interface SysOssMapper extends BaseMapperPlus<SysOss> {

}
