
package com.piggy.sys.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author liyc
 * @date 2021/6/18 组织一级部门id
 */
@Getter
@AllArgsConstructor
public enum OrgDeptIdEnum {

	SYSADMIN(100, "系统管理员"),
	CHESHANG(123, "车商"),
	ZHONGWAIYUN(112, "中外运"),
	PLATFORM(101, "平台"),
	BANK(117, "金融机构"),
	OFFSHORE(120, "离岸公司");

	/**
	 * 类型
	 */
	private final Integer code;

	/**
	 * 描述
	 */
	private final String info;

}
