package com.piggy.sys;

import com.piggy.common.job.annotation.EnableXxlJob;
import com.piggy.common.satoken.annotation.EnableCustomConfig;
import com.piggy.common.satoken.annotation.EnableCustomFeignClients;
import com.piggy.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 系统模块
 *
 * @author shark
 */
@EnableXxlJob
@EnableCustomSwagger2
@EnableCustomFeignClients
@EnableCustomConfig
@RefreshScope
@SpringBootApplication
public class PIGYSysApplication {
    public static void main(String[] args) {
        SpringApplication.run(PIGYSysApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  系统模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" );
    }
}
