package com.piggy.sys.domain.bo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 消息模板添加对象 sys_sms
 *
 * @author shark
 * @date 2021-08-16
 */
@Data
@NoArgsConstructor
@ApiModel("消息模板添加对象")
public class SysSmsAddBo {

	/** 手机号 */
	String phoneNumber;
	/** 接收人用户id */
	private Long userId;
	/** 主题如：发送短信验证码 */
	private String subject;
	/** 模板code */
	private String templateCode;
	/** 签名 */
	private String signName;
	/** 内容参数 */
	private String templateParam;
	/** 0-站内信 1-邮件 2-短信 */
	private Integer type;

}
