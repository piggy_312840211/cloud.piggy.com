package com.piggy.sys.domain.vo;

import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 终端配置场景部门视图对象 sys_client_scene
 *
 * @author piggy
 * @date 2022-09-19
 */
@Data
@ApiModel("终端配置场景部门视图对象")
public class SysClientSceneVo {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	@ApiModelProperty("主键")
	private Long id;

	/** 终端编号 */
	@Excel(name = "终端编号")
	@ApiModelProperty("终端编号")
	private String clientId;

	/** 场景id */
	@Excel(name = "场景id")
	@ApiModelProperty("场景id")
	private Long sceneId;

	/** 核心企业部门id */
	@Excel(name = "核心企业部门id")
	@ApiModelProperty("核心企业部门id")
	private Long coreDeptId;

	/** 是否停用 1 启用 0 停用 */
	@Excel(name = "是否停用 1 启用 0 停用")
	@ApiModelProperty("是否停用 1 启用 0 停用")
	private Integer validFlag;

	/** 场景id */
	@Excel(name = "场景name")
	@ApiModelProperty("场景name")
	private String sceneName;

	/** 核心企业部门id */
	@Excel(name = "核心企业部门名称")
	@ApiModelProperty("核心企业部门名称")
	private String coreDeptName;

}
