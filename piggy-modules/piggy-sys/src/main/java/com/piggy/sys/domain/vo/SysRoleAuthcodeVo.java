package com.piggy.sys.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


/**
 * 角色授权码视图对象 sys_role_authcode
 *
 * @author shark
 * @date 2021-09-14
 */
@Data
@ApiModel("角色授权码视图对象")
public class SysRoleAuthcodeVo {

	private static final long serialVersionUID = 1L;

	/** 序号 */
	@ApiModelProperty("序号")
	private Long id;

	/** 编辑时间 */
	@Excel(name = "编辑时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("编辑时间")
	private Date editTime;

	/** 角色id */
	@Excel(name = "角色id")
	@ApiModelProperty("角色id")
	private Long roleId;

	/** 组织部门id */
	@Excel(name = "组织部门id")
	@ApiModelProperty("组织部门id")
	private Long orgDeptId;

	/** 授权码使用状态 0未使用 1已使用  */
	@Excel(name = "授权码使用状态 0未使用 1已使用 ")
	@ApiModelProperty("授权码使用状态 0未使用 1已使用 ")
	private Integer status;

	@ApiModelProperty("授权码使用状态字典")
	private String statusLabel;

	/** 授权码 */
	@Excel(name = "授权码")
	@ApiModelProperty("授权码")
	private String authCode;

	/** 手机号 */
	@ApiModelProperty("手机号")
	private String phone;

	/** 备注 */
	@Excel(name = "备注")
	@ApiModelProperty("备注")
	private String remark;


	/** 企业id */
	@ApiModelProperty("企业id")
	private Long businessId;

	/** 企业id */
	@ApiModelProperty("场景id")
	private Long sceneId;

	/** 0平台添加1核心企业添加  */
	@ApiModelProperty("0平台添加1核心企业添加")
	private Integer type;
}
