package com.piggy.sys.domain.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 文件上传分页查询对象 sys_oss
 *
 * @author piggy
 * @date 2021-07-20
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("文件上传分页查询对象")
public class SysOssQueryBo extends BaseEntity {

	/** $column.columnComment */
	@ApiModelProperty("$column.columnComment")
	private Long id;
	/** 文件类型 */
	@ApiModelProperty("文件类型")
	private String fileType;
	/** 文件名 */
	@ApiModelProperty("文件名")
	private String name;
	/** 原文件名 */
	@ApiModelProperty("原文件名")
	private String originName;
	/** 类型名 */
	@ApiModelProperty("文件路径")
	private String path;
	/** 状态 */
	@ApiModelProperty("状态")
	private String status;

}
