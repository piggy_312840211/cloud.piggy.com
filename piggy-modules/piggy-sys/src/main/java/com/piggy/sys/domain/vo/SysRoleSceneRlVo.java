package com.piggy.sys.domain.vo;

import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 场景角色视图对象 sys_role_scene_rl
 *
 * @author shark
 * @date 2021-10-15
 */
@Data
@ApiModel("场景角色视图对象")
public class SysRoleSceneRlVo {

	private static final long serialVersionUID = 1L;

	/** ID */
	@ApiModelProperty("ID")
	private Long id;

	/** 场景id */
	@Excel(name = "场景id")
	@ApiModelProperty("场景id")
	private Long sceneId;

	/** 角色ID */
	@Excel(name = "角色ID")
	@ApiModelProperty("角色ID")
	private Long roleId;

	/** 企业类型 */
	@Excel(name = "企业类型")
	@ApiModelProperty("企业类型")
	private String busineseType;


}
