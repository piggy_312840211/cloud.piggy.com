package com.piggy.sys.service;
import com.piggy.sys.api.vo.SysOauthApiConfigVo;

public interface ISysOauthApiConfigService
{
    SysOauthApiConfigVo  getApiConfigInfo();

    void  updateApiConfig(String  callbackUrl);
}
