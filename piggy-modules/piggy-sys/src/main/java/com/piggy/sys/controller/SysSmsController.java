package com.piggy.sys.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.utils.sms.SmsConfig;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.satoken.annotation.Inner;
import com.piggy.common.core.domain.R;
import com.piggy.sys.domain.bo.SysSmsBo;
import com.piggy.sys.domain.bo.SysSmsQueryBo;
import com.piggy.sys.domain.vo.SysSmsVo;
import com.piggy.sys.service.ISysSmsService;
import io.swagger.annotations.ApiOperation;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/sysSms")
public class SysSmsController extends BaseController {

    @Resource
    private ISysSmsService iSysSmsService;

    /**
     * 获取注册用户手机验证码
     * @return
     */
    @Inner
    @GetMapping("/userRegSendSms")
   public R<Boolean> userRegSendSms(String phone) {
       return R.ok(iSysSmsService.userRegSendSms(phone));
    }



    /**
     * 获取注册用户手机验证码
     * @return
     */
    @Inner
    @GetMapping("/getConfig")
    public R<SmsConfig> getConfig() {
        return R.ok(iSysSmsService.getConfig());
    }

    @Inner
    @GetMapping("/sendMail")
    public R<Boolean> sendMail(String content, String subject, List<String> params, String receiver){ return R.ok(iSysSmsService.sendMail(content,subject,params,receiver)); }

    @PostMapping("/sendSms")
    public  R<Boolean> sendSms(@RequestBody SysSmsBo bo) {
    return R.ok(iSysSmsService.sendSms(bo));
    }

    /**
     * 查询消息列表
     */
    @ApiOperation("查询消息列表")
    @SaCheckPermission("incar:sms:list")
    @GetMapping("/list")
    public TableDataInfo<SysSmsVo> list(@Validated SysSmsQueryBo bo) {
        return iSysSmsService.queryPageList(bo);
    }
}
