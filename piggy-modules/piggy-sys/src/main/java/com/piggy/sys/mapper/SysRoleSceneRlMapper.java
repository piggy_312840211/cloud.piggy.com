package com.piggy.sys.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.sys.api.domain.SysRoleSceneRl;

/**
 * 场景角色Mapper接口
 *
 * @author shark
 * @date 2021-10-15
 */
public interface SysRoleSceneRlMapper extends BaseMapperPlus<SysRoleSceneRl> {

}
