package com.piggy.sys.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.sys.api.domain.SysOperLog;

/**
 * 操作日志 数据层
 *
 * @author shark
 */
public interface SysOperLogMapper extends BaseMapperPlus<SysOperLog> {

}
