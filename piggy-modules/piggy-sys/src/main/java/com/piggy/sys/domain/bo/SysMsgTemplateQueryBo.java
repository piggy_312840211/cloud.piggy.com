package com.piggy.sys.domain.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 消息模板分页查询对象 sys_msg_template
 *
 * @author shark
 * @date 2021-06-23
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("消息模板分页查询对象")
public class SysMsgTemplateQueryBo extends BaseEntity {

	/** 消息类型 0-站内信 1-邮件 2-短信 */
	@ApiModelProperty("消息类型 0-站内信 1-邮件 2-短信")
	private Integer type;
	/** 消息内容模板 */
	@ApiModelProperty("消息内容模板")
	private String content;
	/** 变量(jsonStr) */
	@ApiModelProperty("变量(jsonStr)")
	private String param;
	/** 主题 */
	@ApiModelProperty("主题")
	private String subject;
	/** 发送人 */
	@ApiModelProperty("发送人")
	private String sender;
	/** key */
	@ApiModelProperty("key")
	private String msgKey;
	/** secret */
	@ApiModelProperty("secret")
	private String msgSecret;

}
