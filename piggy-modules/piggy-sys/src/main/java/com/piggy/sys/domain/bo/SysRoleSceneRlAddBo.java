package com.piggy.sys.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * 场景角色添加对象 sys_role_scene_rl
 *
 * @author shark
 * @date 2021-10-15
 */
@Data
@ApiModel("场景角色添加对象")
public class SysRoleSceneRlAddBo {


    /** 场景id */
    @ApiModelProperty("场景id")
    @NotNull(message = "场景id不能为空")
    private Long sceneId;

    /** 角色ID */
    @ApiModelProperty("角色ID")
    @NotNull(message = "角色ID不能为空")
    private Long roleId;

    /** 企业类型 */
    @ApiModelProperty("企业类型")
    @NotBlank(message = "企业类型不能为空")
    private String busineseType;
}
