package com.piggy.sys.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;

/**
 * 终端授权申请编辑对象 sys_oauth_request
 *
 * @author piggy
 * @date 2022-09-21
 */
@Data
@ApiModel("终端授权申请编辑对象")
public class SysOauthRequestEditBo {


    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 核心企业id */
    @ApiModelProperty("核心企业id")
    @NotNull(message = "核心企业id不能为空")
    private Long coreBusinessId;

    /** 申请状态 0 待审核 1 审核中 2 审核完成 3 审核失败 */
    @ApiModelProperty("申请状态 0 待审核 1 审核中 2 审核完成 3 审核失败")
    private Integer requestStatus;

    /** 审核失败原因 */
    @ApiModelProperty("审核失败原因")
    private String reason;

    /** 删除状态：0有效，1删除 */
    @ApiModelProperty("删除状态：0有效，1删除")
    private Integer delFlag;

    /** 删除人 */
    @ApiModelProperty("删除人")
    private String deleter;

    /** 申请时间 */
    @ApiModelProperty("申请时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date requestTime;

    /** 审核时间 */
    @ApiModelProperty("审核时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date auditTime;

    /** 审核人 */
    @ApiModelProperty("审核人")
    private String auditor;

    /** 提交人 */
    @ApiModelProperty("提交人")
    private String createBy;
}
