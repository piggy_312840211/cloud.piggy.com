package com.piggy.sys.domain.bo.flow;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 流程节点分页查询对象 flow_node
 *
 * @author piggy
 * @date 2021-06-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("流程节点分页查询对象")
public class FlowNodeQueryBo extends BaseEntity {

	/** $column.columnComment */
	@ApiModelProperty("$column.columnComment")
	private String nodeName;
	/** $column.columnComment */
	@ApiModelProperty("$column.columnComment")
	private String nodeKey;
	/** $column.columnComment */
	@ApiModelProperty("$column.columnComment")
	private Integer type;
	/** $column.columnComment */
	@ApiModelProperty("$column.columnComment")
	private Long menuId;

}
