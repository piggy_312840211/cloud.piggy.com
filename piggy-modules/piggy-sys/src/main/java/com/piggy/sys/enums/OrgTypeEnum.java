
package com.piggy.sys.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author liyc
 * @date 2021/6/18 组织类型
 */
@Getter
@AllArgsConstructor
public enum OrgTypeEnum {
	UNKNOWN(0, "未知"),
	CHESHANG(1, "车商"),
	ZHONGWAIYUN(2, "中外运"),
	PLATFORM(3, "平台"),
	BANK(4, "金融机构"),
	OFFSHORE(5, "离岸公司"),
	SUPPELIER(6, "供应商"),
	SYSADMIN(999, "系统管理员");

	/**
	 * 类型
	 */
	private final Integer code;

	/**
	 * 描述
	 */
	private final String info;

}
