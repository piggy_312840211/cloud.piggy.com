package com.piggy.sys.domain.vo;

import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 消息模板视图对象 sys_msg_receiver
 *
 * @author shark
 * @date 2021-06-23
 */
@Data
@ApiModel("消息模板视图对象")
public class SysMsgReceiverVo {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	@ApiModelProperty("主键")
	private Long id;

	/** 消息id */
	@Excel(name = "消息id")
	@ApiModelProperty("消息id")
	private Long msgId;

	/** 接收人用户id */
	@Excel(name = "接收人用户id")
	@ApiModelProperty("接收人用户id")
	private Long userId;

	/** 手机或邮箱 */
	@Excel(name = "手机或邮箱")
	@ApiModelProperty("手机或邮箱")
	private String link;


}
