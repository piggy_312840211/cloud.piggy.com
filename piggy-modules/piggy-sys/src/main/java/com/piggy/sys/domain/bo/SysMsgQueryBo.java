package com.piggy.sys.domain.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 消息模板分页查询对象 sys_msg
 *
 * @author shark
 * @date 2021-06-23
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("消息模板分页查询对象")
public class SysMsgQueryBo extends BaseEntity {

	/** 消息类型(冗余方便查询) 0-站内信 1-邮件 2-短信 */
	@ApiModelProperty("消息类型(冗余方便查询) 0-站内信 1-邮件 2-短信")
	private Integer type;
	/** 模板id */
	@ApiModelProperty("模板id")
	private Long templateId;
	/** 主题 */
	@ApiModelProperty("主题")
	private String subject;
	/** 内容 */
	@ApiModelProperty("内容")
	private String params;

}
