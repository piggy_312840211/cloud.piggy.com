package com.piggy.sys.domain.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 终端授权申请分页查询对象 sys_oauth_request
 *
 * @author piggy
 * @date 2022-09-21
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("终端授权申请分页查询对象")
public class SysOauthRequestQueryBo extends BaseEntity {

	/** 核心企业id */
	@ApiModelProperty("核心企业id")
	private Long coreBusinessId;
	/** 申请状态 0 待审核 1 审核中 2 审核完成 3 审核失败 */
	@ApiModelProperty("申请状态 0 待审核 1 审核中 2 审核完成 3 审核失败")
	private Integer requestStatus;
	/** 审核失败原因 */
	@ApiModelProperty("审核失败原因")
	private String reason;
	/** 删除状态：0有效，1删除 */
	@ApiModelProperty("删除状态：0有效，1删除")
	private Integer delFlag;
	/** 删除人 */
	@ApiModelProperty("删除人")
	private String deleter;
	/** 申请时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("申请时间")
	private Date requestTime;
	/** 审核时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("审核时间")
	private Date auditTime;
	/** 审核人 */
	@ApiModelProperty("审核人")
	private String auditor;

}
