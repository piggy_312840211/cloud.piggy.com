package com.piggy.sys.domain.bo.flow;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode()
@ApiModel("流程审批对象")
public class ProcessCompleteBo {
	@ApiModelProperty("任务Id")
	private String taskId;
	@ApiModelProperty("变量名称")
	private String checkKey;
	@ApiModelProperty("变量value")
	private Boolean check;
	@ApiModelProperty("备注信息")
	private String msg;
}
