package com.piggy.sys.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.sys.domain.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 角色与菜单关联表 数据层
 *
 * @author shark
 */
@Mapper
public interface SysRoleMenuMapper extends BaseMapperPlus<SysRoleMenu> {

	/**
	 * 批量新增角色菜单信息
	 *
	 * @param roleMenuList 角色菜单列表
	 * @return 结果
	 */
	int batchRoleMenu(List<SysRoleMenu> roleMenuList);

}
