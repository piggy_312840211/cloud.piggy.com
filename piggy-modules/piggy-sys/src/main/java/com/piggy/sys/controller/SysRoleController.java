package com.piggy.sys.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.constant.UserConstants;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import com.piggy.common.satoken.annotation.Inner;
import com.piggy.common.satoken.utils.SecurityUtils;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.domain.SysRole;
import com.piggy.sys.api.domain.SysUser;
import com.piggy.sys.service.ISysRoleService;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 角色信息
 *
 * @author shark
 */
@RestController
@RequestMapping("/role")
public class SysRoleController extends BaseController {
    @Resource
    private ISysRoleService roleService;

    @SaCheckPermission("system:role:list")
    @GetMapping("/list")
    public TableDataInfo list(SysRole role) {
        startPage();
        List<SysRole> list = roleService.selectRoleList(role);
        return getDataTable(list);
    }

    @Log(title = "角色管理", businessType = BusinessType.EXPORT)
    @SaCheckPermission("system:role:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysRole role) throws IOException {
        List<SysRole> list = roleService.selectRoleList(role);
        ExcelUtil<SysRole> util = new ExcelUtil<SysRole>(SysRole.class);
        util.exportExcel(response, list, "角色数据");
    }

    /**
     * 根据角色编号获取详细信息
     */
    @SaCheckPermission("system:role:query")
    @GetMapping(value = "/{roleId}")
    public R getInfo(@PathVariable Long roleId) {
        return R.ok(roleService.selectRoleById(roleId));
    }

    /**
     * 新增角色
     */
    @SaCheckPermission("system:role:add")
    @PostMapping
    public R add(@Validated @RequestBody SysRole role) {
        if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
            return R.fail("新增角色'" + role.getRoleName() + "'失败，角色名称已存在");
        } else if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role))) {
            return R.fail("新增角色'" + role.getRoleName() + "'失败，角色权限已存在");
        }
        role.setCreateBy(SecurityUtils.getUsername());
        return toAjax(roleService.insertRole(role));

    }

    /**
     * 修改保存角色
     */
    @SaCheckPermission("system:role:edit")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public R edit(@Validated @RequestBody SysRole role) {
        roleService.checkRoleAllowed(role);
        if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
            return R.fail("修改角色'" + role.getRoleName() + "'失败，角色名称已存在");
        } else if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role))) {
            return R.fail("修改角色'" + role.getRoleName() + "'失败，角色权限已存在");
        }
        role.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(roleService.updateRole(role));
    }

    /**
     * 修改保存数据权限
     */
    @SaCheckPermission("system:role:edit")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PutMapping("/dataScope")
    public R dataScope(@RequestBody SysRole role) {
        roleService.checkRoleAllowed(role);
        return toAjax(roleService.authDataScope(role));
    }

    /**
     * 状态修改
     */
    @SaCheckPermission("system:role:edit")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public R changeStatus(@RequestBody SysRole role) {
        roleService.checkRoleAllowed(role);
        role.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(roleService.updateRoleStatus(role));
    }

    /**
     * 删除角色
     */
    @SaCheckPermission("system:role:remove")
    @Log(title = "角色管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{roleIds}")
    public R remove(@PathVariable Long[] roleIds) {
        return toAjax(roleService.deleteRoleByIds(roleIds));
    }

    /**
     * 获取角色选择框列表
     */
    @SaCheckPermission("system:role:query")
    @GetMapping("/optionselect")
    public R optionselect() {
        return R.ok(roleService.selectRoleAll());
    }


    @Inner
    @GetMapping("/check/{userId}")
    public boolean selectRoleListByUserId(@PathVariable(value = "userId") Long userId) {
        return roleService.checkRoleNotAdmin(userId);
    }

    @Inner
    @GetMapping("/getAssignees")
    public List<SysUser> getAssignees(@RequestParam("menuIds")  List<String> menuIds) {
        return roleService.getAssignees(menuIds);
    }

    @Inner
    @GetMapping("/getRoleByKey/{roleKey}")
    public R<SysRole> getRoleByKey(@PathVariable(value = "roleKey") String roleKey) {
        return R.ok(roleService.lambdaQuery().eq(SysRole::getRoleKey, roleKey).oneOpt()
                .orElseThrow(() -> new BaseException("角色不存在")));
    }

    @Inner
    @GetMapping("/getRoleUser/{roleId}")
    public R<List<SysUser>>  getRoleUsers(@PathVariable(value = "roleId") Long roleId) {
        return R.ok(roleService.getRoleUsers(roleId));
    }

    @Inner
    @GetMapping("/getRoleUserByRoleIds")
    public R<List<SysUser>> getRoleUserByRoleIds(@RequestParam("roleIds") List<Long> roleIds) {
        return R.ok(roleService.getRoleUserByRoleIds(roleIds));
    }

    @Inner
    @GetMapping("/getRoleUserBySceneIds")
    public R<List<SysUser>> getRoleUserBySceneIds(@RequestParam("sceneIds") List<Long> sceneIds) {
        return R.ok(roleService.getRoleUserBySceneIds(sceneIds));
    }

    @Inner
    @PostMapping("/queryRoleAll")
    public R<List<SysRole>> queryRoleAll() {
        return R.ok(roleService.queryRoleAll());
    }

    @Inner
    @GetMapping("/getById/{roleId}")
    public R<SysRole>  getById(@PathVariable(value = "roleId") Long roleId) {
        return R.ok(roleService.getById(roleId));
    }
}
