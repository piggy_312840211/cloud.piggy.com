package com.piggy.sys.domain.vo;

import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 注册邀请短信详情视图对象 tdms_registration_message
 *
 * @author piggy
 * @date 2021-10-11
 */
@Data
@ApiModel("注册邀请短信详情视图对象")
public class SysRegistrationMessageVo {

	private static final long serialVersionUID = 1L;

	/** ID */
	@ApiModelProperty("ID")
	private Long id;

	/** 邀请方机构ID */
	@Excel(name = "邀请方机构ID")
	@ApiModelProperty("邀请方机构ID")
	private Long deptIdFrom;


}
