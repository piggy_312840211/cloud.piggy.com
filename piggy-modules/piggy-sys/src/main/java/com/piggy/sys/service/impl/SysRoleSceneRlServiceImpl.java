package com.piggy.sys.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.sys.api.bo.SysRoleSceneRlQueryBo;
import com.piggy.sys.api.domain.SysRoleSceneRl;
import com.piggy.sys.domain.bo.SysRoleSceneRlAddBo;
import com.piggy.sys.domain.bo.SysRoleSceneRlEditBo;
import com.piggy.sys.domain.vo.SysRoleSceneRlVo;
import com.piggy.sys.mapper.SysRoleSceneRlMapper;
import com.piggy.sys.service.ISysRoleSceneRlService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 场景角色Service业务层处理
 *
 * @author shark
 * @date 2021-10-15
 */
@Service
public class SysRoleSceneRlServiceImpl extends ServiceImpl<SysRoleSceneRlMapper, SysRoleSceneRl> implements ISysRoleSceneRlService {

    @Override
    public SysRoleSceneRlVo queryById(Long id){
        return getVoById(id, SysRoleSceneRlVo.class);
    }

    @Override
    public TableDataInfo<SysRoleSceneRlVo> queryPageList(SysRoleSceneRlQueryBo bo) {
        PagePlus<SysRoleSceneRl, SysRoleSceneRlVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), SysRoleSceneRlVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<SysRoleSceneRlVo> queryList(SysRoleSceneRlQueryBo bo) {
        return listVo(buildQueryWrapper(bo), SysRoleSceneRlVo.class);
    }

    private LambdaQueryWrapper<SysRoleSceneRl> buildQueryWrapper(SysRoleSceneRlQueryBo bo) {
        Map<String, Object> params = bo.getReqParams();
        LambdaQueryWrapper<SysRoleSceneRl> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getId() != null, SysRoleSceneRl::getId, bo.getId());
        lqw.eq(bo.getSceneId() != null, SysRoleSceneRl::getSceneId, bo.getSceneId());
        lqw.eq(bo.getRoleId() != null, SysRoleSceneRl::getRoleId, bo.getRoleId());
        lqw.eq(StrUtil.isNotBlank(bo.getBusineseType()), SysRoleSceneRl::getBusineseType, bo.getBusineseType());
        lqw.eq(StrUtil.isNotBlank(bo.getStatus()), SysRoleSceneRl::getStatus, bo.getStatus());
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(SysRoleSceneRlAddBo bo) {
        SysRoleSceneRl add = BeanUtil.toBean(bo, SysRoleSceneRl.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(SysRoleSceneRlEditBo bo) {
        SysRoleSceneRl update = BeanUtil.toBean(bo, SysRoleSceneRl.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(SysRoleSceneRl entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

    @Override
    public List<SysRoleSceneRlVo> getSceneRole(Long id) {
        return listVo(new LambdaQueryWrapper<SysRoleSceneRl>().eq(SysRoleSceneRl::getSceneId, id), SysRoleSceneRlVo.class);
    }

    @Override
    public Integer addOrUpdateRole(List<SysRoleSceneRl> roleSceneRls, Long sceneId){
        remove(new LambdaQueryWrapper<SysRoleSceneRl>().eq(SysRoleSceneRl::getSceneId,sceneId));
        return saveBatch(roleSceneRls) ? 1 : 0;
    }
    @Override
    public int activation(Long sceneId){
      return Long.valueOf(lambdaQuery().eq(SysRoleSceneRl::getSceneId, sceneId).count()).intValue();
    }
}
