package com.piggy.sys.domain.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 角色授权码分页查询对象 sys_role_authcode
 *
 * @author shark
 * @date 2021-09-14
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("角色授权码分页查询对象")
public class SysRoleAuthcodeQueryBo extends BaseEntity {

	/** 创建人 */
	@ApiModelProperty("创建人")
	private Long creator;
	/** 创建人姓名 */
	@ApiModelProperty("创建人姓名")
	private String creatorName;
	/** 编辑人 */
	@ApiModelProperty("编辑人")
	private Long editor;
	/** 编辑人姓名 */
	@ApiModelProperty("编辑人姓名")
	private String editorName;
	/** 编辑时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("编辑时间")
	private Date editTime;
	/** 删除人 */
	@ApiModelProperty("删除人")
	private Long deleter;
	/** 删除人姓名 */
	@ApiModelProperty("删除人姓名")
	private String deleterName;
	/** 删除时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("删除时间")
	private Date deleteTime;
	/** 角色id */
	@ApiModelProperty("角色id")
	private Long roleId;
	/** 组织部门id */
	@ApiModelProperty("组织部门id")
	private Long orgDeptId;
	/** 状态 0未使用 1已使用  */
	@ApiModelProperty("状态 0未使用 1已使用 ")
	private Integer status;
	/** 授权码 */
	@ApiModelProperty("授权码")
	private String authCode;
	/** 手机号 */
	@ApiModelProperty("手机号")
	private String phone;

}
