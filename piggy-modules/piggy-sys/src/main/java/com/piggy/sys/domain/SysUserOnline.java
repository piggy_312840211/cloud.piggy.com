package com.piggy.sys.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 当前在线会话
 *
 * @author shark
 */

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class SysUserOnline {
    /**
     * 会话编号
     */
    @ApiModelProperty("会话编号")
    private String tokenId;

    /**
     * 部门名称
     */
	@ApiModelProperty("部门名称")
    private String deptName;

    /**
     * 用户名称
     */
	@ApiModelProperty("用户名称")
    private String userName;

    /**
     * 登录IP地址
     */
	@ApiModelProperty("登录IP地址")
    private String ipaddr;

    /**
     * 登录地址
     */
	@ApiModelProperty("登录地址")
    private String loginLocation;

    /**
     * 浏览器类型
     */
	@ApiModelProperty("浏览器类型")
    private String browser;

    /**
     * 操作系统
     */
	@ApiModelProperty("操作系统")
    private String os;

    /**
     * 登录时间
     */
	@ApiModelProperty("登录时间")
    private Long loginTime;

}
