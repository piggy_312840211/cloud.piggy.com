package com.piggy.sys.domain.bo.flow;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

@Data
@Builder
@EqualsAndHashCode()
@ApiModel("流程构建对象")
public class ProcessInstanceBo {

	@ApiModelProperty("流程Key")
	private String processKey;
	@ApiModelProperty("流程Id")
	private String processId;
	@ApiModelProperty("流程名称")
	private String processName;
	@ApiModelProperty("业务id")
	private String businessKey;
	@ApiModelProperty("流程变量")
	private Map<String, Object> vars;
}
