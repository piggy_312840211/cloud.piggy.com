package com.piggy.sys.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.satoken.utils.DictUtils;
import com.piggy.sys.api.domain.SysDictData;
import com.piggy.sys.mapper.SysDictDataMapper;
import com.piggy.sys.service.ISysDictDataService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * 字典 业务层处理
 *
 * @author shark
 */
@Service
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictData> implements ISysDictDataService {

	@Override
	public TableDataInfo<SysDictData> selectPageDictDataList(SysDictData dictData) {
		LambdaQueryWrapper<SysDictData> lqw = new LambdaQueryWrapper<SysDictData>()
			.eq(StrUtil.isNotBlank(dictData.getDictType()), SysDictData::getDictType, dictData.getDictType())
			.like(StrUtil.isNotBlank(dictData.getDictLabel()), SysDictData::getDictLabel, dictData.getDictLabel())
			.eq(StrUtil.isNotBlank(dictData.getStatus()), SysDictData::getStatus, dictData.getStatus())
			.eq(StrUtil.isNotBlank(dictData.getListClass()), SysDictData::getListClass, dictData.getListClass())
			.in(CollectionUtil.isNotEmpty(dictData.getDictKeyList()),SysDictData::getDictValue,dictData.getDictKeyList())
			.orderByAsc(SysDictData::getDictSort);
		return PageUtils.buildDataInfo(page(PageUtils.buildPage(), lqw));
	}

	/**
	 * 获取字典标签列表(dictType必输，remark、dictKeyList选输)
	 */
	@Override
	public List<SysDictData> getDictList(SysDictData dictData) {
		String dictType=dictData.getDictType();
		if(StrUtil.isEmpty(dictType)){
			throw new BaseException("字典类型不能为空");
		}
		List<SysDictData> dictDataList=null;

		String remark=dictData.getRemark();
		List<String> dictKeyList=dictData.getDictKeyList();
		if(StrUtil.isNotEmpty(remark)&&CollectionUtil.isNotEmpty(dictKeyList)){
			//listClass不为空，dictKeyList不为空
			dictDataList=this.list(new LambdaQueryWrapper<SysDictData>().eq(SysDictData::getStatus,0).eq(SysDictData::getDictType,dictType).eq(SysDictData::getRemark,remark).in(SysDictData::getDictValue,dictKeyList).orderByAsc(SysDictData::getDictSort));
		}else if(StrUtil.isNotEmpty(remark)&&CollectionUtil.isEmpty(dictKeyList)){
			//listClass不为空，dictKeyList为空
			dictDataList=this.list(new LambdaQueryWrapper<SysDictData>().eq(SysDictData::getStatus,0).eq(SysDictData::getDictType,dictType).eq(SysDictData::getRemark,remark).orderByAsc(SysDictData::getDictSort));
		}else if(StrUtil.isEmpty(remark)&&CollectionUtil.isNotEmpty(dictKeyList)){
			//listClass为空，dictKeyList不为空
			dictDataList=this.list(new LambdaQueryWrapper<SysDictData>().eq(SysDictData::getStatus,0).eq(SysDictData::getDictType,dictType).in(SysDictData::getDictValue,dictKeyList).orderByAsc(SysDictData::getDictSort));
		}else{
			dictDataList=this.list(new LambdaQueryWrapper<SysDictData>().eq(SysDictData::getStatus,0).eq(SysDictData::getDictType,dictType).orderByAsc(SysDictData::getDictSort));
		}
		return  dictDataList;
	}

	/**
	 * 根据条件分页查询字典数据
	 *
	 * @param dictData 字典数据信息
	 * @return 字典数据集合信息
	 */
	@Override
	public List<SysDictData> selectDictDataList(SysDictData dictData) {
		return list(new LambdaQueryWrapper<SysDictData>()
			.eq(StrUtil.isNotBlank(dictData.getDictType()), SysDictData::getDictType, dictData.getDictType())
			.like(StrUtil.isNotBlank(dictData.getDictLabel()), SysDictData::getDictLabel, dictData.getDictLabel())
			.eq(StrUtil.isNotBlank(dictData.getStatus()), SysDictData::getStatus, dictData.getStatus())
			.orderByAsc(SysDictData::getDictSort));
	}

	/**
	 * 根据字典类型和字典键值查询字典数据信息
	 *
	 * @param dictType  字典类型
	 * @param dictValue 字典键值
	 * @return 字典标签
	 */
	@Override
	public String selectDictLabel(String dictType, String dictValue) {
		SysDictData sysDictData=getOne(new LambdaQueryWrapper<SysDictData>()
			.select(SysDictData::getDictLabel)
			.eq(SysDictData::getDictType, dictType)
			.eq(SysDictData::getDictValue, dictValue));
		if(sysDictData!=null){
			return sysDictData.getDictLabel();
		}else{
			return "";
		}
	}

	/**
	 * 根据字典数据ID查询信息
	 *
	 * @param dictCode 字典数据ID
	 * @return 字典数据
	 */
	@Override
	public SysDictData selectDictDataById(Long dictCode) {
		return getById(dictCode);
	}

	/**
	 * 批量删除字典数据信息
	 *
	 * @param dictCodes 需要删除的字典数据ID
	 * @return 结果
	 */
	@Override
	public void deleteDictDataByIds(Long[] dictCodes) {
		for (Long dictCode : dictCodes) {
			SysDictData data = selectDictDataById(dictCode);
			List<SysDictData> dictDatas = baseMapper.selectDictDataByType(data.getDictType());
			DictUtils.setDictCache(data.getDictType(), dictDatas);
		}
		baseMapper.deleteBatchIds(Arrays.asList(dictCodes));
	}

	/**
	 * 新增保存字典数据信息
	 *
	 * @param data 字典数据信息
	 * @return 结果
	 */
	@Override
	public int insertDictData(SysDictData data) {
		int row = baseMapper.insert(data);
		if (row > 0) {
			List<SysDictData> dictDatas = baseMapper.selectDictDataByType(data.getDictType());
			DictUtils.setDictCache(data.getDictType(), dictDatas);
		}
		return row;
	}

	/**
	 * 修改保存字典数据信息
	 *
	 * @param data 字典数据信息
	 * @return 结果
	 */
	@Override
	public int updateDictData(SysDictData data) {
		int row = baseMapper.updateById(data);
		if (row > 0) {
			List<SysDictData> dictDatas = baseMapper.selectDictDataByType(data.getDictType());
			DictUtils.setDictCache(data.getDictType(), dictDatas);
		}
		return row;
	}
}
