package com.piggy.sys.domain.bo.flow;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 流程节点编辑对象 flow_node
 *
 * @author piggy
 * @date 2021-06-16
 */
@Data
@ApiModel("流程节点编辑对象")
public class FlowNodeEditBo {


    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private Long id;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String nodeName;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String nodeKey;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private Integer type;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private Long menuId;
}
