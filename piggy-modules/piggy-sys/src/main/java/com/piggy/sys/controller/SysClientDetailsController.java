package com.piggy.sys.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.piggy.common.core.utils.StringUtils;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import com.piggy.common.satoken.annotation.Inner;
import com.piggy.common.sequence.sequence.PigySequence;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.domain.SysClientDetails;
import com.piggy.sys.service.ISysClientDetailsService;
import io.swagger.annotations.Api;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 终端配置 信息操作处理
 *
 * @author ruoyi
 */
@Api(value = "客户端配置", tags = {"客户端配置"})
@RestController
@RequestMapping("/client")
public class SysClientDetailsController extends BaseController
{
    @Resource
    private ISysClientDetailsService sysClientDetailsService;

    @Resource
    private PigySequence clientSeq;

    /**
     * 查询终端配置列表
     * todo 接口未调通
     */
    //@Inner(false)
    @SaCheckPermission("system:client:list")
    @GetMapping("/list")
    public TableDataInfo list(SysClientDetails sysClientDetails)
    {
        startPage();
        List<SysClientDetails> list = sysClientDetailsService.selectSysClientDetailsList(sysClientDetails);
        return getDataTable(list);
    }

    /**
     * 查询终端配置列表
     */
    //@Inner(false)
    @SaCheckPermission("system:client:list")
    @PostMapping("/list")
    public TableDataInfo all(@RequestBody SysClientDetails sysClientDetails)
    {
        startPage();
        List<SysClientDetails> list = sysClientDetailsService.selectSysClientDetailsList(sysClientDetails);
        return getDataTable(list);
    }

    /**
     * 内部接口,获取终端配置详细信息
     */
    @Inner
//    @SaCheckPermission("system:client:query")
    @GetMapping(value = "/{clientId}")
    public R<SysClientDetails> getInfo(@PathVariable("clientId") String clientId)
    {
        return R.ok(sysClientDetailsService.selectSysClientDetailsById(clientId));
    }

    /**
     * 外部接口,获取终端配置详细信息
     */
    @SaCheckPermission("system:client:query")
    @PostMapping(value = "/{clientId}")
    public R<SysClientDetails> detailInfo(@PathVariable("clientId") String clientId)
    {
        return R.ok(sysClientDetailsService.selectSysClientDetailsById(clientId));
    }

    /**
     * 新增终端配置
     */
    @SaCheckPermission("system:client:add")
    @Log(title = "终端配置", businessType = BusinessType.INSERT)
    @PostMapping
    public R add(@RequestBody SysClientDetails sysClientDetails)
    {
        String clientId = clientSeq.nextNo();
        sysClientDetails.setClientId(clientId);
        if (StringUtils.isNotNull(sysClientDetailsService.selectSysClientDetailsById(clientId)))
        {
            return R.fail("新增终端'" + clientId + "'失败，编号已存在");
        }
        return toAjax(sysClientDetailsService.insertSysClientDetails(sysClientDetails));
    }

    /**
     * 修改终端配置
     */
    @SaCheckPermission("system:client:edit")
    @Log(title = "终端配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public R edit(@RequestBody SysClientDetails sysClientDetails)
    {
        return toAjax(sysClientDetailsService.updateSysClientDetails(sysClientDetails));
    }

    /**
     * 删除终端配置
     */
    @SaCheckPermission("system:client:remove")
    @Log(title = "终端配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{clientIds}")
    public R remove(@PathVariable String[] clientIds)
    {
        return toAjax(sysClientDetailsService.deleteSysClientDetailsByIds(clientIds));
    }

    /**
     * 根据resouceId查询终端配置
     */
//    @SaCheckPermission("system:client:query")
//    @Log(title = "根据resouceId查询终端配置", businessType = BusinessType.FORCE)
    @GetMapping("/res/{resourceId}")
    public R<SysClientDetails> getResource(@PathVariable String resourceId)
    {
        return R.ok(sysClientDetailsService.getOne(new LambdaQueryWrapper<SysClientDetails>().eq(SysClientDetails::getResourceIds,resourceId)));
    }
}
