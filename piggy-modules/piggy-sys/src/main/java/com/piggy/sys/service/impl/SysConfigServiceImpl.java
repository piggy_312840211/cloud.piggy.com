package com.piggy.sys.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piggy.common.core.constant.UserConstants;
import com.piggy.common.core.enums.CacheKeyEnums;
import com.piggy.common.core.exception.CustomException;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.datasource.annotation.Master;
import com.piggy.common.redis.cache.CacheUtils;
import com.piggy.sys.api.domain.SysConfig;
import com.piggy.sys.mapper.SysConfigMapper;
import com.piggy.sys.service.ISysConfigService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 参数配置 服务层实现
 *
 * @author shark
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {

	/**
	 * 项目启动时，初始化参数到缓存
	 */
	@PostConstruct
	public void init() {
		loadingConfigCache();
	}

	@Override
	public TableDataInfo<SysConfig> selectPageConfigList(SysConfig config) {
		Map<String, Object> params = config.getParams();
		LambdaQueryWrapper<SysConfig> lqw = new LambdaQueryWrapper<SysConfig>()
			.like(StrUtil.isNotBlank(config.getConfigName()), SysConfig::getConfigName, config.getConfigName())
			.eq(StrUtil.isNotBlank(config.getConfigType()), SysConfig::getConfigType, config.getConfigType())
			.like(StrUtil.isNotBlank(config.getConfigKey()), SysConfig::getConfigKey, config.getConfigKey())
			.apply(Validator.isNotEmpty(params.get("beginTime")),
				"date_format(create_time,'%y%m%d') >= date_format({0},'%y%m%d')",
				params.get("beginTime"))
			.apply(Validator.isNotEmpty(params.get("endTime")),
				"date_format(create_time,'%y%m%d') <= date_format({0},'%y%m%d')",
				params.get("endTime"));
		return PageUtils.buildDataInfo(page(PageUtils.buildPage(), lqw));
	}

	/**
	 * 查询参数配置信息
	 *
	 * @param configId 参数配置ID
	 * @return 参数配置信息
	 */
	@Override
	@Master
	public SysConfig selectConfigById(Long configId) {
		return baseMapper.selectById(configId);
	}

	/**
	 * 根据键名查询参数配置信息
	 *
	 * @param configKey 参数key
	 * @return 参数键值
	 */
	@Override
	public String selectConfigByKey(String configKey) {
		SysConfig configValue = CacheUtils.getCache(getCacheKey(configKey), SysConfig.class);
		if (Validator.isNotNull(configValue)) {
			return configValue.getConfigValue();
		}
		SysConfig retConfig = baseMapper.selectOne(new LambdaQueryWrapper<SysConfig>()
			.eq(SysConfig::getConfigKey, configKey));
		if (Validator.isNotNull(retConfig)) {
			CacheUtils.cache(retConfig, getCacheKey(configKey));
			CacheUtils.cache(retConfig, getGroupKey(retConfig.getGroupKey()), retConfig.getConfigKey());
			return retConfig.getConfigValue();
		}
		return StrUtil.EMPTY;
	}

	@Override
	public SysConfig getConfigByKey(String configKey) {
		SysConfig retConfig = baseMapper.selectOne(new LambdaQueryWrapper<SysConfig>()
			.eq(SysConfig::getConfigKey, configKey));
		if (Validator.isNotNull(retConfig)) {
			CacheUtils.cache(retConfig, getCacheKey(configKey));
			CacheUtils.cache(retConfig, getGroupKey(retConfig.getGroupKey()), retConfig.getConfigKey());
			return retConfig;
		}
		return null;
	}

	@Override
	public Map<String,SysConfig> getConfigByGroupKey(String groupKey) {
		return CacheUtils.getCacheMap(getGroupKey(groupKey), ()-> {
			List<SysConfig> configList = this.list(new LambdaQueryWrapper<SysConfig>()
					.eq(SysConfig::getGroupKey, groupKey));
			if (CollUtil.isEmpty(configList)) {
				return null;
			}
			Map<String, SysConfig> sysConfigMap = new HashMap<>();
			configList.forEach(t-> {
				CacheUtils.cache(t, getCacheKey(t.getConfigKey()));
				CacheUtils.cache(t, getGroupKey(t.getGroupKey()), t.getConfigKey());
				sysConfigMap.put(t.getConfigKey(), t);
			});
			return sysConfigMap;
		});
	}

	/**
	 * 查询参数配置列表
	 *
	 * @param config 参数配置信息
	 * @return 参数配置集合
	 */
	@Override
	public List<SysConfig> selectConfigList(SysConfig config) {
		Map<String, Object> params = config.getParams();
		LambdaQueryWrapper<SysConfig> lqw = new LambdaQueryWrapper<SysConfig>()
			.like(StrUtil.isNotBlank(config.getConfigName()), SysConfig::getConfigName, config.getConfigName())
			.eq(StrUtil.isNotBlank(config.getConfigType()), SysConfig::getConfigType, config.getConfigType())
			.like(StrUtil.isNotBlank(config.getConfigKey()), SysConfig::getConfigKey, config.getConfigKey())
			.apply(Validator.isNotEmpty(params.get("beginTime")),
				"date_format(create_time,'%y%m%d') >= date_format({0},'%y%m%d')",
				params.get("beginTime"))
			.apply(Validator.isNotEmpty(params.get("endTime")),
				"date_format(create_time,'%y%m%d') <= date_format({0},'%y%m%d')",
				params.get("endTime"));
		return baseMapper.selectList(lqw);
	}

	/**
	 * 新增参数配置
	 *
	 * @param config 参数配置信息
	 * @return 结果
	 */
	@Override
	public int insertConfig(SysConfig config) {
		int row = baseMapper.insert(config);
		if (row > 0) {
			CacheUtils.cache(config, getCacheKey(config.getConfigKey()));
			CacheUtils.cache(config, getGroupKey(config.getGroupKey()), config.getConfigKey());
		}
		return row;
	}

	/**
	 * 修改参数配置
	 *
	 * @param config 参数配置信息
	 * @return 结果
	 */
	@Override
	public int updateConfig(SysConfig config) {
		int row = baseMapper.updateById(config);
		if (row > 0) {
			CacheUtils.cache(config, getCacheKey(config.getConfigKey()));
			CacheUtils.cache(config, getGroupKey(config.getGroupKey()), config.getConfigKey());
		}
		return row;
	}

	/**
	 * 批量删除参数信息
	 *
	 * @param configIds 需要删除的参数ID
	 * @return 结果
	 */
	@Override
	public void deleteConfigByIds(Long[] configIds) {
		for (Long configId : configIds) {
			SysConfig config = selectConfigById(configId);
			if (StrUtil.equals(UserConstants.YES, config.getConfigType())) {
				throw new CustomException(String.format("内置参数【%1$s】不能删除 ", config.getConfigKey()));
			}
			CacheUtils.remove(getCacheKey(config.getConfigKey()));
			CacheUtils.remove(getGroupKey(config.getGroupKey()), config.getConfigKey());
		}
		baseMapper.deleteBatchIds(Arrays.asList(configIds));
	}

	/**
	 * 加载参数缓存数据
	 */
	@Override
	public void loadingConfigCache() {
		List<SysConfig> configsList = selectConfigList(new SysConfig());
		for (SysConfig config : configsList) {
			CacheUtils.remove(getCacheKey(config.getConfigKey()));
			CacheUtils.cache(config, getCacheKey(config.getConfigKey()));
			CacheUtils.remove(getGroupKey(config.getGroupKey()), config.getConfigKey());
			CacheUtils.cache(config, getGroupKey(config.getGroupKey()), config.getConfigKey());
		}
	}

	/**
	 * 清空参数缓存数据
	 */
	@Override
	public void clearConfigCache() {
		String keys = CacheKeyEnums.SysConfig.getKey("*");
		CacheUtils.remove(keys);
	}

	/**
	 * 重置参数缓存数据
	 */
	@Override
	public void resetConfigCache() {
		clearConfigCache();
		loadingConfigCache();
	}

	/**
	 * 校验参数键名是否唯一
	 *
	 * @param config 参数配置信息
	 * @return 结果
	 */
	@Override
	public String checkConfigKeyUnique(SysConfig config) {
		Long configId = Validator.isNull(config.getConfigId()) ? -1L : config.getConfigId();
		SysConfig info = baseMapper.selectOne(new LambdaQueryWrapper<SysConfig>().eq(SysConfig::getConfigKey, config.getConfigKey()));
		if (Validator.isNotNull(info) && info.getConfigId().longValue() != configId.longValue()) {
			return UserConstants.NOT_UNIQUE;
		}
		return UserConstants.UNIQUE;
	}

	@Override
	public void updateSendRule(String noticeCondition, String noticeDay) {
		SysConfig sysConfig1=getConfigByKey("noticeCondition");
		sysConfig1.setConfigValue(noticeCondition);
		updateById(sysConfig1);
		updateConfig(sysConfig1);
		SysConfig sysConfig2=getConfigByKey("noticeDay");
		sysConfig2.setConfigValue(noticeDay);
		updateConfig(sysConfig2);
	}

	/**
	 * 设置cache key
	 *
	 * @param configKey 参数键
	 * @return 缓存键key
	 */
	private String getCacheKey(String configKey) {
		return CacheKeyEnums.SysConfig.getKey(configKey);
	}

	private String getGroupKey(String groupKey) {
		return CacheKeyEnums.SysConfig.getKey(groupKey);
	}

}
