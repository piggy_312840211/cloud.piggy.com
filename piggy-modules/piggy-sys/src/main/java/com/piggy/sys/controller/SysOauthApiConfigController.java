package com.piggy.sys.controller;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.vo.SysOauthApiConfigVo;
import com.piggy.sys.service.ISysOauthApiConfigService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

@RestController
@RequestMapping(value="/api/config")
public class SysOauthApiConfigController
{

    @Resource
    private ISysOauthApiConfigService   sysOauthApiConfigService;

    @GetMapping(value="/getApiConfigInfo")
    public R<SysOauthApiConfigVo> getApiConfigInfo()
    {

        return  R.ok(sysOauthApiConfigService.getApiConfigInfo());
    }
    @GetMapping(value="/updateApiConfig")
    public R updateApiConfig(@RequestParam(required = true,name = "callbackUrl") String callbackUrl)
    {
        sysOauthApiConfigService.updateApiConfig(callbackUrl);
        return  R.ok(null,"操作成功!");
    }
}
