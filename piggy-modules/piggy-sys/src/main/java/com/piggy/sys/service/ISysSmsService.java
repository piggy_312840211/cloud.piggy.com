package com.piggy.sys.service;

import com.piggy.common.core.utils.sms.SmsConfig;
import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.sys.domain.SysSms;
import com.piggy.sys.domain.bo.SysSmsAddBo;
import com.piggy.sys.domain.bo.SysSmsBo;
import com.piggy.sys.domain.bo.SysSmsEditBo;
import com.piggy.sys.domain.bo.SysSmsQueryBo;
import com.piggy.sys.domain.vo.SysSmsVo;

import java.util.Collection;
import java.util.List;

/**
 * 消息模板Service接口
 *
 * @author shark
 * @date 2021-08-16
 */
public interface ISysSmsService extends IServicePlus<SysSms> {

	/**
	 * 发送sms
	 * @param bo 消息模板编辑业务对象
	 * @return
	 */
	Boolean sendSms(SysSmsBo bo);

	/**
	 * 发送sms
	 * @return
	 */
	Boolean sendBatchSms(List<SysSmsBo> smsList,String templateCode);

	/**
	 * 发送sms
	 * @param smsList 消息模板编辑业务对象
	 * @return
	 */
	Boolean sendBatchSms(SysSmsBo smsList);

	/**
	 * 发送授权码短信
	 * @return
	 */
	Boolean autoCodeSendSms(String phone,String autoCode);

	/**
	 * 获取注册用户手机验证码
	 * @return
	 */
	Boolean userRegSendSms(String phone);

	/**
	 * 获取config
	 * @return SmsConfig
	 */
	 SmsConfig getConfig();
	/**
	 * 查询单个
	 * @return
	 */
	SysSmsVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<SysSmsVo> queryPageList(SysSmsQueryBo bo);

	/**
	 * 查询列表
	 */
	List<SysSmsVo> queryList(SysSmsQueryBo bo);

	/**
	 * 根据新增业务对象插入消息模板
	 * @param bo 消息模板新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(SysSmsAddBo bo);

	/**
	 * 根据编辑业务对象修改消息模板
	 * @param bo 消息模板编辑业务对象
	 * @return
	 */
	Boolean updateByEditBo(SysSmsEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
	/**
	 * 发送邮箱服务
	 * @param content 邮件正文。 参数传参用{}表示
	 * @param params  邮件中{}的参数依次排入
	 * @param receiver 接收者的邮箱
	 * @return
	 */
	Boolean sendMail(String content,String subject,List<String> params,String receiver);
}
