package com.piggy.sys.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.UserConstants;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import com.piggy.common.satoken.annotation.Inner;
import com.piggy.common.satoken.utils.SecurityUtils;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.domain.SysConfig;
import com.piggy.sys.service.ISysConfigService;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 参数配置 信息操作处理
 *
 * @author shark
 */
@RestController
@RequestMapping("/config")
public class SysConfigController extends BaseController {
    @Resource
    private ISysConfigService configService;

    /**
     * 获取参数配置列表
     */

    @SaCheckPermission("system:config:list")
    @GetMapping("/list")
    public TableDataInfo list(SysConfig config) {
        startPage();
        List<SysConfig> list = configService.selectConfigList(config);
        return getDataTable(list);
    }

    @Log(title = "参数管理", businessType = BusinessType.EXPORT)
    @SaCheckPermission("system:config:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysConfig config) throws IOException {
        List<SysConfig> list = configService.selectConfigList(config);
        ExcelUtil<SysConfig> util = new ExcelUtil<SysConfig>(SysConfig.class);
        util.exportExcel(response, list, "参数数据");
    }

    /**
     * 根据参数编号获取详细信息
     */
    @GetMapping(value = "/{configId}")
    public R<SysConfig> getInfo(@PathVariable Long configId) {
        return R.ok(configService.selectConfigById(configId));
    }

    /**
     * 根据参数键名查询参数值
     */
    @GetMapping(value = "/configKey/{configKey}")
    public R<String> getConfigKey(@PathVariable String configKey) {
        return R.ok(configService.selectConfigByKey(configKey));
    }

    /**
     * 根据参数键名查询参数值-feign
     */
    @Inner
    @GetMapping(value = "/getConfigByKey/{configKey}")
    public R<SysConfig> getConfigByKey(@PathVariable String configKey) {
        return R.ok(configService.getConfigByKey(configKey));
    }

    /**
     * 根据参数键名查询参数值-feign
     */
    @Inner
    @GetMapping(value = "/getConfigByGroupKey/{groupKey}")
    public R<Map<String, SysConfig>> getConfigByGroupKey(@PathVariable String groupKey) {
        return ok(configService.getConfigByGroupKey(groupKey));
    }

    /**
     * 新增参数配置
     */
    @SaCheckPermission("system:config:add")
    @Log(title = "参数管理", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Integer> add(@Validated @RequestBody SysConfig config) {
        if (UserConstants.NOT_UNIQUE.equals(configService.checkConfigKeyUnique(config))) {
            return R.fail("新增参数'" + config.getConfigName() + "'失败，参数键名已存在");
        }
        config.setCreateBy(SecurityUtils.getUsername());
        return R.ok(configService.insertConfig(config));
    }

    /**
     * 修改参数配置
     */
    @SaCheckPermission("system:config:edit")
    @Log(title = "参数管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Integer> edit(@Validated @RequestBody SysConfig config) {
        if (UserConstants.NOT_UNIQUE.equals(configService.checkConfigKeyUnique(config))) {
            return R.fail("修改参数'" + config.getConfigName() + "'失败，参数键名已存在");
        }
        config.setUpdateBy(SecurityUtils.getUsername());
        return R.ok(configService.updateConfig(config));
    }

    /**
     * 删除参数配置
     */
    @SaCheckPermission("system:config:remove")
    @Log(title = "参数管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{configIds}")
    public R<SysConfig> remove(@PathVariable Long[] configIds) {
        configService.deleteConfigByIds(configIds);
        return success();
    }

    /**
     * 刷新参数缓存
     */
    @SaCheckPermission("system:config:remove")
    @Log(title = "参数管理", businessType = BusinessType.CLEAN)
    @DeleteMapping("/refreshCache")
    public R refreshCache() {
        configService.resetConfigCache();
        return success();
    }

    @Inner
    @GetMapping("/updateSendRule")
    public R updateSendRule(String noticeCondition, String noticeDay, @RequestHeader(SecurityConstants.FROM_SOURCE) String source){
        configService.updateSendRule(noticeCondition,noticeDay);
        return success();
    }

    /**
     * 根据键名查询参数配置信息
     *
     * @param configKey 参数键名
     * @return 参数键值
     */
    @Inner
    @GetMapping("/selectConfigByKey")
    public R<String> selectConfigByKey(String configKey){
        return R.ok(configService.selectConfigByKey(configKey));
    }

    @Inner
    @GetMapping("/getConfigById/{configId}")
    public R<SysConfig> getConfigById(@PathVariable("configId") Long configId) {
        return R.ok(configService.selectConfigById(configId));
    }
}
