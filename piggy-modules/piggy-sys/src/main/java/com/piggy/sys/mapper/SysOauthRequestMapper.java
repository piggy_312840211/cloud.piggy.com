package com.piggy.sys.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.sys.api.domain.SysOauthRequest;

/**
 * 终端授权申请Mapper接口
 *
 * @author piggy
 * @date 2022-09-21
 */
public interface SysOauthRequestMapper extends BaseMapperPlus<SysOauthRequest> {

}
