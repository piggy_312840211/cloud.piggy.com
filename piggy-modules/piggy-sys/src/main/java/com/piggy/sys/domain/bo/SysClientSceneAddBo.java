package com.piggy.sys.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;



/**
 * 终端配置场景部门添加对象 sys_client_scene
 *
 * @author piggy
 * @date 2022-09-19
 */
@Data
@ApiModel("终端配置场景部门添加对象")
public class SysClientSceneAddBo {


    /** 终端编号 */
    @ApiModelProperty("终端编号")
    @NotBlank(message = "终端编号不能为空")
    private String clientId;

    /** 场景id */
    @ApiModelProperty("场景id")
    @NotNull(message = "场景id不能为空")
    private Long sceneId;

    /** 核心企业部门id */
    @ApiModelProperty("核心企业部门id")
    @NotNull(message = "核心企业部门id不能为空")
    private Long coreDeptId;

    /** 是否停用 1 启用 0 停用 */
    @ApiModelProperty("是否停用 1 启用 0 停用")
    private Integer validFlag;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
