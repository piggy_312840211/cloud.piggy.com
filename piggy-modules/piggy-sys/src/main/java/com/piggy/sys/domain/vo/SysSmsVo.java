package com.piggy.sys.domain.vo;

import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 消息模板视图对象 sys_sms
 *
 * @author shark
 * @date 2021-08-16
 */
@Data
@ApiModel("消息模板视图对象")
public class SysSmsVo {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	@ApiModelProperty("主键")
	private Long id;

	/** 主题 */
	@Excel(name = "主题")
	@ApiModelProperty("主题")
	private String subject;

	/** 参数内容 */
	@Excel(name = "参数内容")
	@ApiModelProperty("参数内容")
	private String params;

	@Excel(name = "签名")
	@ApiModelProperty("签名")
	private String signName;

	/** 接收人用户id */
	@Excel(name = "接收人用户id")
	@ApiModelProperty("接收人用户id")
	private Long userId;

	/** 模板id */
	@Excel(name = "模板id")
	@ApiModelProperty("模板id")
	private String templateId;

	/** 手机或邮箱 */
	@Excel(name = "手机或邮箱")
	@ApiModelProperty("手机或邮箱")
	private String link;

	/** 发送返回结果 */
	@Excel(name = "发送返回结果")
	@ApiModelProperty("发送返回结果")
	private String sendResult;

	/** 状态 */
	@Excel(name = "状态")
	@ApiModelProperty("状态")
	private Integer status;

	/** 消息类型 */
	@Excel(name = "消息类型")
	@ApiModelProperty("消息类型")
	private Integer type;

	/** AccessKeyID */
	@Excel(name = "AccessKeyID")
	@ApiModelProperty("AccessKeyID")
	private String accessKeyId;

	/** AccessKeySecret */
	@Excel(name = "AccessKeySecret")
	@ApiModelProperty("AccessKeySecret")
	private String accessKeySecret;

	/** 备注 */
	@Excel(name = "备注")
	@ApiModelProperty("备注")
	private String remark;


}
