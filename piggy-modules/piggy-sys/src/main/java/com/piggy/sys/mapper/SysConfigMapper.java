package com.piggy.sys.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.sys.api.domain.SysConfig;

/**
 * 参数配置 数据层
 *
 * @author shark
 */
public interface SysConfigMapper extends BaseMapperPlus<SysConfig> {

}
