package com.piggy.im.server.configuration;

import com.piggy.im.domain.bo.SmsMessage;
import com.piggy.im.server.dispatcher.MessageDispatcher;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author: liuyadu
 * @date: 2018/11/27 14:45
 * @description:
 */
@SpringBootTest
public class SmsTest  {
    @Resource
    private MessageDispatcher dispatcher;

    @Test
    public void testSms() {
        SmsMessage smsNotify = new SmsMessage();
        smsNotify.setPhoneNum("18501115509");
//        smsNotify.setSignName("测试");
        smsNotify.setTemplateCode("{\"code\":\"4279\"}");
        this.dispatcher.dispatch(smsNotify);
        try {
            Thread.sleep(500L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
