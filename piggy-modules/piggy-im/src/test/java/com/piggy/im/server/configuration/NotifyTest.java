package com.piggy.im.server.configuration;

import com.piggy.im.server.service.DelayMessageService;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.HashMap;

/**
 * @author: liuyadu
 * @date: 2019/2/19 15:23
 * @description:
 */
public class NotifyTest {
    @Resource
    private DelayMessageService messageSender;

    @Test
    public void httpNotify() throws Exception {
        messageSender.send("http://www.baidu.com/notity/callback", "order_pay", new HashMap<>());
        System.out.println("发送成功");
        Thread.sleep(500000);
    }
}
