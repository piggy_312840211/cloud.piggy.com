package com.piggy.im.server.configuration;

import com.piggy.im.domain.bo.EmailMessage;
import com.piggy.im.server.dispatcher.MessageDispatcher;

import javax.annotation.Resource;

/**
 * @author: liuyadu
 * @date: 2018/11/27 14:45
 * @description:
 */
public class MailTest  {
    @Resource
    private MessageDispatcher dispatcher;

//    @Test
    public void testMail() {
        EmailMessage message = new EmailMessage();
        message.setTo(new String[]{"729466338@qq.com"});
        message.setSubject("测试");
        message.setContent("测试内容");
        this.dispatcher.dispatch(message);
        try {
            Thread.sleep(50000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
