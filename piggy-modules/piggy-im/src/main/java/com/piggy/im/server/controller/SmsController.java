package com.piggy.im.server.controller;

import cn.hutool.json.JSONUtil;
import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.utils.sms.SmsConfig;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.im.domain.bo.SmsMessage;
import com.piggy.im.server.dispatcher.MessageDispatcher;
import com.piggy.im.service.ISmsClient;
import com.piggy.sys.api.RemoteISysSmsService;
import com.piggy.common.core.domain.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 推送通知
 *
 * @author woodev
 */
@RestController
@Api(value = "短信", tags = "短信")
public class SmsController extends BaseController implements ISmsClient {


    @Resource
    private MessageDispatcher dispatcher;

    @Resource
    private RemoteISysSmsService sysSmsService;

    /**
     * 短信通知
     *smsMessage
     * @return
     */
    @ApiOperation(value = "发送短信", notes = "发送短信")
    @PostMapping("/sms")
    @Override
    public R send(@RequestBody  SmsMessage smsMessage) {
        this.dispatcher.dispatch(smsMessage);
        return success();
    }

    @Override
    @ApiOperation(value = "批量发送短信", notes = "批量发送短信")
    @PostMapping("/sendBatchSms")
    public R sendBatchSms(SmsMessage messages) {
        this.dispatcher.dispatch(messages);
        return success();
    }

    /**
     * 获取消息详细信息
     */
    @ApiOperation("获取短信配置")
    @GetMapping("/config")
    public R<String> getConfig() {
        R<SmsConfig> config = sysSmsService.getConfig(SecurityConstants.INNER);
        return ok(JSONUtil.toJsonStr(config.getData()));
    }

}
