package com.piggy.im.server.configuration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * @author woodev
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Configuration
@RefreshScope
@ConfigurationProperties(prefix="piggy.sms.aliyun")
public class AliyunSmsProperties {

	private String accessKeyId ;
	private String accessKeySecret;
	private String SignName;
	private boolean openSend;
}
