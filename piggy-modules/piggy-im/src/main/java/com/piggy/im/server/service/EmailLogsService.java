package com.piggy.im.server.service;

import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.im.domain.EmailLogs;

/**
 * 邮件发送日志 服务类
 *
 * @author admin
 * @date 2019-07-25
 */
public interface EmailLogsService extends IServicePlus<EmailLogs> {

}
