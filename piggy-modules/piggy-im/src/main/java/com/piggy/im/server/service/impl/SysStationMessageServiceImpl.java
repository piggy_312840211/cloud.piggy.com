package com.piggy.im.server.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.PagePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.satoken.utils.SecurityUtils;
import com.piggy.im.server.enums.MarkEnum;
import com.piggy.im.server.mapper.SysStationMessageMapper;
import com.piggy.im.domain.SysStationMessage;
import com.piggy.im.domain.bo.SysStationMessageAddBo;
import com.piggy.im.domain.bo.SysStationMessageEditBo;
import com.piggy.im.domain.bo.SysStationMessageQueryBo;
import com.piggy.im.domain.vo.SysStationMessageVo;
import com.piggy.im.server.service.ISysStationMessageService;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 站内消息Service业务层处理
 *
 * @author piggy
 * @date 2021-12-09
 */
@Service
public class SysStationMessageServiceImpl extends ServiceImpl<SysStationMessageMapper, SysStationMessage> implements ISysStationMessageService {

    @Override
    public SysStationMessageVo queryById(Long id){
        return getVoById(id, SysStationMessageVo.class);
    }

    @Override
    public TableDataInfo<SysStationMessageVo> queryPageList(SysStationMessageQueryBo bo) {
        if (ObjectUtil.isEmpty(bo.getReceiveId())) {
            Long userId = SecurityUtils.getUserId();
            bo.setReceiveId(userId);
        }
        PagePlus<SysStationMessage, SysStationMessageVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(bo), SysStationMessageVo.class);
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<SysStationMessageVo> queryList(SysStationMessageQueryBo bo) {
        return listVo(buildQueryWrapper(bo), SysStationMessageVo.class);
    }

    private LambdaQueryWrapper<SysStationMessage> buildQueryWrapper(SysStationMessageQueryBo bo) {
        Map<String, Object> params = bo.getReqParams();
        LambdaQueryWrapper<SysStationMessage> lqw = Wrappers.lambdaQuery();
        lqw.eq(Objects.nonNull(bo.getId()),SysStationMessage::getId,bo.getId());
        lqw.eq(StrUtil.isNotBlank(bo.getLevel()), SysStationMessage::getLevel, bo.getLevel());
        lqw.eq(StrUtil.isNotBlank(bo.getTitle()), SysStationMessage::getTitle, bo.getTitle());
        lqw.eq(StrUtil.isNotBlank(bo.getContent()), SysStationMessage::getContent, bo.getContent());
        lqw.eq(bo.getMark() != null, SysStationMessage::getMark, bo.getMark());
        lqw.eq(bo.getReceiveId() != null, SysStationMessage::getReceiveId, bo.getReceiveId());
        lqw.eq(bo.getMsgType() != null, SysStationMessage::getMsgType, bo.getMsgType());
        lqw.orderByDesc(SysStationMessage::getCreateTime);
        return lqw;
    }

    @Override
    public Boolean insertByAddBo(SysStationMessageAddBo bo) {
        SysStationMessage add = BeanUtil.toBean(bo, SysStationMessage.class);
        validEntityBeforeSave(add);
        return save(add);
    }

    @Override
    public Boolean updateByEditBo(SysStationMessageEditBo bo) {
        SysStationMessage update = BeanUtil.toBean(bo, SysStationMessage.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(SysStationMessage entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }

    @Override
    public Boolean haveRead(Collection<Long> ids) {
        if (CollectionUtil.isEmpty(ids)) {
            throw new BaseException("未读id列表不能为空");
        }

        List<SysStationMessage> updateList = ids.stream().map(item -> {
            SysStationMessage update = new SysStationMessage();
            update.setId(item);
            update.setMark(MarkEnum.HAVE_READ.getType());
            update.setUpdateTime(new Date());
            return update;
        }).collect(Collectors.toList());
        return updateBatchById(updateList);
    }

    @Override
    public Integer unreadCount() {
        Long userId = SecurityUtils.getUserId();
        //获取当前用户未读消息个数
        Long count = count(new LambdaQueryWrapper<SysStationMessage>()
                .eq(SysStationMessage::getReceiveId, userId)
                .eq(SysStationMessage::getMark, MarkEnum.UNREAD.getType()));
        return count.intValue();
    }
}
