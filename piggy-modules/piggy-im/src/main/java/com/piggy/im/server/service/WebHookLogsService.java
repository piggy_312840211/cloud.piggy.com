package com.piggy.im.server.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.im.domain.WebHookLogs;

/**
 * 异步通知日志接口
 *
 * @author: liuyadu
 * @date: 2019/2/13 14:39
 * @description:
 */
public interface WebHookLogsService extends IServicePlus<WebHookLogs> {

    /**
     * 添加日志
     *
     * @param log
     */
    void addLog(WebHookLogs log);

    /**
     * 更细日志
     *
     * @param log
     */
    void modifyLog(WebHookLogs log);


    /**
     * 根据主键获取日志
     *
     * @param logId
     * @return
     */
    WebHookLogs getLog(String logId);

    /**
     * 分页查询
     *
     * @param pageParams
     * @return
     */
    TableDataInfo<WebHookLogs> findListPage(WebHookLogs webHookLogs);

}
