package com.piggy.im.server.exchanger;


import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson2.JSONObject;
import com.piggy.im.domain.bo.BaseMessage;
import com.piggy.im.domain.bo.EmailMessage;
import com.piggy.im.domain.bo.EmailTplMessage;
import com.piggy.im.domain.EmailConfig;
import com.piggy.im.domain.EmailLogs;
import com.piggy.im.domain.EmailTemplate;
import com.piggy.im.server.service.EmailConfigService;
import com.piggy.im.server.service.EmailLogsService;
import com.piggy.im.server.service.EmailSender;
import com.piggy.im.server.service.EmailTemplateService;
import freemarker.cache.StringTemplateLoader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.util.StringUtils;

import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.StringWriter;
import java.util.*;

/**
 * @author woodev
 */
@Slf4j
public class EmailExchanger implements MessageExchanger {

    private EmailSender mailSender;
    private EmailConfigService emailConfigService;
    private EmailTemplateService emailTemplateService;
    private EmailLogsService emailLogsService;


    public EmailExchanger(EmailSender mailSender, EmailConfigService emailConfigService, EmailTemplateService emailTemplateService, EmailLogsService emailLogsService) {
        this.mailSender = mailSender;
        this.emailConfigService = emailConfigService;
        this.emailTemplateService = emailTemplateService;
        this.emailLogsService = emailLogsService;
    }

    @Override
    public boolean support(Object message) {
        return message instanceof EmailMessage;
    }

    @Override
    public boolean exchange(BaseMessage message) {
        Assert.notNull(mailSender, "邮件接口没有初始化");
        EmailMessage emailMessage = (EmailMessage) message;
        String error = null;
        Integer result = 0;
        EmailTemplate emailTemplate = null;
        EmailConfig emailConfig = null;
        Map<String, Object> configMap = new HashMap<>();
        Map<String, EmailConfig> senderMap = new HashMap<>();
        try {
            List<EmailConfig> configList = emailConfigService.getCacheConfig();
            for (EmailConfig config : configList) {
                if (config.getIsDefault().intValue() == 1) {
                    // 增加一条默认配置
                    senderMap.put("default", config);
                }
                senderMap.put(config.getConfigId().toString(), config);
            }
            // 模板消息
            if (emailMessage instanceof EmailTplMessage) {
                EmailTplMessage tplMessage = (EmailTplMessage) emailMessage;
                emailTemplate = emailTemplateService.getByCode(tplMessage.getTplCode());
                emailConfig = senderMap.get(emailTemplate.getConfigId());
                String content = freemarkerProcess(tplMessage.getTplParams(), emailTemplate.getTemplate());
                emailMessage.setContent(content);
            }
            if (emailConfig == null) {
                emailConfig = senderMap.get("default");
            }
            // 构建发送者
            JavaMailSenderImpl javaMailSender = buildMailSender(emailConfig);
            mailSender.sendSimpleMail(javaMailSender, emailMessage);
            result = 1;
        } catch (Exception e) {
            error = e.getMessage();
            log.error("发送错误:{}", e);

        } finally {
            // 保存发送日志
            try {
                configMap.put("template", emailTemplate);
                configMap.put("config", emailConfig);
                EmailLogs logs = new EmailLogs();
                logs.setSendTo(StringUtils.arrayToDelimitedString(emailMessage.getTo(), ";"));
                logs.setSendCc(StringUtils.arrayToDelimitedString(emailMessage.getCc(), ";"));
                logs.setSubject(emailMessage.getSubject());
                logs.setContent(emailMessage.getContent());
                logs.setError(error);
                logs.setResult(result);
                logs.setSendNums(1);
                logs.setTplCode(emailTemplate != null ? emailTemplate.getCode() : null);
                logs.setConfig(JSONObject.toJSONString(configMap));
                logs.setAttachments(JSONObject.toJSONString(emailMessage.getAttachments()));
                logs.setCreateTime(new Date());
                logs.setUpdateTime(logs.getCreateTime());
                emailLogsService.save(logs);
            } catch (Exception e) {
                log.error("邮箱日志错误:{}", e);
            }
        }
        return result == 1;
    }


    private JavaMailSenderImpl buildMailSender(EmailConfig config) {
        if (config == null) {
            throw new RuntimeException("缺少默认邮件服务器配置");
        }
        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setHost(config.getSmtpHost());
        sender.setPort(465);
        sender.setUsername(config.getSmtpUsername());
        sender.setPassword(config.getSmtpPassword());
        sender.setDefaultEncoding("UTF-8");
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.starttls.required", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.fallback", "false");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        sender.setJavaMailProperties(props);
        return sender;
    }

    /**
     * freemark处理文字
     *
     * @param input
     * @param templateStr
     * @return
     */
    public static String freemarkerProcess(Map input, String templateStr) {
        StringTemplateLoader stringLoader = new StringTemplateLoader();
        String template = "content";
        stringLoader.putTemplate(template, templateStr);
        Configuration cfg = new Configuration();
        cfg.setTemplateLoader(stringLoader);
        try {
            Template templateCon = cfg.getTemplate(template, "UTF-8");
            StringWriter writer = new StringWriter();
            templateCon.process(input, writer);
            return writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
