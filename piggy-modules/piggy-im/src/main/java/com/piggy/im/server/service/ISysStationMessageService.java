package com.piggy.im.server.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.im.domain.SysStationMessage;
import com.piggy.im.domain.bo.SysStationMessageAddBo;
import com.piggy.im.domain.bo.SysStationMessageEditBo;
import com.piggy.im.domain.bo.SysStationMessageQueryBo;
import com.piggy.im.domain.vo.SysStationMessageVo;

import java.util.Collection;
import java.util.List;

/**
 * 站内消息Service接口
 *
 * @author piggy
 * @date 2021-12-09
 */
public interface ISysStationMessageService extends IServicePlus<SysStationMessage> {
	/**
	 * 查询单个
	 * @return
	 */
	SysStationMessageVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<SysStationMessageVo> queryPageList(SysStationMessageQueryBo bo);

	/**
	 * 查询列表
	 */
	List<SysStationMessageVo> queryList(SysStationMessageQueryBo bo);

	/**
	 * 根据新增业务对象插入站内消息
	 * @param bo 站内消息新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(SysStationMessageAddBo bo);

	/**
	 * 根据编辑业务对象修改站内消息
	 * @param bo 站内消息编辑业务对象
	 * @return
	 */
	Boolean updateByEditBo(SysStationMessageEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	/**
	 * 已读消息
	 * @param ids
	 * @return
	 */
	Boolean haveRead(Collection<Long> ids);

	/**
	 * 未读消息个数
	 * @return
	 */
	Integer unreadCount();
}
