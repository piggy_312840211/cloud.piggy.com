package com.piggy.im.server.exchanger;


import com.piggy.im.domain.bo.BaseMessage;

/**
 * @author woodev
 */

public interface MessageExchanger {

    boolean support(Object message);

    boolean exchange(BaseMessage message);
}
