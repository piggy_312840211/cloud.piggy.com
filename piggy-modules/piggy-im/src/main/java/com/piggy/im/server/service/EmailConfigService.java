package com.piggy.im.server.service;

import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.im.domain.EmailConfig;

import java.util.List;

/**
 * 邮件发送配置 服务类
 *
 * @author admin
 * @date 2019-07-25
 */
public interface EmailConfigService extends IServicePlus<EmailConfig> {
    /**
     * 加载缓存配置
     */
   void loadCacheConfig();

    /**
     * 获取缓存的配置
     *
     * @return
     */
    List<EmailConfig> getCacheConfig();
}
