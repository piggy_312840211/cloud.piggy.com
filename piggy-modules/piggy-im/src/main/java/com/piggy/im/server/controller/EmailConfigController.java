package com.piggy.im.server.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.im.domain.EmailConfig;
import com.piggy.im.server.service.EmailConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * 邮件发送配置 前端控制器
 *
 * @author admin
 * @date 2019-07-25
 */
@Api(value = "邮件发送配置", tags = "邮件发送配置")
@RestController
@RequestMapping("/emailConfig")
public class EmailConfigController extends BaseController {

    @Resource
    private EmailConfigService targetService;

    /**
     * 获取分页数据
     *
     * @return
     */
    @ApiOperation(value = "获取分页数据", notes = "获取分页数据")
    @GetMapping(value = "/list")
    public TableDataInfo<EmailConfig> list(@RequestParam(required = false) EmailConfig emailConfig) {
//        PageParams pageParams = new PageParams(map);
//        EmailConfig query = pageParams.mapToObject(EmailConfig.class);
        QueryWrapper<EmailConfig> queryWrapper = new QueryWrapper();
        return PageUtils.buildDataInfo(targetService.page(PageUtils.buildPage(), queryWrapper));
//        return ResultBody.ok().data(targetService.page(new PageParams(map), queryWrapper));
    }

    /**
     * 根据ID查找数据
     */
    @ApiOperation(value = "根据ID查找数据", notes = "根据ID查找数据")
    @ResponseBody
    @RequestMapping("/get")
    public R<EmailConfig> get(@RequestParam("id") Long id) {
        EmailConfig entity = targetService.getById(id);
        return success(entity);
    }

    /**
     * 添加数据
     *
     * @return
     */
    @ApiOperation(value = "添加数据", notes = "添加数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", required = true, value = "配置名称", paramType = "form"),
            @ApiImplicitParam(name = "smtpHost", required = true, value = "发件服务器域名", paramType = "form"),
            @ApiImplicitParam(name = "smtpUsername", required = true, value = "发件服务器账户", paramType = "form"),
            @ApiImplicitParam(name = "smtpPassword", required = true, value = "发件服务器密码", paramType = "form")
    }
    )
    @PostMapping("/add")
    public R add(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "smtpHost") String smtpHost,
            @RequestParam(value = "smtpUsername") String smtpUsername,
            @RequestParam(value = "smtpPassword") String smtpPassword
    ) {
        EmailConfig entity = new EmailConfig();
        entity.setName(name);
        entity.setSmtpHost(smtpHost);
        entity.setSmtpUsername(smtpUsername);
        entity.setSmtpPassword(smtpPassword);
        targetService.save(entity);
        return success();
    }

    /**
     * 更新数据
     *
     * @return
     */
    @ApiOperation(value = "更新数据", notes = "更新数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "configId", required = true, value = "", paramType = "form"),
            @ApiImplicitParam(name = "name", required = true, value = "配置名称", paramType = "form"),
            @ApiImplicitParam(name = "smtpHost", required = true, value = "发件服务器域名", paramType = "form"),
            @ApiImplicitParam(name = "smtpUsername", required = true, value = "发件服务器账户", paramType = "form"),
            @ApiImplicitParam(name = "smtpPassword", required = true, value = "发件服务器密码", paramType = "form") })
    @PostMapping("/update")
    public R add(
            @RequestParam(value = "configId") Long configId,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "smtpHost") String smtpHost,
            @RequestParam(value = "smtpUsername") String smtpUsername,
            @RequestParam(value = "smtpPassword") String smtpPassword ) {

        EmailConfig entity = new EmailConfig();
        entity.setConfigId(configId);
        entity.setName(name);
        entity.setSmtpHost(smtpHost);
        entity.setSmtpUsername(smtpUsername);
        entity.setSmtpPassword(smtpPassword);
        targetService.updateById(entity);
        return success();

    }

    /**
     * 删除数据
     *
     * @return
     */
    @ApiOperation(value = "删除数据", notes = "删除数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", required = true, value = "id", paramType = "form")
    })
    @PostMapping("/remove")
    public R remove(
            @RequestParam(value = "id") Long id) {
        targetService.removeById(id);
        return success();
    }

    /**
     * 批量删除数据
     *
     * @return
     */
    @ApiOperation(value = "批量删除数据", notes = "批量删除数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", required = true, value = "id", paramType = "form")
    })
    @PostMapping("/batch/remove")
    public R batchRemove(
            @RequestParam(value = "ids") String ids
    ) {
        targetService.removeByIds(Arrays.asList(ids.split(",")));
        return success();
    }
}
