package com.piggy.im.server;

import com.piggy.common.satoken.annotation.EnableCustomConfig;
import com.piggy.common.satoken.annotation.EnableCustomFeignClients;
import com.piggy.common.swagger.annotation.EnableCustomSwagger2;
import com.piggy.im.server.service.EmailConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import javax.annotation.Resource;


/**
 * @author liuyadu
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableCustomFeignClients
@RefreshScope
@SpringBootApplication
public class PIGYImApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(PIGYImApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  消息中心启动成功   ლ(´ڡ`ლ)ﾞ  \n" );
    }

    @Resource
    private EmailConfigService emailConfigService;

    @Override
    public void run(String... strings) throws Exception {
        emailConfigService.loadCacheConfig();
    }
}
