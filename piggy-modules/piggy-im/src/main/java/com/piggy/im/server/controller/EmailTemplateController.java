package com.piggy.im.server.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.im.domain.EmailTemplate;
import com.piggy.im.server.service.EmailTemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * 邮件模板配置 前端控制器
 *
 * @author admin
 * @date 2019-07-25
 */
@Api(value = "邮件模板配置", tags = "邮件模板配置")
@RestController
@RequestMapping("/emailTemplate")
public class EmailTemplateController extends BaseController {

    @Resource
    private EmailTemplateService targetService;

    /**
     * 获取分页数据
     *
     * @return
     */
    @ApiOperation(value = "获取分页数据", notes = "获取分页数据")
    @GetMapping(value = "/list")
    public TableDataInfo list(@RequestParam(required = false) EmailTemplate emailTemplate) {
        QueryWrapper<EmailTemplate> queryWrapper = new QueryWrapper();
        return  PageUtils.buildDataInfo(targetService.page(PageUtils.buildPage(), queryWrapper));


//        PageParams pageParams = new PageParams(map);
//        EmailTemplate query = pageParams.mapToObject(EmailTemplate.class);
//        QueryWrapper<EmailTemplate> queryWrapper = new QueryWrapper();
//        return R.ok().data(targetService.page(new PageParams(map), queryWrapper));
    }

    /**
     * 根据ID查找数据
     */
    @ApiOperation(value = "根据ID查找数据", notes = "根据ID查找数据")
    @ResponseBody
    @RequestMapping("/get")
    public R<EmailTemplate> get(@RequestParam("id") Long id) {
        EmailTemplate entity = targetService.getById(id);
        return success(entity);
    }

    /**
     * 添加数据
     *
     * @return
     */
    @ApiOperation(value = "添加数据", notes = "添加数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", required = true, value = "模板名称", paramType = "form"),
            @ApiImplicitParam(name = "code", required = true, value = "模板编码", paramType = "form"),
            @ApiImplicitParam(name = "configId", required = true, value = "发送服务器配置", paramType = "form"),
            @ApiImplicitParam(name = "template", required = true, value = "模板", paramType = "form"),
            @ApiImplicitParam(name = "reqParams", required = true, value = "模板参数", paramType = "form")
    }
    )
    @PostMapping("/add")
    public R add(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "code") String code,
            @RequestParam(value = "configId") Long configId,
            @RequestParam(value = "template") String template,
            @RequestParam(value = "params") String params
    ) {
        EmailTemplate entity = new EmailTemplate();
        entity.setName(name);
        entity.setCode(code);
        entity.setConfigId(configId);
        entity.setTemplate(template);
        entity.setMsgParams(params);
        targetService.save(entity);
        return success();
    }

    /**
     * 更新数据
     *
     * @return
     */
    @ApiOperation(value = "更新数据", notes = "更新数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tplId", required = true, value = "", paramType = "form"),
            @ApiImplicitParam(name = "name", required = true, value = "模板名称", paramType = "form"),
            @ApiImplicitParam(name = "code", required = true, value = "模板编码", paramType = "form"),
            @ApiImplicitParam(name = "configId", required = true, value = "发送服务器配置", paramType = "form"),
            @ApiImplicitParam(name = "template", required = true, value = "模板", paramType = "form"),
            @ApiImplicitParam(name = "reqParams", required = true, value = "模板参数", paramType = "form")
    })
    @PostMapping("/update")
    public R add(
            @RequestParam(value = "tplId") Long tplId,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "code") String code,
            @RequestParam(value = "configId") Long configId,
            @RequestParam(value = "template") String template,
            @RequestParam(value = "params") String params
    ) {
        EmailTemplate entity = new EmailTemplate();
        entity.setTplId(tplId);
        entity.setName(name);
        entity.setCode(code);
        entity.setConfigId(configId);
        entity.setTemplate(template);
        entity.setMsgParams(params);
        targetService.updateById(entity);
        return success();
    }

    /**
     * 删除数据
     *
     * @return
     */
    @ApiOperation(value = "删除数据", notes = "删除数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", required = true, value = "id", paramType = "form")
    })
    @PostMapping("/remove")
    public R remove(
            @RequestParam(value = "id") Long id
    ) {
        targetService.removeById(id);
        return success();
    }

    /**
     * 批量删除数据
     *
     * @return
     */
    @ApiOperation(value = "批量删除数据", notes = "批量删除数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", required = true, value = "id", paramType = "form")
    })
    @PostMapping("/batch/remove")
    public R batchRemove(
            @RequestParam(value = "ids") String ids
    ) {
        targetService.removeByIds(Arrays.asList(ids.split(",")));
        return success();
    }

}
