package com.piggy.im.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.im.domain.WebHookLogs;
import com.piggy.im.server.mapper.WebHookLogsMapper;
import com.piggy.im.server.service.WebHookLogsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 异步通知日志接口
 *
 * @author: liuyadu
 * @date: 2019/2/13 14:39
 * @description:
 */
@Service
public class WebHookLogsServiceImpl extends ServiceImpl<WebHookLogsMapper, WebHookLogs> implements WebHookLogsService {
    @Resource
    private WebHookLogsMapper webHookLogsMapper;

    /**
     * 添加日志
     *
     * @param log
     */
    @Override
    public void addLog(WebHookLogs log) {
        webHookLogsMapper.insert(log);
    }

    /**
     * 更细日志
     *
     * @param log
     */
    @Override
    public void modifyLog(WebHookLogs log) {
        webHookLogsMapper.updateById(log);
    }

    /**
     * 根据主键获取日志
     *
     * @param logId
     * @return
     */
    @Override
    public WebHookLogs getLog(String logId) {
        return webHookLogsMapper.selectById(logId);
    }

    /**
     * 分页查询
     *
     * @param  webHookLogs
     * @return
     */
    @Override
    public TableDataInfo<WebHookLogs> findListPage(WebHookLogs webHookLogs) {
        QueryWrapper<WebHookLogs> queryWrapper = new QueryWrapper();
        queryWrapper.lambda()
                .likeRight(ObjectUtils.isNotEmpty(webHookLogs.getUrl()), WebHookLogs::getUrl, webHookLogs.getUrl())
                .eq(ObjectUtils.isNotEmpty(webHookLogs.getType()), WebHookLogs::getType, webHookLogs.getType())
                .eq(ObjectUtils.isNotEmpty(webHookLogs.getResult()), WebHookLogs::getResult, webHookLogs.getResult());
        return PageUtils.buildDataInfo(page(PageUtils.buildPage(), queryWrapper));
//        return webHookLogsMapper.selectPage(new Page(pageParams.getPage(), pageParams.getLimit()), queryWrapper);
    }
}
