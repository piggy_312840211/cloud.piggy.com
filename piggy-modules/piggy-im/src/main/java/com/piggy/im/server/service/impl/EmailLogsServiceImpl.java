package com.piggy.im.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piggy.im.domain.EmailLogs;
import com.piggy.im.server.mapper.EmailLogsMapper;
import com.piggy.im.server.service.EmailLogsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 邮件发送日志 服务实现类
 *
 * @author liuyadu
 * @date 2019-07-17
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class EmailLogsServiceImpl extends ServiceImpl<EmailLogsMapper, EmailLogs> implements EmailLogsService {

}
