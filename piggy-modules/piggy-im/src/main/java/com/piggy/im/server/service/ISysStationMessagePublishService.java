package com.piggy.im.server.service;


import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.im.domain.SysStationMessagePublish;
import com.piggy.im.domain.bo.SysPublishMsgBo;
import com.piggy.im.domain.bo.SysStationMessagePublishAddBo;
import com.piggy.im.domain.bo.SysStationMessagePublishEditBo;
import com.piggy.im.domain.bo.SysStationMessagePublishQueryBo;
import com.piggy.im.domain.vo.ReceiverInfoVo;
import com.piggy.im.domain.vo.SysStationMessagePublishVo;

import java.util.Collection;
import java.util.List;

/**
 * 站内消息发布Service接口
 *
 * @author piggy
 * @date 2021-12-09
 */
public interface ISysStationMessagePublishService extends IServicePlus<SysStationMessagePublish> {
	/**
	 * 查询单个
	 * @return
	 */
	SysStationMessagePublishVo queryById(Long id);

	/**
	 * 查询列表
	 */
    TableDataInfo<SysStationMessagePublishVo> queryPageList(SysStationMessagePublishQueryBo bo);

	/**
	 * 查询列表
	 */
	List<SysStationMessagePublishVo> queryList(SysStationMessagePublishQueryBo bo);

	/**
	 * 根据新增业务对象插入站内消息发布
	 * @param bo 站内消息发布新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(SysStationMessagePublishAddBo bo);

	/**
	 * 根据编辑业务对象修改站内消息发布
	 * @param bo 站内消息发布编辑业务对象
	 * @return
	 */
	Boolean updateByEditBo(SysStationMessagePublishEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	void publish(Long id);

	List<ReceiverInfoVo> getMessageReceiveList(String receiverType);

	/**
	 * 消息推送
	 * @param sysPublishMsgBo
	 */
	void publishMsg(SysPublishMsgBo sysPublishMsgBo);
}
