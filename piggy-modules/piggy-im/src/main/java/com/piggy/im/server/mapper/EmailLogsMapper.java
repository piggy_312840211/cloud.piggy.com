package com.piggy.im.server.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.im.domain.EmailLogs;
import org.springframework.stereotype.Repository;

/**
 * @author liuyadu
 */
@Repository
public interface EmailLogsMapper extends BaseMapperPlus<EmailLogs> {
}
