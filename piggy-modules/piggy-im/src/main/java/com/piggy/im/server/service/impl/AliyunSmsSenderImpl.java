package com.piggy.im.server.service.impl;


import cn.hutool.core.lang.Console;
import cn.hutool.json.JSONUtil;
import com.aliyun.dysmsapi20170525.models.SendBatchSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendBatchSmsResponse;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.piggy.common.core.utils.sms.AliSendSmsUtils;
import com.piggy.im.domain.bo.SmsMessage;
import com.piggy.im.server.service.SmsSender;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Collectors;


/**
 * @author woodev
 */
@Data
@Slf4j
public class AliyunSmsSenderImpl implements SmsSender {


    private String accessKeyId ;

    private String accessKeySecret;

    private String SignName;

    private boolean openSend;

    private final static String OK = "OK";

    private final static String CODE = "Code";

    public AliyunSmsSenderImpl() {
        log.info("init aliyunSMS sender:" + this);
    }

    @SneakyThrows
    @Override
    public Boolean send(SmsMessage bo) {
        if (!openSend) return true;
        com.aliyun.dysmsapi20170525.Client client = AliSendSmsUtils.createClient(accessKeyId,accessKeySecret);
            SendSmsRequest sendReq = new SendSmsRequest()
                    .setPhoneNumbers(bo.getPhoneNum())
                    .setTemplateCode(bo.getTemplateCode())
                    .setSignName(bo.getSignName())
                    .setTemplateParam(bo.getTemplateParam());

        //send
        SendSmsResponse sendResp = client.sendSms(sendReq);
        String code = sendResp.body.code;

        //result
        if (!com.aliyun.teautil.Common.equalString(code, OK)) {
            Console.error("错误信息: " + sendResp.body.message + "");
            return false;
        }
        return true;
    }

    @SneakyThrows
    @Override
    public Boolean SendBatchSms(SmsMessage bo) {
        if (!openSend) return true;
        com.aliyun.dysmsapi20170525.Client client = AliSendSmsUtils.createClient(accessKeyId,accessKeySecret);
        SendBatchSmsRequest sendReq = new SendBatchSmsRequest()
                .setPhoneNumberJson(JSONUtil.toJsonStr(bo.getPhoneNumbers()))
                .setSignNameJson(JSONUtil.toJsonStr(bo.getSignNames()))
                .setTemplateCode(bo.getTemplateCode())
                .setTemplateParamJson(JSONUtil.toJsonStr(bo.getTemplateParams()));

        SendBatchSmsResponse sendResp = client.sendBatchSms(sendReq);
        String code = sendResp.body.code;

        if (!com.aliyun.teautil.Common.equalString(code, OK)) {
            Console.error("错误信息: " + sendResp.body.message + "");
            return false;
        }
        return true;
    }

    @SneakyThrows
    @Override
    public Boolean SendBatchSms(List<SmsMessage> parameter,String templateCode) {
        List<String> phoneNumbers = parameter.parallelStream().map(SmsMessage::getPhoneNum).collect(Collectors.toList());
        List<String> signNames = parameter.parallelStream().map(SmsMessage::getSignName).collect(Collectors.toList());
        List<String> templateParams = parameter.parallelStream().map(SmsMessage::getTemplateParam).collect(Collectors.toList());

        if (!isOpenSend()) return true;
        com.aliyun.dysmsapi20170525.Client client = AliSendSmsUtils.createClient(accessKeyId,accessKeySecret);
        SendBatchSmsRequest sendReq = new SendBatchSmsRequest()
                .setPhoneNumberJson(JSONUtil.toJsonStr(phoneNumbers))
                .setSignNameJson(JSONUtil.toJsonStr(signNames))
                .setTemplateCode(templateCode)
                .setTemplateParamJson(JSONUtil.toJsonStr(templateParams));

        SendBatchSmsResponse sendResp = client.sendBatchSms(sendReq);
        String code = sendResp.body.code;

        if (!com.aliyun.teautil.Common.equalString(code, "OK")) {
            Console.error("错误信息: " + sendResp.body.message + "");
            return false;
        }
        return true;
    }
}
