package com.piggy.im.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piggy.im.domain.EmailTemplate;
import com.piggy.im.server.mapper.EmailTemplateMapper;
import com.piggy.im.server.service.EmailTemplateService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 邮件模板配置 服务实现类
 *
 * @author liuyadu
 * @date 2019-07-17
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class EmailTemplateServiceImpl extends ServiceImpl<EmailTemplateMapper, EmailTemplate> implements EmailTemplateService {
    @Resource
    private EmailTemplateMapper emailTemplateMapper;

    /**
     * 根据模板编号获取模板
     *
     * @param code
     * @return
     */
    @Override
    public EmailTemplate getByCode(String code) {
        QueryWrapper<EmailTemplate> queryWrapper = new QueryWrapper();
        queryWrapper.eq("code", code);
        return emailTemplateMapper.selectOne(queryWrapper);
    }
}
