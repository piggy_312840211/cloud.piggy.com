package com.piggy.im.server.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import com.piggy.im.domain.bo.SysPublishMsgBo;
import com.piggy.im.domain.bo.SysStationMessagePublishAddBo;
import com.piggy.im.domain.bo.SysStationMessagePublishEditBo;
import com.piggy.im.domain.bo.SysStationMessagePublishQueryBo;
import com.piggy.im.domain.vo.ReceiverInfoVo;
import com.piggy.im.domain.vo.SysStationMessagePublishVo;
import com.piggy.im.server.service.ISysStationMessagePublishService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.redisson.api.RList;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 站内消息发布Controller
 *
 * @author piggy
 * @date 2021-12-09
 */
@Api(value = "站内消息发布控制器", tags = {"站内消息发布管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/SysStationMessagePublish")
public class SysStationMessagePublishController extends BaseController{

    private final ISysStationMessagePublishService iSysStationMessagePublishService;

    /**
     * 查询站内消息发布列表
     */
    @ApiOperation("查询站内消息发布列表")
    @SaCheckPermission("server:SysStationMessagePublish:list")
    @PostMapping("/list")
    public TableDataInfo<SysStationMessagePublishVo> list(@Validated @RequestBody SysStationMessagePublishQueryBo bo) {
        return iSysStationMessagePublishService.queryPageList(bo);
    }

    /**
     * 导出站内消息发布列表
     */
    @SneakyThrows
    @ApiOperation("导出站内消息发布列表")
    @SaCheckPermission("server:SysStationMessagePublish:export")
    @Log(title = "站内消息发布", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(HttpServletResponse response, @Validated SysStationMessagePublishQueryBo bo) {
        List<SysStationMessagePublishVo> list = iSysStationMessagePublishService.queryList(bo);
        ExcelUtil<SysStationMessagePublishVo> util = new ExcelUtil<>(SysStationMessagePublishVo.class);
         util.exportExcel(response,list, "站内消息发布");
    }

    /**
     * 获取站内消息发布详细信息
     */
    @ApiOperation("获取站内消息发布详细信息")
    @SaCheckPermission("server:SysStationMessagePublish:query")
    @GetMapping("/{id}")
    public R<SysStationMessagePublishVo> getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("id") Long id) {
        return success(iSysStationMessagePublishService.queryById(id));
    }

    /**
     * 新增站内消息发布
     */
    @ApiOperation("新增站内消息发布")
    @SaCheckPermission("server:SysStationMessagePublish:add")
    @Log(title = "站内消息发布", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public R add(@Validated @RequestBody SysStationMessagePublishAddBo bo) {
        return toAjax(iSysStationMessagePublishService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改站内消息发布
     */
    @ApiOperation("修改站内消息发布")
    @SaCheckPermission("server:SysStationMessagePublish:edit")
    @Log(title = "站内消息发布", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public R edit(@Validated @RequestBody SysStationMessagePublishEditBo bo) {
        return toAjax(iSysStationMessagePublishService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除站内消息发布
     */
    @ApiOperation("删除站内消息发布")
    @SaCheckPermission("server:SysStationMessagePublish:remove")
    @Log(title = "站内消息发布" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Integer> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable Long[] ids) {
        return toAjax(iSysStationMessagePublishService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }

    @PatchMapping("/{id}/publish")
    @Log(title = "发布消息通知")
    @Operation(summary = "发布消息通知")
    public void publish(@PathVariable Long id) {
        iSysStationMessagePublishService.publish(id);
    }

    @ApiOperation("查询消息接收列表")
    @SaCheckPermission("server:SysStationMessagePublish:getMessageReceiveList")
    @GetMapping("/getMessageReceiveList")
    public R<List<ReceiverInfoVo>> getMessageReceiveList(@RequestParam("receiverType") String receiverType) {
        return success(iSysStationMessagePublishService.getMessageReceiveList(receiverType));
    }

    /**
     * 推送消息
     */
    @PostMapping("/publishMsg")
    public void publishMsg(@RequestBody SysPublishMsgBo sysPublishMsgBo) {
        iSysStationMessagePublishService.publishMsg(sysPublishMsgBo);
    }


}
