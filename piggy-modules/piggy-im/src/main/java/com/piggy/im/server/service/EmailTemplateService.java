package com.piggy.im.server.service;

import com.piggy.common.core.web.page.IServicePlus;
import com.piggy.im.domain.EmailTemplate;

/**
 * 邮件模板配置 服务类
 *
 * @author admin
 * @date 2019-07-25
 */
public interface EmailTemplateService extends IServicePlus<EmailTemplate> {

    EmailTemplate getByCode(String code);
}
