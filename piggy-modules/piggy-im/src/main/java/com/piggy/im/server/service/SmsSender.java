package com.piggy.im.server.service;

import com.piggy.im.domain.bo.SmsMessage;

import java.util.List;

/**
 * @author woodev
 */
public interface SmsSender {

	/**
	 * 发送短信
	 * @param parameter
	 * @return
	 */
	Boolean send(SmsMessage parameter);

	/**
	 * 批量发送短信
	 * @param parameter
	 * @return
	 */
	Boolean SendBatchSms(SmsMessage parameter);

	/**
	 * 批量发送短信
	 * @param parameter
	 * @return
	 */
	Boolean SendBatchSms(List<SmsMessage> parameter,String templateCode);
}
