package com.piggy.im.server.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.PageUtils;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.im.domain.EmailLogs;
import com.piggy.im.server.service.EmailLogsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 邮件发送日志 前端控制器
 *
 * @author admin
 * @date 2019-07-25
 */
@Api(value = "邮件发送日志", tags = "邮件发送日志")
@RestController
@RequestMapping("/emailLogs")
    public class EmailLogsController {

    @Resource
    private EmailLogsService targetService;

    /**
     * 获取分页数据
     *
     * @return
     */
    @ApiOperation(value = "获取分页数据", notes = "获取分页数据")
    @GetMapping(value = "/list")
    public TableDataInfo list(@RequestParam(required = false) EmailLogs emailLogs) {
        QueryWrapper<EmailLogs> queryWrapper = new QueryWrapper();
        return  PageUtils.buildDataInfo(targetService.page(PageUtils.buildPage(), queryWrapper));

//        PageParams pageParams = new PageParams(map);
//        EmailLogs query = pageParams.mapToObject(EmailLogs.class);
//        QueryWrapper<EmailLogs> queryWrapper = new QueryWrapper();
//        return R.ok().data(targetService.page(new PageParams(map), queryWrapper));
    }

    /**
     * 根据ID查找数据
     */
    @ApiOperation(value = "根据ID查找数据", notes = "根据ID查找数据")
    @ResponseBody
    @RequestMapping("/get")
    public R get(@RequestParam("id") Long id) {
        EmailLogs entity = targetService.getById(id);
        return R.ok(entity);
    }

}
