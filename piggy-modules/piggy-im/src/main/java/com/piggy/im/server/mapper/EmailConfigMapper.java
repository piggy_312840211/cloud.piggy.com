package com.piggy.im.server.mapper;


import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.im.domain.EmailConfig;
import org.springframework.stereotype.Repository;

/**
 * @author liuyadu
 */
@Repository
public interface EmailConfigMapper extends BaseMapperPlus<EmailConfig> {
}
