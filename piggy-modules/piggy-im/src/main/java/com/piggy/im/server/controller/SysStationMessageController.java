package com.piggy.im.server.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.utils.poi.ExcelUtil;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.log.annotation.Log;
import com.piggy.common.log.enums.BusinessType;
import com.piggy.im.domain.bo.SysStationMessageAddBo;
import com.piggy.im.domain.bo.SysStationMessageQueryBo;
import com.piggy.im.domain.vo.SysStationMessageVo;
import com.piggy.im.server.service.ISysStationMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 站内消息Controller
 *
 * @author piggy
 * @date 2021-12-09
 */
@Api(value = "站内消息控制器", tags = {"站内消息管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/SysStationMessage")
public class SysStationMessageController extends BaseController {

    private final ISysStationMessageService iSysStationMessageService;

    /**
     * 查询站内消息列表
     */
    @ApiOperation("查询站内消息列表")
    // @SaCheckPermission("server:SysStationMessage:list")
    @GetMapping("/list")
    public TableDataInfo<SysStationMessageVo> list(@Validated SysStationMessageQueryBo bo) {
        return iSysStationMessageService.queryPageList(bo);
    }

    /**
     * 导出站内消息列表
     */
    @SneakyThrows
    @ApiOperation("导出站内消息列表")
    @SaCheckPermission("server:SysStationMessage:export")
    @Log(title = "站内消息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(HttpServletResponse response, @Validated SysStationMessageQueryBo bo) {
        List<SysStationMessageVo> list = iSysStationMessageService.queryList(bo);
        ExcelUtil<SysStationMessageVo> util = new ExcelUtil<SysStationMessageVo>(SysStationMessageVo.class);
        util.exportExcel(response,list, "站内消息");
    }

    /**
     * 获取站内消息详细信息
     */
    @ApiOperation("获取站内消息详细信息")
    @SaCheckPermission("server:SysStationMessage:query")
    @GetMapping("/{id}")
    public R getInfo(@NotNull(message = "主键不能为空")
                                                  @PathVariable("id") Long id) {
        return R.ok(iSysStationMessageService.queryById(id));
    }

    /**
     * 新增站内消息
     */
    @ApiOperation("新增站内消息")
    @SaCheckPermission("server:SysStationMessage:add")
    @Log(title = "站内消息", businessType = BusinessType.INSERT)
    @PostMapping()
    public R add(@Validated @RequestBody SysStationMessageAddBo bo) {
        return toAjax(iSysStationMessageService.insertByAddBo(bo) ? 1 : 0);
    }


    @ApiOperation("设置已读消息")
    @SaCheckPermission("server:SysStationMessage:haveRead")
    @Log(title = "已读消息", businessType = BusinessType.UPDATE)
    @GetMapping("/haveRead/{ids}")
    public R haveRead(@NotEmpty(message = "主键不能为空") @PathVariable Long[] ids) {
        return toAjax(iSysStationMessageService.haveRead(Arrays.asList(ids)) ? 1 : 0);
    }

    @ApiOperation("未读消息个数")
//    @SaCheckPermission("server:SysStationMessage:unreadCount")
    @GetMapping("/unreadCount")
    public R unreadCount() {
        return R.ok(iSysStationMessageService.unreadCount());
    }

}
