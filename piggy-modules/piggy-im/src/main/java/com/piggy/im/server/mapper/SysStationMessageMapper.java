package com.piggy.im.server.mapper;


import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.im.domain.SysStationMessage;

/**
 * 站内消息Mapper接口
 *
 * @author piggy
 * @date 2021-12-09
 */
public interface SysStationMessageMapper extends BaseMapperPlus<SysStationMessage> {

}
