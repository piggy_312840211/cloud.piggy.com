package com.piggy.im.server.controller;

import com.piggy.common.core.domain.R;
import com.piggy.common.core.web.controller.BaseController;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.im.domain.WebHookLogs;
import com.piggy.im.service.IWebHookClient;
import com.piggy.im.domain.bo.WebHookMessage;
import com.piggy.im.server.service.DelayMessageService;
import com.piggy.im.server.service.WebHookLogsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author woodev
 */
@RestController
@Api(value = "异步通知", tags = "异步通知")
public class WebHookController extends BaseController implements IWebHookClient {

    @Resource
    private DelayMessageService delayMessageService;
    @Resource
    private WebHookLogsService webHookLogsService;

    @ApiOperation(value = "Webhook异步通知", notes = "即时推送，重试通知时间间隔为 5s、10s、2min、5min、10min、30min、1h、2h、6h、15h，直到你正确回复状态 200 并且返回 success 或者超过最大重发次数")
    @Override
    @PostMapping("/webhook")
   public R send(
            @RequestBody WebHookMessage message
    ) throws Exception {
        delayMessageService.send(message);
        return success();
    }

    /**
     * 获取分页异步通知列表
     *
     * @return
     */
    @ApiOperation(value = "获取分页异步通知列表", notes = "获取分页异步通知列表")
    @GetMapping("/webhook/logs")
    public TableDataInfo getLogsListPage(@Validated WebHookLogs webHookLogs) {
        return webHookLogsService.findListPage(webHookLogs);

//         ResultBody.ok().data(webHookLogsService.findListPage(new PageParams(map)));
    }

}
