package com.piggy.im.server.mapper;

import com.piggy.common.core.web.page.BaseMapperPlus;
import com.piggy.im.domain.WebHookLogs;
import org.springframework.stereotype.Repository;

/**
 * @author liuyadu
 */
@Repository
public interface WebHookLogsMapper extends BaseMapperPlus<WebHookLogs> {
}
