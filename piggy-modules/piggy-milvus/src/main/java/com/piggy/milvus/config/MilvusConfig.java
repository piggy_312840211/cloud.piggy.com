package com.piggy.milvus.config;

import io.milvus.client.MilvusServiceClient;
import io.milvus.param.ConnectParam;
import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConditionalOnProperty
@ConfigurationProperties(prefix = "milvus.server")
public class MilvusConfig {

    private String host;
    private Integer port;

    @Bean
    public MilvusServiceClient buildClient() {
        ConnectParam connectParam = ConnectParam.newBuilder()
                .withHost(host)
                .withPort(port)
                .withAuthorization("root","sfzito.com")
                .build();
        return new MilvusServiceClient(connectParam);
    }

}
