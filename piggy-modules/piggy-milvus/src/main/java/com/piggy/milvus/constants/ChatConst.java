package com.piggy.milvus.constants;

public class ChatConst {

    /**
     * 集合名称(库名)
     */
    public static final String COLLECTION_NAME = "chat_collection";

    /**
     * 分片数量
     */
    public static final Integer SHARDS_NUM = 8;

    /**
     * 分区数量
     */
    public static final Integer PARTITION_NUM = 16;

    /**
     * 分区前缀
     */
    public static final String PARTITION_PREFIX = "shards_";

    /**
     * 特征值长度
     */
    public static final Integer FEATURE_DIM = 256;

    /**
     * 字段
     */
    public static class Field {
        /**
         * 主键id
         */
        public static final String QA_ID = "question";
        /**
         * 物料id
         */
        public static final String QUESTION = "question";
        /**
         * 特征值
         */
        public static final String ANSWER= "answer";
    }

}
