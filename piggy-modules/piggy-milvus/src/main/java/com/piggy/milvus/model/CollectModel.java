package com.piggy.milvus.model;

import lombok.Data;

import java.util.List;

@Data
public class CollectModel {

    private String name;

    private String desc;

    private List<FieldModel> fileList;
}
