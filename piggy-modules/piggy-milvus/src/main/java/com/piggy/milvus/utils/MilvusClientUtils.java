package com.piggy.milvus.utils;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.milvus.model.CollectModel;
import com.piggy.milvus.model.FieldValue;
import com.piggy.milvus.model.Record;
import io.milvus.client.MilvusServiceClient;
import io.milvus.common.clientenum.ConsistencyLevelEnum;
import io.milvus.grpc.MutationResult;
import io.milvus.grpc.SearchResults;
import io.milvus.param.MetricType;
import io.milvus.param.R;
import io.milvus.param.RpcStatus;
import io.milvus.param.collection.CreateCollectionParam;
import io.milvus.param.collection.DropCollectionParam;
import io.milvus.param.collection.FieldType;
import io.milvus.param.collection.HasCollectionParam;
import io.milvus.param.dml.InsertParam;
import io.milvus.param.dml.SearchParam;
import io.milvus.response.SearchResultsWrapper;
import org.springframework.context.annotation.Bean;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

public class MilvusClientUtils {

    private static MilvusServiceClient client = null;

    @Resource
    public void setMilvusServiceClient(MilvusServiceClient milvusServiceClient) {
        client = milvusServiceClient;
    }

    @Bean
    public MilvusClientUtils clientUtils() {
        return new MilvusClientUtils();
    }

    /**
     * 增、删、改、差、索引、分区
     */
    public static boolean createCollect(CollectModel collectModel) {

        if (CollUtil.isEmpty(collectModel.getFileList())) {
            return false;
        }

        List<FieldType> fieldSchema = new ArrayList<>();

        collectModel.getFileList().forEach(t-> {

            FieldType.Builder  builder = FieldType.newBuilder().
                    withAutoID(t.getAutoId());
            if (ObjectUtil.isNotNull(t.getPrimary())) {
                builder.withPrimaryKey(t.getPrimary());
            }
            if (ObjectUtil.isNotNull(t.getAutoId())) {
                builder.withAutoID(t.getAutoId());
            }
            if (ObjectUtil.isNotNull(t.getDimension())) {
                builder.withDimension(t.getDimension());
            }
            builder.withDataType(t.getDataType()).
                    withDescription(t.getDesc()).
                    withName(t.getName());

            fieldSchema.add(builder.build());

        });

        R<Boolean> hasR = client.hasCollection(HasCollectionParam.newBuilder().withCollectionName(collectModel.getName()).build());
        MilvusResultUtils.throwIfFailed(hasR);

        if (hasR.getData()) {
            return hasR.getData();
        }

        // create collection
        CreateCollectionParam param = CreateCollectionParam.newBuilder()
                .withCollectionName(collectModel.getName())
                .withDescription(collectModel.getDesc())
                .withFieldTypes(fieldSchema)
                .build();

        R<RpcStatus> response = client.createCollection(param);
        MilvusResultUtils.throwIfFailed(response);

        return false;
    }

    /**
     * drop
     */
    public static boolean dropCollect(CollectModel collectModel) {

        R<Boolean> hasR = client.hasCollection(HasCollectionParam.newBuilder().withCollectionName(collectModel.getName()).build());
        MilvusResultUtils.throwIfFailed(hasR);

        if (!hasR.getData()) {
            return !hasR.getData();
        }

        DropCollectionParam dropParam = DropCollectionParam.newBuilder()
                .withCollectionName(collectModel.getName())
                .build();

        R<RpcStatus> response = client.dropCollection(dropParam);
        MilvusResultUtils.throwIfFailed(response);

        return false;
    }

    /**
     * insert
     */
    public static boolean insert(String collectName, List<Record> recordList) {

        R<Boolean> hasR = client.hasCollection(HasCollectionParam.newBuilder().withCollectionName(collectName).build());
        MilvusResultUtils.throwIfFailed(hasR);

        if (!hasR.getData()) {
            return !hasR.getData();
        }

        if (CollUtil.isEmpty(recordList)) {
            throw new BaseException("数据值无效!");
        }

        recordList.forEach(t1-> {

            List<InsertParam.Field> fieldsInsert = new ArrayList<>();
            t1.getValueList().forEach(t-> {
                fieldsInsert.add(new InsertParam.Field(t.getName(), t.getVector()));
            });

            InsertParam param = InsertParam.newBuilder()
                    .withCollectionName(collectName)
                    .withFields(fieldsInsert)
                    .build();

            R<MutationResult> response = client.insert(param);
            MilvusResultUtils.throwIfFailed(response);

        });

        return true;
    }

    /**
     * insert
     */
    public static List<Long> search(String collectName, List<FieldValue> valueList) {

        R<Boolean> hasR = client.hasCollection(HasCollectionParam.newBuilder().withCollectionName(collectName).build());
        MilvusResultUtils.throwIfFailed(hasR);

        if (!hasR.getData()) {
            throw new BaseException("未找到集合");
        }

        if (CollUtil.isEmpty(valueList)) {
            throw new BaseException("数据值无效!");
        }

        SearchParam param = SearchParam.newBuilder()
                .withCollectionName(collectName)
                .withMetricType(MetricType.IP)
                .withTopK(10)
                .withVectors(valueList.get(0).getVector())
                .withVectorFieldName(valueList.get(0).getName())
                .withConsistencyLevel(ConsistencyLevelEnum.EVENTUALLY)
                .build();
        R<SearchResults> response = client.search(param);
        MilvusResultUtils.throwIfFailed(response);

        SearchResultsWrapper wrapper = new SearchResultsWrapper(response.getData().getResults());
        List<Long> ids = new ArrayList<>();
        wrapper.getIDScore(0).forEach(t-> {
            ids.add(t.getLongID());
        });
        return ids;

    }


}
