package com.piggy.milvus;

import com.piggy.common.satoken.annotation.EnableCustomConfig;
import com.piggy.common.satoken.annotation.EnableCustomFeignClients;
import com.piggy.common.swagger.annotation.EnableCustomSwagger2;
import com.piggy.milvus.annotation.EnableMilvus;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 系统模块
 *
 * @author shark
 */
@EnableCustomSwagger2
@EnableCustomFeignClients
@EnableCustomConfig
@RefreshScope
@EnableMilvus
@SpringBootApplication
public class PIGYMilvusApplication {
    public static void main(String[] args) {
        SpringApplication.run(PIGYMilvusApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  Milvus模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" );
    }
}
