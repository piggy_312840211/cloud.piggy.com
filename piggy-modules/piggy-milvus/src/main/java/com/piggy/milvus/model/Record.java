package com.piggy.milvus.model;

import lombok.Data;

import java.util.List;

@Data
public class Record {
    List<FieldValue> valueList;
}
