package com.piggy.milvus.model;

import io.milvus.grpc.DataType;
import lombok.Data;

@Data
public class FieldModel {
    private String name;
    private String desc;
    private DataType dataType;
    private Boolean primary;
    private Boolean autoId;
    private Integer dimension;
}
