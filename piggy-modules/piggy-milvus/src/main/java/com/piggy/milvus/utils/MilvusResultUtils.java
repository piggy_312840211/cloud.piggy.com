package com.piggy.milvus.utils;

import cn.hutool.core.util.ObjectUtil;
import io.milvus.param.R;
import com.piggy.common.core.exception.base.BaseException;
import io.milvus.param.R.Status;

/**
 * Feign接口返回值错误处理
 */
public class MilvusResultUtils {

    /**
     * 校验返回失败的返回结果，并抛出错误
     *
     * @param result
     */
    public static void throwIfFailed(R<?> result) {

        if (ObjectUtil.isEmpty(result)) {
            throw new BaseException("服务调用失败");
        }

        if (Status.Success.getCode() != result.getStatus()) {
            throw new BaseException(result.getMessage());
        }

    }

    /**
     * 判断Data数据是否为空
     * @param result
     */
    public static void throwIfNull(Object result) {

        if (ObjectUtil.isEmpty(result)) {
            throw new BaseException("服务返回结果为空");
        }
    }

}
