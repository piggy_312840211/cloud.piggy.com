package com.piggy.milvus.model;

import lombok.Data;

import java.util.List;

@Data
public class FieldValue {
    private String name;
    private List<Number> vector;
}
