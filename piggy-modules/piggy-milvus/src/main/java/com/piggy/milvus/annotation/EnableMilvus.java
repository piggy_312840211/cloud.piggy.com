package com.piggy.milvus.annotation;

import com.piggy.milvus.utils.MilvusClientUtils;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zhoushangtao
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import(MilvusClientUtils.class)
public @interface EnableMilvus {
}
