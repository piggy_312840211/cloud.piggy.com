package com.piggy.auth.controller;

import com.piggy.auth.bo.OAuthReq;
import com.piggy.auth.bo.OAuthRes;
import com.piggy.auth.services.SysLoginService;
import com.piggy.common.core.domain.R;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * token 控制
 *
 * @author shark
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/oauth")
public class TokenController
{

    @Resource
    private SysLoginService loginService;

    /**
     * 令牌管理调用
     * @return
     */
    @PostMapping("/login")
    public R<OAuthRes> oauth(@RequestBody OAuthReq oauth) {
        return R.ok(loginService.login(oauth));
    }

    /**
     * 令牌管理调用
     * @return
     */
    @PostMapping("/logout")
    public R<Boolean> logout() {
        return R.ok(loginService.logout());
    }

    /**
     * 踢人
     * @return
     */
    @PostMapping("/kickout")
    public R<Boolean> kickout(@RequestBody OAuthReq oauth) {
        return R.ok(loginService.kickout(oauth));
    }

}
