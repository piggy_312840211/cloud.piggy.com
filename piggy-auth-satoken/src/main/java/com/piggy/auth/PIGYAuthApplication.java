package com.piggy.auth;

import com.piggy.common.satoken.annotation.EnableCustomConfig;
import com.piggy.common.satoken.annotation.EnableCustomFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 认证授权中心
 *
 * @author shark
 */
@EnableCustomConfig
@EnableCustomFeignClients
@RefreshScope
@SpringBootApplication
public class PIGYAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(PIGYAuthApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  认证授权中心启动成功   ლ(´ڡ`ლ)ﾞ  \n" );
    }
}
