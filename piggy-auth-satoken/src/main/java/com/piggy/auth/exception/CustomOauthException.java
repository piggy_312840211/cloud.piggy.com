package com.piggy.auth.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.piggy.common.core.exception.base.BaseException;

/**
 * oauth2自定义异常
 *
 * @author shark
 **/
@JsonSerialize(using = CustomOauthExceptionSerializer.class)
public class CustomOauthException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public CustomOauthException(String msg)
    {
        super(msg);
    }
}
