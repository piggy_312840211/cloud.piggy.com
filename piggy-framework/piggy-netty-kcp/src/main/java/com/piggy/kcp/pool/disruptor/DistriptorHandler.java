package com.piggy.kcp.pool.disruptor;

import com.piggy.kcp.pool.ITask;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DistriptorHandler
{

	private ITask task;


	public void execute()
	{
		try {
			this.task.execute();
			//得主动释放内存
			this.task = null;
		} catch (Throwable throwable) {
			log.error("error",throwable);
		}
	}


	public void setTask(ITask task) {
		this.task = task;
	}
}
