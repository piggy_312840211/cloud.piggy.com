package com.piggy.kcp.pool;

/**
 * Created by JinMiao
 * 2018/5/2.
 */
public interface ITask {
    void execute();
}
