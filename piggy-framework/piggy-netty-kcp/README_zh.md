netty-websocket-spring-boot-starter [![License](http://img.shields.io/:license-apache-brightgreen.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)
===================================

[English Docs](https://github.com/YeautyYE/netty-websocket-spring-boot-starter/blob/master/README.md)

### 简介
本项目帮助你在spring-boot中使用Netty来开发kcp(可信赖得udp协议)

### 要求
- jdk版本为1.8或1.8+


### 快速开始

- 添加依赖:

```xml
	<dependency>
		<groupId>com.piggy</groupId>
		<artifactId>piggy-netty-kcp</artifactId>
		<version>0.12.0</version>
	</dependency>
```
