package com.piggy.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author liyc
 * @date 2021/6/16 区域
 */
@Getter
@AllArgsConstructor
public enum AreaEnum {

    其它(0, "其他"),
    欧洲(1, "欧洲"),
    美国(2, "美国"),
    加拿大(3, "加拿大"),
    墨西哥(4, "墨西哥"),
    香港(5, "香港"),
    台湾(6, "台湾"),
    迪拜(7, "迪拜");

    /**
     * 类型
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String info;

}
