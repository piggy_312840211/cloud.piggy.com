package com.piggy.common.core.validator;

import com.piggy.common.core.validate.DecimalScale;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

public class DecimalScaleValidator implements ConstraintValidator<DecimalScale, BigDecimal> {
    private int max;

    @Override
    public boolean isValid(BigDecimal value, ConstraintValidatorContext context) {
        return value == null || value.scale() <= max;
    }

    @Override
    public void initialize(DecimalScale constraintAnnotation) {
        this.max = constraintAnnotation.maxLength();
    }
}
