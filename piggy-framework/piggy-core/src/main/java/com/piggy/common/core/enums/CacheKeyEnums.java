package com.piggy.common.core.enums;

import cn.hutool.core.util.ObjectUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CacheKeyEnums {
    SysDept("sysDept", "部门", CacheKeyEnums.sysPrefix),
    SysClient("sysClientDetails", "终端配置表", CacheKeyEnums.sysPrefix,-1L),
    SysUser("sysUser", "用户表", CacheKeyEnums.sysPrefix),
    SysUserName("sysUserName", "用户表", CacheKeyEnums.sysPrefix),
    SysConfig("sysConfig", "系统配置表", CacheKeyEnums.sysPrefix, -1L),

    ;

    CacheKeyEnums(String code, String desc, String prefix) {
        this.code = code;
        this.desc = desc;
        this.prefix = prefix;
        this.day = DayOut;
    }

    /** key **/
    private final String code;
    /** 描述 **/
    private final String desc;
    /**
     * 前缀
     */
    private final String prefix;
    private final Long day;

    private final static String sysPrefix = "sys";
    private final static long DayOut = 30L;

    public String getKey(Object...keys) {
        StringBuilder redisKey = new StringBuilder(this.prefix);
        redisKey.append(":").append(this.code);
        for(Object key : keys) {
            if (ObjectUtil.isNotNull(key)) {
                redisKey.append(":").append(key);
            }
        }
        return redisKey.toString();
    }

}
