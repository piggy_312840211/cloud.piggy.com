package com.piggy.common.core.utils.sms;

import lombok.Data;

import java.io.Serializable;

/**
 * 消息模板对象 sys_sms
 *
 * @author shark
 * @date 2021-08-16
 */
@Data
public class SmsConfig implements Serializable {
	/** AccessKeyID */
	private String aliyunAccessKeyId;

	/** AccessKeySecret */
	private String aliyunAccessKeySecret;

	/** signName */
	private String signName;
	/*是否开启短信发送*/
	private boolean openSend;

}
