package com.piggy.common.core.validate;

import com.piggy.common.core.validator.DecimalScaleValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = { DecimalScaleValidator.class})
public @interface DecimalScale {
    int maxLength();
    String message() default "decimal precision must be less or equal to {maxLength}";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
