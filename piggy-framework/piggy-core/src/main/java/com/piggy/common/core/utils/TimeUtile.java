package com.piggy.common.core.utils;

import com.piggy.common.core.exception.base.BaseException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TimeUtile {

	/**
	 * 获取两个日期之间所有的天
	 *
	 * @param minDate yyyy-MM-dd
	 * @param maxDate yyyy-MM-dd
	 * @return
	 */
	public static List<String> getDays(String minDate, String maxDate) {

		// 返回的日期集合
		List<String> days = new ArrayList<String>();

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date start = dateFormat.parse(minDate);
			Date end = dateFormat.parse(maxDate);

			Calendar tempStart = Calendar.getInstance();
			tempStart.setTime(start);

			Calendar tempEnd = Calendar.getInstance();
			tempEnd.setTime(end);
			tempEnd.add(Calendar.DATE, +1);// 日期加1(包含结束)
			while (tempStart.before(tempEnd)) {
				days.add(dateFormat.format(tempStart.getTime()));
				tempStart.add(Calendar.DAY_OF_YEAR, 1);
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return days;
	}

	/**
	 * 获取两个日期之间所有的月份
	 *
	 * @param minDate
	 * @param maxDate
	 * @return
	 * @throws ParseException
	 */
	public static List<String> getMonthBetween(String minDate, String maxDate) throws ParseException {
		ArrayList<String> result = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");//格式化为年月

		Calendar min = Calendar.getInstance();
		Calendar max = Calendar.getInstance();

		min.setTime(sdf.parse(minDate));
		min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

		max.setTime(sdf.parse(maxDate));
		max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);

		Calendar curr = min;
		while (curr.before(max)) {
			result.add(sdf.format(curr.getTime()));
			curr.add(Calendar.MONTH, 1);
		}
		return result;
	}

	/**
	 * 获取年的区间值
	 *
	 * @param minDate
	 * @param maxDate
	 * @return
	 */
	public static List<String> getYears(String minDate, String maxDate) {
		ArrayList<String> result = new ArrayList<String>();
		int minDateInt = Integer.parseInt(minDate);
		int x = Integer.parseInt(maxDate) - Integer.parseInt(minDate);
		if (x > 0) {
			for (int i = 0; i <= x; i++) {
				int time = minDateInt + i;
				result.add(String.valueOf(time));
			}
		} else {
			result.add(minDate);
		}
		return result;
	}

	/**
	 * 根据季度获取月份日期
	 * @param quarter
	 * @param type
	 * @return
	 */
	public static String getQuarter(int quarter, Integer type) {
		String s = "";
		if (type == 1) {
			switch (quarter) {
				case 1:
					s = "01-01";
					break;
				case 2:
					s = "04-01";
					break;
				case 3:
					s = "07-01";
					break;
				case 4:
					s = "10-01";
					break;

			}
		} else {
			switch (quarter) {
				case 1:
					s = "03-31";
					break;
				case 2:
					s = "06-30";
					break;
				case 3:
					s = "09-30";
					break;
				case 4:
					s = "12-31";
					break;

			}
		}

		return s;
	}


	/**
	 * 根据前端传来的值  获得时间区间
	 * @param startTimes 当list长度为1时   获取年区间   2获取季区间  3获取月区间
	 * @param endtimes
	 * @return
	 * @throws ParseException
	 */
	public static List<String> timeSection(ArrayList<String> startTimes, ArrayList<String> endtimes) throws ParseException {
		List<String> years = TimeUtile.getYears(startTimes.get(0), endtimes.get(0));
		List<String> result = new ArrayList<>();
		switch (startTimes.size()) {
			case 1:
				result = years;
				break;
			case 2:
				int sQuarter = Integer.parseInt(startTimes.get(1));
				int eQuarter = Integer.parseInt(endtimes.get(1));
				int difference = eQuarter-sQuarter;
				if(years.size() == 1 ) {
					if(difference >=0){
						for (int i = 0; i <= difference; i++) {
							result.add(startTimes.get(0)+"-"+(sQuarter+i));
						}
					}else {
						throw new BaseException("起始季度不能大于结束季度");
					}
				} else if(years.size() >1) {
					for (int i = 0; i < years.size(); i++) {
						if(i == 0) {
							for (; sQuarter <= 4; sQuarter++) {
								result.add(years.get(i)+"-"+sQuarter);
							}
						} else if(i == years.size()-1) {
							for (; eQuarter > 0; eQuarter--) {
								result.add(years.get(i)+"-"+eQuarter);
							}
						}
					}
				}
				break;
			case 3:
				String startMonth = startTimes.get(0) + "-" +startTimes.get(2);
				String endMonth = endtimes.get(0) + "-" +endtimes.get(2);
				result = TimeUtile.getMonthBetween(startMonth,endMonth);
				break;
		}
		return result;
	}






	/**
	 * 返回年分或者季度需要的条件时间段
	 * @param type 1为年   2为季度
	 * @return
	 * @throws ParseException
	 */
	public static HashMap<String, Object> getLastStatisticsTime(int type) throws ParseException {
		HashMap<String, Object> map = new HashMap<>();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.parse(dateFormat.format(new Date()));
		Calendar cal = dateFormat.getCalendar();
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);
		List<Integer> quarters = new ArrayList<>();
		List<Integer> monthList = new ArrayList<>();
		if (type == 1) {
			map.put("year", year - 1);
			quarters.add(1);
			quarters.add(2);
			quarters.add(3);
			quarters.add(4);
			map.put("quarter",quarters);
		} else if (type == 2) {
			if (type == 2) {
				if (month >= 1 && month < 4) {
					monthList.add(10);
					monthList.add(11);
					monthList.add(12);
					map.put("quarter", String.valueOf(4));
					map.put("year", String.valueOf((year - 1)));
				}
				if (month >= 4 && month < 7) {
					monthList.add(1);
					monthList.add(2);
					monthList.add(3);
					map.put("quarter", String.valueOf(1));
					map.put("year", String.valueOf(year));
				}
				if (month >= 7 && month < 10) {
					monthList.add(4);
					monthList.add(5);
					monthList.add(6);
					map.put("quarter", String.valueOf(2));
					map.put("year", String.valueOf(year));

				}
				if (month >= 10 && month <= 12) {
					monthList.add(7);
					monthList.add(8);
					monthList.add(9);
					map.put("quarter", String.valueOf(3));
					map.put("year", String.valueOf(year));
				}
				map.put("month", monthList);
			}
		}
		return map;
	}


	/**
	 * 获取当前日期下一天
	 * @param date
	 * @return
	 */
	public static Date getNextday(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		date = calendar.getTime();
		return date;
	}

	/**
	 * 通过时间秒毫秒数判断两个时间的间隔
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static int differentDaysByMillisecond(Date date1,Date date2)
	{
		int days = (int) ((date2.getTime() - date1.getTime()) / (1000*3600*24));
		return days;
	}


}
