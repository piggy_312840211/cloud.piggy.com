package com.piggy.common.core.utils;

import cn.hutool.core.bean.BeanException;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;

import java.lang.reflect.Type;

public class BeanFillerUtils {

    public static <T,V> void Filler(T t, Type type, V defValue) {

        BeanUtil.descForEach(t.getClass(), (prop) -> {
            String key = prop.getFieldName();
            if (ObjectUtil.isNotNull(key)) {
                Object value;
                try {
                    value = prop.getValue(t);
                    if (prop.getFieldType().equals(type) && ObjectUtil.isNull(value)) {
                        prop.setValue(t, defValue);
                    }
                } catch (Exception var8) {
                    throw new BeanException(var8, "Get value of [{}] error!", prop.getFieldName());
                }
            }
        });
    }

    public static <T,V> void Filler(T t, String fieldName, V setValue) {

        BeanUtil.descForEach(t.getClass(), (prop) -> {
            String key = prop.getFieldName();
            if (ObjectUtil.isNotNull(key)) {
                if (!key.equals(fieldName)) {
                    return;
                }
                try {
                    prop.setValue(t, setValue);
                } catch (Exception var8) {
                    throw new BeanException(var8, "Get value of [{}] error!", prop.getFieldName());
                }
            }
        });
    }

}
