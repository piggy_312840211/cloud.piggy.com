package com.piggy.common.core.exception;

/**
 * 演示模式异常
 *
 * @author shark
 */
public class DemoModeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public DemoModeException() {
    }
}
