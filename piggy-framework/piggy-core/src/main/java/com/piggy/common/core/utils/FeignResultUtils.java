package com.piggy.common.core.utils;

import cn.hutool.core.util.ObjectUtil;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.common.core.web.page.TableDataInfo;

/**
 * Feign接口返回值错误处理
 */
public class FeignResultUtils {

    /**
     * 校验返回失败的返回结果，并抛出错误
     *
     * @param result
     */
    public static void throwIfFailed(R<?> result) {

        if (ObjectUtil.isEmpty(result)) {
            throw new BaseException("服务调用失败");
        }

        if (R.SUCCESS != result.getCode()) {
            throw new BaseException(result.getMsg());
        }

    }

    /**
     * 判断Data数据是否为空
     * @param result
     */
    public static void throwIfNull(Object result) {

        if (ObjectUtil.isEmpty(result)) {
            throw new BaseException("服务返回结果为空");
        }
    }

    public static void throwIfFailed(TableDataInfo<?> result) {

        if (ObjectUtil.isEmpty(result)) {
            throw new BaseException("服务调用失败");
        }

        if (R.SUCCESS != result.getCode()) {
            throw new BaseException(result.getMsg());
        }

    }

}
