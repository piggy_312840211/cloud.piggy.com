package com.piggy.common.core.annotation;

import java.lang.annotation.*;

/**
 * 数据权限过滤注解
 *
 * @author shark
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataAuthority
{
	/**
	 * 用户获取
	 */
	public String userName() default "admin";

}
