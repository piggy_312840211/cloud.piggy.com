package com.piggy.common.core.web.base;

import com.github.pagehelper.PageInfo;
import com.piggy.common.core.constant.HttpStatus;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.web.page.TableDataInfo;

import java.util.List;

public interface BaseResponse {

    /**
     * 响应请求分页数据
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    default  <T> TableDataInfo<T> getDataTable(List<T> list) {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setRows(list);
        rspData.setMsg("查询成功");
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    /**
     * 响应返回结果
     *
     * @param rows 影响行数
     * @return 操作结果
     */
    default <T> R<T> toAjax(int rows)
    {
        return rows > 0 ? R.ok() : R.fail();
    }

    /**
     * 响应返回结果
     *
     * @param result 结果
     * @return 操作结果
     */
    default <T> R<T> toAjax(boolean result)
    {
        return result ? success() : error();
    }

    /**
     * 返回成功
     */
    default <T> R<T> success()
    {
        return R.ok();
    }

    /**
     * 返回失败消息
     */
    default <T> R<T> error() {
        return R.fail();
    }

    /**
     * 返回成功消息
     */
    default <T> R<T> success(T data) {
        return R.ok(data);
    }

    /**
     * 返回失败消息
     */
    default <T> R<T> error(String message) {
        return R.fail(message);
    }

    /**
     * 返回成功消息
     */
    default <T> R<T> ok(T data) {
        return R.ok(data);
    }

}
