package com.piggy.common.core.constant;

/**
 * @author: liuyadu
 * @date: 2019/2/21 17:46
 * @description:
 */
public class QueueConstants {
    public static final String QUEUE_SCAN_API_RESOURCE = "piggy.scan.api.resource";
    public static final String QUEUE_ACCESS_LOGS = "piggy.access.logs";
}
