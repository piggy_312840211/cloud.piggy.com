package com.piggy.common.core.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;


/**
 * 金额类
 *
 * @author liyc
 */
public class AmountUtils
{


    /**
     * 根据总金额和比例(预付比例、融资利率等)计算相乘结果金额
     *
     * @return BigDecimal 相乘结果金额
     */
    public static BigDecimal getMultiplyAmount(BigDecimal totalAmount, Double pRatio,Integer userDateNum)
    {
		//ratio 不带%  如：20%即0.2
		BigDecimal ratio = new BigDecimal(String.valueOf(pRatio/100));
		BigDecimal resultAmount=new BigDecimal("0");
		if(userDateNum==null){
			//金额*利率  如：预付
			 resultAmount=totalAmount.multiply(ratio).setScale(6, RoundingMode.HALF_UP);
		}else{
			//算利息
			 resultAmount=totalAmount.multiply(ratio).multiply(new BigDecimal(userDateNum)).divide(new BigDecimal("360"),6,BigDecimal.ROUND_UP);
		}
		return resultAmount;
    }


}
