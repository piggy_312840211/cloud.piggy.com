package com.piggy.common.core.constant;

/**
 * 权限相关通用常量
 *
 * @author shark
 */
public class SecurityConstants
{

    /**
     * {bcrypt} 加密的特征码
     */
    public static final String BCRYPT = "{bcrypt}";
    /**
     * 客户端编号
     */
    public static final String CLIENT_ID = "client_id";

    /**
     * 协议字段
     */
    public static final String DETAILS_LICENSE = "license";
    /**
     * 项目的license
     */
    public static final String PIGGY_LICENSE = "made by piggy";

    /**
     * 授权码模式code key 前缀
     */
    public static final String OAUTH_CODE_PREFIX = "oauth:code:";

    /**
     * 激活字段 兼容外围系统接入
     */
    public static final String ACTIVE = "active";
    /**
     * 用户ID字段
     */
    public static final String DETAILS_USER_ID = "user_id";



    /**
     * 标志
     */
    public static final String FROM = "source";

    /**
     * oauth 相关前缀
     */
    public static final String OAUTH_PREFIX = "oauth:";

    /**
     * 前缀
     */
    public static final  String PIGY_PREFIX = "piggy_";

    /**
     * token 相关前缀
     */
//    String TOKEN_PREFIX = "token:";


    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 用户名字段
     */
    public static final String DETAILS_USERNAME = "username";

    /**
     * 登录用户
     */
    public static final String LOGINUSER = "loginUser";

    /**
     * 授权信息字段
     */
    public static final String AUTHORIZATION_HEADER = "authorization";

    /**
     * 请求来源
     */
    public static final String FROM_SOURCE = "from-source";

    /**
     * 启动时是否检查Inner注解安全性
     */
    public static final boolean INNER_CHECK = true;
    /**
     * 内部请求
     */
    public static final String INNER = "inner";
    /**
     * 内部请求，携带token
     */
    public static final String TOKENINNER = "COPYTOKEN";

    /**
     * 资源服务器默认bean名称
     */
    public static final String RESOURCE_SERVER_CONFIGURER = "resourceServerConfigurerAdapter";

    /**
     * 用户标识
     */
    public static final String USER_KEY = "user_key";

    /**
     * 登录用户
     */
    public static final String LOGIN_USER = "login_user";

    /**
     * sys_oauth_client_details 表的字段，不包括client_id、client_secret
     */
    public static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    /**
     * JdbcClientDetailsService 查询语句
     */
    public static final String BASE_FIND_STATEMENT = "select " + CLIENT_FIELDS + " from sys_oauth_client_details";

    /**
     * 按条件client_id 查询
     */
    public static final String DEFAULT_SELECT_STATEMENT = BASE_FIND_STATEMENT + " where client_id = ?";

    /**
     * 默认的查询语句
     */
    public static final String DEFAULT_FIND_STATEMENT = BASE_FIND_STATEMENT + " order by client_id";
}
