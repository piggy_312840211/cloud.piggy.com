package com.piggy.common.core.exception;

/**
 * 权限异常
 *
 * @author shark
 */
public class PreAuthorizeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public PreAuthorizeException() {
    }
}
