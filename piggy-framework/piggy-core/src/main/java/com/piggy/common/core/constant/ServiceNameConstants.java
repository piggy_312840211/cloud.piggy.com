package com.piggy.common.core.constant;

/**
 * 服务名称
 *
 * @author shark
 */
public class ServiceNameConstants {
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "piggy-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "piggy-sys";

    /**
     * 文件服务的serviceid
     */
    public static final String FILE_SERVICE = "piggy-file";

    /**
     * 工作流服务的serviceid
     */
    public static final String MSG_SERVICE = "piggy-im";

    /**
     * ms的serviceid
     */
    public static final String FLOWABLE_SERVICE = "piggy-flow";
}
