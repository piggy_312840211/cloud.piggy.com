package com.piggy.common.core.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * @author liyc
 * @date 2021/6/29
 */
public class BigDecimalSerialize extends JsonSerializer<BigDecimal> {

	private DecimalFormat df = new DecimalFormat("#0.00");

	@Override
	public void serialize(BigDecimal value, JsonGenerator jgen,SerializerProvider provider) throws IOException,
		JsonProcessingException {
		jgen.writeString(df.format(value));
	}
}
