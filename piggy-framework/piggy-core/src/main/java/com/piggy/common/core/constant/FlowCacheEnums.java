package com.piggy.common.core.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum FlowCacheEnums {
    FlowAttr("flowAttr", "流程属性", FlowCacheEnums.flowPrefix),

    ;

    /** key **/
    private final String code;
    /** 描述 **/
    private final String desc;
    /**
     * 前缀
     */
    private final String prefix;

    private final static String flowPrefix = "flow";


    public String getKey(Object...keys) {
        StringBuilder redisKey = new StringBuilder(this.prefix);
        redisKey.append(":").append(this.code);
        for(Object key : keys) {
            redisKey.append(":").append(key);
        }
        return redisKey.toString();
    }

}
