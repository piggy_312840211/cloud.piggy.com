package com.piggy.common.core.constant;

public interface OssConstans {
    /**
     * 区块链模块
     */
    String CHAIN_SERVICE = "piggy-chain";
    /**
     * 云文件存储配置
     */
     String CONTRACT_CLOUDCONFIG = "cloudconfig";

    /**
     * 参数管理 cache key
     */
    String SYS_CONFIG_KEY = "sys_config:";
    /**
     * 云文件存储配置
     */
     String ROLESESSIONNAME  = "piggy";
    /**
     * 云文件存储配置
     */
     String REGIONID  = "cn-shanghai";
    /**
     * 云文件URL过期时间为3600秒（1小时）
     */
    int CLOUD_FILE_EXPIRATION  = 3600 * 1000;

    /**
     * 云服务商
     */
     enum CloudService {
        /**
         * 七牛云
         */
        QINIU(1),
        /**
         * 阿里云
         */
        ALIYUN(2),
        /**
         * 腾讯云
         */
        QCLOUD(3);

        private int value;

        CloudService(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
