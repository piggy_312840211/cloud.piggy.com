package com.piggy.common.core.web.service;

import com.piggy.common.core.web.base.BaseResponse;

/**
 * web层通用数据处理
 *
 * @author shark
 */
public interface BaseService extends BaseResponse {
}
