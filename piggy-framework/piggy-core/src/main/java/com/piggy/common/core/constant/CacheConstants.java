package com.piggy.common.core.constant;

/**
 * 缓存的key 常量
 *
 * @author shark
 */
public class CacheConstants {
    /**
     * oauth 缓存前缀
     */
    public static final String OAUTH_ACCESS = "oauth:access:";

    /**
     * oauth 客户端信息
     */
    public static final String CLIENT_DETAILS_KEY = "oauth:client:details";
    /**
     * 缓存有效期，默认720（分钟）
     */
    public final static long EXPIRATION = 720;

    /**
     * 缓存刷新时间，默认120（分钟）
     */
    public final static long REFRESH_TIME = 120;

    /**
     * 权限缓存前缀
     */
    public final static String LOGIN_TOKEN_KEY = "login_tokens:";
    /**
     * 用户信息缓存
     */
    public final static String USER_DETAILS = "user_details";

    /**
     * 角色信息缓存
     */
    public final static String ROLE_DETAILS = "role_details";

    /**
     * 字典信息缓存
     */
    public final static String DICT_DETAILS = "dict_details";
}
