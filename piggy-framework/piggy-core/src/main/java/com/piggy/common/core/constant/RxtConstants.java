package com.piggy.common.core.constant;

public class RxtConstants {
    /**
     * 发票验真失败
     */
    public static final Integer VERIFY_FAIL = 2;
    /**
     * 发票未验真
     */
    public static final Integer VERIFY_NEVER = 0;
    /**
     * 发票验真成功
     */
    public static final Integer VERIFY_SUCCESS = 1;
    /**
     * 普通发票类型
     */
    public static final String SPECIAL_VAT_INVOICE = "special_vat_invoice";
}
