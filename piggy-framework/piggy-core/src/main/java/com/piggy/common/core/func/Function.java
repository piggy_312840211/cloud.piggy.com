package com.piggy.common.core.func;

@FunctionalInterface
public interface Function<T> {

    /**
     * Applies this function to the given argument.
     *
     * @return the function result
     */
    T apply();
}
