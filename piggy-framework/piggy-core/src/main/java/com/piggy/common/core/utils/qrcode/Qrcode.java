package com.piggy.common.core.utils.qrcode;

import cn.hutool.extra.qrcode.QrCodeUtil;
import com.piggy.common.core.utils.sign.Base64;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Component
public class Qrcode {
	public String makeQrCode(String content) throws IOException {

//		content="123";
//		File generate1 = QrCodeUtil.generate(content, 10, 10,FileUtil.file("d:/qrcode2.jpg"));
		BufferedImage imageBuffer = QrCodeUtil.generate(content, 300, 300);

		ByteArrayOutputStream imagestream = new ByteArrayOutputStream();
		ImageIO.write(imageBuffer, "png", imagestream);
		String imageBase64 = Base64.encode(imagestream.toByteArray());

		return imageBase64;

	}


}
