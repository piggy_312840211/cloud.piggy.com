/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the piggy.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.piggy.common.sequence;

import com.piggy.common.sequence.builder.SnowflakeSeqTwoBuilder;
import com.piggy.common.sequence.properties.SequenceSnowflakeProperties;
import com.piggy.common.sequence.sequence.PigySequence;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author shark
 * @date 2019-05-26
 */
@Configuration
@ComponentScan("com.piggy.common.sequence")
@ConditionalOnMissingBean(PigySequence.class)
public class SequenceAutoConfiguration {

	/**
	 * snowflak 算法作为发号器实现
	 * @param properties
	 * @return
	 */
	@Bean
	@ConditionalOnBean(SequenceSnowflakeProperties.class)
	public PigySequence snowflakeSequence(SequenceSnowflakeProperties properties) {
		return SnowflakeSeqTwoBuilder.create().datacenterId(properties.getDatacenterId())
				.workerId(properties.getWorkerId()).build();
	}

}
