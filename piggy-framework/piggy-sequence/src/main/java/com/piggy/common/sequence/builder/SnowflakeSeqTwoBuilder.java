package com.piggy.common.sequence.builder;


import com.piggy.common.sequence.sequence.PigySequence;
import com.piggy.common.sequence.sequence.impl.SnowflakeSequenceTwo;

/**
 * 基于雪花算法，序列号生成器构建者
 *
 * @author xuan on 2018/5/30.
 */
public class SnowflakeSeqTwoBuilder implements SeqBuilder {

	/**
	 * 数据中心ID，值的范围在[0,31]之间，一般可以设置机房的IDC[必选]
	 */
	private long datacenterId;

	/**
	 * 工作机器ID，值的范围在[0,31]之间，一般可以设置机器编号[必选]
	 */
	private long workerId;

	public static SnowflakeSeqTwoBuilder create() {
		return new SnowflakeSeqTwoBuilder();
	}

	@Override
	public PigySequence build() {
		return new SnowflakeSequenceTwo(this.workerId, this.datacenterId);
	}

	public SnowflakeSeqTwoBuilder datacenterId(long datacenterId) {
		this.datacenterId = datacenterId;
		return this;
	}

	public SnowflakeSeqTwoBuilder workerId(long workerId) {
		this.workerId = workerId;
		return this;
	}

}
