package com.piggy.common.sequence.properties;

/**
 * @author shark
 * @date 2019-05-26
 */

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author shark
 * @date 2019/5/26
 * <p>
 * 发号器DB配置属性
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Component
@ConfigurationProperties(prefix = "piggy.xsequence.db")
public class SequenceDbProperties extends BaseSequenceProperties {

	/**
	 * 表名称
	 */
	private String tableName = "pigx_sequence";

	/**
	 * 重试次数
	 */
	private int retryTimes = 1;

}
