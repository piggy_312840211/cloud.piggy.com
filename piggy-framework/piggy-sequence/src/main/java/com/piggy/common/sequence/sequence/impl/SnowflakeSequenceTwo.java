package com.piggy.common.sequence.sequence.impl;


import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.piggy.common.sequence.exception.SeqException;
import com.piggy.common.sequence.sequence.PigySequence;

/**
 * 使用雪花算法 一个long类型的数据，64位。以下是每位的具体含义。 <br>
 * snowflake的结构如下(每部分用-分开): <br>
 * 0 - 0000000000 0000000000 0000000000 0000000000 0 - 00000 - 00000 - 000000000000 <br>
 * （1）第一位为未使用 （2）接下来的41位为毫秒级时间(41位的长度可以使用69年) （3）然后是5位datacenterId （4）5位workerId
 * （5）最后12位是毫秒内的计数（12位的计数顺序号支持每个节点每毫秒产生4096个ID序号） <br>
 * 一共加起来刚好64位，为一个Long型。(转换成字符串长度为18)
 *
 * @author xuan on 2018/5/9.
 */
public class SnowflakeSequenceTwo implements PigySequence {

	private Snowflake snowflake = null;

	private SnowflakeSequenceTwo() {
	}

	public SnowflakeSequenceTwo(long workerId, long datacenterId) {
		snowflake = IdUtil.getSnowflake(workerId, datacenterId);
	}

	@Override
	public synchronized long nextValue() throws SeqException {
		return snowflake.nextId();
	}

	/**
	 * 下一个生成序号（带格式）
	 * @return
	 * @throws SeqException
	 */
	@Override
	public String nextNo() throws SeqException {
		return String.valueOf(nextValue());
	}

}
