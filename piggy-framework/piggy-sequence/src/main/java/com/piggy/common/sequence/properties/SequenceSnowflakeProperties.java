package com.piggy.common.sequence.properties;

import cn.hutool.core.net.NetUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author shark
 * @date 2019-05-26
 * <p>
 * Snowflake 发号器属性
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Component
@ConfigurationProperties(prefix = "piggy.xsequence.snowflake")
public class SequenceSnowflakeProperties extends BaseSequenceProperties {

	/**
	 * 数据中心ID，值的范围在[0,31]之间，一般可以设置机房的IDC[必选]
	 */
	private long datacenterId;

	/**
	 * 工作机器ID，值的范围在[0,31]之间，一般可以设置机器编号[必选]
	 */
	private final long workerId = NetUtil.ipv4ToLong(NetUtil.getLocalhostStr()) % 31L;

}
