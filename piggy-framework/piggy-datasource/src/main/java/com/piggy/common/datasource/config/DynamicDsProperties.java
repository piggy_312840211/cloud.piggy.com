package com.piggy.common.datasource.config;

import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceProperties;
import lombok.Data;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.LinkedHashMap;
import java.util.Map;

@Data
@RefreshScope
@ConfigurationProperties(DynamicDataSourceProperties.PREFIX)
public class DynamicDsProperties implements DisposableBean {

    /**
     * 每一个数据源
     */
    private Map<String, DataSourceProperty> datasource = new LinkedHashMap<>();

    @Override
    public void destroy() throws Exception {
        datasource = new LinkedHashMap<>();
    }
}