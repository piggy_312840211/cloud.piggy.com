package com.piggy.sys.api.vo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@ApiModel("api回调配置视图vo")
public class SysOauthApiConfigVo
{

    @ApiModelProperty("终端编号")
    private  String  clientId;

    @ApiModelProperty("回调url")
    private  String  callbackUrl;

    @ApiModelProperty("备注")
    private  String  remark;
}
