package com.piggy.sys.api.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 角色授权码对象 sys_role_authcode
 *
 * @author piggy
 * @date 2021-09-14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Accessors(chain = true)
@TableName("sys_role_authcode")
public class SysRoleAuthcode implements Serializable {

    private static final long serialVersionUID=1L;


    /** 序号 */
    @TableId(value = "id")
    private Long id;

    /** 创建人 */
	@TableField(fill = FieldFill.INSERT)
    private Long creator;

    /** 创建人姓名 */
	@TableField(fill = FieldFill.INSERT)
    private String creatorName;

    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 编辑人 */
	@TableField(fill = FieldFill.UPDATE)
    private Long editor;

    /** 编辑人姓名 */
	@TableField(fill = FieldFill.UPDATE)
    private String editorName;

    /** 编辑时间 */
	@TableField(fill = FieldFill.UPDATE)
    private Date editTime;

    /** 删除人 */
    private Long deleter;

    /** 删除人姓名 */
    private String deleterName;

    /** 删除时间 */
    private Date deleteTime;

    /** 删除状态：0有效，2删除 */
    @TableLogic
    private Long delFlag;

    /** 角色id */
    private Long roleId;

    /** 组织部门id */
    private Long orgDeptId;

    /** 企业id */
    private Long businessId;

    /** 企业id */
    private Long sceneId;

    /** 状态 0未使用 1已使用  */
    private Integer status;

    /** 0平台添加1核心企业添加  */
    private Integer type;

	/** 手机号 */
	private String businessType;

    /** 授权码 */
    private String authCode;

	/** 手机号 */
	private String phone;

    /** 备注 */
    private String remark;

}
