package com.piggy.sys.api.factory;

import com.piggy.sys.api.RemoteRoleService;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.domain.SysRole;
import com.piggy.sys.api.domain.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 角色服务降级处理
 *
 * @author shark
 */
@Component
public class RemoteRoleFallbackFactory implements FallbackFactory<RemoteRoleService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteRoleFallbackFactory.class);

    @Override
    public RemoteRoleService create(Throwable throwable) {
        log.error("角色服务调用失败:{}", throwable.getMessage());
        return new RemoteRoleService()
        {

            @Override
            public boolean checkRoleNotAdmin(Long userId, String source) {
                return false;
            }

            @Override
            public R<SysRole> getRoleByKey(String roleKey, String source) {
                return R.fail("角色服务调用失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysUser>> getRoleUsers(Long roleId, String source) {
                return R.fail("角色服务调用失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysUser>> getRoleUserByRoleIds(List<Long> roleIds, String source) {
                return R.fail("角色服务调用失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysUser>> getRoleUserBySceneIds(List<Long> sceneIds, String source) {
                return R.fail("角色服务调用失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysRole>> queryRoleAll(String source) {
                return R.fail("角色服务调用失败:" + throwable.getMessage());
            }

            @Override
            public R<SysRole> getById(Long roleId, String source) {
                return R.fail("角色服务调用失败:" + throwable.getMessage());
            }
        };
    }
}
