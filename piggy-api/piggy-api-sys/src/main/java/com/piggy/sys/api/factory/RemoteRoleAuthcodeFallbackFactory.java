package com.piggy.sys.api.factory;

import com.piggy.sys.api.RemoteRoleAuthcodeService;
import com.piggy.sys.api.bo.SysRoleAuthcodeEditBo;
import com.piggy.common.core.domain.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RemoteRoleAuthcodeFallbackFactory implements FallbackFactory<RemoteRoleAuthcodeService> {

    @Override
    public RemoteRoleAuthcodeService create(Throwable throwable) {
        log.error("角色服务调用失败:{}", throwable.getMessage());
        return new RemoteRoleAuthcodeService()
        {
            @Override
            public R<Boolean> updateBySceneIdAndBusinessId(SysRoleAuthcodeEditBo bo, String source) {
                return R.fail("角色服务调用失败:" + throwable.getMessage());
            }

        };
    }
}
