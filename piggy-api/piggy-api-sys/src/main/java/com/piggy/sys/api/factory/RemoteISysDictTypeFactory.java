package com.piggy.sys.api.factory;

import com.piggy.sys.api.domain.SysDictData;
import com.piggy.sys.api.RemoteISysDictTypeService;
import com.piggy.common.core.domain.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class RemoteISysDictTypeFactory implements FallbackFactory<RemoteISysDictTypeService> {

    @Override
    public RemoteISysDictTypeService create(Throwable cause) {
        log.error("字典服务调用失败:{}", cause.getMessage());
        return new RemoteISysDictTypeService(){

            @Override
            public R<List<SysDictData>> selectDictDataByType(String dictType, String source) {
                log.error("字典服务调用失败:{}", cause.getMessage());
                return null;
            }

        };
    }
}
