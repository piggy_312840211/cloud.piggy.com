package com.piggy.sys.api.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


/**
 * 注册邀请短信详情添加对象 tdms_registration_message
 *
 * @author piggy
 * @date 2021-10-11
 */
@Data
@ApiModel("注册邀请短信详情添加对象")
public class SmsRegisterMessageAddBo {


	@ApiModelProperty("短信发送对象")
	List<SysRegistrationMessageAddBo> sysRegistrationMessageAddBoList;


	/** 0核心企业1供应商2经销商3金融机构 */
	@ApiModelProperty("0核心企业1供应商2经销商3金融机构")
	private String busineseType;

	/** 场景ID */
	@ApiModelProperty("场景ID")
	private Long  sceneId;


}
