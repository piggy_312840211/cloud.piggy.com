package com.piggy.sys.api.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("sys_oauth_api_config")
@ApiModel("api回调配置表")
public class SysOauthApiConfig implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     * 终端编号
     */
    private String clientId;

    /**
     * 回调url
     */
    private String callbackUrl;

    /**
     * 备注
     */
    private String  remark;
}
