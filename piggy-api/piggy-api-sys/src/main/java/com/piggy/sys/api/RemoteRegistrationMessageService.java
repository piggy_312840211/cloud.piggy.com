package com.piggy.sys.api;

import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.domain.SysRegistrationMessage;
import com.piggy.sys.api.factory.RemoteRegistrationMessageFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(contextId = "remoteRegistrationMessageService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteRegistrationMessageFallbackFactory.class)
public interface RemoteRegistrationMessageService {

    /**
     * 发送邀请短信
     * @param registrationMessage
     * @param source
     * @return
     */
    @PostMapping("/register/sendRegisterMessage")
    R<Boolean> sendRegisterMessage(@RequestBody SysRegistrationMessage registrationMessage, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
