package com.piggy.sys.api;

import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.domain.SysDept;
import com.piggy.sys.api.factory.RemoteDeptFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户服务
 *
 * @author shark
 */
@FeignClient(contextId = "remoteDeptService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteDeptFallbackFactory.class)
public interface RemoteDeptService {


    @GetMapping("/dept/getById")
    public R<SysDept> getById(@RequestParam("deptId") Long deptId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过用户名查询用户信息
     *
     * @param source 请求来源
     * @return 结果
     */
    @GetMapping("/dept/getOrgType")
    Integer getOrgType(@RequestParam("deptId") Long deptId,@RequestParam("sceneKey") String sceneKey, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过用户名查询用户信息
     *
     * @param source 请求来源
     * @return 结果
     */
    @GetMapping("/dept/getOrgDeptId/{deptId}")
     Long getOrgDeptId(@PathVariable("deptId") Long deptId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
    /**
     * 通过用户名查询用户信息
     *
     * @param source 请求来源
     * @return 结果
     */
    @GetMapping("/dept/{deptId}")
     R<SysDept> getInfo(@PathVariable("deptId") Long deptId, @RequestHeader("temp") String source);
    /**
     * 通过用户名查询用户信息
     *
     * @param source 请求来源
     * @return 结果
     */
    @GetMapping("/dept/getDeptListByOrgType")
     R<List<SysDept>> getDeptListByOrgType(@RequestParam("sceneKey") String sceneKey, @RequestParam("busineseType") Integer busineseType, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping("/dept/queryDeptAll")
    R<List<SysDept>> queryDeptAll(@RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping("/dept/saveSysDept")
    R<SysDept> saveSysDept(@RequestBody SysDept sysDept,@RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping("/{clientId}")
    R<SysDept> detailInfo(@PathVariable("deptId") Long deptId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
