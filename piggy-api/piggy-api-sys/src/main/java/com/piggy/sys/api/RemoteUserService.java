package com.piggy.sys.api;

import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.domain.SysUser;
import com.piggy.sys.api.factory.RemoteUserFallbackFactory;
import com.piggy.sys.api.model.LoginUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户服务
 *
 * @author shark
 */
@FeignClient(contextId = "remoteUserService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteUserService {
    /**
     * 通过用户名查询用户信息
     *
     * @param username 用户名
     * @param source 请求来源
     * @return 结果
     */
    @GetMapping("/user/info/{username}")
    public R<LoginUser> getUserInfo(@PathVariable("username") String username, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 注册用户信息
     *
     * @param sysUser 用户信息
     * @param source 请求来源
     * @return 结果
     */
    @PostMapping("/user/register")
    public R<Boolean> registerUserInfo(@RequestBody SysUser sysUser, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 根据用户id获取用户
     *
     * @param userId 用户信息id
     * @param source 请求来源
     * @return 结果
     */
    @GetMapping(value = {  "/user/{userId}" })
    public R<SysUser> getInfo(@PathVariable(value = "userId", required = false) Long userId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 根据部门获取获取用户
     * @param deptId 部门id
     * @param source 请求来源
     * @return 结果
     */
    @GetMapping(value = {  "/user/getUser/{deptId}" })
    R<List<SysUser>> getUserByDeptId(@PathVariable(value = "deptId", required = false) Long deptId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 根据部门列表获取用户
     * @param deptIds
     * @param source
     * @return
     */
    @GetMapping("/user/getUserByDeptIds")
    R<List<SysUser>> getUserByDeptIds(@RequestParam("deptIds") List<Long> deptIds, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping("/user/queryUserAll")
    R<List<SysUser>> queryUserAll(@RequestHeader(SecurityConstants.FROM_SOURCE) String source);
    /**
     * 根据手机号码列表获取用户
     * @param phoneNumber
     * @param source
     * @return
     */
    @GetMapping("/user/getUsersByPhoneNumber")
    R<List<SysUser>> getUsersByPhoneNumber(@RequestParam("phoneNumber") String phoneNumber, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/user/getUserByID")
    R<SysUser> getUserByID(@RequestParam("userId") String userId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
