package com.piggy.sys.api.bo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
@ApiModel("文件上传")
public class OssFileUploadBo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String fileName;

    private String content;
}
