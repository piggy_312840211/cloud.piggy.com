package com.piggy.sys.api;

import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.sys.api.domain.SysUser;
import com.piggy.sys.api.factory.RemoteRoleMenuFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 角色菜单服务
 *
 * @author shark
 */
@FeignClient(contextId = "remoteRoleMenuService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteRoleMenuFallbackFactory.class)
public interface RemoteRoleMenuService {

    @GetMapping("/role/getAssignees")
    List<SysUser> getAssignees(@RequestParam("menuIds")  List<String> menuIds, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
