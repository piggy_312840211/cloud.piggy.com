package com.piggy.sys.api.bo;

import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 场景角色分页查询对象 sys_role_scene_rl
 *
 * @author shark
 * @date 2021-10-15
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("场景角色分页查询对象")
public class SysRoleSceneRlQueryBo extends BaseEntity {

	/** ID */
	@ApiModelProperty("ID")
	private Long id;
	/** 场景id */
	@ApiModelProperty("场景id")
	private Long sceneId;
	/** 角色ID */
	@ApiModelProperty("角色ID")
	private Long roleId;
	/** 企业类型 */
	@ApiModelProperty("企业类型")
	private String busineseType;
	/** 启用状态：
0、启用
1、禁用 */
	@ApiModelProperty("启用状态")
	private String status;

}
