package com.piggy.sys.api;

import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.domain.SysOss;
import com.piggy.sys.api.factory.RemoteOssFallbackFactory;
import com.piggy.sys.api.vo.SysOssVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 用户服务
 *
 * @author shark
 */
@FeignClient(contextId = "remoteOssService", value = ServiceNameConstants.SYSTEM_SERVICE,fallbackFactory = RemoteOssFallbackFactory.class)
public interface RemoteOssService {
    /**
     * 上传文件
     *
     * @param file 文件流
     * @param source 请求来源
     * @return 结果
     */
    @PostMapping(value = "/oss/uploadFileToCloud", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    SysOss uploadFileToCLoud(@RequestPart("file") MultipartFile file, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping(value = "/oss/getSysOssVo")
    R<SysOssVo> getSysOssById(@RequestParam("id") String id, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
