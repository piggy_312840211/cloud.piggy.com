package com.piggy.sys.api.factory;

import com.piggy.sys.api.domain.SysDept;
import com.piggy.sys.api.RemoteDeptService;
import com.piggy.common.core.domain.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户服务降级处理98
 *
 * @author shark
 */
@Component
public class RemoteDeptFallbackFactory implements FallbackFactory<RemoteDeptService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteDeptFallbackFactory.class);

    @Override
    public RemoteDeptService create(Throwable throwable) {
        log.error("部门服务调用失败:{}", throwable.getMessage());

        return new RemoteDeptService()
        {
            @Override
            public R<SysDept> getById(Long deptId, String source) {
                return R.fail("系统服务调用失败:" + throwable.getMessage());
            }

            @Override
            public Integer getOrgType(Long deptId, String sceneKey, String source) {
                return null;
            }

            @Override
            public Long getOrgDeptId(Long deptId, String source) {
                return null;
            }

            @Override
            public R<SysDept> getInfo(Long deptId, String source) {
                return R.fail("系统服务调用失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysDept>> getDeptListByOrgType(String sceneKey, Integer busineseType, String source) {
                return R.fail("系统服务调用失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysDept>> queryDeptAll(String source) {
                return R.fail("系统服务调用失败:" + throwable.getMessage());
            }

            @Override
            public R<SysDept> saveSysDept(SysDept sysDept, String source) {
                return null;
            }


            @Override
            public R<SysDept> detailInfo(Long deptId, String source) {
                return R.fail("系统服务调用失败:" + throwable.getMessage());
            }

        };
    }
}
