package com.piggy.sys.api.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 注册邀请短信详情对象 tdms_registration_message
 *
 * @author piggy
 * @date 2021-10-11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Accessors(chain = true)
@TableName("tdms_registration_message")
public class SysRegistrationMessage implements Serializable {

    private static final long serialVersionUID=1L;


    /** 邀请方机构ID */
    private Long deptIdFrom;

    /** ID */
    @TableId(value = "id")
    private Long id;

	/** 场景id */
	private Long sceneId;

	/** 角色id */
	private Long roleId;

    /** 发送方id */
    private Long coreBusinessId;

    /** 邀请方企业名称 */
    private String nameFrom;

    /** 接收方机构ID */
    private Long deptIdTo;

    /** 接收方id */
    private Long businessId;

	/** 0核心企业1供应商2经销商3金融机构 */
	private String busineseType;

    /** 接收方企业名称 */
    private String nameTo;

    /** 手机号码 */
    private String phone;

    /** 短信信息 */
    private String phoneMessage;

    /** 公共邮箱 */
    private String email;

    /** 邮箱内容 */
    private String emailMessage;

    /** 发送状态
0、失败
1、成功 */
    private Long sendStatus;

    /** 删除标志（0代表存在 2代表删除） */
    @TableLogic
    private String delFlag;

    /** 创建者 */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 更新者 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /** 更新时间 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
