package com.piggy.sys.api.factory;

import com.piggy.sys.api.domain.SysRoleSceneRl;
import com.piggy.sys.api.RemoteISysRoleSceneRlService;
import com.piggy.sys.api.bo.SysRoleSceneRlQueryBo;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.vo.SysRoleSceneRlVo;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RemoteISysRoleSceneRlFallbackFactory implements FallbackFactory<RemoteISysRoleSceneRlService> {
    @Override
    public RemoteISysRoleSceneRlService create(Throwable throwable) {
        return new RemoteISysRoleSceneRlService() {

            @Override
            public R getSceneRole(Long id, String source) {
                return null;
            }

            @Override
            public R addOrUpdateRole(List<SysRoleSceneRl> roleSceneRls, Long sceneId, String source) {
                return null;
            }

            @Override
            public int activation(Long sceneId, String source) {
                return 0;
            }

            @Override
            public R<List<SysRoleSceneRlVo>> queryList(SysRoleSceneRlQueryBo bo, String source) {
                return R.fail("系统服务调用失败:" + throwable.getMessage());
            }
        };
    }
}
