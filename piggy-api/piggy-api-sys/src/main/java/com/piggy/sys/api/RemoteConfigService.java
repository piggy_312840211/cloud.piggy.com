package com.piggy.sys.api;

import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.domain.R;
import com.piggy.common.core.web.base.BaseResponse;
import com.piggy.sys.api.domain.SysConfig;
import com.piggy.sys.api.factory.RemoteConfigFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * 角色菜单服务
 *
 * @author shark
 */
@FeignClient(contextId = "remoteConfigService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteConfigFallbackFactory.class)
public interface RemoteConfigService extends BaseResponse {

    @GetMapping("/config/getConfigByKey/{configKey}")
    R<SysConfig> getConfigByKey(@PathVariable("configKey") String configKey, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/config/selectConfigByKey")
    R<String> selectConfigByKey(@RequestParam("configKey") String configKey, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/config/getConfigById/{configId}")
    R<SysConfig> getConfigById(@PathVariable("configId") Long configId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping(value = "/getConfigByGroupKey/{groupKey}")
    R<Map<String, SysConfig>> getConfigByGroupKey(@PathVariable("groupKey") String groupKey, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/config/updateSendRule")
    R updateSendRule(@RequestParam("noticeCondition")String noticeCondition, @RequestParam("noticeDay") String noticeDay, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
