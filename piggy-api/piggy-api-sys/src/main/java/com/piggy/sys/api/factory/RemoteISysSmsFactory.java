package com.piggy.sys.api.factory;

import com.piggy.common.core.utils.sms.SmsConfig;
import com.piggy.sys.api.RemoteISysSmsService;
import com.piggy.sys.api.bo.SysSmsBo;
import com.piggy.common.core.domain.R;
import org.springframework.cloud.openfeign.FallbackFactory;

import java.util.List;

public class RemoteISysSmsFactory implements FallbackFactory<RemoteISysSmsService> {
    @Override
    public RemoteISysSmsService create(Throwable cause) {
        return new RemoteISysSmsService() {
            @Override
            public R<Boolean> userRegSendSms(String phone, String source) {
                return R.fail("系统服务调用失败:" + cause.getMessage());
            }

            @Override
            public R<SmsConfig> getConfig(String source) {
                return R.fail("系统服务调用失败:" + cause.getMessage());
            }

            @Override
            public R<Boolean> sendMail(String content, String subject, List<String> params, String receiver) {
                return R.fail("系统服务调用失败:" + cause.getMessage());
            }

            @Override
            public R<Boolean> sendSms(SysSmsBo bo) {
                return R.fail("系统服务调用失败:" + cause.getMessage());
            }
        };
    }
}
