package com.piggy.sys.api.factory;

import com.piggy.common.core.domain.R;
import com.piggy.sys.api.RemoteClientService;
import com.piggy.sys.api.domain.SysClientDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户服务降级处理98
 *
 * @author shark
 */
@Component
public class RemoteClientFallbackFactory implements FallbackFactory<RemoteClientService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteClientFallbackFactory.class);

    @Override
    public RemoteClientService create(Throwable throwable) {
        log.error("客户端调用失败:{}", throwable.getMessage());
        return new RemoteClientService()
        {
            @Override
            public R<SysClientDetails> getClientDetailsById(String clientId, String from)
            {
                return R.fail("获取客户端失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysClientDetails>> listClientDetails(String from)
            {
                return R.fail("获取客户端列表失败:" + throwable.getMessage());
            }
        };
    }
}
