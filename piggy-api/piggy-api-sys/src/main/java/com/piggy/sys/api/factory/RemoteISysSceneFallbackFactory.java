package com.piggy.sys.api.factory;

import com.piggy.sys.api.RemoteISysRoleSceneRlService;
import org.springframework.cloud.openfeign.FallbackFactory;

public class RemoteISysSceneFallbackFactory implements FallbackFactory<RemoteISysRoleSceneRlService> {
    @Override
    public RemoteISysRoleSceneRlService create(Throwable cause) {
        return null;
    }
}
