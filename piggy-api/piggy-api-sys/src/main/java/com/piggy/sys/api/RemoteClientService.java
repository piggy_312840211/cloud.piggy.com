package com.piggy.sys.api;

import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.factory.RemoteClientFallbackFactory;
import com.piggy.sys.api.domain.SysClientDetails;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户服务
 *
 * @author shark
 */
@FeignClient(contextId = "remoteClientService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteClientFallbackFactory.class)
public interface RemoteClientService {

    /**
     * 通过clientId 查询客户端信息
     * @param clientId 用户名
     * @param source 调用标志
     * @return R
     */
    @GetMapping("/client/{clientId}")
    R<SysClientDetails> getClientDetailsById(@PathVariable("clientId") String clientId,
                                             @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 查询全部客户端
     * @param source 调用标识
     * @return R
     */
    @GetMapping("/client/list")
    R<List<SysClientDetails>> listClientDetails(@RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
