package com.piggy.sys.api.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 终端配置场景部门对象 sys_client_scene
 *
 * @author piggy
 * @date 2022-09-19
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("sys_client_scene")
public class SysClientScene implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 终端编号
     */
    private String clientId;

    /**
     * 场景id
     */
    private Long sceneId;

    /**
     * 核心企业部门id
     */
    private Long coreDeptId;

    /**
     * 是否停用 1 启用 0 停用
     */
    private Integer validFlag;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
