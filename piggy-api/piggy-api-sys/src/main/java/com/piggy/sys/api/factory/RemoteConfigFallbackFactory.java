package com.piggy.sys.api.factory;

import com.piggy.common.core.domain.R;
import com.piggy.sys.api.domain.SysConfig;
import com.piggy.sys.api.RemoteConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 系统配置服务降级处理
 *
 * @author shark
 */
@Slf4j
@Component
public class RemoteConfigFallbackFactory implements FallbackFactory<RemoteConfigService> {

    @Override
    public RemoteConfigService create(Throwable throwable) {
        log.error("系统配置服务调用失败:{}", throwable.getMessage());
        return new RemoteConfigService()
        {

            @Override
            public R<SysConfig> getConfigByKey(String configKey, String source) {
                return R.fail("获取系统参数失败:" + throwable.getMessage());
            }

            @Override
            public R<String> selectConfigByKey(String configKey, String source) {
                return R.fail("获取系统参数失败:" + throwable.getMessage());
            }

            @Override
            public R<SysConfig> getConfigById(Long configId, String source) {
                return R.fail("获取系统参数失败:" + throwable.getMessage());
            }

            @Override
            public R updateSendRule(String noticeCondition,String noticeDay,  String source) {
                return error("获取系统参数失败:" + throwable.getMessage());
            }

            @Override
            public R<Map<String, SysConfig>> getConfigByGroupKey(String groupKey, String source) {
                return error("获取系统参数失败:" + throwable.getMessage());
            }
        };
    }
}
