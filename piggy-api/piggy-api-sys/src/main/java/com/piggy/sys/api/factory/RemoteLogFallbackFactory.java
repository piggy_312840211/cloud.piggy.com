package com.piggy.sys.api.factory;

import com.piggy.sys.api.domain.SysLogininfor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.RemoteLogService;
import com.piggy.sys.api.domain.SysOperLog;

/**
 * 日志服务降级处理
 *
 * @author shark
 */
@Component
public class RemoteLogFallbackFactory implements FallbackFactory<RemoteLogService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteLogFallbackFactory.class);

    @Override
    public RemoteLogService create(Throwable throwable) {
        log.error("日志服务调用失败:{}", throwable.getMessage());
        return new RemoteLogService()
        {
            @Override
            public R<Boolean> saveLog(SysOperLog sysOperLog, String source)
            {
                log.error("日志服务调用失败:{}", "savelog");
                return R.fail("日志服务调用失败:" + throwable.getMessage());
            }

            @Override
            public R<Boolean> saveLogininfor(SysLogininfor sysLogininfor, String source)
            {
                log.error("日志服务调用失败:{}", "saveLogininfor");
                return R.fail("日志服务调用失败:" + throwable.getMessage());
            }
        };
    }
}
