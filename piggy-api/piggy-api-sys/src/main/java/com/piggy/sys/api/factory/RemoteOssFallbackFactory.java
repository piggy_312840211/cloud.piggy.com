package com.piggy.sys.api.factory;

import com.piggy.common.core.domain.R;
import com.piggy.sys.api.RemoteOssService;
import com.piggy.sys.api.domain.SysOss;
import com.piggy.sys.api.vo.SysOssVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * oss服务降级处理98
 *
 * @author shark
 */
@Component
public class RemoteOssFallbackFactory implements FallbackFactory<RemoteOssService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteOssFallbackFactory.class);

    @Override
    public RemoteOssService create(Throwable throwable) {
        log.error("oss服务调用失败:{}", throwable.getMessage(),throwable);
        return new RemoteOssService()
        {
            @Override
            public SysOss uploadFileToCLoud(MultipartFile file, String source) {
                return null;
            }

            @Override
            public R<SysOssVo> getSysOssById(String id, String source) {
                return null;
            }



        };
    }
}
