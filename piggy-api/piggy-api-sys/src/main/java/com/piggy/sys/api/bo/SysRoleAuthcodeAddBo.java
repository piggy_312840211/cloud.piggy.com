package com.piggy.sys.api.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 角色授权码添加对象 sys_role_authcode
 *
 * @author piggy
 * @date 2021-09-14
 */
@Data
@AllArgsConstructor
@Builder
@Accessors(chain = true)
@ApiModel("角色授权码添加对象")
public class SysRoleAuthcodeAddBo {



    /** 角色id */
    @ApiModelProperty("角色id")
	@NotNull(message = "角色id不能为空")
    private Long roleId;

    /** 组织部门id */
    @ApiModelProperty("组织部门id")
	@NotNull(message = "组织部门id不能为空")
    private Long orgDeptId;

    /** 状态 0未使用 1已使用  */
    @ApiModelProperty("状态 0未使用 1已使用 ")
    private Integer status;

    /** 0平台添加1核心企业添加 */
    @ApiModelProperty("0平台添加1核心企业添加 ")
    private Integer type;

    /** 授权码 */
    @ApiModelProperty("授权码")
	@NotBlank(message = "授权码不能为空")
    private String authCode;

	/** 手机号 */
	@ApiModelProperty("手机号")
	private String phone;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;
}
