package com.piggy.sys.api;

import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.utils.sms.SmsConfig;
import com.piggy.sys.api.factory.RemoteDeptFallbackFactory;
import com.piggy.sys.api.bo.SysSmsBo;
import com.piggy.common.core.domain.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(contextId = "remoteISysSmsService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteDeptFallbackFactory.class)
public interface RemoteISysSmsService {

    /**
     * 获取注册用户手机验证码
     * @return
     */
    @GetMapping("/sysSms/userRegSendSms")
    R<Boolean> userRegSendSms(@RequestParam("phone")String phone, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/sysSms/getConfig")
    R<SmsConfig> getConfig(@RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/sysSms/sendMail")
    R<Boolean> sendMail(@RequestParam("content")String content, @RequestParam("subject")String subject, @RequestParam("params")List<String> params, @RequestParam("receiver")String receiver);


    @PostMapping("/sysSms/sendSms")
    R<Boolean> sendSms(@RequestBody SysSmsBo bo);



}
