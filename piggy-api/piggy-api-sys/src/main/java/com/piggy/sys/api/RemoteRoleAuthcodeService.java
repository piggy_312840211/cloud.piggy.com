package com.piggy.sys.api;

import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.sys.api.bo.SysRoleAuthcodeEditBo;
import com.piggy.sys.api.factory.RemoteRoleAuthcodeFallbackFactory;
import com.piggy.common.core.domain.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(contextId = "remoteRoleAuthcodeService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteRoleAuthcodeFallbackFactory.class)
public interface RemoteRoleAuthcodeService {

    @PostMapping("/roleAuthcode/updateBySceneIdAndBusinessId")
    R<Boolean> updateBySceneIdAndBusinessId(@RequestBody SysRoleAuthcodeEditBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
