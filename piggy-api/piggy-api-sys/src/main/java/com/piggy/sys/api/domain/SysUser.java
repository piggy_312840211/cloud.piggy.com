package com.piggy.sys.api.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.piggy.common.core.annotation.Excel;
import com.piggy.common.core.annotation.Excel.ColumnType;
import com.piggy.common.core.annotation.Excel.Type;
import com.piggy.common.core.annotation.Excels;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户对象 sys_user
 *
 * @author shark
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Accessors(chain = true)
@TableName("sys_user")
@ApiModel("用户对象")
public class SysUser implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    @Excel(name = "用户序号", cellType = ColumnType.NUMERIC, prompt = "用户编号")
    @TableId(value = "user_id",type = IdType.AUTO)
    private Long userId;

    /** 部门ID */
    @ApiModelProperty(value = "部门ID",required = true)
    @Excel(name = "部门编号", type = Type.IMPORT)
    private Long deptId;

    /** 登录名称*/
    @ApiModelProperty(value = "登录名称",required = true)
    @NotBlank(message = "登录名称不能为空")
    @Size(min = 0, max = 30, message = "登录名称长度不能超过30个字符")
    @Excel(name = "登录名称")
    private String userName;

    /** 用户昵称 */
    @ApiModelProperty(value = "用户昵称",required = true)
    @Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
    @Excel(name = "用户名称")
    private String nickName;

    /** 用户邮箱 */
    @ApiModelProperty(value = "用户邮箱",required = true)
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    @Excel(name = "用户邮箱")
    private String email;

    /** 手机号码 */
    @ApiModelProperty(value = "手机号码",required = true)
    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    @NotBlank(message = "手机号不能为空")
    @Excel(name = "手机号码")
    private String phonenumber;

    /** 用户性别 */
    @ApiModelProperty(value = "用户性别",required = true)
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /** 用户头像 */
    @ApiModelProperty("用户头像")
    private String avatar;

    /** 密码 */
    @ApiModelProperty(value = "密码",required = true)
    @JsonProperty
    private String password;

    /** 盐加密 */
    @ApiModelProperty("盐加密")
    @TableField(exist = false)
    private String salt;

    /** 帐号状态（0正常 1停用） */
    @ApiModelProperty(value = "帐号状态（0正常 1停用）",required = true)
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    @ApiModelProperty("删除标志")
    @TableLogic
    private String delFlag;

    /** 最后登录IP */
    @ApiModelProperty("最后登录IP")
    @Excel(name = "最后登录IP", type = Type.EXPORT)
    private String loginIp;

    /** 最后登录时间 */
    @ApiModelProperty("最后登录时间")
    @Excel(name = "最后登录时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
    private Date loginDate;

    /** 创建者 */
    @ApiModelProperty(value = "创建者",required = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间",required = true)
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新者 */
    @ApiModelProperty(value = "更新者",required = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /** 更新时间 */
    @ApiModelProperty(value = "更新时间",required = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;

    /**
     * 请求参数
     */
    @ApiModelProperty("请求参数")
    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();

    /** 部门对象 */
    @ApiModelProperty("部门对象")
    @Excels({
            @Excel(name = "部门名称", targetAttr = "deptName", type = Type.EXPORT),
            @Excel(name = "部门负责人", targetAttr = "leader", type = Type.EXPORT)
    })
    @TableField(exist = false)
    private SysDept dept;

    /** 角色对象 */
    @ApiModelProperty("角色对象")
    @TableField(exist = false)
    private List<SysRole> roles;

    /** 角色组 */
    @ApiModelProperty("角色组")
    @TableField(exist = false)
    private Long[] roleIds;

    /** 岗位组 */
    @ApiModelProperty("岗位组")
    @TableField(exist = false)
    private Long[] postIds;


    /**
     * 角色ID
     */
    @TableField(exist = false)
    private Long roleId;

    public SysUser(Long userId)
    {
        this.userId = userId;
    }

    public boolean isAdmin()
    {
        return isAdmin(this.userId);
    }

    public static boolean isAdmin(Long userId)
    {
        return userId != null && 1L == userId;
    }

}
