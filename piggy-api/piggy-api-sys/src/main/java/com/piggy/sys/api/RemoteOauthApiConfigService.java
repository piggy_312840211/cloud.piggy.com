package com.piggy.sys.api;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.factory.RemoteOauthApiConfigFallbackFactory;
import com.piggy.sys.api.vo.SysOauthApiConfigVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(contextId = "remoteOauthApiConfigService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteOauthApiConfigFallbackFactory.class)
public interface RemoteOauthApiConfigService
{
    @GetMapping(value="/api/config/getApiConfigInfo")
    R<SysOauthApiConfigVo> getApiConfigInfo();

    @GetMapping(value="/api/config/updateApiConfig")
    R  updateApiConfig(@RequestParam(required = true,name = "callbackUrl") String callbackUrl);
}
