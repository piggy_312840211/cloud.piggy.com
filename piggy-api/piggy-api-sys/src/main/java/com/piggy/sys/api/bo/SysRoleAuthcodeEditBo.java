package com.piggy.sys.api.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 角色授权码编辑对象 sys_role_authcode
 *
 * @author shark
 * @date 2021-09-14
 */
@Data
@ApiModel("角色授权码编辑对象")
public class SysRoleAuthcodeEditBo {


    /** 序号 */
    @ApiModelProperty("序号")
	@NotNull(message = "id不能为空")
    private Long id;

    /** 创建人 */
    @ApiModelProperty("创建人")
    private Long creator;

    /** 创建人姓名 */
    @ApiModelProperty("创建人姓名")
    private String creatorName;

    /** 编辑人 */
    @ApiModelProperty("编辑人")
    private Long editor;

    /** 编辑人姓名 */
    @ApiModelProperty("编辑人姓名")
    private String editorName;

    /** 编辑时间 */
    @ApiModelProperty("编辑时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date editTime;

    /** 删除人 */
    @ApiModelProperty("删除人")
    private Long deleter;

    /** 删除人姓名 */
    @ApiModelProperty("删除人姓名")
    private String deleterName;

    /** 删除时间 */
    @ApiModelProperty("删除时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date deleteTime;

    /** 角色id */
    @ApiModelProperty("角色id")
	@NotNull(message = "角色id不能为空")
    private Long roleId;

    /** 组织部门id */
    @ApiModelProperty("组织部门id")
	@NotNull(message = "组织部门id不能为空")
    private Long orgDeptId;

    /** 状态 0未使用 1已使用  */
    @ApiModelProperty("状态 0未使用 1已使用 ")
	@NotNull(message = "状态不能为空")
    private Integer status;

    /** 授权码 */
    @ApiModelProperty("授权码")
	@NotBlank(message = "授权码不能为空")
    private String authCode;

	/** 手机号 */
	@ApiModelProperty("手机号")
	@NotBlank(message = "手机号不能为空")
	private String phone;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;

    /** 手机号 */
    private String businessType;

    /** 场景id */
    private Long sceneId;

    /** 企业id */
    private Long businessId;
}
