package com.piggy.sys.api.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 终端授权申请对象 sys_oauth_request
 *
 * @author piggy
 * @date 2022-09-21
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("sys_oauth_request")
public class SysOauthRequest implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 核心企业id
     */
    private Long coreBusinessId;

    /**
     * 申请状态 0 待审核 1 审核中 2 审核完成 3 审核失败
     */
    private Integer requestStatus;

    /**
     * 审核失败原因
     */
    private String reason;

    /**
     * 删除状态：0有效，1删除
     */
    @TableLogic
    private Integer delFlag;

    /**
     * 删除人
     */
    private String deleter;

    /**
     * 申请时间
     */
    private Date requestTime;

    /**
     * 审核时间
     */
    private Date auditTime;

    /**
     * 审核人
     */
    private String auditor;

    /**
     * 提交人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
