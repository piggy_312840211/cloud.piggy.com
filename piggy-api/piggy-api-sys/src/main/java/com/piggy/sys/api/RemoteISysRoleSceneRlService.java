package com.piggy.sys.api;

import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.sys.api.bo.SysRoleSceneRlQueryBo;
import com.piggy.sys.api.domain.SysRoleSceneRl;
import com.piggy.sys.api.factory.RemoteISysRoleSceneRlFallbackFactory;
import com.piggy.sys.api.vo.SysRoleSceneRlVo;
import com.piggy.common.core.domain.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(contextId = "remoteISysRoleSceneRlService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteISysRoleSceneRlFallbackFactory.class)
public interface RemoteISysRoleSceneRlService {

    /**
     * 根据场景id获取角色列表
     * @param id
     * @return
     */
    @GetMapping("/roleSceneRl/getSceneRole")
    R getSceneRole(@RequestParam("id")Long id, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping("/roleSceneRl/addOrUpdateRole/{sceneId}")
    R addOrUpdateRole(@RequestBody List<SysRoleSceneRl> roleSceneRls, @PathVariable("sceneId") Long sceneId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/roleSceneRl/activation")
    int activation(@RequestParam("sceneId")Long sceneId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping("/roleSceneRl/queryList")
    R<List<SysRoleSceneRlVo>> queryList(@RequestBody SysRoleSceneRlQueryBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
