package com.piggy.sys.api.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 场景角色对象 sys_role_scene_rl
 *
 * @author piggy
 * @date 2021-10-15
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("sys_role_scene_rl")
public class SysRoleSceneRl implements Serializable {

    private static final long serialVersionUID=1L;


    /** ID */
    @TableId(value = "id")
    private Long id;

    /** 场景id */
    private Long sceneId;

    /** 角色ID */
    private Long roleId;

    /** 企业类型 */
    private String busineseType;

    /** 启用状态：
0、启用
1、禁用 */
    private String status;

    /** 备注 */
    private String remark;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者 */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 更新者 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /** 更新时间 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
