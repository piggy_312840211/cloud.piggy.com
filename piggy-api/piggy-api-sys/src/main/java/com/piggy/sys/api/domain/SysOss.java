package com.piggy.sys.api.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 文件上传对象 sys_oss
 *
 * @author piggy
 * @date 2021-07-20
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("sys_oss")
public class SysOss implements Serializable {

    private static final long serialVersionUID=1L;


    /** $column.columnComment */
    @TableId(value = "id")
    private Long id;

    /** URL地址 */
    private String url;

    /*大小*/
    private Long size;

    /** 文件类型 */
    private String fileType;

    /** 文件名 */
    private String name;

    /** 原文件名 */
    private String originName;

    /** 类型名 */
    private String path;

    /** 解析值 */
    private String value;

    /** 状态 */
    private String status;

    /** 创建者 */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 更新者 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /** 更新时间 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /** 备注 */
    private String remark;

}
