package com.piggy.sys.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel("流程任务出参")
public class FlowTaskVo implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty("任务Id")
	private String taskId;
	@ApiModelProperty("任务名称")
	private String taskName;
	@ApiModelProperty("创建时间")
	private Date createTime;
	@ApiModelProperty("优先级")
	private Integer priority;
	@ApiModelProperty("代理人")
	private String assignee;
	@ApiModelProperty("流程定义键")
	private String taskDefinitionKey;
}
