package com.piggy.sys.api.factory;

import com.piggy.sys.api.RemoteRegistrationMessageService;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.domain.SysRegistrationMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class RemoteRegistrationMessageFallbackFactory implements FallbackFactory<RemoteRegistrationMessageService> {

    private static final Logger log = LoggerFactory.getLogger(RemoteRoleFallbackFactory.class);

    @Override
    public RemoteRegistrationMessageService create(Throwable throwable) {
        log.error("注册服务调用失败:{}", throwable.getMessage());
        return new RemoteRegistrationMessageService()
        {


            @Override
            public R<Boolean> sendRegisterMessage(SysRegistrationMessage registrationMessage, String source) {
                return R.fail("注册服务调用失败:" + throwable.getMessage());
            }

        };
    }
}
