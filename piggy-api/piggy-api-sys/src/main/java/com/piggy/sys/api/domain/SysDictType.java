package com.piggy.sys.api.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.piggy.common.core.annotation.Excel;
import com.piggy.common.core.annotation.Excel.ColumnType;
import com.piggy.common.core.web.domain.BaseEntity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 字典类型表 sys_dict_type
 *
 * @author shark
 */

@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("sys_dict_type")
@ApiModel("字典类型")
public class SysDictType implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 字典主键 */
    @ApiModelProperty("字典主键")
    @Excel(name = "字典主键", cellType = ColumnType.NUMERIC)
    @TableId(value = "dict_id",type = IdType.AUTO)
    private Long dictId;

    /** 字典名称 */
    @ApiModelProperty(value = "字典名称",required = true)
    @Excel(name = "字典名称")
    @NotBlank(message = "字典名称不能为空")
    @Size(min = 0, max = 100, message = "字典类型名称长度不能超过100个字符")
    private String dictName;

    /** 字典类型 */
    @ApiModelProperty(value = "字典类型 ",required = true)
    @Excel(name = "字典类型")
    @NotBlank(message = "字典类型不能为空")
    @Size(min = 0, max = 100, message = "字典类型类型长度不能超过100个字符")
    private String dictType;

    /** 状态（0正常 1停用） */
    @ApiModelProperty("状态（0正常 1停用）")
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 创建者 */
    @ApiModelProperty("创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新者 */
    @ApiModelProperty("更新者")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /** 更新时间 */
    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;

    /**
     * 请求参数
     */
    @ApiModelProperty("请求参数")
    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();

}
