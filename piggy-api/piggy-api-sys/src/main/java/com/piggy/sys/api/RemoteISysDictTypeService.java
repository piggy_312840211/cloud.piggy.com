package com.piggy.sys.api;

import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.domain.SysDictData;
import com.piggy.sys.api.factory.RemoteDeptFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@FeignClient(contextId = "remoteISysDictTypeService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteDeptFallbackFactory.class)
public interface RemoteISysDictTypeService {
    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    @PostMapping("/dict/data/getDictType")
    public R<List<SysDictData>> selectDictDataByType(@RequestParam("dictType")  String dictType, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);


//    @PostMapping("/dict/data/getDictType")
//    public R<List<SysDictData>> selectDictDataByType1(@RequestParam("dictType")  String dictType, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
