package com.piggy.sys.api.model;

import com.piggy.sys.api.domain.SysClientDetails;
import com.piggy.sys.api.domain.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Set;

/**
 * 用户信息
 *
 * @author shark
 */
@Data
@Accessors(chain = true)
@ApiModel("登录用户身份权限")
public class LoginUser implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户唯一标识
     */
    private String token;

    /**
     * 用户名id
     */
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 登录时间
     */
    private Long loginTime;

    /**
     * 过期时间
     */
    private Long expireTime;

    /**
     * 登录IP地址
     */
    private String ipaddr;

    /**
     * 权限列表
     */
    private Set<String> permissions;

    /**
     * 角色列表
     */
    private Set<String> roles;

    /**
     * 用户信息
     */
    private SysUser sysUser;

    private SysClientDetails clientDetails;

    /**
     * 登录地点
     */
    @ApiModelProperty("登录地点")
    private String loginLocation;

    /**
     * 浏览器类型
     */
    @ApiModelProperty("浏览器类型")
    private String browser;

    /**
     * 操作系统
     */
    @ApiModelProperty("操作系统")
    private String os;

    private String userType;

}
