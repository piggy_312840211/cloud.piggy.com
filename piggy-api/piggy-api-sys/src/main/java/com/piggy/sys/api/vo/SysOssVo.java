package com.piggy.sys.api.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.annotation.Excel;
import com.piggy.oss.cloud.OSSFactory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


/**
 * 文件上传视图对象 sys_oss
 *
 * @author piggy
 * @date 2021-07-20
 */
@Data
@ApiModel("文件上传视图对象")
public class SysOssVo {

	private static final long serialVersionUID = 1L;

	/** $pkColumn.columnComment */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

	/** URL地址 */
	@Excel(name = "URL地址")
	@ApiModelProperty("URL地址")
	private String url;

	/** 文件类型 */
	@Excel(name = "文件类型")
	@ApiModelProperty("文件类型")
	private String fileType;

	/** 文件名 */
	@Excel(name = "文件名")
	@ApiModelProperty("文件名")
	private String name;

	/** 原文件名 */
	@Excel(name = "原文件名")
	@ApiModelProperty("原文件名")
	private String originName;

	/** 类型名 */
	@Excel(name = "文件路径")
	@ApiModelProperty("文件路径")
	private String path;

	/** 解析值 */
	@Excel(name = "解析值")
	@ApiModelProperty("解析值")
	private String value;

	@Excel(name = "大小")
	@ApiModelProperty("大小")
	private String size;

	/** 状态 */
	@Excel(name = "状态")
	@ApiModelProperty("状态")
	private String status;

	/** 创建者 */
	@Excel(name = "创建者")
	@ApiModelProperty("创建者")
	private String createBy;

	/** 创建时间 */
	@Excel(name = "创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createTime;

	/** 备注 */
	@Excel(name = "备注")
	@ApiModelProperty("备注")
	private String remark;

	public String getUrl() {
		return OSSFactory.build().buildUrl(this.path);
	}
}
