package com.piggy.sys.api.factory;

import com.piggy.common.core.domain.R;
import com.piggy.sys.api.RemoteOauthApiConfigService;
import com.piggy.sys.api.vo.SysOauthApiConfigVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class RemoteOauthApiConfigFallbackFactory  implements FallbackFactory<RemoteOauthApiConfigService>
{
    private static final Logger log = LoggerFactory.getLogger(RemoteOauthApiConfigFallbackFactory.class);

    @Override
    public RemoteOauthApiConfigService create(Throwable throwable) {
        log.error("客户端调用失败:{}", throwable.getMessage());
        return new RemoteOauthApiConfigService() {

            @Override
            public R<SysOauthApiConfigVo> getApiConfigInfo() {
                return null;
            }

            @Override
            public R updateApiConfig(String callbackUrl) {
                return null;
            }
        };
    }
}
