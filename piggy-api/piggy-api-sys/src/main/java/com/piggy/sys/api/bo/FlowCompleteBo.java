package com.piggy.sys.api.bo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
@ApiModel("流程签署")
public class FlowCompleteBo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String processDefinitionKey;
    private String taskDefinitionKey;
    private Long businessKey;
    private Map<String, Object> vars;

}
