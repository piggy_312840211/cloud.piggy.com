package com.piggy.sys.api.factory;

import com.piggy.sys.api.RemoteRoleMenuService;
import com.piggy.sys.api.domain.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 角色服务降级处理
 *
 * @author shark
 */
@Component
public class RemoteRoleMenuFallbackFactory implements FallbackFactory<RemoteRoleMenuService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteRoleMenuFallbackFactory.class);

    @Override
    public RemoteRoleMenuService create(Throwable throwable) {
        log.error("角色菜单服务调用失败:{}", throwable.getMessage());
        return new RemoteRoleMenuService()
        {

            @Override
            public List<SysUser> getAssignees(List<String> menuIds, String source) {
                return null;
            }

        };
    }
}
