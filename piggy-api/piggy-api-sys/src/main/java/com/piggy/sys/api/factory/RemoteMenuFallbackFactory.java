package com.piggy.sys.api.factory;

import com.piggy.sys.api.RemoteMenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 菜单服务降级处理
 *
 * @author shark
 */
@Component
public class RemoteMenuFallbackFactory implements FallbackFactory<RemoteMenuService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteMenuFallbackFactory.class);

    @Override
    public RemoteMenuService create(Throwable throwable) {
        log.error("菜单服务调用失败:{}", throwable.getMessage());
        return new RemoteMenuService()
        {

            @Override
            public List<Long> getButtonIdsByUserId(Long userId, String source) {
                return null;
            }

        };
    }
}
