package com.piggy.sys.api.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 参数配置表 sys_config
 *
 * @author shark
 */

@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("sys_config")
public class SysConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 参数主键
     */
    @ApiModelProperty("参数主键")
    @Excel(name = "参数主键", cellType = Excel.ColumnType.NUMERIC)
    @TableId(value = "config_id", type = IdType.AUTO)
    private Long configId;

    /**
     * 参数名称
     */
	@ApiModelProperty("参数名称")
    @Excel(name = "参数名称")
    @NotBlank(message = "参数名称不能为空")
    @Size(min = 0, max = 100, message = "参数名称不能超过100个字符")
    private String configName;

    /**
     * 参数键名
     */
	@ApiModelProperty("参数键名")
    @Excel(name = "参数键名")
    @NotBlank(message = "参数键名长度不能为空")
    @Size(min = 0, max = 100, message = "参数键名长度不能超过100个字符")
    private String configKey;

    /**
     * 参数键值
     */
	@ApiModelProperty("参数键值")
    @Excel(name = "参数键值")
    @NotBlank(message = "参数键值不能为空")
    @Size(min = 0, max = 1000, message = "参数键值长度不能超过1000个字符")
    private String configValue;

    /**
     * 系统内置（Y是 N否）
     */
	@ApiModelProperty("系统内置（Y是 N否）")
    @Excel(name = "系统内置", readConverterExp = "Y=是,N=否")
    private String configType;

    /**
     * 参数键名
     */
    @ApiModelProperty("分组key")
    @Excel(name = "分组key")
    @NotBlank(message = "分组key长度不能为空")
    @Size(min = 0, max = 100, message = "分组key长度不能超过100个字符")
    private String groupKey;

    /**
     * 参数键名
     */
    @ApiModelProperty("分组name")
    @Excel(name = "分组名称")
    @NotBlank(message = "分组name长度不能为空")
    @Size(min = 0, max = 100, message = "分组name长度不能超过100个字符")
    private String groupName;

    /**
     * 创建者
     */
	@ApiModelProperty("创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
	@ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新者
     */
	@ApiModelProperty("更新者")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
	@ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 备注
     */
	@ApiModelProperty("备注")
    private String remark;

    /**
     * 请求参数
     */
	@ApiModelProperty("请求参数")
    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();

}
