package com.piggy.sys.api.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 终端配置表 sys_oauth_client_details
 *
 * @author ruoyi
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("sys_oauth_client_details")
@ApiModel("客户端信息")
public class SysClientDetails implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     * 终端编号
     */
    private String clientId;

    /**
     * 资源ID标识
     */
    private String resourceIds;

    /**
     * 核心企业id
     */
    private Long coreDepId;

    /**
     * 终端安全码
     */
    private String clientSecret;

    /**
     * 终端授权范围
     */
    private String scope;

    /**
     * 终端授权类型
     */
    private String authorizedGrantTypes;

    /**
     * 服务器回调地址
     */
    private String webServerRedirectUri;

    /**
     * 访问资源所需权限
     */
    private String authorities;

    /**
     * 设定终端的access_token的有效时间值（秒）
     */
    private Integer accessTokenValidity;

    /**
     * 设定终端的refresh_token的有效时间值（秒）
     */
    private Integer refreshTokenValidity;

    /**
     * 附加信息
     */
    private String additionalInformation;

    /**
     * 是否登录时跳过授权
     */
    private String autoapprove;

    /**
     * 终端明文安全码
     */
    private String originSecret;

    /**
     * 状态
     */
    private Integer clientStatus;

    @TableField(exist = false)
    private List<SysClientScene> sceneList;
}
