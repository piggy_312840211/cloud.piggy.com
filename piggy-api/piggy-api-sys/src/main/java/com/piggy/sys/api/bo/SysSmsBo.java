package com.piggy.sys.api.bo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;


/**
 * 消息模板添加对象 sys_sms
 *
 * @author shark
 * @date 2021-08-16
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel("消息发送对象")
public class SysSmsBo {

	/**
	 * 接收人用户id
	 */
	private Long userId;

	/**
	 * 接收人用户id
	 */
	private Long sendUserId;
	/**
	 * 主题如：发送短信验证码
	 */
	private String subject;

	/**
	 * 签名
	 */
	private String signName;

	/**
	 * 0-站内信 1-邮件 2-短信
	 */
	private Integer type;

	/**
	 * 手机号(单个)
	 */
	String phoneNumber;
	/**
	 * 模板code(单个)
	 */
	private String templateCode;
	/**
	 * 内容参数(单个)
	 */
	private String templateParam;

	/**
	 * 示例值：
	 * ["1590000****","1350000****"]
	 */
	private List<String> phoneNumbers;
	/**
	 * ["阿里云","阿里巴巴"]
	 */
	private List<String> signNames;
	/**
	 * [{"name":"TemplateParamJson"},{"name":"TemplateParamJson"}]
	 */
	private List<String> templateParams;
}
