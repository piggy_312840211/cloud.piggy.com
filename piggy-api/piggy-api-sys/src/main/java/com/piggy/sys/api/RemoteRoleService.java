package com.piggy.sys.api;

import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.domain.SysRole;
import com.piggy.sys.api.domain.SysUser;
import com.piggy.sys.api.factory.RemoteRoleFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色服务
 *
 * @author shark
 */
@FeignClient(contextId = "remoteRoleService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteRoleFallbackFactory.class)
public interface RemoteRoleService {

    @GetMapping("/role/check/{userId}")
    public boolean checkRoleNotAdmin(@PathVariable(value = "userId") Long userId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/role/getRoleByKey/{roleKey}")
    public R<SysRole> getRoleByKey(@PathVariable(value = "roleKey") String roleKey, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/role/getRoleUser/{roleId}")
    R<List<SysUser>> getRoleUsers(@PathVariable(value = "roleId") Long roleId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/role/getRoleUserByRoleIds")
    R<List<SysUser>> getRoleUserByRoleIds(@RequestParam("roleIds") List<Long> roleIds,  @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/role/getRoleUserBySceneIds")
    R<List<SysUser>> getRoleUserBySceneIds(@RequestParam("sceneIds") List<Long> sceneIds,  @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping("/role/queryRoleAll")
    R<List<SysRole>> queryRoleAll(@RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/role/getById/{roleId}")
    public R<SysRole> getById(@PathVariable(value = "roleId") Long roleId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
