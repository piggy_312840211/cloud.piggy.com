package com.piggy.oss.utils;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.aliyun.ocr20191230.Client;
import com.aliyun.ocr20191230.models.RecognizePdfRequest;
import com.aliyun.ocr20191230.models.RecognizePdfResponse;
import com.aliyun.teaopenapi.models.Config;
import com.piggy.common.core.constant.Constants;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.common.core.utils.SpringUtils;
import com.piggy.common.redis.service.RedisService;
import com.piggy.oss.cloud.CloudStorageConfig;
import com.piggy.oss.constants.CloudConstants;

public class OCRUtils {
	private static RedisService redisCache;

	static {
		OCRUtils.redisCache =  SpringUtils.getBean(RedisService.class);
	}
	/**
	 * 使用AK&SK初始化阿里云账号Client
	 * @param accessKeyId
	 * @param accessKeySecret
	 * @return Client
	 * @throws Exception
	 */
	public static Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
		Config config = new Config()
			// 您的AccessKey ID
			.setAccessKeyId(accessKeyId)
			// 您的AccessKey Secret
			.setAccessKeySecret(accessKeySecret);
		// 访问的域名
		config.endpoint = "ocr.cn-shanghai.aliyuncs.com";
		return new Client(config);
	}

	/**
	 * 调用阿里云视觉平台识别
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public static String pdfOcr(String url) {
		StringBuffer result=new StringBuffer();
		try{
			//获取云存储配置信息
			String cloudConfig = redisCache.getCacheObject(Constants.SYS_CONFIG_KEY + CloudConstants.CONTRACT_CLOUDCONFIG);
			if (StrUtil.isEmpty(cloudConfig))throw new BaseException("系统未配置云服务");
			CloudStorageConfig config = JSON.parseObject(cloudConfig, CloudStorageConfig.class);

			Client client = createClient(config.getAliyunAccessKeyId(), config.getAliyunAccessKeySecret());
			RecognizePdfRequest recognizePdfRequest = new RecognizePdfRequest()
				.setFileURL(url);
			// 复制代码运行请自行打印 API 的返回值
			RecognizePdfResponse recognizePdfResponse = client.recognizePdf(recognizePdfRequest);
//        System.out.println(recognizePdfResponse.getBody().getData().getWordsInfo());

			for(int i=0;i<recognizePdfResponse.body.getData().getWordsInfo().size();i++){
				result.append(recognizePdfResponse.body.getData().getWordsInfo().get(i).getWord().replaceAll(" ",""));
			}

		}catch (Exception e){
//			throw new BaseException("网络异常，请重新上传");
			result.append("OCR mistake!");
		}
		return result.toString();

	}
	public  String pdfOcr1(String url) throws Exception {
		//获取云存储配置信息
		String cloudConfig = redisCache.getCacheObject(Constants.SYS_CONFIG_KEY + CloudConstants.CONTRACT_CLOUDCONFIG);
		if (StrUtil.isEmpty(cloudConfig))throw new BaseException("系统未配置云服务");
		CloudStorageConfig config = JSON.parseObject(cloudConfig, CloudStorageConfig.class);

		Client client = createClient(config.getAliyunAccessKeyId(), config.getAliyunAccessKeySecret());
		RecognizePdfRequest recognizePdfRequest = new RecognizePdfRequest()
			.setFileURL(url);
		// 复制代码运行请自行打印 API 的返回值
		RecognizePdfResponse recognizePdfResponse = client.recognizePdf(recognizePdfRequest);
//        System.out.println(recognizePdfResponse.getBody().getData().getWordsInfo());
		StringBuffer result=new StringBuffer();
		for(int i=0;i<recognizePdfResponse.body.getData().getWordsInfo().size();i++){
			result.append(recognizePdfResponse.body.getData().getWordsInfo().get(i).getWord().replaceAll(" ",""));
		}
		return result.toString();
	}

	public static void main(String[] args_) throws Exception {
	}
}
