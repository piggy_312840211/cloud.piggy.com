package com.piggy.oss.utils;

import com.piggy.oss.cloud.OSSFactory;

public class OSSUtils {

    public static String getUrl(String url) {
        return OSSFactory.build().buildUrl(url);
    }
}
