package com.piggy.oss.config;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 百度智能云配置
 */
@Data
public class BaiduOCRConfig {
	//百度云的apiKey
	@NotBlank(message="百度云的apiKey不能为空")
	private String apiKey;
	//百度云的secretKey
	@NotBlank(message="百度云的secretKey不能为空")
	private String secretKey;
}
