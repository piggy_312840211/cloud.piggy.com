package com.piggy.oss.constants;

public class CloudConstants {
    /**
     * 云文件存储配置
     */
    public static final String CONTRACT_CLOUDCONFIG = "cloudconfig";
    /**
     * 云文件存储配置
     */
    public static final String ROLESESSIONNAME  = "piggy";
    /**
     * 云文件存储配置
     */
    public static final String REGIONID  = "cn-shanghai";
    /**
     * 百度智能云配置
     */
    public static final String OCRCONFIG = "ocrConfig";
    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "sys_config:";
    /**token
     * 百度云有效期（分钟）
     */
    public static final Integer BAIDUCLOUD_TOKEN_EXPIRATION = 100;
}
