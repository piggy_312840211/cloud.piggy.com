/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.piggy.oss.cloud;


import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.piggy.common.core.exception.base.BaseException;
import com.piggy.common.core.utils.SpringUtils;
import com.piggy.common.core.constant.OssConstans;
import com.piggy.common.redis.service.RedisService;

/**
 * 文件上传Factory
 *
 * @author piggy piggy@piggy.com
 */
public final class OSSFactory {
    private static RedisService redisCache;

    static {
        OSSFactory.redisCache =  SpringUtils.getBean(RedisService.class);
	}

    public static CloudStorageService build(){
        //获取云存储配置信息
        String cloudConfig = redisCache.getCacheObject(OssConstans.SYS_CONFIG_KEY + OssConstans.CONTRACT_CLOUDCONFIG);
        if (StrUtil.isEmpty(cloudConfig))throw new BaseException("系统未配置云服务");

		CloudStorageConfig config = JSON.parseObject(cloudConfig, CloudStorageConfig.class);

        if(config.getType() == OssConstans.CloudService.QINIU.getValue()){
            return new QiniuCloudStorageService(config);
        }else if(config.getType() == OssConstans.CloudService.ALIYUN.getValue()){
            return new AliyunCloudStorageService(config);
        }else if(config.getType() == OssConstans.CloudService.QCLOUD.getValue()){
            return new QcloudCloudStorageService(config);
        }
        return null;
    }
}
