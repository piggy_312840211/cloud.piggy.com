package com.piggy.im.domain.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.annotation.Excel;
import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


/**
 * 站内消息发布分页查询对象 sys_station_message_publish
 *
 * @author piggy
 * @date 2021-12-09
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("站内消息发布分页查询对象")
public class SysStationMessagePublishQueryBo extends BaseEntity {

	@ApiModelProperty("id")
	private Long  id;
	/** 消息级别 */
	@ApiModelProperty("消息级别")
	private String level;
	/** 0=为发布;1=已发布 */
	@ApiModelProperty("0=为发布;1=已发布")
	private Integer status;
	/** 编码 */
	@ApiModelProperty("编码")
	private String type;
	/** 名称 */
	@ApiModelProperty("名称")
	private String title;
	/** 接受者ID */
	@ApiModelProperty("接受者ID")
	private String receiver;
	/** 消息类型 */
	@ApiModelProperty("消息类型")
	@JsonFormat(shape = JsonFormat.Shape.ARRAY)
	private List<Integer> msgType;
	/** 描述 */
	@ApiModelProperty("描述")
	private String description;
	/** 状态 */
	@ApiModelProperty("状态")
	private String state;

}
