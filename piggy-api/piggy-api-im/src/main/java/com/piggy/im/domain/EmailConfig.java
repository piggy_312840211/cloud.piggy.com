package com.piggy.im.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 邮件发送配置
 *
 * @author admin
 * @date 2019-07-25
 */
@Data
@Accessors(chain = true)
@TableName("msg_email_config")
@ApiModel(value="EmailConfig对象", description="邮件发送配置")
public class EmailConfig implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "config_id", type = IdType.AUTO)
    private Long configId;

    @ApiModelProperty(value = "配置名称")
    private String name;

    @ApiModelProperty(value = "发件服务器域名")
    private String smtpHost;

    @ApiModelProperty(value = "发件服务器账户")
    private String smtpUsername;

    @ApiModelProperty(value = "发件服务器密码")
    private String smtpPassword;

    @ApiModelProperty(value = "保留数据0-否 1-是 不允许删除")
    private Integer isPersist;

    @ApiModelProperty(value = "是否为默认 0-否 1-是 ")
    private Integer isDefault;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 请求参数
     */
    @ApiModelProperty("请求参数")
    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
