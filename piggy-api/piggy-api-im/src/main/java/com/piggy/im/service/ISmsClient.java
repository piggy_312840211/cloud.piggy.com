package com.piggy.im.service;

import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.domain.R;
import com.piggy.im.domain.bo.SmsMessage;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 推送通知
 * @author woodev
 */
@FeignClient(contextId = "smsService", value = ServiceNameConstants.MSG_SERVICE)
public interface ISmsClient {
    /**
     * 短信通知
     * @param message
     * @return
     */
    @ApiOperation("发送短信")
    @PostMapping("/sms")
    R send(
            @RequestBody SmsMessage message
    );
    /**
     * 批量短信通知
     * @param messages
     * @return
     */
    @ApiOperation("发送短信")
    @PostMapping("/sendBatchSms")
    R sendBatchSms(
            @RequestBody SmsMessage messages
    );

    @ApiOperation("获取短信配置")
    @GetMapping("/config")
    R<String> getConfig();

}
