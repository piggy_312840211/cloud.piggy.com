package com.piggy.im.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author woodev
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel("短信消息")
public class SmsMessage extends BaseMessage {

    private static final long serialVersionUID = -8924332753124953766L;

    @ApiModelProperty("手机号码")
    private String phoneNum;
    @ApiModelProperty("短信签名")
    private String signName;
    @ApiModelProperty("模板编号")
    private String templateCode;
    @ApiModelProperty("模板参数")
    private String templateParam;

    @ApiModelProperty("0-站内信 1-邮件 2-短信")
    private Integer type;
    @ApiModelProperty("主题如：发送短信验证码")
    private String subject;
    @ApiModelProperty("接收人用户id")
    private Long userId;
    @ApiModelProperty("发送人用户id")
    private Long sendUserId;
    /**
     * 示例值：
     * ["1590000****","1350000****"]
     */
    @ApiModelProperty("批量手机号")
    private List<String> phoneNumbers;
    /**
     * ["阿里云","阿里巴巴"]
     */
    @ApiModelProperty("批量签名")
    private List<String> signNames;
    /**
     * [{"name":"TemplateParamJson"},{"name":"TemplateParamJson"}]
     */
    @ApiModelProperty("批量内容")
    private List<String> templateParams;
}
