package com.piggy.im.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


/**
 * 站内消息发布视图对象 sys_station_message_publish
 *
 * @author piggy
 * @date 2021-12-09
 */
@Data
@ApiModel("站内消息发布视图对象")
public class SysStationMessagePublishVo {

	private static final long serialVersionUID = 1L;

	/** ID */
	@ApiModelProperty("ID")
	private Long id;

	/** 消息级别 */
	@Excel(name = "消息级别")
	@ApiModelProperty("消息级别")
	private String level;

	/** 0=为发布;1=已发布 */
	@Excel(name = "0=为发布;1=已发布")
	@ApiModelProperty("0=为发布;1=已发布")
	private Integer status;

	/** 编码 */
	@Excel(name = "编码")
	@ApiModelProperty("编码")
	private String type;

	/** 消息类型 */
	@Excel(name = "消息类型")
	@ApiModelProperty("消息类型")
	private Integer msgType;

	/** 名称 */
	@Excel(name = "名称")
	@ApiModelProperty("名称")
	private String title;

	/** 接受者ID */
	@Excel(name = "接受者ID")
	@ApiModelProperty("接受者ID")
	private String receiver;

	/** 状态 */
	@Excel(name = "状态")
	@ApiModelProperty("状态")
	private String state;

	/** 创建时间 */
	@Excel(name = "创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createTime;

	@Excel(name = "内容")
	@ApiModelProperty("内容")
	private String content;


}
