package com.piggy.im.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


/**
 * 站内消息视图对象 sys_station_message
 *
 * @author piggy
 * @date 2021-12-21
 */
@Data
@ApiModel("站内消息视图对象")
public class SysStationMessageVo {

	private static final long serialVersionUID = 1L;

	/** ID */
	@ApiModelProperty("ID")
	private Long id;

	/** 消息级别 */
	@Excel(name = "消息级别")
	@ApiModelProperty("消息级别")
	private String level;

	/** 名称 */
	@Excel(name = "名称")
	@ApiModelProperty("名称")
	private String title;

	/** $column.columnComment */
	@Excel(name = "名称")
	@ApiModelProperty("$column.columnComment")
	private String content;

	/** 状态（0=未读 1=已读） */
	@Excel(name = "状态" , readConverterExp = "0==未读,1==已读")
	@ApiModelProperty("状态（0=未读 1=已读）")
	private Integer mark;

	/** 接收人ID */
	@Excel(name = "接收人ID")
	@ApiModelProperty("接收人ID")
	private Long receiveId;

	/** 消息类型(1通知 2公告 3待办 4预警) */
	@Excel(name = "消息类型(1通知 2公告 3待办 4预警)")
	@ApiModelProperty("消息类型(1通知 2公告 3待办 4预警)")
	private Integer msgType;

	/** 状态 */
	@Excel(name = "状态")
	@ApiModelProperty("状态")
	private String state;

	/** 创建时间 */
	@Excel(name = "创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("创建时间")
	private Date createTime;


}
