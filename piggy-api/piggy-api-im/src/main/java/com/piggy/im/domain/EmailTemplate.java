package com.piggy.im.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 邮件模板配置
 *
 * @author admin
 * @date 2019-07-25
 */
@Data
@Accessors(chain = true)
@TableName("msg_email_template")
@ApiModel(value="EmailTemplate对象", description="邮件模板配置")
public class EmailTemplate implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "tpl_id", type = IdType.AUTO)
    private Long tplId;

    @ApiModelProperty(value = "模板名称")
    private String name;

    @ApiModelProperty(value = "模板编码")
    private String code;

    @ApiModelProperty(value = "发送服务器配置")
    private Long configId;

    @ApiModelProperty(value = "模板")
    private String template;

    @ApiModelProperty(value = "模板参数")
    private String msgParams;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
