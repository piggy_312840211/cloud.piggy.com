package com.piggy.im.service;

import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.im.domain.bo.SysPublishMsgBo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 推送通知
 * @author woodev
 */
@FeignClient(contextId = "pushService", value = ServiceNameConstants.MSG_SERVICE)
public interface IPushClient {

    /**
     * 消息推送
     * @param sysPublishMsgBo
     */
    @PostMapping("/SysStationMessagePublish/publishMsg")
    void publishMsg(@RequestBody SysPublishMsgBo sysPublishMsgBo);


}
