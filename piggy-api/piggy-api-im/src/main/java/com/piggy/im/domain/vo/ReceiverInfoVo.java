package com.piggy.im.domain.vo;

import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("接收信息视图对象")
public class ReceiverInfoVo {

    private static final long serialVersionUID = 1L;

    /** ID */
    @ApiModelProperty("ID")
    private Long id;

    /** 消息级别 */
    @Excel(name = "名称")
    @ApiModelProperty("名称")
    private String name;
}
