package com.piggy.im.service;

import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.domain.R;
import com.piggy.im.domain.bo.WebHookMessage;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 推送通知
 *
 * @author woodev
 */
@FeignClient(contextId = "webHookService", value = ServiceNameConstants.MSG_SERVICE)
public interface IWebHookClient {

    /**
     * Webhook异步通知
     *
     * @param message
     * @return
     */
    @ApiOperation("Webhook异步通知")
    @PostMapping("/webhook")
    R send(
            @RequestBody WebHookMessage message
    ) throws Exception;
}
