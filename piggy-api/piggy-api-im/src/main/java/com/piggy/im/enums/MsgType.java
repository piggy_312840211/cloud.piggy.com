package com.piggy.im.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.piggy.common.core.enums.DictionaryEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 实体注释中生成的类型枚举
 * 消息表
 * </p>
 *
 * @author zuihou
 * @date 2020-11-21
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "MsgType", description = "消息类型-枚举")
public enum MsgType implements DictionaryEnum<Integer> {
    /**
     * NOTIFY="通知"
     */
    NOTIFY(1,"NOTIFY"),
    /**
     * NOTICE="公告"
     */
    NOTICE(2,"NOTICE"),

    /**
     * TO_DO="待办"
     */
    TO_DO(3,"TO_DO"),
    /**
     * EARLY_WARNING="预警"
     */
    EARLY_WARNING(4,"EARLY_WARNING"),
    ;
    @EnumValue
    @JsonValue
    private Integer type;

    private String desc;

    @JsonCreator
    public static MsgType of(Integer type) {
        if (type == null) {
            return null;
        }
        for (MsgType info : values()) {
            if (info.type.equals(type)) {
                return info;
            }
        }
        return null;
    }

    public boolean eq(String val) {
        return this.name().equalsIgnoreCase(val);
    }

    public boolean eq(MsgType val) {
        if (val == null) {
            return false;
        }
        return eq(val.name());
    }

    @Override
    public Integer getValue() {
        return this.type;
    }

    @Override
    public String toString() {
        return String.valueOf(type);
    }



}
