package com.piggy.im.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 站内消息对象 sys_station_message
 *
 * @author piggy
 * @date 2021-12-09
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("sys_station_message")
public class SysStationMessage implements Serializable {

    private static final long serialVersionUID=1L;


    /** ID */
    @TableId(value = "id")
    private Long id;

    /** 消息级别 */
    private String level;

    /** 名称 */
    private String title;

    /** $column.columnComment */
    private String content;

    /** 描述 */
    private String description;

    /** 状态（0=未读 1=已读） */
    private Integer mark;

    /** 接收人ID */
    private Long receiveId;

    /** 消息类型(1通知 2公告 3待办 4预警) */
    private Integer msgType;

    /** 状态 */
    private String state;

    /** 创建者 */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 更新者 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /** 更新时间 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
