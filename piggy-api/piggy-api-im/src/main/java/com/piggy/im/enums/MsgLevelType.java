package com.piggy.im.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.piggy.common.core.enums.DictionaryEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "MsgLevelType", description = "消息级别-枚举")
public enum MsgLevelType implements DictionaryEnum<Integer> {

    LOW(0,"低"),

    MIDDLE(1,"中"),

    HIGH(2,"高");

    @EnumValue
    @JsonValue
    private Integer type;

    private String desc;

    @JsonCreator
    public static MsgLevelType of(Integer type) {
        if (type == null) {
            return null;
        }
        for (MsgLevelType info : values()) {
            if (info.type.equals(type)) {
                return info;
            }
        }
        return null;
    }

    public boolean eq(String val) {
        return this.name().equalsIgnoreCase(val);
    }

    public boolean eq(MsgType val) {
        if (val == null) {
            return false;
        }
        return eq(val.name());
    }

    @Override
    public Integer getValue() {
        return this.type;
    }

    @Override
    public String toString() {
        return String.valueOf(type);
    }
}
