package com.piggy.im.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.piggy.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 邮件发送日志
 *
 * @author admin
 * @date 2019-07-25
 */
@Data
@Accessors(chain = true)
@TableName("msg_email_logs")
@ApiModel(value="EmailLogs对象", description="邮件发送日志")
public class EmailLogs implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "log_id", type = IdType.AUTO)
    private Long logId;

    private String subject;

    private String sendTo;

    private String sendCc;

    private String content;

    @ApiModelProperty(value = "附件路径")
    private String attachments;

    @ApiModelProperty(value = "发送次数")
    private Integer sendNums;

    @ApiModelProperty(value = "错误信息")
    private String error;

    @ApiModelProperty(value = "0-失败 1-成功")
    private Integer result;

    @ApiModelProperty(value = "发送配置")
    private String config;

    @ApiModelProperty(value = "模板编号")
    private String tplCode;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

}
