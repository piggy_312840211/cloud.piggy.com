package com.piggy.im.domain.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 站内消息发布对象 sys_station_message_publish
 *
 * @author piggy
 * @date 2021-12-09
 */
@Data
public class SysPublishMsgBo implements Serializable {

    private static final long serialVersionUID=1L;

    /** 标题 */
    private String title;

    /** 内容 */
    private String content;

    /** 描述 */
    private String description;

    /** 消息级别  MsgLevelType枚举*/
    private String level;

    /** 消息类型:MsgType枚举*/
    private Integer msgType;

    /**
     * 发送的用户列表
     */
    private List<Long> userIdList;

}
