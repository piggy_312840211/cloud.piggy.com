package com.piggy.im.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import javax.validation.constraints.*;



/**
 * 站内消息发布添加对象 sys_station_message_publish
 *
 * @author piggy
 * @date 2021-12-09
 */
@Data
@ApiModel("站内消息发布添加对象")
public class SysStationMessagePublishAddBo {


    /** 消息级别 */
    @ApiModelProperty("消息级别")
    private String level;

    /** 0=为发布;1=已发布 */
    @ApiModelProperty("0=为发布;1=已发布")
    private Integer status;

    /** 消息类型 */
    @ApiModelProperty("消息类型")
    private Integer msgType;

    /** 编码 */
    @ApiModelProperty("编码")
    private String type;

    /** 名称 */
    @ApiModelProperty("名称")
    private String title;

    /** 接受者ID */
    @ApiModelProperty("接受者ID")
    private String receiver;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String content;

    /** 描述 */
    @ApiModelProperty("描述")
    private String description;
}
