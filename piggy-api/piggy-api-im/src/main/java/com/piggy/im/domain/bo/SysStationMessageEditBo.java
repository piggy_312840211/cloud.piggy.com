package com.piggy.im.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 站内消息编辑对象 sys_station_message
 *
 * @author piggy
 * @date 2021-12-21
 */
@Data
@ApiModel("站内消息编辑对象")
public class SysStationMessageEditBo {


    /** ID */
    @ApiModelProperty("ID")
    private Long id;

    /** 消息级别 */
    @ApiModelProperty("消息级别")
    private String level;

    /** 名称 */
    @ApiModelProperty("名称")
    private String title;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String content;

    /** 描述 */
    @ApiModelProperty("描述")
    private String description;

    /** 状态（0=未读 1=已读） */
    @ApiModelProperty("状态（0=未读 1=已读）")
    private Integer mark;

    /** 接收人ID */
    @ApiModelProperty("接收人ID")
    private Long receiveId;

    /** 消息类型(1通知 2公告 3待办 4预警) */
    @ApiModelProperty("消息类型(1通知 2公告 3待办 4预警)")
    private Integer msgType;
}
