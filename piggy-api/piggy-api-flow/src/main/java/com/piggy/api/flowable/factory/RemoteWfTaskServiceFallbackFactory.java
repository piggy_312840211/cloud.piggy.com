package com.piggy.api.flowable.factory;

import com.piggy.api.flowable.RemoteWfTaskService;
import com.piggy.api.flowable.domain.bo.WfTaskBo;
import com.piggy.common.core.domain.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;

@Component
public class RemoteWfTaskServiceFallbackFactory implements FallbackFactory<RemoteWfTaskService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteWfTaskServiceFallbackFactory.class);

    @Override
    public RemoteWfTaskService create(Throwable cause) {
        log.error("flowable服务调用失败:{}", cause.getMessage());
        return new RemoteWfTaskService() {
            @Override
            public R stopProcess(WfTaskBo bo, String source) {
                return null;
            }

            @Override
            public R revokeProcess(WfTaskBo bo, String source) {
                return null;
            }

            @Override
            public R processVariables(String taskId, String source) {
                return null;
            }

            @Override
            public R complete(WfTaskBo bo, String source) {
                return null;
            }

            @Override
            public R taskReject(WfTaskBo bo, String source) {
                return null;
            }

            @Override
            public R taskReturn(WfTaskBo bo, String source) {
                return null;
            }

            @Override
            public R findReturnTaskList(WfTaskBo bo, String source) {
                return null;
            }

            @Override
            public R delete(WfTaskBo bo, String source) {
                return null;
            }

            @Override
            public R claim(WfTaskBo bo, String source) {
                return null;
            }

            @Override
            public R unClaim(WfTaskBo bo, String source) {
                return null;
            }

            @Override
            public R delegate(WfTaskBo bo, String source) {
                return null;
            }

            @Override
            public R transfer(WfTaskBo bo, String source) {
                return null;
            }

            @Override
            public R getNextFlowNode(WfTaskBo bo, String source) {
                return null;
            }

            @Override
            public void genProcessDiagram(HttpServletResponse response, String processId, String source) {

            }

            @Override
            public R getFlowViewer(String procInsId, String source) {
                return null;
            }
        };
    }
}
