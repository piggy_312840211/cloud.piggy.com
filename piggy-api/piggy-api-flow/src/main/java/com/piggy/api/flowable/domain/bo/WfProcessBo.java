package com.piggy.api.flowable.domain.bo;

import com.piggy.common.core.web.domain.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;

/**
 * 流程业务对象
 *
 * @author KonBAI
 * @createTime 2022/6/11 01:15
 */
@Data
@Accessors(chain = true)
@ApiModel("流程业务对象")
public class WfProcessBo {

    @ApiModelProperty("流程标识")
    private String processKey;

    @ApiModelProperty("流程名称")
    private String processName;

    @ApiModelProperty("业务标识")
    private String businessKey;

    @ApiModelProperty("流程实例Id")
    private String procInsId;

    @ApiModelProperty("流程分类")
    private String category;

    @ApiModelProperty("状态")
    private String state;

    @ApiModelProperty("参数map")
    private  Map<String, Object> variables;

    @ApiModelProperty("查询参数")
    private String queryValue;

    @ApiParam(value = "分页查询")
    private  PageQuery pageQuery;
}
