package com.piggy.api.flowable.domain.vo;

import lombok.Data;

import java.util.Date;

@Data
public class CommentVo {
    /** unique identifier for this comment */
    String id;

    /** reference to the user that made the comment */
    String userId;

    /** time and date when the user made the comment */
    Date time;

    String taskId;

    /** reference to the task on which this comment was made */

    /** reference to the process instance on which this comment was made */
    String processInstanceId;

    /** reference to the type given to the comment */
    String type;

    /**
     * the full comment message the user had related to the task and/or process instance
     *
     */
    String fullMessage;
}
