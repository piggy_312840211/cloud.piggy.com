package com.piggy.api.flowable;

import com.piggy.api.flowable.factory.RemoteWfDefinitionServiceFallbackFactory;
import com.piggy.common.core.constant.ServiceNameConstants;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(contextId = "remoteWfDefinitionService", value = ServiceNameConstants.FLOWABLE_SERVICE, fallbackFactory = RemoteWfDefinitionServiceFallbackFactory.class)
public interface RemoteWfDefinitionService {
}
