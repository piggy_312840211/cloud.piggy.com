package com.piggy.api.flowable;

import com.piggy.api.flowable.domain.bo.WfTaskBo;
import com.piggy.api.flowable.domain.vo.WfTaskVo;
import com.piggy.api.flowable.factory.RemoteWfTaskServiceFallbackFactory;
import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.domain.R;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@FeignClient(contextId = "remoteWfTaskService", value = ServiceNameConstants.FLOWABLE_SERVICE, fallbackFactory = RemoteWfTaskServiceFallbackFactory.class)
public interface RemoteWfTaskService {

    @PostMapping(value = "/workflow/task/stopProcess")
    public R stopProcess(@RequestBody WfTaskBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping(value = "/workflow/task/revokeProcess")
    public R revokeProcess(@RequestBody WfTaskBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping(value = "/workflow/task/processVariables/{taskId}")
    public R processVariables(@ApiParam(value = "流程任务Id") @PathVariable(value = "taskId") String taskId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping(value = "/workflow/task/complete")
    public R complete(@RequestBody WfTaskBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping(value = "/workflow/task/reject")
    public R taskReject(@RequestBody WfTaskBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping(value = "/workflow/task/return")
    public R taskReturn(@RequestBody WfTaskBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping(value = "/workflow/task/returnList")
    public R findReturnTaskList(@RequestBody WfTaskBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @DeleteMapping(value = "/workflow/task/delete")
    public R delete(@RequestBody WfTaskBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping(value = "/workflow/task/claim")
    public R claim(@RequestBody WfTaskBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping(value = "/workflow/task/unClaim")
    public R unClaim(@RequestBody WfTaskBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping(value = "/workflow/task/delegate")
    public R delegate(@RequestBody WfTaskBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping(value = "/workflow/task/transfer")
    public R transfer(@RequestBody WfTaskBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping(value = "/workflow/task/nextFlowNode")
    public R<WfTaskVo> getNextFlowNode(@RequestBody WfTaskBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 生成流程图
     *
     * @param processId 任务ID
     */
    @RequestMapping("/workflow/task/diagram/{processId}")
    public void genProcessDiagram(HttpServletResponse response,
                                  @PathVariable("processId") String processId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 生成流程图
     *
     * @param procInsId 任务ID
     */
    @RequestMapping("/workflow/task/flowViewer/{procInsId}")
    public R getFlowViewer(@PathVariable("procInsId") String procInsId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
