package com.piggy.api.flowable.factory;

import com.piggy.api.flowable.RemoteWfProcessService;
import com.piggy.api.flowable.domain.bo.WfCopyBo;
import com.piggy.api.flowable.domain.bo.WfProcessBo;
import com.piggy.api.flowable.domain.vo.WfCopyVo;
import com.piggy.api.flowable.domain.vo.WfDefinitionVo;
import com.piggy.api.flowable.domain.vo.WfTaskVo;
import com.piggy.common.core.web.domain.PageQuery;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.core.domain.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class RemoteWfProcessServiceFallbackFactory implements FallbackFactory<RemoteWfProcessService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteWfProcessServiceFallbackFactory.class);

    @Override
    public RemoteWfProcessService create(Throwable cause) {
        log.error("flowable服务调用失败:{}", cause.getMessage());
        return new RemoteWfProcessService() {
            @Override
            public TableDataInfo<WfDefinitionVo> list(PageQuery pageQuery, String source) {
                return null;
            }

            @Override
            public R<?> getForm(String definitionId, String deployId, String source) {
                return null;
            }

            @Override
            public R start(String processDefId, Map<String, Object> variables, String source) {
                return null;
            }

            @Override
            public R detail(String procInsId, String deployId, String taskId) {
                return null;
            }

            @Override
            public TableDataInfo<WfTaskVo> ownProcess(PageQuery pageQuery, String source) {
                return null;
            }

            @Override
            public TableDataInfo<WfTaskVo> todoProcess(PageQuery pageQuery, String source) {
                return null;
            }

            @Override
            public TableDataInfo<WfTaskVo> claimProcess(WfProcessBo processBo, String source) {
                return null;
            }

            @Override
            public TableDataInfo<WfTaskVo> finishedProcess(PageQuery pageQuery, String source) {
                return null;
            }

            @Override
            public TableDataInfo<WfCopyVo> copyProcess(WfCopyBo copyBo, String source) {
                return null;
            }

            @Override
            public R<WfTaskVo> start(WfProcessBo processBo, String source) {
                return null;
            }

            @Override
            public TableDataInfo<WfTaskVo> userTodoList(WfProcessBo processBo, String source) {
                return null;
            }
        };
    }
}
