package com.piggy.api.flowable.factory;

import com.piggy.api.flowable.RemoteWfCategoryService;
import com.piggy.api.flowable.domain.bo.WfCategoryBo;
import com.piggy.api.flowable.domain.vo.WfCategoryVo;
import com.piggy.common.core.domain.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class RemoteWfCategoryServiceFallbackFactory implements FallbackFactory<RemoteWfCategoryService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteWfCategoryServiceFallbackFactory.class);

    @Override
    public RemoteWfCategoryService create(Throwable cause) {
        log.error("flowable服务调用失败:{}", cause.getMessage());
        return new RemoteWfCategoryService() {
            @Override
            public R<WfCategoryVo> getInfo(Long categoryId, String source) {
                return null;
            }

            @Override
            public R add(WfCategoryBo bo, String source) {
                return null;
            }

            @Override
            public R edit(WfCategoryBo bo, String source) {
                return null;
            }

        };
    }
}
