package com.piggy.api.flowable;

import com.piggy.api.flowable.domain.bo.WfCopyBo;
import com.piggy.api.flowable.domain.bo.WfProcessBo;
import com.piggy.api.flowable.domain.vo.WfCopyVo;
import com.piggy.api.flowable.domain.vo.WfDefinitionVo;
import com.piggy.api.flowable.domain.vo.WfTaskVo;
import com.piggy.api.flowable.factory.RemoteWfProcessServiceFallbackFactory;
import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.web.domain.PageQuery;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.core.domain.R;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient(contextId = "remoteWfProcessService", value = ServiceNameConstants.FLOWABLE_SERVICE, fallbackFactory = RemoteWfProcessServiceFallbackFactory.class)
public interface RemoteWfProcessService {

    @GetMapping(value = "/workflow/process/list")
    public TableDataInfo<WfDefinitionVo> list(PageQuery pageQuery, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/workflow/process/getProcessForm")
    public R<?> getForm(@RequestParam(value = "definitionId") String definitionId,
                        @RequestParam(value = "deployId") String deployId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping("/workflow/process/start/{processDefId}")
    public R start(@PathVariable(value = "processDefId") String processDefId,
                   @RequestBody Map<String, Object> variables, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/workflow/process/detail")
    public R detail(@RequestParam(value = "procInsId") String procInsId,@RequestParam(value = "deployId")  String deployId,
                    @RequestParam(value = "taskId") String taskId);

    @ApiOperation(value = "我拥有的流程", response = WfTaskVo.class)
    @GetMapping(value = "/workflow/process/ownList")
    public TableDataInfo<WfTaskVo> ownProcess(PageQuery pageQuery, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping(value = "/workflow/process/todoList")
    public TableDataInfo<WfTaskVo> todoProcess(PageQuery pageQuery, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping(value = "/workflow/process/claimList")
    public TableDataInfo<WfTaskVo> claimProcess(WfProcessBo processBo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping(value = "/workflow/process/finishedList")
    public TableDataInfo<WfTaskVo> finishedProcess(PageQuery pageQuery, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping(value = "/workflow/process/copyList")
    public TableDataInfo<WfCopyVo> copyProcess(@RequestBody WfCopyBo copyBo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 根据流程key. businesskey，启动实列
     */
    @PostMapping("/workflow/process/start")
    public R<WfTaskVo> start(@ApiParam(value = "流程业务对象") @RequestBody WfProcessBo processBo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping (value = "/workflow/process/userTodoList")
    public TableDataInfo<WfTaskVo> userTodoList(@ApiParam(value = "流程业务对象") @RequestBody WfProcessBo processBo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
