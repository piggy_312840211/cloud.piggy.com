package com.piggy.api.flowable;

import com.piggy.api.flowable.domain.bo.WfCategoryBo;
import com.piggy.api.flowable.domain.vo.WfCategoryVo;
import com.piggy.api.flowable.factory.RemoteWfCategoryServiceFallbackFactory;
import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.constant.ServiceNameConstants;
import com.piggy.common.core.validate.AddGroup;
import com.piggy.common.core.validate.EditGroup;
import com.piggy.common.core.domain.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@FeignClient(contextId = "remoteWfCategoryService", value = ServiceNameConstants.FLOWABLE_SERVICE, fallbackFactory = RemoteWfCategoryServiceFallbackFactory.class)
public interface RemoteWfCategoryService {

    /**
     * 获取流程分类详细信息
     */
    @GetMapping("/workflow/category/{categoryId}")
    public R<WfCategoryVo> getInfo(@PathVariable("categoryId") Long categoryId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 新增流程分类
     */
    @PostMapping("/workflow/category")
    public R add(@Validated(AddGroup.class) @RequestBody WfCategoryBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 修改流程分类
     */
    @PutMapping("/workflow/category")
    public R edit(@Validated(EditGroup.class) @RequestBody WfCategoryBo bo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
