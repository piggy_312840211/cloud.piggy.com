package com.piggy.api.flowable.factory;

import com.piggy.api.flowable.RemoteWfDefinitionService;
import com.piggy.api.flowable.domain.vo.WfDefinitionVo;
import com.piggy.common.core.web.domain.PageQuery;
import com.piggy.common.core.web.page.TableDataInfo;
import com.piggy.common.core.domain.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class RemoteWfDefinitionServiceFallbackFactory implements FallbackFactory<RemoteWfDefinitionService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteWfDefinitionServiceFallbackFactory.class);

    @Override
    public RemoteWfDefinitionService create(Throwable cause) {
        log.error("flowable服务调用失败:{}", cause.getMessage());
        return new RemoteWfDefinitionService() {

        };
    }
}
