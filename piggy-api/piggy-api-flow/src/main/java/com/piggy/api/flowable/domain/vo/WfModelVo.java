package com.piggy.api.flowable.domain.vo;

import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author KonBAI
 * @createTime 2022/6/21 9:16
 */
@Data
@ApiModel("流程模型视图对象")
public class WfModelVo {

    @Excel(name = "模型ID")
    @ApiModelProperty("模型ID")
    private String modelId;

    @Excel(name = "模型名称")
    @ApiModelProperty("模型名称")
    private String modelName;

    @Excel(name = "模型Key")
    @ApiModelProperty("模型Key")
    private String modelKey;

    @Excel(name = "分类编码")
    @ApiModelProperty("分类编码")
    private String category;

    @Excel(name = "版本")
    @ApiModelProperty("版本")
    private Integer version;

    @Excel(name = "表单类型")
    @ApiModelProperty("表单类型")
    private Integer formType;

    @Excel(name = "表单ID")
    @ApiModelProperty("表单ID")
    private Long formId;

    @Excel(name = "模型描述")
    @ApiModelProperty("模型描述")
    private String description;

    @Excel(name = "创建时间")
    @ApiModelProperty("创建时间")
    private Date createTime;

    @Excel(name = "流程xml")
    @ApiModelProperty("流程xml")
    private String bpmnXml;

    @Excel(name = "表单内容")
    @ApiModelProperty("表单内容")
    private String content;
}
