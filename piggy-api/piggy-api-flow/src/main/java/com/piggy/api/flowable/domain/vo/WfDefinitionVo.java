package com.piggy.api.flowable.domain.vo;

import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;



/**
 * 流程定义视图对象 workflow_definition
 *
 * @author KonBAI
 * @date 2022-01-17
 */
@Data
@ApiModel("流程定义视图对象")
public class WfDefinitionVo {

    private static final long serialVersionUID = 1L;

    /**
     * 流程定义ID
     */
    @Excel(name = "流程定义ID")
    @ApiModelProperty("流程定义ID")
    private String definitionId;

    /**
     * 流程名称
     */
    @Excel(name = "流程名称")
    @ApiModelProperty("流程名称")
    private String processName;

    /**
     * 流程Key
     */
    @Excel(name = "流程Key")
    @ApiModelProperty("流程Key")
    private String processKey;

    /**
     * 分类编码
     */
    @Excel(name = "分类编码")
    @ApiModelProperty("分类编码")
    private String category;

    @ApiModelProperty("版本")
    private Integer version;

    /**
     * 表单ID
     */
    @Excel(name = "表单ID")
    @ApiModelProperty("表单ID")
    private Long formId;

    /**
     * 表单名称
     */
    @Excel(name = "表单名称")
    @ApiModelProperty("表单名称")
    private String formName;

    /**
     * 部署ID
     */
    @Excel(name = "部署ID")
    @ApiModelProperty("部署ID")
    private String deploymentId;

    /**
     * 流程定义状态: 1:激活 , 2:中止
     */
    @Excel(name = "流程定义状态: 1:激活 , 2:中止")
    @ApiModelProperty("流程定义状态: 1:激活 , 2:中止")
    private Boolean suspended;

    /**
     * 部署时间
     */
    @Excel(name = "部署时间")
    @ApiModelProperty("部署时间")
    private Date deploymentTime;
}
