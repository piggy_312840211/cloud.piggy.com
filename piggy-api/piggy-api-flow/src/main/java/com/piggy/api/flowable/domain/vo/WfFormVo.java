package com.piggy.api.flowable.domain.vo;

import com.piggy.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author KonBAI
 * @createTime 2022/3/7 22:07
 */
@Data
@ApiModel("流程分类视图对象")
public class WfFormVo {

    private static final long serialVersionUID = 1L;

    /**
     * 表单主键
     */
    @Excel(name = "表单ID")
    @ApiModelProperty("表单ID")
    private Long formId;

    /**
     * 表单名称
     */
    @Excel(name = "表单名称")
    @ApiModelProperty("表单名称")
    private String formName;

    /**
     * 表单内容
     */
    @Excel(name = "表单内容")
    @ApiModelProperty("表单内容")
    private String content;

    /**
     * 备注
     */
    @Excel(name = "备注")
    @ApiModelProperty("备注")
    private String remark;
}
