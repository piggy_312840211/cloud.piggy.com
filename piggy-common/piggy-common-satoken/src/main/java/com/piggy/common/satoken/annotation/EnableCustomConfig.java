package com.piggy.common.satoken.annotation;

import com.piggy.common.satoken.config.LoadBalanceConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Documented
@Inherited
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
// 指定要扫描的Mapper类的包的路径
@MapperScan("com.piggy.**.mapper")
@Import({ LoadBalanceConfig.class })
public @interface EnableCustomConfig {

}
