package com.piggy.common.satoken.utils;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.piggy.common.satoken.helper.LoginHelper;
import com.piggy.sys.api.model.LoginUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Set;

/**
 * 权限获取工具类
 *
 * @author shark
 */
public class SecurityUtils {

    @Getter
    @AllArgsConstructor
    public static enum AdminRoleEnum {
        ADMIN(1L, "admin", "admin"),
        ;
        /**
         * 类型
         */
        private final Long userId;
        /**
         * 角色
         */
        private final String role;
        /**
         * 用户名
         */
        private final String userName;

    }

    @Getter
    @AllArgsConstructor
    public static enum AuthTypeEnums {
        PASSWD("password", "密码授权模式"),
        CLIENT("client_credentials", "客户端授权模式"),
        AUTH_CODE("authorization_code", "授权码模式"),
        IMPLICIT("implicit", "简化模式"),
        UNKNOWN("unknown", "unknown"),
        ;
        /**
         * 类型
         */
        private final String code;
        /**
         * 描述
         */
        private final String name;
    }

    @Getter
    @AllArgsConstructor
    public static enum DefaultClientIdEnums {
        WEB("web", "web端"),
        ;
        /**
         * 类型
         */
        private final String code;

        /**
         * 描述
         */
        private final String name;
    }

    /**
     * 获取用户
     */
    public static String getUsername() {
        if (ObjectUtil.isNull(getOauthUser())) {
            return null;
        }
        return getOauthUser().getUserName();
    }

    /**
     * 客户端登录时获取clientId
     */
    public static String getClientId() {
        return getOauthUser().getClientDetails().getClientId();
    }

    /**
     * 获取用户
     */
    public static Long getUserId() {
        return getOauthUser().getSysUser().getUserId();
    }

    /**
     * 获取用户
     */
    public static Long getDeptId() {
        return getOauthUser().getSysUser().getDeptId();
    }

    /**
     * 获取用户
     */
    public static LoginUser getOauthUser() {
        return LoginHelper.getLoginUser();
    }

    /**
     * 生成BCryptPasswordEncoder密码
     *
     * @param password 密码
     * @return 加密字符串
     */
    public static String encryptPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    /**
     * 判断密码是否相同
     *
     * @param rawPassword     真实密码
     * @param encodedPassword 加密后字符
     * @return 结果
     */
    public static boolean matchesPassword(String rawPassword, String encodedPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    /**
     * 判断是否未超级管理员和平台管理员
     */
    public static boolean isSysAdmin() {
        Set<String> roles = getOauthUser().getRoles();
        if (CollectionUtil.isEmpty(roles)) {
            return false;
        }

        if (roles.contains(AdminRoleEnum.ADMIN.getRole())) {
            return true;
        }

        return false;
    }

    /**
     * 是否为管理员
     *
     * @param userId 用户ID
     * @return 结果
     */
    public static boolean isAdmin(Long userId) {
        return userId != null && 1L == userId;
    }

    /**
     * 是否为管理员
     *
     * @return 结果
     */
    public static boolean isPassword() {
        return getOauthUser().getClientDetails().getAuthorizedGrantTypes().contains(AuthTypeEnums.PASSWD.getCode());
    }

    /**
     * 是否为client_credentials
     *
     * @return 结果
     */
    public static boolean isClientCredentials() {
        return getOauthUser().getClientDetails().getAuthorizedGrantTypes().contains(AuthTypeEnums.CLIENT.getCode());
    }

    /**
     * 是否为auth_code
     *
     * @return 结果
     */
    public static boolean isAuthCode() {
        return getOauthUser().getClientDetails().getAuthorizedGrantTypes().contains(AuthTypeEnums.AUTH_CODE.getCode());
    }

    /**
     * 是否为implicit
     *
     * @return 结果
     */
    public static boolean isImplicit() {
        return getOauthUser().getClientDetails().getAuthorizedGrantTypes().contains(AuthTypeEnums.IMPLICIT.getCode());
    }

    /**
     * 是否为UNKNOWN
     *
     * @return 结果
     */
    public static boolean isUnkown() {
        return getOauthUser().getClientDetails().getAuthorizedGrantTypes().contains(AuthTypeEnums.UNKNOWN.getCode());
    }

    public static void main(String[] args) {
        System.out.println(SecurityUtils.encryptPassword("12345678"));
    }

}
