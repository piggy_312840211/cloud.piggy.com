package com.piggy.common.satoken.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 设备类型
 * 针对一套 用户体系
 *
 * @author Lion Li
 */
@Getter
@AllArgsConstructor
public enum DeviceType {

    /**
     * pc端
     */
    PC("PC"),

    /**
     * app端
     */
    APP("APP"),

    /**
     * 微信公众号端
     */
    MP("MP"),

    /**
     * 小程序端
     */
    MINI("MINI"),

    /**
     * 小程序端
     */
    UNKNOWN("UNKNOWN"),

    ;

    private final String device;

    public static DeviceType getByCode(String device) {
        return Arrays.stream(DeviceType.values()).filter(t -> t.getDevice().equals(device)).findFirst().orElse(UNKNOWN);
    }

}
