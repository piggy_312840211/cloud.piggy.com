package com.piggy.common.satoken.dao;

import cn.dev33.satoken.dao.SaTokenDao;
import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.strategy.SaStrategy;
import cn.dev33.satoken.util.SaFoxUtil;
import com.alibaba.fastjson2.JSON;
import com.piggy.common.redis.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Sa-Token 持久层实现 [Redis存储、fastjson2序列化]
 *
 * @author sikadai
 *
 */
@Component
public class SaTokenDaoRedisFastjson2 implements SaTokenDao {

	/**
	 * String专用
	 */
	@Resource
	private RedisService redisService;

	/**
	 * 标记：是否已初始化成功
	 */
	public boolean isInit;

	@Autowired
	public void init() {
		// 不重复初始化
		if(this.isInit) {
			return;
		}

		SaStrategy.me.createSession = (sessionId) -> new SaSessionForFastjson2Customized(sessionId);
		this.isInit = true;
	}

	/**
	 * 获取Value，如无返空
	 */
	@Override
	public String get(String key) {
		return redisService.getCacheObject(key);
	}

	/**
	 * 写入Value，并设定存活时间 (单位: 秒)
	 */
	@Override
	public void set(String key, String value, long timeout) {
		if(timeout == 0 || timeout <= SaTokenDao.NOT_VALUE_EXPIRE)  {
			return;
		}
		// 判断是否为永不过期
		if(timeout == SaTokenDao.NEVER_EXPIRE) {
			redisService.setCacheObject(key, value);
		} else {
			redisService.setCacheObject(key, value, timeout, TimeUnit.SECONDS);
		}
	}

	/**
	 * 修改指定key-value键值对 (过期时间不变)
	 */
	@Override
	public void update(String key, String value) {
		long expire = getTimeout(key);
		// -2 = 无此键
		if(expire == SaTokenDao.NOT_VALUE_EXPIRE) {
			return;
		}
		this.set(key, value, expire);
	}

	/**
	 * 删除Value
	 */
	@Override
	public void delete(String key) {
		redisService.deleteObject(key);
	}

	/**
	 * 获取Value的剩余存活时间 (单位: 秒)
	 */
	@Override
	public long getTimeout(String key) {
		return redisService.getExpire(key);
	}

	/**
	 * 修改Value的剩余存活时间 (单位: 秒)
	 */
	@Override
	public void updateTimeout(String key, long timeout) {
		// 判断是否想要设置为永久
		if(timeout == SaTokenDao.NEVER_EXPIRE) {
			long expire = getTimeout(key);
			if(expire == SaTokenDao.NEVER_EXPIRE) {
				// 如果其已经被设置为永久，则不作任何处理
			} else {
				// 如果尚未被设置为永久，那么再次set一次
				this.set(key, this.get(key), timeout);
			}
			return;
		}
		redisService.expire(key, timeout, TimeUnit.SECONDS);
	}


	/**
	 * 获取Object，如无返空
	 */
	@Override
	public Object getObject(String key) {
		return redisService.getCacheObject(key);
	}

	@Override
	public SaSession getSession(String sessionId) {
		Object obj = getObject(sessionId);
		if (obj == null) {
			return null;
		}
		return JSON.parseObject((String) obj, SaSessionForFastjson2Customized.class);
	}

	/**
	 * 写入Object，并设定存活时间 (单位: 秒)
	 */
	@Override
	public void setObject(String key, Object object, long timeout) {
		if(timeout == 0 || timeout <= SaTokenDao.NOT_VALUE_EXPIRE)  {
			return;
		}
		String toValue = object instanceof String ? (String) object : JSON.toJSONString(object);
		// 判断是否为永不过期
		if(timeout == SaTokenDao.NEVER_EXPIRE) {
			redisService.setCacheObject(key, toValue);
		} else {
			redisService.setCacheObject(key, toValue, timeout, TimeUnit.SECONDS);
		}
	}

	/**
	 * 更新Object (过期时间不变)
	 */
	@Override
	public void updateObject(String key, Object object) {
		long expire = getObjectTimeout(key);
		// -2 = 无此键
		if(expire == SaTokenDao.NOT_VALUE_EXPIRE) {
			return;
		}
		String toValue = object instanceof String ? (String) object : JSON.toJSONString(object);
		this.setObject(key, toValue, expire);
	}

	/**
	 * 删除Object
	 */
	@Override
	public void deleteObject(String key) {
		redisService.deleteObject(key);
	}

	/**
	 * 获取Object的剩余存活时间 (单位: 秒)
	 */
	@Override
	public long getObjectTimeout(String key) {
		return redisService.getExpire(key);
	}

	/**
	 * 修改Object的剩余存活时间 (单位: 秒)
	 */
	@Override
	public void updateObjectTimeout(String key, long timeout) {
		// 判断是否想要设置为永久
		if(timeout == SaTokenDao.NEVER_EXPIRE) {
			long expire = getObjectTimeout(key);
			if(expire == SaTokenDao.NEVER_EXPIRE) {
				// 如果其已经被设置为永久，则不作任何处理
			} else {
				// 如果尚未被设置为永久，那么再次set一次
				this.setObject(key, this.getObject(key), timeout);
			}
			return;
		}
		redisService.expire(key, timeout, TimeUnit.SECONDS);
	}

	/**
	 * 搜索数据
	 */
	@Override
	public List<String> searchData(String prefix, String keyword, int start, int size, boolean sortType) {
		Collection<String> keys = redisService.keys(prefix + "*" + keyword + "*");
		return SaFoxUtil.searchList(new ArrayList<>(keys), start, size, sortType);
	}

}
