package com.piggy.common.satoken.constants;

public interface SaTokenConst {

    /**
     * The name of the token.
     */
    String BEARER = "Bearer";

    /**
     * The name of the header.
     */
    String AUTHORIZATION = "Authorization";

}
