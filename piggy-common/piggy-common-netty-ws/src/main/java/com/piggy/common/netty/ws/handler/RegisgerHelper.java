package com.piggy.common.netty.ws.handler;

import java.util.ArrayList;
import java.util.List;

public class RegisgerHelper {

    private final static List<WsHandler>  handlers = new ArrayList<>();

    public static void addWsHandler(WsHandler handler) {
        handlers.add(handler);
    }

    public static List<WsHandler> get() {
        return handlers;
    }

}
