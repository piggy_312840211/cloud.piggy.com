package com.piggy.common.netty.ws.message;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Hccake 2021/1/12
 * @version 1.0
 */
@Getter
@AllArgsConstructor
public enum MessageType {

	PING(0, "PING消息"),
	PONG(1, "PONG消息"),
	TEXT(2, "文本类型"),
	LINK(3, "链接类型"),
	VOICE(4, "语音类型"),
	VODEO(5, "视频类型"),

	;

	private final Integer type;

	private final String desc;

}
