package com.piggy.common.netty.ws.server;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum WsReqType {

    Handshake(10, "Handshake"),
    Open(20, "Open"),
    Message(30, "Message"),
    Binary(40, "Binary"),
    Event(50, "Event"),
    Close(60, "Close"),
    Error(70, "Error"),

    ;

    private Integer reqType;

    private String reqDesc;

}
