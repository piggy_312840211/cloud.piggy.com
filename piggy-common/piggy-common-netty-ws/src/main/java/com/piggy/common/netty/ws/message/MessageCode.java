package com.piggy.common.netty.ws.message;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Hccake 2021/1/12
 * @version 1.0
 */
@Getter
@AllArgsConstructor
public enum MessageCode {

	PING(10000, "PING", MessageType.PING),
	PONG(10001, "PONG", MessageType.PONG),
	
	TEXT(11000, "文本消息", MessageType.TEXT),
	EMOJI(11001, "表情符消息", MessageType.TEXT),
	ACT1(110002, "活动消息1", MessageType.LINK),
	;

	private final Integer code;

	private final String desc;

	private final MessageType type;

}
