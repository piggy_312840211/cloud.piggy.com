package com.piggy.common.netty.ws.handler;

import com.piggy.common.netty.ws.server.WsReqType;
import com.piggy.common.netty.ws.session.WsSession;

public interface WsHandler {

    public void process(WsReqType reqType, String sid, WsSession wsSession);

}
