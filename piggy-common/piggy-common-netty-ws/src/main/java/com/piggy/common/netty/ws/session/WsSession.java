package com.piggy.common.netty.ws.session;


import com.piggy.common.netty.ws.message.MessageDO;
import com.piggy.netty.ws.pojo.Session;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;

import java.util.Map;

@Accessors(chain = true)
@Data
public class WsSession {

    private Session session;

    private String sid;

    private String message;

    private byte[] bytes;

    private HttpHeaders headers;

    private MultiValueMap reqMap;

    private Map pathMap;

    private MessageDO messageDO;

    private IdleStateEvent event;
    
}
