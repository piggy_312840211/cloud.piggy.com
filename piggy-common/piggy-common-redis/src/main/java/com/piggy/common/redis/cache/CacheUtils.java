package com.piggy.common.redis.cache;


import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.TypeReference;
import com.piggy.common.core.enums.CacheKeyEnums;
import com.piggy.common.core.func.Function;
import com.piggy.common.core.utils.SpringUtils;
import com.piggy.common.redis.service.RedisService;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class CacheUtils {

    private static RedisService redisService = null;

    public static RedisService getRedis() {
        if (ObjectUtil.isEmpty(redisService)) {
            redisService = SpringUtils.getBean(RedisService.class);
        }
        return redisService;
    }

    public static <T> void cache(T t, String key) {
        if (Objects.isNull(t)) {
            return;
        }
        getRedis().deleteObject(key);
        getRedis().setCacheObject(key, JSON.toJSONString(t));
    }

    public static <T> void cache(T t, String key, String hKey) {
        if (Objects.isNull(t)) {
            return;
        }
        getRedis().deleteObject(key);
        getRedis().setCacheMapValue(key, hKey, JSON.toJSONString(t));
    }

    public static <T> void cache(T t, String key, Long timeout, TimeUnit timeUnit) {
        if (Objects.isNull(t)) {
            return;
        }
        getRedis().deleteObject(key);
        getRedis().setCacheObject(key, JSON.toJSONString(t), timeout, timeUnit);
    }

    public static <T,K> void cache(T t, K key, CacheKeyEnums keyEnums, Long timeout, TimeUnit timeUnit) {
        if (Objects.isNull(t) || Objects.isNull(key)) {
            return;
        }
        getRedis().deleteObject(keyEnums.getKey(key));
        getRedis().setCacheObject(keyEnums.getKey(key), JSON.toJSONString(t), timeout, timeUnit);
    }

    public static <T> void cacheIfAbsent(T t, String key, Long timeout, TimeUnit timeUnit) {
        if (Objects.isNull(t)) {
            return;
        }
        if (getRedis().hasKey(key)) {
            return;
        }
        cache(t, key, timeout, timeUnit);
    }

    public static <T,K> void cacheIfAbsent(T t, K key, CacheKeyEnums keyEnums, Long timeout, TimeUnit timeUnit) {
        if (Objects.isNull(t) || Objects.isNull(key)) {
            return;
        }
        if (getRedis().hasKey(keyEnums.getKey(key))) {
            return;
        }
        cache(t, keyEnums.getKey(key), timeout, timeUnit);
    }

    public static <K> void remove(K key, CacheKeyEnums keyEnums) {
        getRedis().deleteObject(keyEnums.getKey(key));
    }

    public static void remove(String key) {
        getRedis().deleteObject(key);
    }

    public static void remove(String key, String hKey) {
        getRedis().deleteObject(key, hKey);
    }

    public static <T> T getCache(String key, String hKey, Class<T> tClass) {
        if (!getRedis().hasKey(key, hKey)) {
            return null;
        }
        String value = getRedis().getCacheMapValue(key, hKey);
        return JSON.parseObject(value, tClass);
    }

    public static <T> List<T> getCacheList(String key) {
        if (!getRedis().hasKey(key)) {
            return null;
        }
        Collection<String> map = getRedis().getHCacheList(key);
        return JSON.parseObject(map.toString(), new TypeReference<List<T>>() {});
    }

    public static <T> Map<String,T> getCacheMap(String key) {
        if (!getRedis().hasKey(key)) {
            return null;
        }
        Map<String,String> map = getRedis().getCacheMap(key);
        return JSON.parseObject(map.toString(), new TypeReference<Map<String, T>>() {});
    }

    public static <T> Map<String,T> getCacheMap(String key, Function<Map<String, T>> method) {
        if (getRedis().hasKey(key)) {
            Map<String,String> map = getRedis().getCacheMap(key);
            return JSON.parseObject(map.toString(), new TypeReference<Map<String, T>>() {});
        }
        return method.apply();
    }

    public static <T> T getCache(String key, String hKey, TypeReference<T> tClass) {
        if (!getRedis().hasKey(key, hKey)) {
            return null;
        }
        return JSON.parseObject(redisService.getCacheMapValue(key, hKey), tClass);
    }

    public static <T,K> T getCache(K key, CacheKeyEnums keyEnums, Class<T> tClass, Function<T> method) {
        String redisKey = keyEnums.getKey(key);
        return getCache(redisKey, tClass, method);
    }

    public static <T,K> T getCache(K key, CacheKeyEnums keyEnums, Class<T> tClass) {
        String redisKey = keyEnums.getKey(key);
        return getCache(redisKey, tClass);
    }

    public static <T> T getCache(String key, Class<T> tClass, Function<T> method) {
        if (getRedis().hasKey(key)) {
            return JSON.parseObject((String) redisService.getCacheObject(key), tClass);
        }
        return method.apply();
    }

    public static <T> T getCache(String key, Class<T> tClass) {
        if (getRedis().hasKey(key)) {
            return JSON.parseObject((String) redisService.getCacheObject(key), tClass);
        }
        return null;
    }

    public static <T> T getCache(String key, TypeReference<T> tClass) {
        if (getRedis().hasKey(key)) {
            return JSON.parseObject(redisService.getCacheObject(key), tClass);
        }
        return null;
    }

    public static <T> T getCache(String key, TypeReference<T> tClass, Function<T> method) {
        if (getRedis().hasKey(key)) {
            return JSON.parseObject(redisService.getCacheObject(key), tClass);
        }
        return method.apply();
    }


}
