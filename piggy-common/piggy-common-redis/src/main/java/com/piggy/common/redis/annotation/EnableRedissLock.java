package com.piggy.common.redis.annotation;

import com.piggy.common.redis.lock.RedissLockUtil;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author zhoushangtao
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import(RedissLockUtil.class)
public @interface EnableRedissLock {
}
