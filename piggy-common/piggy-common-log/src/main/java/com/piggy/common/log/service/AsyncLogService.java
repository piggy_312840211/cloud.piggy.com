package com.piggy.common.log.service;

import com.piggy.common.core.constant.SecurityConstants;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.piggy.sys.api.RemoteLogService;
import com.piggy.sys.api.domain.SysOperLog;

import javax.annotation.Resource;

/**
 * 异步调用日志服务
 *
 * @author shark
 */
@Service
public class AsyncLogService {
    @Resource
    private RemoteLogService remoteLogService;

    /**
     * 保存系统日志记录
     */
    @Async
    public void saveSysLog(SysOperLog sysOperLog) {
        remoteLogService.saveLog(sysOperLog, SecurityConstants.INNER);
    }
}
