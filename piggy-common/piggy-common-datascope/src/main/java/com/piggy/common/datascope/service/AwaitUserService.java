package com.piggy.common.datascope.service;

import com.piggy.common.core.constant.SecurityConstants;
import com.piggy.common.core.utils.StringUtils;
import com.piggy.common.satoken.utils.SecurityUtils;
import com.piggy.sys.api.RemoteUserService;
import com.piggy.common.core.domain.R;
import com.piggy.sys.api.model.LoginUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 同步调用用户服务
 *
 * @author ruoyi
 */
@Service
public class AwaitUserService
{
    private static final Logger log = LoggerFactory.getLogger(AwaitUserService.class);

    @Resource
    private RemoteUserService remoteUserService;

    /**
     * 查询当前用户信息
     *
     * @return 用户基本信息
     */
    public LoginUser info()
    {
        String username = SecurityUtils.getUsername();
        R<LoginUser> userResult = remoteUserService.getUserInfo(username, SecurityConstants.INNER);
        if (StringUtils.isNull(userResult) || StringUtils.isNull(userResult.getData()))
        {
            log.info("数据权限范围查询用户：{} 不存在.", username);
            return null;
        }
        return userResult.getData();
    }
}
