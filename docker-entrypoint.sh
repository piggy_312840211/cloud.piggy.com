#!/bin/sh

if [ ! -z $1 ]; then
    MODULE=$1
    shift
fi

if [ -z $JAVA_OPTS ]; then
    JAVA_OPTS='-Xms1024m -Xmx4096m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=512m -Djava.security.egd=file:/dev/./urandom -Duser.timezone=GMT+8 -Dfile.encoding=utf-8'
fi

if [ -z $MODULE ]; then
    echo "MODULE environment is not set"
    exit 127
else
    PACKAGE=/srv/piggy-$MODULE.jar
fi

DEBUG='-Xdebug -Xrunjdwp:transport=dt_socket,suspend=n,server=y,address=5555'
SKYWALKING="-javaagent:/srv/skywalking/agent/skywalking-agent.jar -Dskywalking.collector.backend_service=oap.piggy.com:11800 -Dskywalking.agent.service_name=${MODULE}"

exec java ${JAVA_OPTS}  -jar ${PACKAGE} $@